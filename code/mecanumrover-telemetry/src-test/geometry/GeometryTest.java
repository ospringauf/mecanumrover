/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package geometry;

import org.junit.Test;

import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;


public class GeometryTest {

	@Test
	public void test() {
		Line l1 = new Line(0, -10, 0, 10);
		Line l2 = new Line(-10, 2, 10, 2);
		l1.setStrokeWidth(0.1);
		l2.setStrokeWidth(0.1);
		
		Shape s = Shape.intersect(l1, l2);
		System.out.println(s);
	}

}
