/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package de.konteno.ao.mrover.telemetry;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Stream;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import de.konteno.ao.mrover.common.Const;
import de.konteno.ao.mrover.common.event.CompassEvt;
import de.konteno.ao.mrover.common.event.LidarScanEvt;
import de.konteno.ao.mrover.common.event.SonarDistanceEvt;
import de.konteno.ao.mrover.common.track.Track;
import de.konteno.ao.mrover.common.track.Pose;
import de.konteno.ao.mrover.service.comm.MqttBridgeService;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.scene.transform.Affine;

public class RoverModel implements MqttCallback {

	private static final String ROVER_TM = "rover/tm/#";
	private MqttClient client;

	public LongProperty time = new SimpleLongProperty();

//	public IntegerProperty speed = new SimpleIntegerProperty(50);

	public DoubleProperty heading = new SimpleDoubleProperty(0.0);

	public SimpleStringProperty status = new SimpleStringProperty();

	public BooleanProperty connected = new SimpleBooleanProperty(false);

	public BooleanProperty trackDr = new SimpleBooleanProperty(true);

	public BooleanProperty whilePressed = new SimpleBooleanProperty(true);

	public final Track drTrack = new Track("rover/tm/dr");
	public final Track odoTrack = new Track("rover/tm/odo");
//	public final Track sonarTrack = odoTrack; // base for sonar mapping

	public Runnable trackChanged; // callback
	public Runnable distanceChanged; // callback
	public int[] sonarScan;
	public LidarScanEvt lidarScan;

	public void connect(String adr) throws Exception {
		disconnect();

		System.out.println("MQTT connect " + adr);
		client = new MqttClient(adr, "telemetry");
		client.connect();
		client.setCallback(this);
		client.subscribe(ROVER_TM, 0);
		status.set("connected");
		connected.set(true);
	}

	@Override
	public void connectionLost(Throwable arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken arg0) {
		// TODO Auto-generated method stub
	}

	public Stream<Pose> allWaypoints() {
		return Stream.concat(odoTrack.stream(), drTrack.stream());
	}

	public Track sonarTrack() {
		return trackDr.getValue() ? drTrack : odoTrack;
	}

	@Override
	public void messageArrived(String topic, MqttMessage payload) throws Exception {
//		System.out.println("mqtt in: " + topic + " " + payload);

		try {
			String msg = payload.toString();
			switch (topic) {
			case MqttBridgeService.TopicTelemetryDr:
			case MqttBridgeService.TopicTelemetryOdo:
				Track t = topic.equals(odoTrack.mqttTopic) ? odoTrack : drTrack;

				if (t != null) {
					try {
						Pose w = Pose.parseMqtt(msg);
						t.add(w);
						Platform.runLater(() -> heading.setValue(w.heading.toDegree()));
					} catch (Exception e) {
						e.printStackTrace();
					}
					trackChanged();

				}
				break;

			case MqttBridgeService.TopicTelemetrySonar:
				SonarDistanceEvt evt = SonarDistanceEvt.parse(msg);
				sonarScan = evt.distance;
				if (distanceChanged != null) {
					distanceChanged.run();
				}
				break;

			case MqttBridgeService.TopicTelemetryLidar:
				lidarScan = LidarScanEvt.parseMqtt(msg);
				if (distanceChanged != null) {
					distanceChanged.run();
				}
				break;

			case MqttBridgeService.TopicTelemetryImu:
				CompassEvt compEvt = CompassEvt.parseMqtt(msg);
				Platform.runLater(() -> heading.set(compEvt.getHeading().toDegree()));
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void trackChanged() {
		if (trackChanged != null) {
			trackChanged.run();
		}
	}

	public void disconnect() {
		try {
			if (client != null) {
				System.out.println("MQTT disconnect");
				if (client.isConnected()) {
					client.unsubscribe(ROVER_TM);
					client.disconnect();
				}
				client.close();
				connected.set(false);
			}
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void sendCmd(String topic, String payload) {
		try {
			client.publish(topic, new MqttMessage(payload.getBytes()));
		} catch (MqttException e) {
			e.printStackTrace();
		}
	}

	public void clearTrack() {
		sendCmd("rover/cmd/reset", "");
		odoTrack.clear();
		drTrack.clear();
		lidarScan = null;

		trackChanged();
	}

	public long getMinTime() { return allWaypoints().map(p -> p.time).min(Long::compareTo).get(); }

	public long getMaxTime() { return allWaypoints().map(p -> p.time).max(Long::compareTo).get(); }

	public void myPaint(GraphicsContext g) {

		// bounding box in mouse coords
		Rectangle bounds = new Rectangle(0, 0, 0, 0);

		allWaypoints().forEach(p -> bounds.add(p.pos.x, p.pos.y));
		bounds.grow(50, 50);
		drawGrid(g, bounds);

		drawTrack(g, drTrack, Color.LIGHTSLATEGRAY);
		drawTrack(g, odoTrack, Color.ORANGERED);

		if (sonarScan != null && sonarTrack().size() > 0) {
			drawSonar(g);
		}

//		if (lidarScan != null && sonarTrack().size() > 0)
//		{
//			drawLidar(g);
//		}
	}

	/**
	 * visualize sonar beam cones as overlays
	 */
	private void drawSonar(GraphicsContext g) {
		Pose p = sonarTrack().last;
		if (p == null || sonarScan == null)
			return;

		for (int i = 0; i < 8; ++i) {
			int d = sonarScan[i];
			if (d >= Const.SONAR_SENSOR_LIMIT) {
				continue;
			}
			g.save();

			g.setGlobalAlpha(0.2);
			g.setFill(Color.LIGHTSALMON);

			Affine t = g.getTransform();
			t.appendTranslation(p.pos.x, p.pos.y); // rover center
			t.appendRotation(-p.heading.toDegree()); // rover heading
			t.appendTranslation(Const.sonarPosition[i].x, Const.sonarPosition[i].y); // sensor offset
			g.setTransform(t);

			int beamdir = (int) (Const.sonarDirection[i].toDegree() - 90); // FX 0 deg = 3 o'clock
			g.fillArc(-d, -d, 2 * d, 2 * d, beamdir - (Const.sonarBeamWidth / 2), Const.sonarBeamWidth, ArcType.ROUND);
			g.restore();
		}
	}

	private void drawTrack(GraphicsContext g, Track trk, Color color) {
		if (trk.size() == 0) {
			return;
		}
		g.setStroke(Color.LIGHTGRAY);
		Pose p = trk.last;
		drawRover(g, p, color);

		double t = getMinTime() + (time.get() / 1000.0) * (getMaxTime() - getMinTime());
		Optional<Pose> p1 = trk.stream().filter(x -> x.time <= t).reduce((a, b) -> b); // last before time t
		if (p1.isPresent()) {
			drawRover(g, p1.get(), color);
		}

		g.setStroke(color);
		drawRoverPath(g, trk);
	}

	private void drawGrid(GraphicsContext g, Rectangle bounds) {
		g.save();
		g.setStroke(Color.LIGHTGRAY);
		g.setLineWidth(0.5);
		for (int x = 20; x < bounds.x + bounds.width; x = x + 20) {
			g.strokeLine(x, bounds.y, x, bounds.y + bounds.height);
		}
		for (int x = -20; x > bounds.x; x = x - 20) {
			g.strokeLine(x, bounds.y, x, bounds.y + bounds.height);
		}
		for (int y = 20; y < bounds.y + bounds.height; y = y + 20) {
			g.strokeLine(bounds.x, y, bounds.x + bounds.width, y);
		}
		for (int y = -20; y > bounds.y; y = y - 20) {
			g.strokeLine(bounds.x, y, bounds.x + bounds.width, y);
		}

		g.setStroke(Color.BLACK);
		g.strokeLine(0, bounds.y, 0, bounds.y + bounds.height);
		g.strokeLine(bounds.x, 0, bounds.x + bounds.width, 0);
		g.restore();
	}

	private void drawRover(GraphicsContext g, Pose p, Color color) {
		Rectangle r = new Rectangle(0, 0, 0, 0);
		r.grow(22 / 2, 30 / 2); // rover is 22x30cm

		g.save(); // save current transformation
		Affine t = g.getTransform();
		t.appendTranslation(p.pos.x, p.pos.y); // rover center
		t.appendRotation(-p.heading.toDegree()); // rover heading
		g.setTransform(t);

		// g.setStroke(Color.DARKSLATEBLUE);
		g.setStroke(color);
		g.setFill(color);
		g.setLineWidth(0.2);
		g.strokeRect(r.x, r.y, r.width, r.height);
		g.setGlobalAlpha(0.2);
		g.fillRect(r.x, r.y, r.width, r.height);
		g.restore(); // restore transformation

	}

	private void drawRoverPath(GraphicsContext g, ArrayList<Pose> l) {
		// map mouse points to screen
		double[] x = l.stream().mapToDouble(p -> p.pos.x).toArray();
		double[] y = l.stream().mapToDouble(p -> p.pos.y).toArray();

		g.strokePolyline(x, y, l.size());
	}

	public void sendPowerDistribution(double lr, double fb) {
		sendCmd("rover/set/powerdist", String.format("%3.1f %3.1f", lr, fb));
	}
}
