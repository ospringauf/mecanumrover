/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package de.konteno.ao.mrover.telemetry;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

import de.konteno.ao.mrover.common.Const;
import de.konteno.ao.mrover.common.event.LidarScanEvt;
import de.konteno.ao.mrover.common.event.SonarDistanceEvt;
import de.konteno.ao.mrover.common.track.Pose;

/**
 * Rasterized map of the environment,
 * stored as a grayscale bitmap where each pixel represents a 5x5cm (10x10cm,...) square
 * and the color (0..255) represents the probability of an obstance.
 * 
 * Updated by distance sensor input, uses alpha blending for "coloring" the area of
 * each sonar beam cone.
 *
 */
public class Map {

	public static final int SCALE = 5; // resolution [cm] =10
	public static final int WIDTH = 1000/SCALE; // 10x10m map
	public static final int HEIGHT = 1000/SCALE;
	
	public BufferedImage img;
	private Graphics2D g;
	private AffineTransform initialXform;
	
	private Color colorFree =     new Color(255,255,255, 64); // transparent white a=32
	private Color colorObstacle = new Color(  0,  0,  0, 8); // transparent black a=8

	public Map() {
		img = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_BYTE_GRAY);
		WritableRaster r = img.getRaster();
		//imagebuffer = (DataBufferByte) r.getDataBuffer();
		
		clearMap();
		
		g = (Graphics2D)img.getGraphics();
		
		// prepare map (global transformation)
		initialXform = AffineTransform.getTranslateInstance(HEIGHT/2, WIDTH/2);
		initialXform.scale(1.0/SCALE, -1.0/SCALE);
		g.transform(initialXform);
		g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));
	}
	
	public void clearMap() {
		g = (Graphics2D)img.getGraphics();
		g.setColor(Color.GRAY);
		g.fillRect(0, 0, WIDTH, HEIGHT);		
	}
	
	
	public void drawSonar(Pose p, int[] distance) {
		if (distance == null)
			return;
		boolean[] ahead = SonarDistanceEvt.ahead(p.relativeCourse); // only consider "ahead" sensors (relative course) 
		int offset = (int)(0.5*SCALE);
		
		for (int i=0; i<8; ++i) {
			int d = distance[i];
			
			AffineTransform t = g.getTransform();
			
			t.translate(p.pos.x, p.pos.y); // rover center
			t.rotate(-p.heading.toRad()); // rover heading
			t.translate(Const.sonarPosition[i].x, Const.sonarPosition[i].y); // sensor offset (in rover coordinates!)
			g.setTransform(t); // center at sonar sensor
			
			// beam direction (relative to rover heading)
			int beamdir = (int) (Const.sonarDirection[i].toDegree() - 90); // FX 0deg=x-axis 

			Rectangle r = new Rectangle();
			r.grow(d+offset, d+offset);
//			r.grow(d, d);
			
			if (d < Const.SONAR_SENSOR_LIMIT && ahead[i]) {
				// obstacle ahead detected
				Color c = new Color(0,0,0, 16 - (d*16/Const.SONAR_SENSOR_LIMIT)); // the closer the darker
				g.setColor(c);
//				g.setColor(colorObstacle);
				g.drawArc(r.x, r.y, r.width, r.height, beamdir-(Const.sonarBeamWidth/2), Const.sonarBeamWidth);
			}
			
			// nothing within {d} cm
			r.grow(-SCALE, -SCALE);
			g.setColor(colorFree);			
			g.fillArc(r.x, r.y, r.width, r.height, beamdir-(Const.sonarBeamWidth/2), Const.sonarBeamWidth);
			
			g.setTransform(initialXform);
		}		
	}
	
	public void drawLidar(LidarScanEvt e) {
		long t0 = System.currentTimeMillis();
		if (e == null)
			return;

		int offset = (int)(0.5*SCALE);
		double beamWidth = e.beamWidth();
		int arcAngle = (int) Math.ceil(beamWidth);

		g.translate(e.pose.pos.x, e.pose.pos.y); // rover center
		g.rotate(-e.pose.heading.toRad()); // rover heading
//		g.transform(t); // center at sonar sensor

		for (int i=0; i<e.scan.length; i++) {
			int d = e.scan[i];
			if (d==0)
				continue;
			
			// beam direction (relative to rover heading)
			int beamdir = (int) (270 - (i * beamWidth)); 

			Rectangle r = new Rectangle();
			r.grow(d+offset, d+offset);
			
			if (d < Const.LIDAR_SENSOR_LIMIT) {
				Color c = new Color(0,0,0, 32 - (d*32/Const.LIDAR_SENSOR_LIMIT)); // the closer the darker
				g.setColor(c);
				g.drawArc(r.x, r.y, r.width, r.height, beamdir, arcAngle);
			}
			
			// nothing within {d} cm
			r.grow(-SCALE, -SCALE);
			g.setColor(colorFree);			
			g.fillArc(r.x, r.y, r.width, r.height, beamdir, arcAngle);

		}		
		
		g.setTransform(initialXform);
//		System.out.println("draw: " + (System.currentTimeMillis() - t0));
	}
	
	public void drawLidar0(Pose p, int[] scan) {
		long t0 = System.currentTimeMillis();
		if (scan == null)
			return;

		int offset = (int)(0.5*SCALE);

		AffineTransform t = g.getTransform();	
		t.translate(p.pos.x, p.pos.y); // rover center
		t.rotate(-p.heading.toRad()); // rover heading
		g.setTransform(t); // center at sonar sensor

		for (int i=0; i<scan.length-1; i=i+2) {
			int d = 0;
			if (scan[i] > 0 && scan[i+1]>0) 
				d = (scan[i] + scan[i+1])/2;
			else 
				d = Math.max(scan[i], scan[i+1]);
			if (d==0)
				continue;
			
			
			// beam direction (relative to rover heading)
			int beamdir = 270 - (i / 2); 

			Rectangle r = new Rectangle();
			r.grow(d+offset, d+offset);
			
			if (d < Const.LIDAR_SENSOR_LIMIT) {
				Color c = new Color(0,0,0, 32 - (d*32/Const.LIDAR_SENSOR_LIMIT)); // the closer the darker
				g.setColor(c);
				//g.drawArc(r.x, r.y, r.width, r.height, beamdir-(Const.lidarBeamWidth/2), Const.lidarBeamWidth);
				g.drawArc(r.x, r.y, r.width, r.height, beamdir, 1);
			}
			
			// nothing within {d} cm
			r.grow(-SCALE, -SCALE);
			g.setColor(colorFree);			
			//g.fillArc(r.x, r.y, r.width, r.height, beamdir-(Const.lidarBeamWidth/2), Const.lidarBeamWidth);
			g.fillArc(r.x, r.y, r.width, r.height, beamdir, 1);

		}		
		
		g.setTransform(initialXform);
//		System.out.println("draw0: " + (System.currentTimeMillis() - t0));
	}
	
//	public void drawSonar1(Waypoint p, int[] distance) {
//		
//		SonarDistanceEvt s = new SonarDistanceEvt(distance);
//		boolean[] relevant = s.distOnCourse(p.relativeCourse); // only consider "forward" sensors (relative course) 
//		int freeInset = (int)(0.5*SCALE);
//		
//		// obstacles
//		for (int i=0; i<8; ++i) {
//			int d = distance[i];
//
//			if (!(d < Const.DISTANCE_SENSOR_LIMIT && relevant[i])) {
//				continue;
//			}
//
//			AffineTransform t = g.getTransform();
//			
//			t.translate(p.x, p.y); // rover center
//			t.rotate(-p.heading.toRad()); // rover heading
//			t.translate(Const.sonarPosition[i].x, Const.sonarPosition[i].y); // sensor offset
//			g.setTransform(t);
//			
//			int beamdir = (int) (Const.sonarDirection[i].toDegree() - 90); // FX 0deg=x-axis 
//			
//			g.setColor(colorObstacle);
//			g.drawArc(-d-freeInset, -d-freeInset, 2*(d+freeInset), 2*(d+freeInset), beamdir-(Const.sonarBeamWidth/2), Const.sonarBeamWidth);
//			
//			g.setTransform(initialXform);
//		}
//
//		// free space
//		for (int i=0; i<8; ++i) {
//			int d = distance[i];
//			
//			AffineTransform t = g.getTransform();
//			
//			t.translate(p.x, p.y); // rover center
//			t.rotate(-p.heading.toRad()); // rover heading
//			t.translate(Const.sonarPosition[i].x, Const.sonarPosition[i].y); // sensor offset
//			g.setTransform(t);
//			
//			int beamdir = (int) (Const.sonarDirection[i].toDegree() - 90); // FX 0deg=x-axis 
//			
//			g.setColor(colorFree);
//			g.fillArc(-d, -d, 2*d, 2*d, beamdir-(Const.sonarBeamWidth/2), Const.sonarBeamWidth);
//			
//			g.setTransform(initialXform);
//		}
//	}
	
}
