/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package de.konteno.ao.mrover.telemetry;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.ResourceBundle;

import de.konteno.ao.mrover.common.track.Pose;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.transform.Affine;
import javafx.stage.Stage;

public class MainController implements Initializable {

	@FXML
	VBox vbox;

	@FXML
	ComboBox<String> comboMqttAddress;

	@FXML
	Slider timeSlider;

	@FXML
	Label timeLabel;

	@FXML
	Label statusLabel;

	@FXML
	Canvas canvas;

//	@FXML
//	Canvas mapCanvas;

	@FXML
	AnchorPane canvasPane;

	@FXML
	AnchorPane controlPane;

	@FXML
	ComboBox<MqttCommand> commandList;

	@FXML
	TextField mqttPayloadField;

	@FXML
	ToggleButton connectToggle;
	
	@FXML
	Slider speedSlider;
	
	@FXML
	Label headingLabel;

	@FXML
	CheckBox trackDr;

	@FXML
	CheckBox whilePressed;

	@FXML
	Slider pwrLeftRight;

	@FXML
	Slider pwrFrontBack;

	RoverModel rover = new RoverModel();

	private SimpleStringProperty mqttAddress = new SimpleStringProperty();

	private SimpleStringProperty mqttPayload = new SimpleStringProperty();

	// invert Y, shift by 200
	private Affine canvasXform = new Affine(1, 0, 200, 0, -1, 200);

	private Point2D panStart;
	
	private Map map = new Map();

	double scale = 1;

	@FXML
	private void handleCanvasScroll(ScrollEvent e) throws Exception {
		double f = (e.getDeltaY() > 0) ? 1.2 : 1 / 1.2;
		scale *= f;

		// shift view
		Point2D m = new Point2D(e.getX(), e.getY());
		Point2D p = canvasXform.inverseTransform(m);

		canvasXform.appendScale(f, f);
		Point2D p2 = canvasXform.inverseTransform(m);
		canvasXform.appendTranslation(p2.getX() - p.getX(), p2.getY() - p.getY());

		paint();
	}

	@FXML
	private void handleCanvasClick(MouseEvent e) {
		panStart = new Point2D(e.getX(), e.getY());
	}

	@FXML
	private void handleCanvasDrag(MouseEvent e) {
		double dx = e.getX() - panStart.getX();
		double dy = e.getY() - panStart.getY();
		panStart = new Point2D(e.getX(), e.getY());

		canvasXform.appendTranslation(dx / scale, -dy / scale);
		paint();
	}

	public void connect() throws Exception {
		System.out.println("connect " + connectToggle.selectedProperty().get());
		if (connectToggle.selectedProperty().get()) {
			rover.connect(mqttAddress.get());
		} else {
			rover.disconnect();
		}

		// catch close event and disconnect
		// TODO no better way?!?
		Stage stage = (Stage) vbox.getScene().getWindow();
		stage.setOnCloseRequest(e -> rover.disconnect());
	}

	public void stop() {
		rover.sendCmd("rover/cmd/stop", "");
	}

	public void sendCommand() {
		MqttCommand c = commandList.getSelectionModel().getSelectedItem();
		rover.sendCmd(c.topic, mqttPayload.get());
	}

	public void clearTrack() {
		rover.clearTrack();
		map.clearMap();
	}

	void paint() {
		GraphicsContext g = canvas.getGraphicsContext2D();
		g.setTransform(canvasXform);

		g.setFill(Color.WHITE);
		g.fillRect(-1000, -1000, 2000, 2000);

		paintMapPixels(g);
		
		rover.myPaint(g);
	}

	WritableImage tgt = new WritableImage(map.WIDTH, map.HEIGHT);

	private void paintMapSmooth(GraphicsContext g) {
		boolean sonarAvailable = rover.sonarScan != null && rover.sonarTrack().size() > 0;
		boolean scanAvailable = sonarAvailable || rover.lidarScan != null;
		if (scanAvailable)
		{
			if (sonarAvailable) {
				Pose p = rover.sonarTrack().last;
				map.drawSonar(p, rover.sonarScan);
			}
			map.drawLidar(rover.lidarScan);

			WritableImage image = SwingFXUtils.toFXImage(map.img, tgt);

			g.save();
			g.translate(-0.5*map.SCALE*map.WIDTH, 0.5*map.SCALE*map.HEIGHT);
			
			g.scale(map.SCALE, -map.SCALE);
			g.drawImage(image, 0, 0);
			g.restore();
		}
	}

	BufferedImage after = new BufferedImage(map.SCALE*map.WIDTH, map.SCALE*map.HEIGHT, BufferedImage.TYPE_BYTE_GRAY);
	WritableImage tgt5 = new WritableImage(map.SCALE*map.WIDTH, map.SCALE*map.HEIGHT);

	/**
	 * "pixelated" map
	 * JavaFx does not offer control over interpolation/smoothing when scaling images
	 * https://bugs.openjdk.java.net/browse/JDK-8091877
	 * 
	 * so we scale the AWT image before painting ...
	 * 
	 */
	private void paintMapPixels(GraphicsContext g) {
		boolean sonarAvailable = rover.sonarScan != null && rover.sonarTrack().size() > 0;
		boolean scanAvailable = sonarAvailable || rover.lidarScan != null;
		if (scanAvailable)
		{
			if (sonarAvailable) {
				Pose p = rover.sonarTrack().last;
				map.drawSonar(p, rover.sonarScan);
			}
			map.drawLidar(rover.lidarScan);
			
			AffineTransform at = new AffineTransform();
			at.scale(map.SCALE, map.SCALE);
			AffineTransformOp scaleOp = new AffineTransformOp(at, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
			after = scaleOp.filter(map.img, after);
			WritableImage image = SwingFXUtils.toFXImage(after, tgt5);
			
			g.save();
			g.translate(-0.5*map.SCALE*map.WIDTH, 0.5*map.SCALE*map.HEIGHT);
			g.scale(1, -1);
			g.drawImage(image, 0, 0);
			g.restore();
		}
	}
//
//	private void paintSonar() {
//		if (rover.distance != null && rover.drTrack.size() > 0)
//		{
//			Waypoint p = rover.drTrack.last;
//			sonarMap.drawSonar(p, rover.distance);
//
//			Image image = SwingFXUtils.toFXImage(sonarMap.img, null);
//			mapCanvas.getGraphicsContext2D().drawImage(image, 0, 0);
//		}
//	}
//	
	

	public void testDraw() {
//		Map map = new Map();
//		Image image = SwingFXUtils.toFXImage(map.img, null);
//		mapCanvas.getGraphicsContext2D().drawImage(image, 0, 0);
	}

	public void remoteClick(MouseEvent e) {
//		System.out.println(e.getSource());
		Button b = (Button) e.getSource();
		
		if (rover.whilePressed.getValue() && e.getEventType().equals(MouseEvent.MOUSE_RELEASED)) {
			rover.sendCmd("rover/cmd/stop", "");
			return;
		}
		
		switch (b.textProperty().get()) {
		case "fwd":
			rover.sendCmd("rover/cmd/drive", "0");
			break;
		case "right":
			rover.sendCmd("rover/cmd/drive", "90");
			break;
		case "back":
			rover.sendCmd("rover/cmd/drive", "180");
			break;
		case "left":
			rover.sendCmd("rover/cmd/drive", "270");
			break;
		case "cw":
			rover.sendCmd("rover/cmd/rotate", "right");
			break;
		case "ccw":
			rover.sendCmd("rover/cmd/rotate", "left");
			break;
		case "stop":
			rover.sendCmd("rover/cmd/stop", "");
			break;
		}

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		final ObservableList<String> ips = FXCollections.observableArrayList();
		ips.add("tcp://192.168.1.50:1883");
		ips.add("tcp://192.168.1.51:1883");
		ips.add("tcp://127.0.0.1:1883");
		ips.add("tcp://127.0.0.1:1884");
		comboMqttAddress.itemsProperty().set(ips);
		comboMqttAddress.valueProperty().bindBidirectional(mqttAddress);

		statusLabel.textProperty().bindBidirectional(rover.status);

		timeSlider.valueProperty().bindBidirectional(rover.time);
		timeLabel.textProperty().bind(Bindings.convert(rover.time));
		headingLabel.textProperty().bind(Bindings.convert(rover.heading));

		// resize canvas with parent
		canvas.widthProperty().bind(canvasPane.widthProperty());
		canvas.heightProperty().bind(canvasPane.heightProperty());

		connectToggle.disableProperty().bind(mqttAddress.isEmpty());
		controlPane.disableProperty().bind(rover.connected.not());

		// repaint if track or time changed
		rover.trackChanged = () -> Platform.runLater(() -> paint());
		rover.distanceChanged = () -> Platform.runLater(() -> paint());
		rover.time.addListener((n) -> paint());

		final ObservableList<MqttCommand> commands = FXCollections.observableArrayList();
		commands.add(new MqttCommand("rover/cmd/drive", "angle"));
		commands.add(new MqttCommand("rover/cmd/rotate", "right"));
		commands.add(new MqttCommand("rover/cmd/turn", "+60"));
		commands.add(new MqttCommand("rover/cmd/turn", "north"));
		commands.add(new MqttCommand("rover/cmd/stop", ""));
		commands.add(new MqttCommand("rover/cmd/shutdown", ""));
		commands.add(new MqttCommand("rover/cmd/demo", "square"));
		commands.add(new MqttCommand("rover/cmd/demo", "squareTurn"));
		commands.add(new MqttCommand("esp/cmd/compdump", ""));
		commands.add(new MqttCommand("rover/cmd/calibratecompass", ""));
		commands.add(new MqttCommand("rover/cmd/calibrateodo", ""));
		commandList.setItems(commands);
		commandList.getSelectionModel().selectedItemProperty().addListener((ov, o, n) -> mqttPayload.set(n.payload));
		mqttPayloadField.textProperty().bindBidirectional(mqttPayload);
		
		speedSlider.valueProperty().addListener((ov, s0, s1) -> rover.sendCmd("rover/set/maxspeed", Integer.toString(s1.intValue())));
		ChangeListener<Number> pwrListener = (ov, s0, s1) -> rover.sendPowerDistribution(pwrLeftRight.getValue(), pwrFrontBack.getValue());
		pwrLeftRight.valueProperty().addListener(pwrListener); 
		pwrFrontBack.valueProperty().addListener(pwrListener);

//		rover.distanceChanged = () -> Platform.runLater(() -> paintSonar());

		trackDr.selectedProperty().bindBidirectional(rover.trackDr);
		whilePressed.selectedProperty().bindBidirectional(rover.whilePressed);
		
		paint();

	}

}
