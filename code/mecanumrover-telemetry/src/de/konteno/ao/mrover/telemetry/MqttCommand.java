/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package de.konteno.ao.mrover.telemetry;

public class MqttCommand {
	public String topic;
	public String payload;
	
	public MqttCommand (String t, String p) {
		topic = t;
		payload = p;
	}
	
	@Override
	public String toString() {
		return topic + ":" + payload;
	}
}
