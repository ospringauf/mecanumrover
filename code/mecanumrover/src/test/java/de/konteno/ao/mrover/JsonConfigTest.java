package de.konteno.ao.mrover;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.konteno.ao.mrover.driver.odo.OdoSensor;

public class JsonConfigTest {

    @Test
    public void testSerialize() throws Exception {
        OdoSensor s1 = new OdoSensor();
        OdoSensor s2 = new OdoSensor();
        
        System.out.println(s1.toJson());

        System.out.println(OdoSensor.toJsonArray(s1, s2));
    }
    
    @Test
    public void testDeserialize() throws Exception {
        String j1 = "{\"scaleX\":0.011,\"scaleY\":0.022,\"rot\":{\"degree\":11.0}}";
        
        OdoSensor s = OdoSensor.fromJson(j1);
        assertEquals(0.011, s.scaleX, 0.0001);
        assertEquals(0.022, s.scaleY, 0.0001);
        assertEquals(11.0, s.rot.toDegree(), 0.0001);
     
        String j2 = "[{\"scaleX\":0.123,\"scaleY\":0.012195121951219513,\"rot\":{\"degree\":-90.0}},{\"scaleX\":0.234,\"scaleY\":0.012195121951219513,\"rot\":{\"degree\":-90.0}}]";
        OdoSensor[] s2 = OdoSensor.fromJsonArray(j2);
        System.out.println(s2);
        assertEquals(0.123, s2[0].scaleX, 0.0001);
        assertEquals(0.234, s2[1].scaleX, 0.0001);
    }

}
