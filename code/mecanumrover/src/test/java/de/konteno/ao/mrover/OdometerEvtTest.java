/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover;

import static org.junit.Assert.*;

import org.junit.Test;

import de.konteno.ao.mrover.common.event.OdometerEvt;

public class OdometerEvtTest {

	@Test
	public void test() {
		OdometerEvt e = OdometerEvt.parse(" -0.3  12.7 124 ");
		assertEquals(-0.3, e.countX, 0.0001);
		assertEquals(12.7, e.countY, 0.0001);
		assertEquals(124, e.sigQual);
	}

}
