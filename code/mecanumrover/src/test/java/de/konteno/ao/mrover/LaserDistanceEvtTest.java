/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.konteno.ao.mrover.common.event.LaserDistanceEvt;

public class LaserDistanceEvtTest {

	
	@Test
	public void testParse() {
		LaserDistanceEvt d = LaserDistanceEvt.parseEsp(" -45 85 7");
		assertEquals(-90.0, d.angle[0].toDegree(), 0);
		assertEquals(85, d.distance[0]);
		assertEquals(0.0, d.angle[1].toDegree(), 0);
		assertEquals(7, d.distance[1]);
	}

}
