/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.parsetools.RecordParser;

public class RecordParserTest {

	String line;

	@Test
	public void test() {
		String s = new String();
		for (int i=10; i>=0; i--) {
			s = s + "$this is line " + i + "\r\n";
		}
		
		RecordParser p = RecordParser.newDelimited("\r\n", b -> {
			line = b.toString();
			System.out.println("parsed: " + line);
		});
		
		while (s.length() > 0) {
			byte x[] = new byte[10];
			int end = Math.min(10,  s.length());
			s.getBytes(0, end, x, 0);
			p.handle(Buffer.buffer(x));
			s = s.substring(end);
		}
		
		assertEquals("$this is line 0", line);
	}

}
