/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.konteno.ao.mrover.common.SerialMessage;

public class SerialMessageTest {

	@Test
	public void testParse() {
		SerialMessage m = SerialMessage.parseNmea("$topic,arg1*EE");
		System.out.println(m.getTopic());
		System.out.println(m.getPayload());
		System.out.println(m.getChecksum());
		assertEquals("topic", m.getTopic());
		assertEquals("arg1", m.getPayload());
		assertEquals(0xee, m.getChecksum());
		
		assertEquals(m.calcChecksum(), m.getChecksum());

		m = SerialMessage.parseNmea("$command*EE");
		System.out.println(m.getTopic());
		System.out.println(m.getPayload());
		System.out.println(m.getChecksum());
		assertEquals("command", m.getTopic());
		assertEquals(null, m.getPayload());
		assertEquals(0xee, m.getChecksum());

	}
	
	@Test
	public void testBuild() {
		SerialMessage m = new SerialMessage("TOP1", "arg1,3,x");
		String actual = m.toLineFormat();
		assertEquals("$TOP1,arg1,3,x*DB", actual);
	}

}
