package de.konteno.ao.mrover;

import static org.junit.Assert.*;

import org.junit.Test;

public class PowerDistTest {

	public void distributePower0(double lr, double fb) {
		double f = (fb <= 0) ? 1.0 : 100.0/(100.0+Math.abs(fb));
		double b = (fb >= 0) ? 1.0 : 100.0/(100.0+Math.abs(fb));
		double l = (lr <= 0) ? 1.0 : 100.0/(100.0+Math.abs(lr));
		double r = (lr >= 0) ? 1.0 : 100.0/(100.0+Math.abs(lr));
		System.out.format("motor power factors: %3.2f %3.2f %3.2f %3.2f\n", f*l, f*r, b*l, b*r);
	}

	double rem(double x) {
		return (100.0-Math.abs(x))/100.0;
	}
	public void distributePower(double lr, double fb) {
		double l = (lr <= 0) ? 1.0 : rem(lr);
		double r = (lr >= 0) ? 1.0 : rem(lr);
		double f = (fb <= 0) ? 1.0 : rem(fb);
		double b = (fb >= 0) ? 1.0 : rem(fb);
		System.out.format("motor power factors: %3.2f %3.2f %3.2f %3.2f\n", f*l, f*r, b*l, b*r);
	}
	
	@Test
	public void test() {
		distributePower0(0, 0);
		distributePower(0,0);
		System.out.println();
		distributePower(95,0);
	}

}
