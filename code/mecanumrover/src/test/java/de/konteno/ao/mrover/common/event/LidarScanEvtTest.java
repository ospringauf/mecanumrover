package de.konteno.ao.mrover.common.event;

import static org.junit.Assert.*;

import org.junit.Test;

import de.konteno.ao.mrover.common.track.Pose;
import de.konteno.ao.mrover.common.track.TrackType;
import de.konteno.ao.mrover.common.unit.Angle;
import de.konteno.ao.mrover.common.unit.Pos;

public class LidarScanEvtTest {

	@Test
	public void test1() {
		int[] scan = { 1, 0, 2, 0, 3, 0 };
		LidarScanEvt e = new LidarScanEvt(scan, null);
		e = e.reduce(2);
		
		assertEquals(3, e.scan.length);
		assertArrayEquals(new int[]{1, 2, 3}, e.scan);
	}

	@Test
	public void test2() {
		int[] scan = { 1, 3, 2, 4, 3, 5, 4};
		LidarScanEvt e = new LidarScanEvt(scan, null);
		e = e.reduce(2);
		
		assertEquals(3, e.scan.length);
		//assertArrayEquals(new int[]{2, 3, 4}, e.scan);
		assertArrayEquals(new int[]{1, 2, 3}, e.scan);
	}

	@Test
	public void testParse() {
		Pose p = new Pose(Pos.of(101,  102), Angle.ZERO, Angle.ZERO, TrackType.ODO);
		p.time = 99;
		int[] scan = { 201, 201, 203 };
		LidarScanEvt e = new LidarScanEvt(scan, p);
		String m = e.mqttString();
		System.out.println(m);
		e = LidarScanEvt.parseMqtt(m);
		assertEquals(99, e.pose.time);
		assertEquals(101, e.pose.pos.x, 0.01);
		assertEquals(201, e.scan[0]);
	}
}
