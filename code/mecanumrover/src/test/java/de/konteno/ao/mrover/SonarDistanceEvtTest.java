/*********************************************************************
 * Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package de.konteno.ao.mrover;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.konteno.ao.mrover.common.event.SonarDistanceEvt;


public class SonarDistanceEvtTest {

	@Test
	public void testParse() {
		SonarDistanceEvt d = SonarDistanceEvt.parse(" 150  38  32 150 150 150 150 150");
		assertEquals(38, d.distance[1]);
		assertEquals(32, d.distance[2]);

	}

}
