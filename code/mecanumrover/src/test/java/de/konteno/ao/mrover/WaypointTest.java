/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover;

import static org.junit.Assert.*;

import org.junit.Test;

import de.konteno.ao.mrover.common.track.TrackType;
import de.konteno.ao.mrover.common.track.Pose;
import de.konteno.ao.mrover.common.unit.Angle;
import de.konteno.ao.mrover.common.unit.Pos;

public class WaypointTest {

	@Test
	public void testMoveForward() {
		Pose w0 = new Pose(Pos.ZERO, Angle.ZERO, Angle.ZERO, TrackType.ODO);
		Pose w1 = w0.addRelative(Pos.of(0, 10), Angle.fromDegree(0));
		
		assertEquals(0.0, w1.pos.x, 0.0);
		assertEquals(10.0, w1.pos.y, 0.0);
		assertEquals(0.0, w1.heading.toDegree(), 0);
		System.out.println(w1);
	}

	@Test
	public void testMoveDiagonal() {
		Pose w0 = new Pose(Pos.ZERO, Angle.ZERO, Angle.ZERO, TrackType.ODO);
		
		// go 10cm in 45°
		Pose w1 = w0.addRelative(Pos.of(0, 10), Angle.fromDegree(45));
		assertEquals(7.07, w1.pos.x, 0.01);
		assertEquals(7.07, w1.pos.y, 0.01);
		assertEquals(45.0, w1.heading.toDegree(), 0);
		System.out.println(w1);
		
		// go 10cm in 90°
		Pose w2 = w1.addRelative(Pos.of(0, 10), Angle.fromDegree(90));
		assertEquals(17.07, w2.pos.x, 0.01);
		assertEquals(7.07, w2.pos.y, 0.01);
		assertEquals(90.0, w2.heading.toDegree(), 0);
		System.out.println(w2);
	
	}
	
	@Test
	public void testLeft() {
		Pose w0 = new Pose(Pos.ZERO, Angle.ZERO, Angle.ZERO, TrackType.ODO);
		
		// go 10cm in 0° heading / -90° relative
		Pose w1 = w0.addRelative(Pos.of(-10, 0), Angle.fromDegree(0));
		assertEquals(-10, w1.pos.x, 0.01);
		assertEquals(0, w1.pos.y, 0.01);
		assertEquals(0.0, w1.heading.toDegree(), 0);
		System.out.println(w1);
	}

	@Test
	public void test45() {
		Pose w0 = new Pose(Pos.ZERO, Angle.ZERO, Angle.ZERO, TrackType.ODO);
		
		// go 10cm in 0° heading / 45° relative
		Pose w1 = w0.addRelative(Pos.of(7.07, 7.07), Angle.fromDegree(0));
//		assertEquals(-10, w1.pos.x, 0.01);
//		assertEquals(0, w1.pos.y, 0.01);
		assertEquals(0.0, w1.heading.toDegree(), 0);
		System.out.println(w1);
	}

}
