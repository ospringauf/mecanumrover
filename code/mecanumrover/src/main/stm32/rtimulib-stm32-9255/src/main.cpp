////////////////////////////////////////////////////////////////////////////
//
//  This file is part of RTIMULib-Arduino
//
//  Copyright (c) 2014-2015, richards-tech
//  2019 Oliver Springauf - adapted for A&O Mecanumrover
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of 
//  this software and associated documentation files (the "Software"), to deal in 
//  the Software without restriction, including without limitation the rights to use, 
//  copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
//  Software, and to permit persons to whom the Software is furnished to do so, 
//  subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all 
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
//  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
//  PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
//  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
//  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <Wire.h>
#include "I2Cdev.h"
#include "RTIMUSettings.h"
#include "RTIMU.h"
#include "RTFusionRTQF.h" 
#include "CalLib.h"
#include <EEPROM.h>


RTIMU *imu;                                           // the IMU object
RTFusionRTQF fusion;                                  // the fusion object
RTIMUSettings settings;                               // the settings object

//  DISPLAY_INTERVAL sets the rate at which results are displayed
#define DISPLAY_INTERVAL  25                         // interval between pose displays
#define INGORE_UNCHANGED (1000/DISPLAY_INTERVAL)
#define MY_SAMPLE_RATE 200
#define OUTPUT_THRESHOLD (0.1/RTMATH_RAD_TO_DEGREE)
#undef DRIFT_DEBUG

//  SERIAL_PORT_SPEED defines the speed to use for the debug serial port
#define  SERIAL_PORT_SPEED  115200


unsigned long lastDisplay;
unsigned long lastRate;
int sampleCount;

void resetImu() {
    // reset
    delay(200);
    pinMode(PB9, OUTPUT);
    digitalWrite(PB9, LOW);
    delay(200);
    digitalWrite(PB9, HIGH);
    delay(200);
    // Serial.println("IMU reset");
}


void setupImu() {
    int errcode;
    resetImu();

    // STM32 blue pill can handle 200Hz
    settings.m_MPU9255CompassSampleRate = MY_SAMPLE_RATE;
    settings.m_MPU9255GyroAccelSampleRate = MY_SAMPLE_RATE;
    imu = RTIMU::createIMU(&settings);                        // create the imu object
  
    Serial.print("ArduinoIMU starting using device "); Serial.println(imu->IMUName());
    if ((errcode = imu->IMUInit()) < 0) {
        Serial.print("Failed to init IMU: "); Serial.println(errcode);
    }
  
    // experimental: skip gyro bias calibration
    // imu->setGyroBiasValid(true);

    if (imu->getCalibrationValid())
        Serial.println("Using compass calibration");
    else
        Serial.println("No valid compass calibration data");

    lastDisplay = lastRate = millis();
    sampleCount = 0;

    // Slerp power controls the fusion and can be between 0 and 1
    // 0 means that only gyros are used, 1 means that only accels/compass are used
    // In-between gives the fusion mix.
    
    //fusion.setSlerpPower(0.02);
    fusion.setSlerpPower(0.0);
    
    // use of sensors in the fusion algorithm can be controlled here
    // change any of these to false to disable that sensor
    
    fusion.setGyroEnable(true);
    fusion.setAccelEnable(true);
    fusion.setCompassEnable(true);

    // LED signals calibration phase
    digitalWrite(LED_BUILTIN, LOW);
    // delay(1000);
}

void setup()
{
    Serial.begin(SERIAL_PORT_SPEED);

    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);

    Wire.begin();
    setupImu();
}


// void recvCommand()
// {
//   if (Serial.available() > 0)
//   {
//     char c = Serial.read();
//     switch (c)
//     {
 
//     case 'r':
//       Serial.println("reset");
//       resetImu();
//       imu->IMUInit();
//       break;

//     // reset Arduino
//     case 'R':
//       // Serial.println("reset");
//       break;

//     default:
//       Serial.print("unknown cmd: ");
//       Serial.println(c);
//       break;
//     }
//   }
// }

int unchanged = INGORE_UNCHANGED;
float lastYaw = 0.0;

unsigned long t0 = 0;
float yaw0;

float drift; // rad/sec
int drift_ignore = 0;

// dynamically adjust gyro bias by "drift" measured in quiet periods of time
void updateDrift(unsigned long now, float yaw) {
    if (drift_ignore > 0) {
        drift_ignore--;
        return;
    }
    if ((abs(yaw-yaw0) > 8*OUTPUT_THRESHOLD)) {
        drift = 0.0;
        yaw0 = yaw;
        t0 = now;
        drift_ignore = MY_SAMPLE_RATE; // skip a few values until device is still again
    } else if (now - t0 > 5000) {
        drift = (yaw - yaw0) * 1000 / (now - t0);
        yaw0 = yaw;
        t0 = now;

#ifdef DRIFT_DEBUG
        Serial.print("   drift = "); 
        Serial.print(drift * RTMATH_RAD_TO_DEGREE * 60);
        Serial.print(" deg/min "); 
        Serial.print(1000* drift);
        Serial.print(" mrad/s - bias: "); 
        Serial.print(1000* imu->m_gyroBias.z());
        Serial.println();
#endif

        imu->m_gyroBias.setZ(imu->m_gyroBias.z() - (0.8*drift));
    }
}

void loop()
{  
    unsigned long now = millis();
    unsigned long delta = now - lastDisplay;
    unsigned long loopCount = 1;
    float yaw;
    // int freq;
  
    // get the latest data if ready yet
    while (imu->IMURead()) {                                
        // this flushes remaining data in case we are falling behind
        if (++loopCount >= 10) {
            Serial.print("#");
            continue;
        }
        fusion.newIMUData(imu->getGyro(), imu->getAccel(), imu->getCompass(), imu->getTimestamp());
        sampleCount++;
    }

    
    const RTVector3& fus = fusion.getFusionPose();
    yaw = fus.z();

    if (imu->IMUGyroBiasValid() && (t0 == 0)) {
        // Serial.println(", gyro bias valid");
        digitalWrite(LED_BUILTIN, HIGH);
        t0 = now; 
        yaw0 = yaw;
    }
    
    if (imu->IMUGyroBiasValid()) {
        updateDrift(now, yaw);
    }

    if (delta >= DISPLAY_INTERVAL) {
        // freq = sampleCount * 1000 / delta;
        //  RTMath::display("Gyro:", (RTVector3&)imu->getGyro());                // gyro data
        //  RTMath::display("Accel:", (RTVector3&)imu->getAccel());              // accel data
        //  RTMath::display("Mag:", (RTVector3&)imu->getCompass());              // compass data
        // const RTVector3& comp = imu->getCompass();
        // RTMath::displayRollPitchYaw("Pose:", (RTVector3&)fus); // fused output

        if ((abs(yaw - lastYaw) > OUTPUT_THRESHOLD) || (unchanged <= 0)) {
            // Serial.print(freq); Serial.print(" Hz;");
            // Serial.print(" yaw = "); Serial.print(yaw * RTMATH_RAD_TO_DEGREE);
            Serial.print("$YAW,"); 
            Serial.print(yaw * RTMATH_RAD_TO_DEGREE); 
            Serial.print("*");

            // Serial.print(" com="); Serial.print(comp.z());
            Serial.println();
            lastYaw = yaw;
            unchanged = INGORE_UNCHANGED;
        } else {
            //Serial.println(unchanged);
            unchanged--;
        }
        sampleCount = 0;
        lastDisplay = now;
    }

    // recvCommand();
}
