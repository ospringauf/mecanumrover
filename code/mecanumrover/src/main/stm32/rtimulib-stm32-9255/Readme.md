# A&O Rover IMU/AHRS

based on RTIMULIB https://github.com/SalimTerryLi/RTIMULib-Arduino
fork of https://github.com/richardstechnotes/RTIMULib-Arduino with support for MCU-9255 IMU

This is the IMU fusion algorithm for my gyro/accel based IMU using the STM32 MCU and MCU-9255 sensor.

Code adapted slightly for STM32 (arduino framework) and Platformio IDE.

Prints current YAW angle (degrees) on the serial port at 115200 baud.
Flashing the STM32 (STM32F103 blue pill board) requires th ST Link programmer.