#!/usr/bin/python

# from http://iot-projects.com/index.php?id=raspberry-pi-shutdown-button

# monitor GPIO.29
# if HIGH for more than 1.5s, initiate shutdown

import RPi.GPIO as GPIO
import time
import os
import socket
import smbus
import time
import sys
import fcntl
import struct

import luma.core
import luma.oled
import luma.core.interface.serial
import luma.oled.device
from luma.core.render import canvas
from PIL import ImageFont

serial = luma.core.interface.serial.i2c(port=1, address=0x3C)
device = luma.oled.device.sh1106(serial, width=128, height=64, rotate=0)
lines=["" for x in range(8)]

def get_ip_address(ifname):
  try:
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])
  except:
    return ifname + ":none"


def lcd_clear():
    for x in range(len(lines)):
	lines[x]=""
    with canvas(device) as draw:
	draw.rectangle((0, 0, device.width, device.height), outline=0, fill=0)


def lcd_string(message,line):
    lines[line-1]=message
    with canvas(device) as draw:
	# font1 = ImageFont.load_default()
	font1=ImageFont.load("/home/pi/rover/bitbuntu.pil")
	draw.rectangle((0, 0, device.width, device.height), outline=0, fill=0)
	for i in range(len(lines)):
		draw.text((0, 10*i), lines[i], font=font1, fill="white")
#		print(lines[i])
    
# GPIO.29 = BCM21
GPIO.setmode(GPIO.BCM)
GPIO.setup(21,GPIO.IN)

button_previous = 1
button_current = 1
brojac = 0
flag_pressed = 0
loop = 1


def check_ip(iface, current, line):
  # check IP address and show on display, if changed
  c = get_ip_address(iface)
  if (c != current):
    lcd_string(c, line)
  return c
      
# print IP adr on startup
ip1 = check_ip("wlan0", "?", 1)
ip2 = check_ip("eth0", "?", 2)


# watchdog loop
while True:
  button_current = GPIO.input(21);
  flag_pressed = button_previous + button_current

  if (loop % 100 == 0):
    loop = 0
    ip1 = check_ip("wlan0", ip1, 1)
    ip2 = check_ip("eth0", ip2, 2)

  if (not(flag_pressed)):
    brojac += 1
  else:
    brojac = 0

  if (button_current and (not button_previous)):
    device = luma.oled.device.sh1106(serial, width=128, height=64, rotate=0)
    lcd_clear()
    lcd_string("RESTART",1)
    # os.system("sudo shutdown -r now")
    os.system("sudo killall java")
    time.sleep(1)
    os.system("sudo java -jar /home/pi/rover/mecanumrover-0.0.1-SNAPSHOT-jar-with-dependencies.jar remote &")
    time.sleep(1)
    #print("restart")

  if ((not flag_pressed) and  brojac >= 15):    
    device = luma.oled.device.sh1106(serial, width=128, height=64, rotate=0)
    lcd_clear()
    lcd_string("SHUTDOWN",1)
    time.sleep(1)
    os.system("sudo poweroff")
    #print("shutdown")
    break

  loop = loop + 1
  button_previous = button_current
  time.sleep(0.1)

