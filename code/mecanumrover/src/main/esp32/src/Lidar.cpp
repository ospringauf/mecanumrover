// ----------------------------------------------------------------------------
// A&O MECANUMROVER
// ESP32 - sensor data acquisition
// https://bitbucket.org/ospringauf/mecanumrover
// ----------------------------------------------------------------------------

#include <Wire.h>
#include "Lidar.h"
#include "esp_log.h"
#include "Rover.h"

/*
  VL53L0X (laser range finder) setup for 2 devices:
  - both devices will start up with the same I²C address
  - shutdown 1st device
  - assign another address to 2nd device
  - activate 1st device
  - assign another address to 1st device

  https://github.com/ScruffR/VL53L0X
  https://github.com/adafruit/Adafruit_VL53L0X 

*/

#define VL53L0X_TIMEOUT 200
#define VL53L0X_BUDGET 40

void Lidar::configure(uint8_t adr1, uint8_t adr2)
{
  ESP_LOGD(TAG, "lidar configure");
  lockI2c();

  pinMode(_xshut1, OUTPUT);
  digitalWrite(_xshut1, LOW); // shutdown #1
  delay(5);

  // probe LRF2 address
  Wire.beginTransmission(adr2);
  if (Wire.endTransmission() != 0) {
    // assign #2 address
    ESP_LOGD(TAG, "LRF2 init");
    lrf2.init(); // I2C warnings "ack error addr 29" after RESET, when #2 is already at 0x31
  } else {
    ESP_LOGD(TAG, "device found at LRF2 address");
  }
  ESP_LOGD(TAG, "LRF2 set address");
  lrf2.setAddress(adr2);
  lrf2.init();

  ESP_LOGD(TAG, "LRF2 configure");
  lrf2.setMeasurementTimingBudget(VL53L0X_BUDGET);
  lrf2.setTimeout(VL53L0X_TIMEOUT);

  // assign #1 address
  //digitalWrite(XSHUT1,LOW);  <-- nein, 1. Sensor aktiviert lassen!
  ESP_LOGD(TAG, "LRF1 init");
  digitalWrite(_xshut1, HIGH);
  delay(5);
  lrf1.init();
  lrf1.setAddress(adr1);
  lrf1.setMeasurementTimingBudget(VL53L0X_BUDGET);
  lrf1.setTimeout(VL53L0X_TIMEOUT);

  // test read
  uint16_t d;
  d = lrf1.readRangeSingleMillimeters();
  ESP_LOGD(TAG, "LRF1 dist = %d", d);
  d = lrf2.readRangeSingleMillimeters();
  ESP_LOGD(TAG, "LRF2 dist = %d", d);

  delay(10);
  pinMode(_xshut1, INPUT);
  releaseI2c();
}

void Lidar::startRanging()
{
  // inital servo pos
  servoh.pointTo(-45, 400); // half left, #2 pointing fwd
  servov.pointTo(90, 400);  // level

  lockI2c();
  lrf1.startContinuous();
  lrf2.startContinuous();
  releaseI2c();
}

void Lidar::stopRanging()
{
  lockI2c();
  lrf1.stopContinuous();
  lrf2.stopContinuous();
  releaseI2c();

  // park servo into safe position
  servov.pointTo(180, 400);
  servoh.pointTo(-90, 400);
}

// The status of the last I²C write transmission. See the Wire.endTransmission() documentation for return values.
// https://www.arduino.cc/en/Reference/WireEndTransmission
// 20180727 - i2c is now self-healing !? https://github.com/espressif/arduino-esp32/issues/1679
uint8_t Lidar::checkWireStatus(uint8_t status)
{
  if (status != 0)
  {
    digitalWrite(LED_ONBOARD, HIGH);
    ESP_LOGE(TAG, "wire status=%d", status);
    Wire.begin(); // Wire.reset();
    delay(20);
    digitalWrite(LED_ONBOARD, LOW);
    return 1;
  }
  return 0;
}

void Lidar::read()
{
  lockI2c();
  uint16_t d1 = lrf1.readRangeContinuousMillimeters();

  if (!checkWireStatus(lrf1.last_status) && !lrf1.timeoutOccurred())
  {
    d1 = myround(d1 / 10.0); // centimeters
    updated |= (d1 != distance[0]);
    distance[0] = d1;
  }

  // delay(2);
  uint16_t d2 = lrf2.readRangeContinuousMillimeters();

  if (!checkWireStatus(lrf2.last_status) && !lrf2.timeoutOccurred())
  {
    d2 = myround(d2 / 10.0); // centimeters
    updated |= (d2 != distance[1]);
    distance[1] = d2;
  }
  releaseI2c();
}


char *Lidar::printData(char *buf)
{
  sprintf(buf, "%3d %3d %3d", servoh.angle, distance[0], distance[1]);
  return buf;
}

void Lidar::readAndPrint()
{
  Serial.print("Distance: ");
  Serial.print(lrf1.readRangeContinuousMillimeters());
  Serial.print(", ");
  Serial.print(lrf1.getAddress(), HEX);
  Serial.print(", ");
  Serial.print(lrf2.readRangeContinuousMillimeters());
  Serial.print(", ");
  Serial.print(lrf2.getAddress(), HEX);
  Serial.println();
}
