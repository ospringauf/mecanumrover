// ----------------------------------------------------------------------------
// A&O MECANUMROVER
// ESP32 - sensor data acquisition
// https://bitbucket.org/ospringauf/mecanumrover
// ----------------------------------------------------------------------------

#ifndef ADNS5020_H__
#define ADNS5020_H__

#undef MOUSEDEBUG

#include "Arduino.h"
#include "esp_log.h"
#include "Rover.h"

#define ADNS5020_REG_PRODUCT_ID     0x00
#define ADNS5020_REG_REVISION_ID    0x01
#define ADNS5020_REG_MOTION         0x02
#define ADNS5020_REG_DELTA_X        0x03
#define ADNS5020_REG_DELTA_Y        0x04
#define ADNS5020_REG_SQUAL          0x05
#define ADNS5020_REG_BURST_MODE     0x63
#define ADNS5020_REG_CONTROL        0x0d
#define ADNS5020_REG_PIXEL_GRAB     0x0b
#define ADNS5020_REG_CHIP_RESET     0x3a

#define ADNS5020_FRAME_LENGTH       225

// Avago ADNS-5020-EN optical mouse sensor
// see http://strofoland.com/arduino-projects/reading-a5020-optical-sensor-using-arduino-part2/
// see https://www.bidouille.org/hack/mousecam

class ADNS5020 {
  public:
    ADNS5020(int8_t sclk, int8_t sdio, int8_t ncs, int8_t nreset, int16_t cpi) :
      _sclk(sclk), _sdio(sdio), _ncs(ncs), _nreset(nreset), _cpi(cpi) {};
    
    void init()
    {      
        ESP_LOGD(TAG, "[%d] init", id);
        pinMode(_sclk, OUTPUT);
        pinMode(_sdio, INPUT);
        if (_ncs >= 0) pinMode(_ncs, OUTPUT);
        if (_nreset >= 0) pinMode(_nreset, OUTPUT);
        disable();
    };

    uint8_t id = 42;
    byte motion; // motion flag is in MSB(!)
    int8_t dx;
    int8_t dy;
    byte squal;
    byte shutter_upper;
    byte shutter_lower;
    byte max_pixel;
    byte pixel_sum;
    byte frame[ADNS5020_FRAME_LENGTH];
    const char* status = "?";

    // rotation/scaling param (used in Odometer)
    float fcos = 1;
    float fsin = 0;
    
    void reset();    
    void softReset();
    void hardReset();
    void powerUp();
    void powerDown();

    void setTransform(float angle, float scale) {
      fcos = scale * cos(angle);
      fsin = scale * sin(angle);
    }
    float transformDx();
    float transformDy();

    void setResolution(int cpi);
    void identify();
    void readDelta();
    void readBurst(bool force);
    void readFrame();

    char* mousecamFrame(char *buf, uint16_t buflen);
    char* mousecamDelta(char *buf, uint16_t buflen);
    void printDelta();
    void printAll();
    
  private:   
    int8_t _sclk;
    int8_t _sdio;
    int8_t _ncs;
    int8_t _nreset;
    int16_t _cpi;
    bool _powered = true;

    void enable();  // NCS=high
    void disable(); // NCS=low

    void setDelta(int8_t rx, int8_t ry) {
      dx = rx;
      dy = ry;
    }
    
    void pushByte(byte data);
    byte pullByte();
    byte readRegister(byte address);
    void writeRegister(byte address, byte data);
    void printd3(int i);
  
};

#endif  // ADNS5020_H__
