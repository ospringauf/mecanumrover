// ----------------------------------------------------------------------------
// A&O MECANUMROVER
// ESP32 - sensor data acquisition
// https://bitbucket.org/ospringauf/mecanumrover
// ----------------------------------------------------------------------------

#include "ADNS5020.h"
#include "Arduino.h"
#include <Print.h>
#include "esp_log.h"
#include "Rover.h"

// ADNS5020 timings (microseconds)
#define T_PD          50000 // from power down to valid motion
#define T_WAKEUP      55000
#define T_SWW         30 // 30us between write commands
#define T_SWR         20 // 20us between write and read
#define T_SRX         1 // 500ns between read and next command
#define T_SRAD        4 // 4us between address and data
#define T_BEXIT       1 // 250ns min NCS(high) after burst
#define T_NCS_SCLK    1 // 120ns after enable NSC(low) 
#define T_SCLK_NSC_R  1 // 120ns after read before disable NCS(high)
#define T_SCLK_NSC_W  20 // 20us after write before disable
#define T_DLY_SDIO    1 // 120ns SDIO delay after SCLK
#define T_NCS_SDIO    1 // 500ns after disble to SDIO floating
#define T_HOLD        1 // 500ns
#define T_SETUP       1 // 120ns

// #define T_PD          50000
// #define T_WAKEUP      100000
// #define T_SWW         30 // 30us between write commands
// #define T_SWR         20 // 20us between write and read
// #define T_SRX         2 // 500ns between read and next command
// #define T_SRAD        8 // 4us between address and data
// #define T_BEXIT       1 // 250ns min NCS(high) after burst
// #define T_NCS_SCLK    1 // 120ns after enable NSC(low) 
// #define T_SCLK_NSC_R  1 // 120ns after read before disable NCS(high)
// #define T_SCLK_NSC_W  20 // 20us after write before disable
// #define T_DLY_SDIO    2 // 120ns SDIO delay after SCLK
// #define T_NCS_SDIO    1 // 500ns after disble 
// #define T_HOLD        2 // 500ns
// #define T_SETUP       2 // 120ns



void ADNS5020::reset() {
  // digitalWrite(_sclk, HIGH); //?
  // delayMicroseconds(10);
  enable();

  // Initiate chip reset
  if (_nreset < 0)
    softReset();
  else
    hardReset();

  // Set resolution 
  setResolution(_cpi);
  disable();
}


void ADNS5020::softReset() {
  ESP_LOGV(TAG, "[%d] soft reset", id);
  
  enable();
  writeRegister(ADNS5020_REG_CHIP_RESET, 0x5a);
  delayMicroseconds(T_WAKEUP);
  disable();
}


void ADNS5020::hardReset() {
  ESP_LOGV(TAG, "[%d] hard reset", id);
  digitalWrite(_nreset, LOW);
  delayMicroseconds(T_PD); 
  digitalWrite(_nreset, HIGH);
  delayMicroseconds(T_WAKEUP); 
}

void ADNS5020::powerDown() {
  ESP_LOGV(TAG, "[%d] power down", id);
  if (_powered) {
    enable();
    writeRegister(ADNS5020_REG_CONTROL, 0b00000010);
    disable();
  }
  _powered = false;
}

void ADNS5020::powerUp() {
  ESP_LOGV(TAG, "[%d] power up", id);
  _powered = true;
  reset();
}


void ADNS5020::enable() {
  if (_ncs >= 0) {
    digitalWrite(_ncs, LOW);    
    delayMicroseconds(T_NCS_SCLK);
  }
  if (!_powered) powerUp();
}


/**
 * SDIO is in high-Z (floating) when disabled (NCS=high)
 */
void ADNS5020::disable() {
  if (_ncs >= 0) {
    digitalWrite(_ncs, HIGH);
    delayMicroseconds(T_SCLK_NSC_R);
    delayMicroseconds(T_NCS_SDIO);
  }
}


// formatted output for "mousecam" capture
// deprecated - send binary frame data over MQTT instead
char* ADNS5020::mousecamFrame(char *buf, uint16_t buflen) {
  // read frame
  enable();
  writeRegister(ADNS5020_REG_PIXEL_GRAB, 1);
  int count = 0;
  byte data;
  do {
    delayMicroseconds(2); //?
    do 
      data = readRegister(ADNS5020_REG_PIXEL_GRAB);
    while ((data & 0x80) == 0); // Data is valid
    frame[count++] = data & 0x7f;
  }
  while (count != ADNS5020_FRAME_LENGTH);
  disable();

  String s = "";
  for (int i = 0; i < ADNS5020_FRAME_LENGTH; i++) {
    byte pix = frame[i];
    if ( pix < 0x10 ) s += "0";
    s += String(pix, HEX);
  }
  s.toUpperCase();
  s.toCharArray(buf, buflen);
  return buf;
}


char* ADNS5020::mousecamDelta(char *buf, uint16_t buflen) {
  enable();
  motion = readRegister(ADNS5020_REG_MOTION); // Freezes DX and DY until they are read or MOTION is read again.
  setDelta(readRegister(ADNS5020_REG_DELTA_X), readRegister(ADNS5020_REG_DELTA_Y));
  squal = readRegister(ADNS5020_REG_SQUAL);
  disable();
  
  String s = "";
  s += dx;
  s += " ";
  s += dy;
  s.toUpperCase();
  s.toCharArray(buf, buflen);
  return buf;
}

void ADNS5020::printDelta() {
  // Serial.print(motion, BIN);
  Serial.print(" SQUAL:");  Serial.print(squal, DEC);
  Serial.print(" DELTA:"); Serial.print(dx, DEC);
  Serial.print(" ");       Serial.print(dy, DEC);
  Serial.println();
}


void ADNS5020::printAll() {
  Serial.print(" DX:");  printd3(dx);
  Serial.print(" DY:");  printd3(dy);
  Serial.print(" SQ:");  printd3(squal);
  Serial.print(" SU:");  printd3(shutter_upper);
  Serial.print(" SL:");  printd3(shutter_lower);
  Serial.print(" MP:");  printd3(max_pixel);
  Serial.print(" PS:");  printd3(pixel_sum);
  Serial.println();
}

void ADNS5020::readDelta()
{
  enable();
  motion = readRegister(ADNS5020_REG_MOTION); // Freezes DX and DY until they are read or MOTION is read again.
  setDelta(readRegister(ADNS5020_REG_DELTA_X), readRegister(ADNS5020_REG_DELTA_Y));
  squal = readRegister(ADNS5020_REG_SQUAL);
  disable();
}

/**
 * rotation + scaling
 */
float ADNS5020::transformDx() 
{
  return ((float)dx * fcos) - ((float)dy * fsin);
}

float ADNS5020::transformDy() 
{
  return ((float)dx * fsin) + ((float)dy * fcos);
}

/**
 * standard burst read 
 */
void ADNS5020::readBurst(bool force) {
  enable();
  dx = dy = 0;
  motion = readRegister(ADNS5020_REG_MOTION); // Freezes DX and DY until they are read or MOTION is read again.
  if (force || (motion != 0)) {
    pushByte(ADNS5020_REG_BURST_MODE);
    delayMicroseconds(T_SRAD); // tSRAD= 4us min.

    setDelta(pullByte(), pullByte());
    squal = pullByte();
    shutter_upper = pullByte();
    shutter_lower = pullByte();
    max_pixel = pullByte();
    pixel_sum = pullByte();
  }
  disable();
}


/**
 * read 225 pixels from pixel grabber register.
 * MSB indicates if the register contains valid data.
 */
void ADNS5020::readFrame() {
  enable();
  writeRegister(ADNS5020_REG_PIXEL_GRAB, 1);
  int count = 0;
  byte data;
  do {
    delayMicroseconds(2); //?
    do 
      data = readRegister(ADNS5020_REG_PIXEL_GRAB);
    while ((data & 0x80) == 0); // Data valid?
    frame[count++] = data & 0x7f;
  }
  while (count != ADNS5020_FRAME_LENGTH);
  disable();
}


void ADNS5020::identify() {
  enable();
  byte productId = readRegister(ADNS5020_REG_PRODUCT_ID);
  byte revisionId = readRegister(ADNS5020_REG_REVISION_ID);
  ESP_LOGD(TAG, "[%d] ADNS5020 productId=%2X (expected: 0x12) rev=%2X", id, productId, revisionId);
  if (productId != 0x12) {
    ESP_LOGW(TAG, "[%d] unknown productId. carry on.", id);
  }
  status = (productId == 0x12)? (char*)"+" : (char*)"-";

  // printId();
  // Serial.print("identify ADNS5020: productId ");
  // Serial.print(productId, HEX);
  // Serial.print(", rev. ");
  // Serial.print(revisionId, HEX);
  // Serial.println(productId == 0x12 ? " OK." : " Unknown productID. Carry on.");
  disable();
}



void ADNS5020::setResolution(int cpi) {
  enable();
  _cpi = cpi;
  byte data = (_cpi == 1000) ? 0b00000001 : 0b00000000;
  writeRegister(ADNS5020_REG_CONTROL, data);
  delayMicroseconds(T_SCLK_NSC_W);
  disable();
}



// --- private/protected ------------------------------------------------------
// --- internal state

void ADNS5020::printd3(int i) {
  int n = abs(i);
  if (i>=0) Serial.print(" ");
  if (n<10) Serial.print("  ");
  else if (n<100) Serial.print(" ");
  Serial.print(i);
}

// --- private/protected ------------------------------------------------------
// --- i/o (bit-bangig, SPI style)

byte ADNS5020::pullByte() { 
  pinMode(_sdio, INPUT);

  byte res = 0;
  for (byte i = 128; i > 0 ; i >>= 1) {
    digitalWrite(_sclk, LOW); // sensor outputs on falling edge
    delayMicroseconds(T_DLY_SDIO); // wait for data ready
    res |= i * digitalRead(_sdio);
    digitalWrite(_sclk, HIGH);
    delayMicroseconds(T_HOLD); //x - 0.5us HOLD
  }

  delayMicroseconds(T_SRX);

  return res;
}


void ADNS5020::pushByte(byte data) {

  pinMode (_sdio, OUTPUT);

  for (byte i = 128; i > 0 ; i >>= 1) {
    digitalWrite(_sclk, LOW);
    digitalWrite(_sdio, (data & i) != 0 ? HIGH : LOW);
    delayMicroseconds(T_SETUP); 
    digitalWrite(_sclk, HIGH); // sensor reads on rising clock
    delayMicroseconds(T_HOLD); 
  }
  pinMode(_sdio, INPUT);
}


byte ADNS5020::readRegister(byte address) {
  address &= 0x7F; // MSB indicates read mode: 0
  pushByte(address);
  
  delayMicroseconds(T_SRAD); // min 4us delay addr/data

  byte data = pullByte();
  return data;
}


void ADNS5020::writeRegister(byte address, byte data) {
  address |= 0x80; // MSB indicates write mode: 1

  pushByte(address);
  delayMicroseconds(T_SWW);

  pushByte(data);
  delayMicroseconds(T_SWW);
}
