// ----------------------------------------------------------------------------
// A&O MECANUMROVER
// ESP32 - sensor data acquisition
// https://bitbucket.org/ospringauf/mecanumrover
// ----------------------------------------------------------------------------

#include "ADNS5020.h"
#include "Rover.h"
#include "Odometer.h"

/**
 * This is the driver for the odometer board,
 * which controls multiple (currently up to 4) ADNS-5020-EN optical mouse sensors.
 * Sensors are addressed individually by sending the binary address (0..3) to
 * a 74HC138 decoder, which then sets the NCS (active low) for one sensor, 
 * and disconnects (NCS high) all others from the common SCLK/SDIO bus.
 * 
 * Since the ADNS-5020s are 5V devices, a logic level converter is needed 
 * to translate bus and enable signals between the ESP32 and the decoder/sensors.
 * 
 * Note that chip select (NCS) is controlled entirely by this driver, not be the individual 
 * sensor drivers.
 */

#define ODO_MODE_SHORT 0
#define ODO_MODE_VERBOSE 1

Odometer::Odometer(uint8_t n, uint8_t sclk, uint8_t sdio, int8_t adr0, int8_t adr1) : sdx(0), sdy(0), nmice(n),
                                                                                      _sclk(sclk), _sdio(sdio), _adr0(adr0), _adr1(adr1),
                                                                                      updates(0), enabled(false)
{
    // create 0..4 mouse instances
    nmice = n > 4 ? 4 : n;
    for (int i = 0; i < nmice; ++i)
    {
        mouse[i] = new ADNS5020(sclk, sdio, -1, -1, MOUSE_CPI);
        mouse[i]->id = i;

        // rotate sensor coordinates to match rover coordinates
        mouse[i]->setTransform(-PI/2.0, 1.0);
    }
}

void Odometer::init(uint8_t mask)
{
    ESP_LOGD(TAG, "odo2 init");
    pinMode(_adr0, OUTPUT);
    if (_adr1 >= 0)
        pinMode(_adr1, OUTPUT);
    pinMode(_sclk, OUTPUT);
    pinMode(_sdio, OUTPUT);

    powerUp(mask);
    for (int i = 0; i < nmice; ++i)
    {
        if (mask & (1 << i))
        {
            identify(i);
            delay(100);
        }
    }
}

uint8_t Odometer::select(uint8_t m)
{
    if (m == selected)
        return m;

    ESP_LOGV(TAG, "odo2 select %d", m);
    selected = m;

    // delay(1);
    pinMode(_sclk, OUTPUT);
    pinMode(_sdio, OUTPUT);
    digitalWrite(_adr0, (m >> 0) & 0b1);
    if (_adr1 >= 0)
        digitalWrite(_adr1, (m >> 1) & 0b1);

    digitalWrite(_sclk, HIGH); //?
    digitalWrite(_sdio, LOW);  //?
    // delayMicroseconds(200);
    delay(1);

    return m;
}

void Odometer::reset(uint8_t i)
{
    select(i);
    mouse[i]->reset();
}

void Odometer::identify(uint8_t i)
{
    select(i);
    mouse[i]->identify();
}

void Odometer::powerUp(uint8_t mask)
{
    for (int i = 0; i < nmice; ++i)
    {
        if (mask & (1 << i))
        {
            select(i);
            mouse[i]->powerUp();
            delay(100);
        }
    }
}

void Odometer::powerDown(uint8_t mask)
{
    for (int i = 0; i < nmice; ++i)
    {
        if (mask & (1 << i))
        {
            select(i);
            mouse[i]->powerDown();
            delay(100);
        }
    }
}

void Odometer::shutdown()
{
    ESP_LOGD(TAG, "odo2 shutdown");
    for (int i = 0; i < nmice; ++i)
    {
        select(i);
        mouse[i]->powerDown();
    }
    pinMode(_sdio, INPUT);
    pinMode(_sclk, INPUT);
    pinMode(_adr0, INPUT);
    if (_adr1 >= 0)
        pinMode(_adr1, INPUT);
}

void Odometer::readBurst(uint8_t i)
{
    select(i);
    mouse[i]->readBurst(false);
    // "NCS must be raised after each burst-mode transaction is complete to terminate burst-mode."
    // so let's select the next sensor ...
    select((i + 1) % nmice);
}

void Odometer::read2(uint8_t m1, uint8_t m2)
{
    readBurst(m1);
    readBurst(m2);

    float dx1 = 0, dx2 = 0, dy1 = 0, dy2 = 0;

    if (mouse[m1]->motion | mouse[m2]->motion)
    {
        float f1 = 0;
        if (mouse[m1]->motion)
        {
            f1 = mouse[m1]->squal == 0 ? 1 : mouse[m1]->squal;
            dx1 = mouse[m1]->transformDx();
            dy1 = mouse[m1]->transformDy();
        }
        float f2 = 0;
        if (mouse[m2]->motion)
        {
            f2 = mouse[m2]->squal == 0 ? 1 : mouse[m2]->squal;
            dx2 = mouse[m2]->transformDx();
            dy2 = mouse[m2]->transformDy();
        }

        // weighted sum of both sensors
        float ddx = (f1 / (f1 + f2) * dx1) + (f2 / (f1 + f2) * dx2);
        float ddy = (f1 / (f1 + f2) * dy1) + (f2 / (f1 + f2) * dy2);

        dx[m1] += dx1; dy[m1] += dy1;
        dx[m2] += dx2; dy[m2] += dy2;
        sdx += ddx;
        sdy += ddy;
        sq[m1] += mouse[m1]->squal;
        sq[m2] += mouse[m2]->squal;

        updates++;
    }
}

char *Odometer::printData(char *buf)
{
    if (mode == ODO_MODE_SHORT) 
    {
        // weighted sum over both sensors
        uint16_t avgSqual = (sq[0] + sq[1])/(2 * updates);
        sprintf(buf, "%3.0f %3.0f %d", sdx, sdy, avgSqual);
    }
    else if (mode == ODO_MODE_VERBOSE) 
    {
        // separate readings for left/right sensor
        sprintf(buf, "%3.0f %3.0f %d  %3.0f %3.0f %d", 
        dx[0], dy[0], sq[0]/updates, 
        dx[1], dy[1], sq[1]/updates);
    }
    // reset incrementing fields
    for (uint8_t i = 0; i<4; ++i) {
        dx[i] = dy[i] = 0;
        sq[i] = 0;
    }
    sdx = sdy = 0;
    updates = 0;
    return buf;
}
