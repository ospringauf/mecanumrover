// ----------------------------------------------------------------------------
// A&O MECANUMROVER
// ESP32 - sensor data acquisition
// https://bitbucket.org/ospringauf/mecanumrover
// ----------------------------------------------------------------------------

#include "Arduino.h"
#include "Rover.h"
#include <functional>

#define MYSERIAL_CALLBACK_SIGNATURE std::function<void(char *, uint8_t *, unsigned int)> callback
#define MYSERIAL_BUFSIZE 160

/**
 * supports passing NMEA-style messages over serial connection 
 * and acts as a drop-in replacement for an MQTT client
 * 
 */

// // USAGE:
// void callback(char *topic, byte *payload, unsigned int length)
// {
//     // ...
// }
// void setup()
// {
//     ser.connect(115200, SERIAL_8N1, 13, 15);
//     ser.setCallback(callback);
// }
// void loop()
// {
//     ser.loop();
//     delay(500);
// }

class MySerial
{

  public:
    MySerial() : port(Serial), uart0(true) {}

    MySerial(int portNr) : port(*(new HardwareSerial(portNr))), uart0(false) {}

    void connect(unsigned long baud, uint32_t config = SERIAL_8N1, int8_t rxPin = -1, int8_t txPin = -1)
    {
        if (!uart0)
        {
            port.begin(baud, config, rxPin, txPin);
            ESP_LOGI(TAG, "open UART on RX=%d TX=%d", rxPin, txPin);
        }
    }

    void disconnect()
    {
        if (!uart0) {
            port.end();
            connected = false;
        }
    }

    // char *readString1()
    // {
    //     if (port.available())
    //     {
    //         // size_t n = port.readBytesUntil('\n', buf, MYSERIAL_BUFSIZE);
    //         // buf[n] = 0;
    //         // Serial.println();
    //         int idx = 0;
    //         while (port.available())
    //         {
    //             buf[idx++] = port.read();
    //         }
    //         buf[idx] = 0;
    //         Serial.printf("in=[%s]\r\n", buf);
    //         parseAndCallback();

    //         return buf;
    //     }
    //     else
    //     {
    //         return NULL;
    //     }
    // }

    char *readLine()
    {
        if (port.available())
        {
            digitalWrite(LED_ONBOARD, HIGH);
            size_t n = port.readBytesUntil('\n', buf, MYSERIAL_BUFSIZE); // 1000ms timeout!
            if (n == 0)
                return NULL;
            buf[n] = 0; // terminate string in buffer
            if (buf[n - 1] == '\r')
                buf[n - 1] = 0;
            ESP_LOGD(TAG, "IN : %s", buf);

            parseAndCallback();

            return buf;
        }
        else
        {
            return NULL;
        }
    }

    void loop()
    {
        digitalWrite(LED_ONBOARD, LOW);
        // read any available input (commands)
        while (readLine())
            ;
    }

    void setCallback(MYSERIAL_CALLBACK_SIGNATURE)
    {
        this->callback = callback;
    }

    void publish(const char *topic, const char *payload)
    {
        digitalWrite(LED_ONBOARD, HIGH);
        // concat "topic,payload" and get checksum for that
        int l = 0;
        for (int i = 0; topic[i] != 0; ++i)
            sbuf[l++] = topic[i];
        sbuf[l++] = ',';
        for (int i = 0; payload[i] != 0; ++i)
            sbuf[l++] = payload[i];
        sbuf[l] = 0;
        uint8_t crc = crc8(sbuf, l); // calc CRC

        port.print("$");
        port.print(sbuf);
        port.print("*"); // append checksum
        port.print(crc, HEX);
        port.println();

        ESP_LOGD(TAG, "%s,%s", topic, payload);
        published++;
    }

    uint8_t crc8(char *pointer, uint16_t len)
    {
        uint8_t CRC = 0x00;
        uint16_t tmp;
        while (len > 0)
        {
            tmp = CRC << 1;
            tmp += *pointer;
            CRC = (tmp & 0xFF) + (tmp >> 8);
            pointer++;
            --len;
        }
        return CRC;
    }

    HardwareSerial &port;
    bool connected = false;
    uint16_t published = 0;

  private:
    char buf[MYSERIAL_BUFSIZE];  // receive buffer
    char sbuf[MYSERIAL_BUFSIZE]; // send buffer
    char topbuf[MYSERIAL_BUFSIZE];
    char paybuf[MYSERIAL_BUFSIZE];
    bool uart0;

    MYSERIAL_CALLBACK_SIGNATURE;

    // split NMEA-style messages: $topic,payload*cs
    void parseAndCallback()
    {
        // only handle $... messages
        if (buf[0] != '$')
            return;
        char *p = buf + 1; // 1st byte of topic, after $

        // run topic pointer to first ',' and copy topic into buffer
        int it = 0;
        while (*p != ',' && *p != 0)
            topbuf[it++] = *p++;
        topbuf[it] = 0; // it = topbuf length
        ++p;            // 1st byte of payload

        // run payload pointer to '*' and copy payload into buffer
        int ip = 0;
        while (*p != '*' && *p != 0)
            paybuf[ip++] = *p++;
        paybuf[ip] = 0; // ip = payload length
        // p now it at *

        // checksum validation
        int crcx = strtol(p + 1, NULL, 16);    // extract checksum from message: parse hex number after '*'
        int crcc = crc8(buf + 1, p - buf - 1); // calc CRC for text between $ and *
        if (crcx != crcc)
        {
            ESP_LOGE(TAG, "**** crc mismatch, got %X - expected %X ****n", crcx, crcc);
            return;
        }

        // invoke callback
        if (callback)
        {
            connected = true; // got a valid message
            ESP_LOGD(TAG, "topic=[%s] payload=[%s] crcx=%X crcc=%X", topbuf, paybuf, crcx, crcc);
            callback(topbuf, (byte *)paybuf, ip);
        }
    }
};
