// ----------------------------------------------------------------------------
// A&O MECANUMROVER
// ESP32 - sensor data acquisition
// https://bitbucket.org/ospringauf/mecanumrover
// ----------------------------------------------------------------------------

#include "Arduino.h"
#include "DistanceSensor.h"
#include "esp32-hal-adc.h"

// see http://esp-idf.readthedocs.io/en/latest/api-reference/peripherals/adc.html
class AdcInput
{
  public:
    // ESP32 has 12bit A/D converter, but is very non-linear
    // at 12bit/11db use 12500/v for Sharp 0A41SK distance
    // https://www.esp32.com/viewtopic.php?f=19&t=2881&start=10#p13739
    // better use voltage divider (100k/68k) and 11bit ADC with 2.5db amplifier
    AdcInput(uint8_t _pin, uint16_t _r1 = 0, uint16_t _r2 = 1) : pin(_pin), r1(_r1), r2(_r2),
                                                                 bits(11), atten(ADC_2_5db)
    {
        maxValue = (float)((1 << bits) - 1);
        ESP_LOGD(TAG, "max value=%5f", maxValue);
        switch (atten)
        {
        case ADC_0db:
            maxVoltage = 1.1;
            break;

        case ADC_2_5db:
            maxVoltage = 1.5;
            break;

        case ADC_6db:
            maxVoltage = 2.2;
            break;

        case ADC_11db:
            maxVoltage = 3.9;
            break;
        }
        analogReadResolution(bits);
        analogSetAttenuation(atten);
    }

    float readVoltage()
    {
        uint16_t x = analogRead(pin);
        ESP_LOGD(TAG, "value=%5d", x);
        float v = (x * maxVoltage) / maxValue;

        // voltage divider: v = output voltage; calc input voltage based on R1, R2 values
        float vi = v * (r1 + r2) / r2;
        return vi;
    }

  protected:
    uint8_t pin;
    uint16_t r1;
    uint16_t r2;
    uint8_t bits;
    adc_attenuation_t atten;
    float maxValue;
    float maxVoltage;
};

class Sharp0A41 : public DistanceSensor, public AdcInput
{

  public:
    uint8_t distance;
    uint8_t updated = 0;
    bool enabled = false;

    // average sample rate of 0A41SK is 16.5+/-4ms
    // at 12bit/11db use 12500/v for distance
    Sharp0A41(uint8_t _pin, uint16_t _r1 = 0, uint16_t _r2 = 1) : AdcInput(_pin, _r1, _r2), distance(DISTANCE_LIMIT)
    {
    }

    float readDistance()
    {
        uint16_t v0 = analogRead(pin);
        delayMicroseconds(500);
        uint16_t v1 = analogRead(pin);
        delayMicroseconds(500);
        uint16_t v2 = analogRead(pin);
        delayMicroseconds(500);
        uint16_t v3 = analogRead(pin);

        //uint16_t v = ((v0+v1+v2+v3)/4)-15;
        uint16_t v = v0 < v1 ? v0 : v1;
        v = v < v2 ? v : v2;
        v = v < v3 ? v : v3;
        v -= 15;

        uint8_t d = (uint8_t)((v == 0 || v < 80) ? DISTANCE_LIMIT : 6200 / v);
        d = myround(d, 30); // cutoff at 30cm
        updated = (d != distance);
        distance = d;
        return distance;
    }
};