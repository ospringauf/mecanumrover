// ----------------------------------------------------------------------------
// A&O MECANUMROVER
// ESP32 - sensor data acquisition
// https://bitbucket.org/ospringauf/mecanumrover
// ----------------------------------------------------------------------------

#include "ADNS5020.h"
#include "Rover.h"

/**
 * This is the driver for the odometer board,
 * which controls multiple (currently up to 4) ADNS-5020-EN optical mouse sensors.
 * Sensors are addressed individually by sending the binary address (0..3) to
 * a 74HC138 decoder, which then sets the NCS (active low) for one sensor, 
 * and disconnects (NCS high) all others from the common SCLK/SDIO bus.
 * 
 * Since the ADNS-5020s are 5V devices, a logic level converter is needed 
 * to translate bus and enable signals between the ESP32 and the decoder/sensors.
 * 
 * Note that chip select (NCS) is controlled entirely by this driver, not be the individual 
 * sensor drivers.
 */


#define ODO_MODE_SHORT 0
#define ODO_MODE_VERBOSE 1

class Odometer
{
  public:
    ADNS5020 *mouse[4];

    // weighted sum dx/dy over several measurement intervals
    float sdx;
    float sdy;
    // inrementing readings for each sensor
    float dx[4];
    float dy[4];
    uint16_t sq[4];
    uint16_t updates; // number of updates since last readout/transmission

    bool enabled;
    uint8_t nmice;
    uint8_t selected = 0;
    uint8_t mode = ODO_MODE_SHORT;

    Odometer(uint8_t n, uint8_t sclk, uint8_t sdio, int8_t adr0, int8_t adr1); 

    void init(uint8_t mask);
    uint8_t select(uint8_t m);
    void reset(uint8_t i);
    void identify(uint8_t i);
    void powerUp(uint8_t mask);
    void powerDown(uint8_t mask);
    void shutdown();
    void readBurst(uint8_t i);    
    void read2(uint8_t m1, uint8_t m2);
    char* printData(char *buf);

  private:
    int8_t _adr0;
    int8_t _adr1;
    uint8_t _sclk;
    uint8_t _sdio;
};