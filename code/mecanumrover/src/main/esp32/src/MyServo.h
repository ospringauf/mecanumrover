
// ----------------------------------------------------------------------------
// A&O MECANUMROVER
// ESP32 - sensor data acquisition
// https://bitbucket.org/ospringauf/mecanumrover
// ----------------------------------------------------------------------------

#ifndef MYSERVO_H
#define MYSERVO_H

#include "Arduino.h"

// driver for SG90 / MG90S servos
// see https://www.youtube.com/watch?v=Nc6RTx0oK0I&feature=youtu.be
// see https://arduino-projekte.info/servo-ansteuern-arduino-esp8266-esp32/

class MyServo
{

  public:
    int16_t minAngle = 0;
    int16_t maxAngle = 180;
    int16_t angle = 0;

    MyServo(uint8_t pin, uint8_t channel, int16_t mina = 0, int16_t maxa = 180) : 
        minAngle(mina), maxAngle(maxa), _pin(pin), _channel(channel)
    {
    }

    void init()
    {
        ledcSetup(_channel, _freq, _bits);
    }

    void pointTo(int degrees, int wait = 300)
    {
        angle = degrees;
        // values for 10bit, 50Hz:
        // int min = 26; // (26/1024)*20ms ~ 0.5 ms (-90°)
        // int max = 123; // (123/1024)*20ms ~ 2.4 ms (+90°)

        // SG90 servo: 500us .. 2400us pulse width
        // MG90 servo: 400us .. 2400us pulse width
        // speed: something like 150ms/60° @3.3V
        uint16_t minDuty = 0.0005 * _freq * (1 << _bits); // 85
        uint16_t maxDuty = 0.0024 * _freq * (1 << _bits); // 408
        int duty = map(degrees, minAngle, maxAngle, minDuty, maxDuty);
        // double pulse = duty / (_freq * (1 << _bits));

        // Serial.print("servo min=");Serial.print(minDuty);Serial.print(" max=");
        // Serial.print(maxDuty); Serial.print(" duty="); Serial.print(duty);
        // Serial.print(" pulse="); Serial.println(10000.0*pulse);

        ledcAttachPin(_pin, _channel);
        ledcWrite(_channel, duty);
        delay(wait);
        ledcDetachPin(_pin);
    }

  private:
    uint8_t _pin;
    uint8_t _channel;

    // best: 166/16
    double _freq = 166; // initial: 166
    uint8_t _bits = 16; // initial: 10
};

#endif
