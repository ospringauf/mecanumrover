// ----------------------------------------------------------------------------
// A&O MECANUMROVER
// ESP32 - sensor data acquisition
// https://bitbucket.org/ospringauf/mecanumrover
// ----------------------------------------------------------------------------

#ifndef COMPASS_H
#define COMPASS_H

#include "Rover.h"
#include "BNO055ESP32.h"

// stabilized compass
// using BNO055 driver from https://github.com/ShellAddicted/BNO055ESP32

class Compass
{

  public:
    BNO055 bno;
    bool enabled = false;
    bool updated = false;
    bool initialized = false;

    bno055_calibration_t calibration;
    bno055_vector_t vector;
    double heading = 0.0;

    Compass(uart_port_t port, gpio_num_t tx, gpio_num_t rx) : bno(port, /* ESP-TX*/ tx, /* ESP-RX*/ rx){};

    void setCompassMode();
    void init();
    char *printData(char *buf);
    void dumpOffsets();
    char* dumpOffsetsBuf(char *buf);
    void readCalibration();
    void read();
    bool uncalibrated();

  private:
};

#endif