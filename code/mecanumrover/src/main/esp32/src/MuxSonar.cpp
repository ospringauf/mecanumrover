// ----------------------------------------------------------------------------
// A&O MECANUMROVER
// ESP32 - sensor data acquisition
// https://bitbucket.org/ospringauf/mecanumrover
// ----------------------------------------------------------------------------

#include "MuxSonar.h"
#include "Arduino.h"


void MuxSonar::select(uint8_t channel, uint8_t sig_dir)
{
    pinMode(_sig, sig_dir);
    // address lines A0..A3
    digitalWrite(_a0, channel & 0b0001);
    if (_a1 >= 0)
        digitalWrite(_a1, (channel>>1) & 0b1);
    if (_a2 >= 0)
        digitalWrite(_a2, (channel>>2) & 0b1);
    if (_a3 >= 0)
        digitalWrite(_a3, (channel>>3) & 0b1);
    delayMicroseconds(1); //***   
}

void MuxSonar::reset() {
    for (int i = 0; i < _channels; ++i)
    {
        distance[i] = DISTANCE_LIMIT;
        select(2*i, OUTPUT);
        digitalWrite(_sig, LOW);
        delay(1);

        // // http://therandomlab.blogspot.de/2015/05/repair-and-solve-faulty-hc-sr04.html?m=1
        // // send a LOW pulse on the ECHO pin
        // select(2*i+1, OUTPUT);
        // digitalWrite(_sig, LOW);
        // delayMicroseconds(200);
        // select(2*i+1, INPUT);
    }
}

long MuxSonar::read(uint8_t chan)
{
    // switch to TRIG channel and send a 10us HI on TRIG
    select(2 * chan, OUTPUT);
    digitalWrite(_sig, LOW);
    delayMicroseconds(5); //3 10
    digitalWrite(_sig, HIGH);
    delayMicroseconds(15); //10 15
    digitalWrite(_sig, LOW);
    delayMicroseconds(5); // give the sensor 5us time to recognize the "LOW"

    // switch to ECHO channel and wait until ECHO goes HI
    // select(2 * chan + 1, INPUT_PULLDOWN);
    select(2 * chan + 1, INPUT);
    long duration = pulseIn(_sig, HIGH, (DISTANCE_LIMIT * 58)); // limit to 1.5m, then timeout and return 0
    // ESP_LOGI(TAG, "sonar dist = %d;", duration);

    // // http://therandomlab.blogspot.de/2015/05/repair-and-solve-faulty-hc-sr04.html?m=1
    // // stuck?
    // if (duration == 0) {
    //     select(2 * chan + 1, OUTPUT);
    //     //delayMicroseconds(2); //???
    //     digitalWrite(_sig, LOW);
    //     delayMicroseconds(200);
    //     pinMode(2*chan + 1, INPUT);
    // }

    // // pull ECHO LOW
    // pinMode(_sig, OUTPUT);
    // digitalWrite(_sig, LOW);
    // delayMicroseconds(20);
    // pinMode(_sig, INPUT);

    long d = (duration == 0) ? DISTANCE_LIMIT : duration / 58; // timeout -> max distance
    d = myround(d); 

    uint8_t chanmask = (1 << chan);
    bool suppress = (d == 0) || (d == DISTANCE_LIMIT) || (abs(d-distance[chan]) > 50);
    if (suppress && !(_invalid & chanmask))
    {
        // suppress single errors
        _invalid |= chanmask; // set invalid bit
    }
    else
    {
        // valid data
        if (d != distance[chan])
        {
            distance[chan] = d;
            updated |= chanmask;
        }
        _invalid &= ~chanmask; // clear invalid bit
    }

    return d;
}

// TODO beste Reihenfolge (Echos vermeiden)
// TODO Wartezeit zwischen Signalen?
void MuxSonar::readAll(uint32_t delayMs)
{
    for (int s = 0; s < _channels; ++s)
    {
        uint8_t i = sensorOrder[s];
        if (mask & (1 << i)) {
            read(i);
            if (i < _channels - 1)
                delay(delayMs);
        }
    }
}

char* MuxSonar::printData(char *buf) 
{
    sprintf(buf, "%3d %3d %3d %3d %3d %3d %3d %3d",
        distance[0],
        distance[1],
        distance[2],
        distance[3],
        distance[4],
        distance[5],
        distance[6],
        distance[7]);
    return buf;
}