#include <Arduino.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "BNO055ESP32.h"

// see https://github.com/ShellAddicted/BNO055ESP32

//BNO055 bno(UART_NUM_1, GPIO_NUM_17, GPIO_NUM_16/*, GPIO_NUM_4*/);
BNO055 bno(UART_NUM_1, GPIO_NUM_2, GPIO_NUM_15/*, GPIO_NUM_4*/);

void setup()
{

    // This values are random, see exampleCalibration.cpp for more details.
	bno055_offsets_t storedOffsets;
	storedOffsets.accelOffsetX = 29;
	storedOffsets.accelOffsetY = 24;
	storedOffsets.accelOffsetZ = 16;
	storedOffsets.magOffsetX = 139;
	storedOffsets.magOffsetY = -71;
	storedOffsets.magOffsetZ = -423;
	storedOffsets.gyroOffsetX = 0;
	storedOffsets.gyroOffsetY = -1;
	storedOffsets.gyroOffsetZ = 0;
	storedOffsets.accelRadius = 1000;
    storedOffsets.magRadius = 747;

    delay(2000);
    try
    {
        bno.begin(); //BNO055 is in CONFIG_MODE until it is changed
        delay(1000);
        // bno.enableExternalCrystal();
        bno.setPwrMode(BNO055_PWR_MODE_NORMAL);
        bno.disableExternalCrystal();
        bno.setSensorOffsets(storedOffsets);
        // bno.setAxisRemap(BNO055_REMAP_CONFIG_P1, BNO055_REMAP_SIGN_P1); // see datasheet, section 3.4

        
        ESP_LOGI(TAG, "Setup Done.");
    }
    catch (BNO055BaseException &ex)
    {
        ESP_LOGE(TAG, "Setup Failed, Error: %s", ex.what());
        return;
    }
    catch (std::exception &ex)
    {
        ESP_LOGE(TAG, "Setup Failed, Error: %s", ex.what());
        return;
    }

    int8_t temperature = bno.getTemp();
    ESP_LOGI(TAG, "TEMP: %d°C", temperature);

    byte swlsb, swmsb;
    bno.read8(BNO055_REG_SW_REV_ID_LSB, &swlsb);
    bno.read8(BNO055_REG_SW_REV_ID_MSB, &swmsb);
    ESP_LOGI(TAG, "SW rev %u.%u", swmsb, swlsb);

    byte stest;
    bno.read8(BNO055_REG_ST_RESULT, &stest);
    byte stmcu = (stest>>3)&1;
    byte stgyr = (stest>>2)&1;
    byte stmag = (stest>>2)&1;
    byte stacc = (stest>>0)&1;
    ESP_LOGI(TAG, "self test MCU:%u GYR:%u MAG:%u ACC:%u", stmcu, stgyr, stmag, stacc);

    // for (uint8_t i=0; i<3; ++i) {
    //     bno.write8(BNO055_REG_SYS_TRIGGER, 0x1);
    //     delay(100);
    //     bno.read8(BNO055_REG_ST_RESULT, &stest);
    //     byte stmcu = (stest>>3)&1;
    //     byte stgyr = (stest>>2)&1;
    //     byte stmag = (stest>>2)&1;
    //     byte stacc = (stest>>0)&1;
    //     ESP_LOGI(TAG, "self test MCU:%u GYR:%u MAG:%u ACC:%u", stmcu, stgyr, stmag, stacc);
    // }

        bno.setOprModeNdof();
        // bno.setOprModeNdofFmcOff();
        // bno.setOprModeMagOnly();
        // bno.setOprModeGyroOnly();

}

int n = 0;


void loop()
{
    try
    {
        //Calibration 3 = fully calibrated, 0 = uncalibrated
        bno055_calibration_t cal = bno.getCalibration();
        bno055_vector_t v = bno.getVectorEuler();
        // bno055_vector_t v = bno.getVectorGyroscope();

        byte bopm, berr, bsta;
        bno.read8(BNO055_REG_OPR_MODE, &bopm);
        bno.read8(BNO055_REG_SYS_ERR, &berr);
        bno.read8(BNO055_REG_SYS_STATUS, &bsta);
        int8_t temp = bno.getTemp();
        // ESP_LOGI(TAG, "BNO055 OP mode: %u  ERR: %u  STATUS: %u", bopm, berr, bsta); 

        ESP_LOGI(TAG, "X=%.2f Y=%.2f Z=%.2f | Calib SYS:%u GYR:%u ACC:%u MAG:%u | M:%u ERR:%u STA:%u T:%u", 
        v.x, v.y, v.z, cal.sys, cal.gyro, cal.accel, cal.mag, bopm, berr, bsta, temp);
    
        ++n;
        if (n%20 == 0 && cal.sys == 3) {
            bno.setOprModeConfig();
            bno055_offsets_t offs = bno.getSensorOffsets();
            ESP_LOGI(TAG, "offs.accelOffsetX = %d;", offs.accelOffsetX);
            ESP_LOGI(TAG, "offs.accelOffsetY = %d;", offs.accelOffsetY);
            ESP_LOGI(TAG, "offs.accelOffsetZ = %d;", offs.accelOffsetZ);
	        ESP_LOGI(TAG, "offs.magOffsetX = %d;", offs.magOffsetX);
            ESP_LOGI(TAG, "offs.magOffsetY = %d;", offs.magOffsetY);
            ESP_LOGI(TAG, "offs.magOffsetZ = %d;", offs.magOffsetZ);
	        ESP_LOGI(TAG, "offs.gyroOffsetX = %d;", offs.gyroOffsetX);
            ESP_LOGI(TAG, "offs.gyroOffsetY = %d;", offs.gyroOffsetY);
            ESP_LOGI(TAG, "offs.gyroOffsetZ = %d;", offs.gyroOffsetZ);
	        ESP_LOGI(TAG, "offs.accelRadius = %d;", offs.accelRadius);
            ESP_LOGI(TAG, "offs.magRadius = %d;", offs.magRadius);
            bno.setOprModeNdof();
            // bno.setOprModeNdofFmcOff();
        }

    }
    catch (BNO055BaseException &ex)
    {
        ESP_LOGE(TAG, "Error: %s", ex.what());
        return;
    }
    catch (std::exception &ex)
    {
        ESP_LOGE(TAG, "Error: %s", ex.what());
    }
    //vTaskDelay(100 / portTICK_PERIOD_MS); // in fusion mode output rate is 100hz
    delay(500);
}