// ----------------------------------------------------------------------------
// A&O MECANUMROVER
// ESP32 - sensor data acquisition
// https://bitbucket.org/ospringauf/mecanumrover
// ----------------------------------------------------------------------------

#ifndef MUXSONAR_H__
#define MUXSONAR_H__

#include "Arduino.h"
#include "DistanceSensor.h"


/**
 * multiple HC-SR04P ultrasonic sensors (max. 8)
 * connected to a CD74HC4067 16-channel multiplexer
 * (TRIG1, ECHO1, TRIG2, ECHO2, ...)
 *
 */
class MuxSonar : DistanceSensor {

    public:
        MuxSonar(uint8_t channels, int8_t sig, int8_t a0, int8_t a1 = -1, int8_t a2 = -1, int8_t a3 = -1) : 
            _channels(channels), _sig(sig), _a0(a0), _a1(a1), _a2(a2), _a3(a3) {};
        
        void init()
        {
            pinMode(_sig, OUTPUT);
            if (_a0 >= 0) pinMode(_a0, OUTPUT);
            if (_a1 >= 0) pinMode(_a1, OUTPUT);  
            if (_a2 >= 0) pinMode(_a2, OUTPUT);  
            if (_a3 >= 0) pinMode(_a3, OUTPUT);  
        }

        long read(uint8_t chan);
        void readAll(uint32_t delayMs = 30);
        char* printData(char *buf); 
        void reset();

        uint8_t distance[8] = {DISTANCE_LIMIT};
        uint8_t updated = 0;
        bool enabled = false;
        uint8_t mask = 0b11111111;

    private:
        uint8_t _channels;
        uint8_t _sig;
        int8_t _a0;
        int8_t _a1;
        int8_t _a2;
        int8_t _a3;
        uint8_t _invalid = 0; // one-time suppress bit for each sensor
        //uint8_t sensorOrder[8] = {0, 2, 4, 6, 1, 3, 5, 7};
        //uint8_t sensorOrder[8] = {0, 4, 2, 6, 1, 5, 3, 7};
        uint8_t sensorOrder[8] = {0, 4, 1, 6, 7, 2, 5, 3};

        void select(uint8_t channel, uint8_t sig_dir);

};

#endif