// ----------------------------------------------------------------------------
// A&O MECANUMROVER
// ESP32 - sensor data acquisition
// https://bitbucket.org/ospringauf/mecanumrover
// ----------------------------------------------------------------------------

#include "esp_log.h"
#include "Compass.h"
#include "BNO055ESP32.h"
#include "Arduino.h"

/*
    Compass (Bosch BNO055 in NDOF mode)

    using BNO055 driver from 
    https://github.com/ShellAddicted/BNO055ESP32
*/

void Compass::setCompassMode()
{
    bno.setOprModeNdof();
}

void Compass::init()
{
    // previous best calibration values
    bno055_offsets_t off;
    // storedOffsets.accelOffsetX = 14;
    // storedOffsets.accelOffsetY = 1;
    // storedOffsets.accelOffsetZ = 8;
    // storedOffsets.magOffsetX = 97;
    // storedOffsets.magOffsetY = -226;
    // storedOffsets.magOffsetZ = -240;
    // storedOffsets.gyroOffsetX = 2;
    // storedOffsets.gyroOffsetY = -1;
    // storedOffsets.gyroOffsetZ = -1;
    // storedOffsets.accelRadius = 1000;
    // storedOffsets.magRadius = 763;
    off.accelOffsetX = 14;
    off.accelOffsetY = 1;
    off.accelOffsetZ = 8;
    off.magOffsetX = 72;
    off.magOffsetY = -283;
    off.magOffsetZ = -206;
    off.gyroOffsetX = 0;
    off.gyroOffsetY = -1;
    off.gyroOffsetZ = 0;
    off.accelRadius = 1000;
    off.magRadius = 737;

    try
    {
        bno.begin(); //BNO055 is in CONFIG_MODE until it is changed
        // bno.setPwrMode(BNO055_PWR_MODE_NORMAL);
        bno.disableExternalCrystal();
        bno.setSensorOffsets(off);
        // bno.setAxisRemap(BNO055_REMAP_CONFIG_P1, BNO055_REMAP_SIGN_P1); // see datasheet, section 3.4

        initialized = true;
        ESP_LOGI(TAG, "Setup Done.");
    }
    catch (BNO055BaseException &ex)
    {
        ESP_LOGE(TAG, "Setup Failed, Error: %s", ex.what());
        return;
    }
    catch (std::exception &ex)
    {
        ESP_LOGE(TAG, "Setup Failed, Error: %s", ex.what());
        return;
    }

    // byte swlsb, swmsb;
    // bno.read8(BNO055_REG_SW_REV_ID_LSB, &swlsb);
    // bno.read8(BNO055_REG_SW_REV_ID_MSB, &swmsb);
    // ESP_LOGI(TAG, "BNO055 rev %u.%u", swmsb, swlsb);
    int16_t sw = bno.getSWRevision();
    uint8_t bl_rev = bno.getBootloaderRevision();
    ESP_LOGI(TAG, "SW rev: %d, bootloader rev: %u", sw, bl_rev);

    // byte stest;
    // bno.read8(BNO055_REG_ST_RESULT, &stest);
    // byte stmcu = (stest >> 3) & 1;
    // byte stgyr = (stest >> 2) & 1;
    // byte stmag = (stest >> 2) & 1;
    // byte stacc = (stest >> 0) & 1;
    // ESP_LOGI(TAG, "self test MCU:%u GYR:%u MAG:%u ACC:%u", stmcu, stgyr, stmag, stacc);
    bno055_self_test_result_t res = bno.getSelfTestResult();
    ESP_LOGI(TAG, "Self-Test Results: MCU: %u, GYR:%u, MAG:%u, ACC: %u", res.mcuState, res.gyrState, res.magState, res.accState);

    setCompassMode();
};

char *Compass::printData(char *buf)
{
    sprintf(buf, "%3.1f %1d%1d%1d%1d", heading, calibration.sys, calibration.mag, calibration.accel, calibration.gyro);
    return buf;
}

bool Compass::uncalibrated()
{
    return calibration.sys == 0 &&
           calibration.mag == 0 &&
           calibration.accel == 0 &&
           calibration.gyro == 0;
}

void Compass::dumpOffsets()
{
    try
    {
        bno.setOprModeConfig();
        bno055_offsets_t offs = bno.getSensorOffsets();
        ESP_LOGI(TAG, "storedOffsets.accelOffsetX = %d;", offs.accelOffsetX);
        ESP_LOGI(TAG, "storedOffsets.accelOffsetY = %d;", offs.accelOffsetY);
        ESP_LOGI(TAG, "storedOffsets.accelOffsetZ = %d;", offs.accelOffsetZ);
        ESP_LOGI(TAG, "storedOffsets.magOffsetX = %d;", offs.magOffsetX);
        ESP_LOGI(TAG, "storedOffsets.magOffsetY = %d;", offs.magOffsetY);
        ESP_LOGI(TAG, "storedOffsets.magOffsetZ = %d;", offs.magOffsetZ);
        ESP_LOGI(TAG, "storedOffsets.gyroOffsetX = %d;", offs.gyroOffsetX);
        ESP_LOGI(TAG, "storedOffsets.gyroOffsetY = %d;", offs.gyroOffsetY);
        ESP_LOGI(TAG, "storedOffsets.gyroOffsetZ = %d;", offs.gyroOffsetZ);
        ESP_LOGI(TAG, "storedOffsets.accelRadius = %d;", offs.accelRadius);
        ESP_LOGI(TAG, "storedOffsets.magRadius = %d;", offs.magRadius);
        setCompassMode();
    }
    catch (BNO055BaseException &ex)
    {
        ESP_LOGE(TAG, "Error: %s", ex.what());
        return;
    }
    catch (std::exception &ex)
    {
        ESP_LOGE(TAG, "Error: %s", ex.what());
    }
}

char *Compass::dumpOffsetsBuf(char *buf)
{
    try
    {
        bno.setOprModeConfig();
        bno055_offsets_t offs = bno.getSensorOffsets();
        sprintf(buf, "off.accelOffsetX = %d; off.accelOffsetY = %d; off.accelOffsetZ = %d; off.magOffsetX = %d; off.magOffsetY = %d; off.magOffsetZ = %d; off.gyroOffsetX = %d; off.gyroOffsetY = %d; off.gyroOffsetZ = %d; off.accelRadius = %d; off.magRadius = %d;",
                offs.accelOffsetX, offs.accelOffsetY, offs.accelOffsetZ,
                offs.magOffsetX, offs.magOffsetY, offs.magOffsetZ,
                offs.gyroOffsetX, offs.gyroOffsetY, offs.gyroOffsetZ,
                offs.accelRadius, offs.magRadius);
        setCompassMode();
    }
    catch (BNO055BaseException &ex)
    {
        ESP_LOGE(TAG, "Error: %s", ex.what());
    }
    catch (std::exception &ex)
    {
        ESP_LOGE(TAG, "Error: %s", ex.what());
    }
    return buf;
}

void Compass::readCalibration()
{
    //Calibration 3 = fully calibrated, 0 = uncalibrated
    calibration = bno.getCalibration();
}

void Compass::read()
{
    try
    {
        readCalibration();
        vector = bno.getVectorEuler();
        double d = abs(heading - vector.x);
        updated = (d >= 0.2);
        if (updated)
        {
            heading = vector.x;
        }

        // byte bopm, berr, bsta;
        // bno.read8(BNO055_REG_OPR_MODE, &bopm);
        // bno.read8(BNO055_REG_SYS_ERR, &berr);
        // bno.read8(BNO055_REG_SYS_STATUS, &bsta);
        // int8_t temp = bno.getTemp();

        // ESP_LOGI(TAG, "X=%.2f Y=%.2f Z=%.2f | Calib SYS:%u GYR:%u ACC:%u MAG:%u | M:%u ERR:%u STA:%u T:%u",
        //          vector.x, vector.y, vector.z, calibration.sys, calibration.gyro, calibration.accel, calibration.mag, bopm, berr, bsta, temp);

        // ++n;
        // if (n % 20 == 0 && calibration.sys == 3)
        // {
        //     dumpOffsets();
        // }
    }
    catch (BNO055BaseException &ex)
    {
        ESP_LOGE(TAG, "Error: %s", ex.what());
        return;
    }
    catch (std::exception &ex)
    {
        ESP_LOGE(TAG, "Error: %s", ex.what());
    }
}