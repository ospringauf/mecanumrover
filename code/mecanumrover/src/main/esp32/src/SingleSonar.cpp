// ----------------------------------------------------------------------------
// A&O MECANUMROVER
// ESP32 - sensor data acquisition
// https://bitbucket.org/ospringauf/mecanumrover
// ----------------------------------------------------------------------------


#include "Arduino.h"

// test code for a single HC-SR04P sensor

#define SONAR1_TRIG 16
#define SONAR1_ECHO 17



void sonar_setup() {
  pinMode(SONAR1_TRIG, OUTPUT);
  pinMode(SONAR1_ECHO, INPUT);
  // pinMode(led, OUTPUT);
  // pinMode(led2, OUTPUT);
}

void sonar_read() {  
  long duration, distance;

  // send a 10us HI on TRIG
  digitalWrite(SONAR1_TRIG, LOW);
  delayMicroseconds(3);       
  digitalWrite(SONAR1_TRIG, HIGH);
  delayMicroseconds(10); 
  digitalWrite(SONAR1_TRIG, LOW);

  // wait until ECHO goes HI
  duration = pulseIn(SONAR1_ECHO, HIGH, 9000); // limit to 1.5m, then timeout and return 0
  distance = duration / 58; 

  Serial.print("sonar: "); Serial.print(distance); Serial.println(" cm");
}
