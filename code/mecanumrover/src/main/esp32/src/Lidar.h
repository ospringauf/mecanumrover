// ----------------------------------------------------------------------------
// A&O MECANUMROVER
// ESP32 - sensor data acquisition
// https://bitbucket.org/ospringauf/mecanumrover
// ----------------------------------------------------------------------------

#ifndef LIDAR_H
#define LIDAR_H

#include <VL53L0X.h>
#include "Rover.h"
#include "DistanceSensor.h"
#include "MyServo.h"

// see https://github.com/ScruffR/VL53L0X
// see https://github.com/pololu/vl53l0x-arduino

class Lidar : DistanceSensor
{

  public:
    VL53L0X lrf1;
    VL53L0X lrf2;
    MyServo servoh;
    MyServo servov;

    uint8_t distance[2] = {DISTANCE_LIMIT};
    bool enabled = false;
    bool updated = false;

    Lidar(uint8_t xshut1) : servoh(SERVO_H, 1, 85, -95),
                            servov(SERVO_V, 2, 10, 190), 
                            _xshut1(xshut1)
                            {};

    void init()
    {
        servoh.init();
        servov.init();
        pinMode(_xshut1, OUTPUT);
        digitalWrite(_xshut1, HIGH);
    };

    void configure(uint8_t adr1, uint8_t adr2);
    void startRanging();
    void stopRanging();

    void readAndPrint();
    char *printData(char *buf);
    void read();

  private:
    uint8_t _xshut1;

    uint8_t checkWireStatus(uint8_t status);
};

#endif