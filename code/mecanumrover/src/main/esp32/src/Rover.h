// ----------------------------------------------------------------------------
// A&O MECANUMROVER
// ESP32 - sensor data acquisition
// https://bitbucket.org/ospringauf/mecanumrover
// ----------------------------------------------------------------------------

#ifndef ROVER_H
#define ROVER_H

static const char* TAG = "rover";


// ESP32 Pins
#define GPIO_0 0       // BOOT pin - must be HIGH/Z at boot, pull down button for programming mode
#define X_UART0_TX 1   // reserved for now
#define UART1_RX GPIO_NUM_2    // BNO055 serial transmit, DOIT-Board: onboard LED
#define X_UART0_RX 3   // reserved for now
#define SERVO_H 4      // horizontal lidar servo
#define MUX_S1 5       // sonar multiplexer address line 0
#define X_FLASHD1 6    // reserved
#define X_FLASHD0 7    // reserved
#define X_FLASHDSCK 8  // reserved
#define X_FLASHD2 9    // reserved
#define X_FLASHD3 10   // reserved
#define X_FLASHCMD 11  // reserved
#define LED_ONBOARD 12 // LED; MTDI strapping pin (must be LOW at boot, internal pulldown)
#define LRF2_XSHUT 13  // laser sensor 2 shutdown
#define MOUSE_SDIO 14  // mouse sensor common I/O line
#define UART1_TX GPIO_NUM_15     // BNO055 serial receive; also MTDO strapping pin
#define SERVO_V 16     // vertical lidar servo
#define MUX_S0 17      // sonar multiplexer address line 3
#define MUX_S2 18      // sonar multiplexer address line 1
#define MUX_S3 19      // sonar multiplexer address line 2
// GPIO20 N/A
#define X_SDA 21   // I2C data
#define X_SCL 22   // I2C clock
#define MUX_SIG 23 // sonar multiplexer data line
// GPIO24 N/A
#define FREE_25 25    //
#define MOUSE_SCLK 26 // mouse sensor common clock line
#define MOUSE_ADR0 27 // mouse sensor chip select ADR0
// GPIO28-31 N/A
#define FREE_32 32  //
#define FREE_33 33  //
#define FREE_34 34  // input only!
#define FREE_35 35  // input only!
#define IR_INPUT 36 // infrared sensor analog input (input only pin)
// GPIO37-38 N/A
#define ANALOG_IN1 39 // MAX471 current sensor; input only!

// I2C addresses
#define LRF1_ADR 0x29 // VL53L0X laser sensor 1, default adr
#define LRF2_ADR 0x31 // VL53L0X laser sensor 2, alternative adr
#define OLED_ADR 0x3C // SSD1306 128x32 OLED display

#define MOUSE_CPI 500

#define RPI_SERIAL_PORT 1 // UART1
#define RPI_SERIAL_BAUD 115200

void lockI2c();
void releaseI2c();

// https://github.com/espressif/arduino-esp32/issues/565
// NONE=0, ERROR=1, WARN=2, INFO=3, DEBUG=4, VERBOSE=5
// note: this setting has no effect, must change CORE_DEBUG_LEVEL in platformio.ini
#ifdef CONFIG_LOG_DEFAULT_LEVEL
#undef CONFIG_LOG_DEFAULT_LEVEL
#endif
#define CONFIG_LOG_DEFAULT_LEVEL 5

#endif