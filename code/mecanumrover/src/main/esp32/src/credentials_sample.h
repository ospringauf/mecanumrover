#define HOME_SSID "my-wifi-ssid"
#define HOME_PASSWORD "my-wifi-password"

#define ROVER_SSID "another-ssid"
#define ROVER_PASSWORD "another-password"

#define MQTT_SERVER "192.168.1.1"
#define MQTT_PORT 1883
#define MQTT_USER "esp32"
#define MQTT_PASSWORD ""
