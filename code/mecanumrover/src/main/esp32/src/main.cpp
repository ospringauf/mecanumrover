// ----------------------------------------------------------------------------
// A&O MECANUMROVER
// ESP32 - sensor data acquisition
// https://bitbucket.org/ospringauf/mecanumrover
// ----------------------------------------------------------------------------

#include "Rover.h"

#include <WiFi.h>
#include <PubSubClient.h>
#include <Wire.h>
#include <VL53L0X.h>
#include "esp_log.h"
#include "driver/gpio.h"

#include "Lidar.h"
#include "ADNS5020.h"
#include "MuxSonar.h"
#include "AdcInput.cpp"
#include "Odometer.h"
#include <U8g2lib.h>
#include "MyServo.h"
#include "Compass.h"

#include "credentials.h"

#include "MySerial.cpp"

// ----------------------------------------------------------------------------
// TODO
// - LRF init funktioniert nicht zuverlässig, wenn für die VL53L0X kein power cycle erfolgt ist
// - Bus-Fehler bei Verwendung des Displays? vorläufig abgeschaltet (undef OLED_DISPLAY)
//    https://github.com/olikraus/u8g2/issues/646
//    Konflikt beim I2C-Zugriff zwischen Display und LRF? Workaround: Zuweisung auf Core1
// - Stromaufnahme der einzelnen Komponenten ermitteln - Abschaltung? (MOSFET?)
// - Sonar-Messung dauert 200ms für 8 Sensoren. Reihenfolge ändern?
// - Ultraschall-Sensoren funktionieren nicht zuverlässig, wenn Spannung < 3.4V
//
// DONE
// - Daten nur dann senden, wenn Änderungen vorliegen
// - MQTT bricht ab -> MQTT-Client nicht threadsafe? -> eigener Task
// - Verbindungsabbrüche
// - gemessene Entfernungen "logaritmisch" runden (zB 0-20:1cm, 20-50:2cm, 50-150:5cm) -> weniger Updates
// - Sonar instabil zB bei Wechsel 2/3 (sonar / sonar+odo) --> Spannung auf 3.3V zu niedrig
// - IR-Sensor stört Sonar? -> aktuell abgeschaltet -- Ursache: Stromversorgung (s.o.)
// - LRF1_XSHUT ist immer HIGH -> einsparen
// - Servo-Ansteuerung (PWM)
// - I2C Fehler bei VL53L0X ([E][esp32-hal-i2c.c:161] i2cWrite(): Busy Timeout! Addr: 29)
//    wire status = 5
//    https://www.esp32.com/viewtopic.php?f=13&t=3852
//    vorläufiger Workaround: Prio=2 und Lidar::checkWireStatus
//    tritt nicht mehr auf (Serial statt WIFI Verbindung)
// - Pins einsparen
//    LRFx_XSHUT könnten über GPIO-Expander (MCP23017, PFC8574)  oder 
//    Decoder (74HC138/74HC139) gesteuert werden, nicht zeitkritisch
//    -> nur 1 XSHUT-Pin notwendig
// - analog input 36 liefert "0" bei 0 .. 0.15V
//    see https://www.esp32.com/viewtopic.php?f=19&t=2881&start=10#p13739
//    added 0.12V offset (160k pullup)
// - Wire.reset gelöscht? -> kann entfallen oder durch Wire.begin ersetzt werden
//    https://github.com/espressif/arduino-esp32/commits/master/libraries/Wire/src/Wire.cpp
//    https://github.com/espressif/arduino-esp32/pull/1539/commits/13dcfe5c13bc882f8923d137ed604aa65b6db057
// ----------------------------------------------------------------------------



// others
#define PING_INTERVAL (5000)
#define OLED_DISPLAY

#ifdef __cplusplus
extern "C" {
#endif

uint8_t temprature_sens_read(); // ESP32 internal temperature sensor
//uint8_t g_phyFuns;

#ifdef __cplusplus
}
#endif

// ----------------------------------------------------------------------------

SemaphoreHandle_t xI2cSemaphore;
SemaphoreHandle_t xDisplaySemaphore;

MySerial mqtt; // use UART0 for MQTT-style communication with RPi; make sure debugging output is off! (platformio.ini)

// 2x VL53L0X laser range finder (LRF) sensors on I2C bus
Lidar lidar(LRF2_XSHUT);

// 4x/8x ultrasonic HC-SR04P sensors on 74HC4067 16-channel multiplexer
MuxSonar sonar(8, MUX_SIG, MUX_S0, MUX_S1, MUX_S2, MUX_S3);

// 2x Avago ADNS-5020-EN mouse sensors (optical flow)
// shared SDIO+SCLK, separate chip select (NCS), no hardware RESET line
// bit-banged "SPI" 
// param: SCLK, SDIO, NCS, NRESET, CPI
// ADNS5020 mouse1(MOUSE_SCLK, MOUSE_SDIO, -1, -1, 500);
// ADNS5020 mouse2(MOUSE_SCLK, MOUSE_SDIO, -1, -1, 500);
// Odometer odo(mouse1, mouse2);

// #mice, SCLK, SDIO, ADR0, ADR1
Odometer odo(2, MOUSE_SCLK, MOUSE_SDIO, MOUSE_ADR0, -1);
#define ODO2_MASK 0b0011


// Sharp 0A41SK (GP2Y0A41SK) analog infrared sensor 4-30cm
// 100k/68k voltage divider
Sharp0A41 infrared(IR_INPUT, 98, 68);
AdcInput amperes(ANALOG_IN1, 12, 12);

// OLED display
// U8G2_SSD1306_128X32_UNIVISION_F_HW_I2C u8g2(U8G2_R2, /* reset=*/U8X8_PIN_NONE, /* clock=*/SCL, /* data=*/SDA); // pin remapping with ESP8266 HW I2C
// U8G2_SSD1306_128X32_UNIVISION_F_SW_I2C u8g2(U8G2_R2, /*clock=*/X_SCL, /* data=*/X_SDA);
U8G2_SSD1306_128X32_UNIVISION_F_HW_I2C u8g2(U8G2_R2);

// BNO055 from https://github.com/ShellAddicted/BNO055ESP32
Compass compass(UART_NUM_1, /* ESP-TX*/ UART1_TX, /* ESP-RX*/ UART1_RX);

uint8_t display_update = 0;
const char* display_info = NULL;
unsigned long lastTransmission = 0;
unsigned long lastPing = 0;
unsigned long nloop = 0;
unsigned long ndisplay = 0;
uint8_t currentMode = 0;

char buf[800];

const char* TOPIC_TEST        = "esp/info";
const char* TOPIC_CMD_ALL     = "esp/cmd/#";
const char* TOPIC_CMD_TEMP    = "esp/cmd/t";
// const char* TOPIC_CMD_IDLE    = "esp/cmd/idle";
const char* TOPIC_CMD_MODE    = "esp/cmd/mode";
const char* TOPIC_CMD_SERVOH  = "esp/cmd/servoh";
const char* TOPIC_CMD_SERVOV  = "esp/cmd/servov";
const char* TOPIC_CMD_SLEEP   = "esp/cmd/sleep";
const char* TOPIC_CMD_SONARMASK  = "esp/cmd/sonarmask";
const char* TOPIC_CMD_ODOMODE = "esp/cmd/odomode";
const char* TOPIC_CMD_COMPDUMP = "esp/cmd/compdump";


const char* TOPIC_OUT_TEMP    = "esp/out/cputemp";
const char* TOPIC_OUT_SONAR   = "SON"; //"esp/out/sonar";
const char* TOPIC_OUT_ODO     = "ODO"; //"esp/out/odo";
const char* TOPIC_OUT_PING    = "PING"; //"esp/out/ping";
// const char* TOPIC_OUT_IR      = "esp/out/ir";
const char* TOPIC_OUT_LRF     = "LRF"; //"esp/out/lrf";
const char* TOPIC_OUT_COMP    = "COMP"; //"esp/out/comp";


void publish_temperature() {
    uint8_t temp_farenheit= temprature_sens_read();
    float temp_celsius = ( temp_farenheit - 32 ) / 1.8;
    sprintf(buf, "%3.1f", temp_celsius);
    mqtt.publish(TOPIC_OUT_TEMP, buf);
}

void lockI2c() {
  xSemaphoreTake(xI2cSemaphore, 1000);
}

void releaseI2c() {
  xSemaphoreGive(xI2cSemaphore);
}

// -----------------------------------------------
// update status display (OLED)

void displayStatus(const char* info = NULL) 
{
  display_update = 1;
  display_info = info;
}

void displayStatusReal(const char* info = NULL) 
{
#ifdef OLED_DISPLAY  
  // prevent simultaneous calls from different threads (garbled output!?)
  xSemaphoreTake(xDisplaySemaphore, 1000);

  u8g2.clearBuffer(); // clear the internal memory

  String s = "";
  s +=  " ";
  s += mqtt.connected?        "___ " : "    ";
  s += odo.enabled ?          "___ " : "    ";
  s += sonar.enabled ?        "___ " : "    ";
  s += lidar.enabled ?        "___ " : "    ";
  //s += infrared.enabled ?     "___ " : "    ";
  s += compass.enabled ?      "___ " : "    ";

  s.toCharArray(buf, 30);
  u8g2.drawStr(0, 9, buf);
  u8g2.drawStr(0, 7,  " MSG ODO SON LRF BNO"); // display width = 21 characters = 128 pixel

  if (info != NULL) {
    u8g2.drawStr(0, 21,  info);
  } else {
    s = "";
    // number of sent messages (max 4 digits)
    sprintf(buf, "%4d ", mqtt.published % 10000);
    s += buf;
    // mice status
    sprintf(buf, "%s-%s ", odo.mouse[0]->status, odo.mouse[1]->status);
    s += buf;
    sprintf(buf, "%3d ", sonar.mask);
    s += sonar.enabled ?        buf    : "    ";
    sprintf(buf, "%3d ", lidar.servoh.angle);
    s += lidar.enabled ?        buf    : "    ";
    // s += infrared.enabled ?     "    " : "    ";
    sprintf(buf, "%1d%1d%1d%1d", compass.calibration.sys, compass.calibration.mag, compass.calibration.accel, compass.calibration.gyro);
    //s += compass.enabled ?        buf    : "    ";
    s += buf;
    s.toCharArray(buf, 30);
    u8g2.drawStr(0, 21, buf);
  }

  // display voltage
  sprintf(buf, "%1.2f", amperes.readVoltage() - 0.112); //subtract  voltage divider bias
  u8g2.drawStr(105, 32, buf);

  lockI2c();
  u8g2.sendBuffer(); // transfer internal memory to the display
  releaseI2c();

  // // i2c is now self-healing !?
  // if (Wire.getWriteError() > 0) {
  //   ESP_LOGI(TAG, "display bus error");
  //   Wire.begin(); // Wire.reset();
  //   u8g2.sendBuffer(); // transfer internal memory to the display
  // }
  xSemaphoreGive(xDisplaySemaphore);

#endif  
}

// -----------------------------------------------
// set active mode
//
void roverActive(uint8_t flags) {
  currentMode = flags & 0b1111;
  ESP_LOGI(TAG, "switch to mode %d", flags);

  bool lidar0 = lidar.enabled;

  odo.enabled       = flags & 0b0001;
  sonar.enabled     = flags & 0b0010;
  lidar.enabled     = flags & 0b0100;
  //infrared.enabled  = flags & 0b1000;
  compass.enabled  = flags & 0b1000;

  ESP_LOGI(TAG, "  IR:%d  LRF:%d   SONAR:%d   ODO:%d", infrared.enabled, lidar.enabled, sonar.enabled, odo.enabled);

  sonar.reset();

  if (!lidar0 && lidar.enabled) {
    lidar.startRanging();
  }
    
  if (lidar0 && !lidar.enabled) {
    lidar.stopRanging();
  }

  if (odo.enabled)
    odo.powerUp(ODO2_MASK);
  else
    odo.powerDown(ODO2_MASK);

  if (flags == 0) Wire.begin(); //Wire.reset();

  // infrared.readDistance();
  displayStatus();
}


// -----------------------------------------------
// set inactive
//
void roverIdle() {
  roverActive(0);
}


// -----------------------------------------------
// incoming command handler (MQTT style)
//
void callback(char* topic, byte* payload, unsigned int length) {
  payload[length]=0;
  ESP_LOGD(TAG, "got message: %s -- %s", topic, payload);

  // decode topic, process

  // TEMPERATURE
  if (strcmp(topic, TOPIC_CMD_TEMP)==0) {
    publish_temperature();
  }
  else if (strcmp(topic, TOPIC_CMD_MODE)==0) {
    uint8_t x = atoi((char*)payload);
    roverActive(x);
  }
  else if (strcmp(topic, TOPIC_CMD_SERVOH)==0) {
    int x = atoi((char*)payload);
    lidar.servoh.pointTo(x);
  }
  else if (strcmp(topic, TOPIC_CMD_SERVOV)==0) {
    int x = atoi((char*)payload);
    lidar.servov.pointTo(x);
  }
  else if (strcmp(topic, TOPIC_CMD_SONARMASK)==0) {
    int x = atoi((char*)payload);
    sonar.mask = (uint8_t)x;
    sonar.reset();
  }
  else if (strcmp(topic, TOPIC_CMD_ODOMODE)==0) {
    int x = atoi((char*)payload);
    odo.mode = (uint8_t)x;
  }
  else if (strcmp(topic, TOPIC_CMD_COMPDUMP)==0) {
    compass.dumpOffsetsBuf(buf);
    mqtt.publish(TOPIC_TEST, buf);
  }
  // experimental
  else if (strcmp(topic, TOPIC_CMD_SLEEP)==0) {
    roverIdle();
    //wifi.stop();
    displayStatus("SLEEP");
    esp_err_t err = esp_sleep_enable_ext0_wakeup(GPIO_NUM_0, 0);
    ESP_LOGD(TAG, "sleep: %d", err);
    esp_light_sleep_start();

    ESP_LOGD(TAG, "wakeup");
    displayStatus("AWAKE");
  }
}

// -----------------------------------------------
// write updated measurement to serial port (-> RPi)
//
void publishData(unsigned long nloop) {
  mqtt.loop();
  lastTransmission = millis();

  //if (nloop%3 == 0) {
    {
    // only every ..th round ...
    bool pingNow = lastTransmission - lastPing > PING_INTERVAL;

    // if (infrared.enabled && (infrared.updated || pingNow)) {
    //   infrared.updated = 0;
    //   sprintf(buf, "%3d", infrared.distance);
    //   mqtt.publish(TOPIC_OUT_IR, buf);
    // }

    if (sonar.enabled && (sonar.updated || pingNow)) {
      sonar.updated = 0;
      sonar.printData(buf);
      mqtt.publish(TOPIC_OUT_SONAR, buf);
    }

    if (lidar.enabled && (lidar.updated || pingNow)) {
      lidar.updated = 0;
      lidar.printData(buf);
      mqtt.publish(TOPIC_OUT_LRF, buf);
    }

    if (odo.enabled && (odo.updates > 0)) {
      // float dx, dy;
      // byte sq;
      odo.printData(buf);
      mqtt.publish(TOPIC_OUT_ODO, buf);
    }

    if (compass.enabled && (compass.updated || pingNow) && !compass.uncalibrated()) {
      compass.updated = 0;
      compass.printData(buf);
      mqtt.publish(TOPIC_OUT_COMP, buf);
    }

    if (pingNow) {
      lastPing = lastTransmission;
      sprintf(buf, "%6d", (int)(lastTransmission/1000));
      mqtt.publish(TOPIC_OUT_PING, buf); // publish ping number
      displayStatus();
    }
  }
}

// ----------------------------------------------------------------------------
// Tasks


void taskReadIr(void *parameter) {
  while (1) {
    if (infrared.enabled) infrared.readDistance();
    delay(100L);
  }
}

void taskReadSonar(void *parameter) {
  while (1) {
    if (sonar.enabled) sonar.readAll(25);
    delay(25L);
  }
}

void taskReadLrf(void *parameter) {
  while (1) {
    if (lidar.enabled) lidar.read();

    // VL53L0X has "33ms timing budget" - max 30Hz
    delay(75L); 
  }
}

void taskReadOdo(void *parameter) {
  while (1) {
    if (odo.enabled) odo.read2(0, 1);
    delay(50L);
  }
}

void taskReadCompass(void *parameter) {
  while (1) {
    if (compass.enabled) 
      compass.read();
    delay(50L);
  }
}

void taskDisplay(void *parameter) {
  while (1) {
    if (display_update != 0) 
    {
      display_update = 0;
      displayStatusReal(display_info);
      display_info = NULL;
    }

    delay(200L);
  }
}

void taskMqtt(void *parameter) {
  uint8_t n = 0;
  mqtt.setCallback(callback);

  // use Serial/UART0, no need to open connection
  // mqtt.connect(RPI_SERIAL_BAUD, SERIAL_8N1, UART1_RX, UART1_TX);
  // mqtt.connect(RPI_SERIAL_BAUD, SERIAL_8N1, X_UART0_RX, X_UART0_TX);
  displayStatus();

  mqtt.publish(TOPIC_TEST, "=== Hello from ESP32 ===");

  while (1) {
    publishData(++n);
    delay(20L);
  }
}


void taskWatch(void *parameter) {  
  unsigned long n = 0;

  while (1) {

    // display voltage on 3.3V rail
    if ((++n)%2==0)
    {
      displayStatus();      
    }

    if (lastTransmission > 0) {
      digitalWrite(LED_ONBOARD, LOW);

      if (millis() - lastTransmission > PING_INTERVAL) {
        digitalWrite(LED_ONBOARD, HIGH);
        ESP_LOGW(TAG, "watch: connection lost?  MQTT=%d", mqtt.connected);
      }

      if (millis() - lastTransmission > 5*PING_INTERVAL) {
        digitalWrite(LED_ONBOARD, HIGH);
        ESP.restart();
      }
    }
    delay((PING_INTERVAL/2));
  }
}


/**
 * watch button on GPIO0
 * uses ESP32 internal "pullup" feature because button on pin0 
 * is connecting to GND (GPIO0 is also "bootloader selection" pin)
 */
void taskButton0(void *parameter) {
  pinMode(GPIO_0, INPUT);
  gpio_set_pull_mode(GPIO_NUM_0, GPIO_PULLUP_ONLY);
  uint8_t state = 0;

  while (1) {
    int x = 1 - digitalRead(GPIO_0);
    // debounce - only accept button if we got 8 successive HIGH readings
    if ((state == 0b01111111) && (x == 1)) {
      ESP_LOGI(TAG, "*** button pressed ***");
      mqtt.publish("esp/out/button", "0");

      // dump compass calibration
      if (compass.enabled) {
        compass.dumpOffsets();
      }

      if (currentMode == 0) 
        roverActive(1);
      else 
        roverActive(2*currentMode);
      
    }

    state <<= 1;
    state += x;
    delay(20);
  }

}

// ----------------------------------------------------------------------------

int nled = 0;

void setup() {
  // watchdog LED
  pinMode(LED_ONBOARD, OUTPUT);
  digitalWrite(LED_ONBOARD, (nled++)%2);

  esp_log_level_set("*", ESP_LOG_ERROR);
  // esp_log_level_set("rover", ESP_LOG_DEBUG);
  ESP_LOGI(TAG, "*** Rover Logging ***");
  xI2cSemaphore = xSemaphoreCreateBinary();
  xDisplaySemaphore = xSemaphoreCreateBinary();

  Serial.begin(115200);
  delay(100);
  digitalWrite(LED_ONBOARD, (nled++)%2);

  Wire.begin();
  // Wire.setClock(100000);
  // Wire.setTimeout(500); // someone said that might help

  delay(100);
  digitalWrite(LED_ONBOARD, (nled++)%2);
  
  lidar.init();
  lidar.configure(LRF1_ADR, LRF2_ADR);
  Wire.begin(); // Wire.reset();
  digitalWrite(LED_ONBOARD, (nled++)%2);
  
#ifdef OLED_DISPLAY  
  // OLED display
  u8g2.begin();
  u8g2.setFont(u8g2_font_6x10_tf);
  u8g2.setContrast(1); // dim
  
  displayStatus();
  digitalWrite(LED_ONBOARD, (nled++)%2);
#endif  

  sonar.init();
  digitalWrite(LED_ONBOARD, (nled++)%2);

  // mouse1.init();
  // mouse2.init();
  odo.init(ODO2_MASK);
  odo.powerDown(ODO2_MASK);
  digitalWrite(LED_ONBOARD, (nled++)%2);

  compass.init();
  digitalWrite(LED_ONBOARD, (nled++)%2);

  displayStatus();
  digitalWrite(LED_ONBOARD, (nled++)%2);

  roverIdle(); // start in idle mode, send "esp/cmd/mode ..." message to activate

  // https://techtutorialsx.com/2017/05/09/esp32-running-code-on-a-specific-core/
  uint32_t core1 = xPortGetCoreID();

  // EVERYTHING THAT USES I2C STAYS ON CORE 1!

  //                      FUNCTION        NAME          STACK   PARAM   PRIO  HANDLE
  xTaskCreatePinnedToCore(taskMqtt,       "mqtt",        5000,  NULL,   1,    NULL, core1);
  xTaskCreate            (taskReadSonar,  "readSonar",   5000,  NULL,   3,    NULL); // sonar gets higher prio to prevent task switch during echo measurement
  xTaskCreatePinnedToCore(taskReadLrf,    "lrf",         5000,  NULL,   2,    NULL, core1); // I2C errors when interrupted?
  xTaskCreate            (taskReadOdo,    "odo",         5000,  NULL,   1,    NULL);
  xTaskCreate            (taskReadCompass,"compass",     5000,  NULL,   1,    NULL);
  xTaskCreatePinnedToCore(taskButton0,    "but0",        5000,  NULL,   1,    NULL, core1);
  xTaskCreatePinnedToCore(taskDisplay,    "display",     5000,  NULL,   1,    NULL, core1);

  // xTaskCreate            (taskReadIr,     "readIr",     10000,  NULL,   1,    NULL);
  // xTaskCreate            (taskWatch,      "watch",       5000,  NULL,   1,    NULL);


  ESP_LOGI(TAG, "display status and setup end");
  displayStatus();

  // some blinking ...
  digitalWrite(LED_ONBOARD, (nled++)%2); delay(100);
  digitalWrite(LED_ONBOARD, (nled++)%2); delay(100);
  digitalWrite(LED_ONBOARD, (nled++)%2); delay(100);
  digitalWrite(LED_ONBOARD, (nled++)%2); delay(100);
  digitalWrite(LED_ONBOARD, (nled++)%2); delay(100);
  digitalWrite(LED_ONBOARD, (nled++)%2); delay(100);
  digitalWrite(LED_ONBOARD, LOW);
}


// ----------------------------------------------------------------------------

void loop() {

  ++nloop;

  // ----------------------------------------------------------------------------
  // communication/publish data

  // publishData(nloop);
  // return;

  // ----------------------------------------------------------------------------

  // lidar.read();
  // Serial.println(lidar.printData(buf));
  // lidar.readAndPrint();

  // sonar.readAll();
  // sprintf(buf, "sonar: %3d  %3d  %3d  %3d", sonar.distance[0], sonar.distance[1], sonar.distance[2], sonar.distance[3]);
  // Serial.println(buf);

  // // INFRARED test
  // Serial.println(getInfraredDistance(36));


  // ATTENTION: The maximum message size, including header, is 128 bytes by default. This is configurable via MQTT_MAX_PACKET_SIZE in PubSubClient.h.
  // publish frame data
  // mqtt.publish("adns5020/FRAME", mouse2.mousecamFrame(buf, 750));
  // mqtt.publish("adns5020/DELTA", mouse1.mousecamDelta(buf, 750));
  // mouse1.readFrame();
  // mqtt.publish("adns5020/FRAMEB", mouse1.frame, ADNS5020_FRAME_LENGTH);
  
  // mouse1.readBurst(false);
  // // if (mouse1.motion != 0)
  // // {
  // //   Serial.print("[ 1 ]  ");
  // //   mouse1.printAll();
  // // }


  // mouse2.readBurst(false);
  // // if (mouse2.motion != 0)
  // // {
  // //   Serial.print("[ 2 ]  ");
  // //   mouse2.printAll();
  // // }

  // if (mouse1.motion | mouse2.motion) {
  //   sprintf(buf,"%3f %3f %3f %3f", mouse1.x, mouse1.y, mouse2.x, mouse2.y);
  //   mqtt.publish("adns5020/POS", buf);
  // }
  
  delay(500);

}

// ----------------------------------------------------------------------------
// ADNS board test


// #define MOSFET_G 23

// void setup1() {
//   Serial.begin(115200);
//   Serial.println(" === hello ===");
  
//   pinMode(MOSFET_G, OUTPUT);
//   digitalWrite(MOSFET_G, HIGH);
//   delay(1000);
//   digitalWrite(MOSFET_G, LOW);
//   delay(1000);
//   digitalWrite(MOSFET_G, HIGH);
  
//   delay(500);
//   Serial.println("=== odo init");
//   odo.init(ODO2_MASK);

//   // delay(1000);
//   // Serial.println("=== odo powerdown 2/3");
//   // odo2.powerDown(2);
//   // odo2.powerDown(3);

//   // Serial.println("odo shutdown");
//   // odo2.shutdown();
//   // delay(1000);

//   // Serial.println("odo init and powerup");
//   // odo2.init();
//   // odo2.powerUp(0);
//   // odo2.powerUp(1);
//   // odo2.powerUp(2);
//   // odo2.powerUp(3);
  

//   // WiFi.begin(STA_SSID, STA_PASSWORD);
//   // delay(100);

//   // Serial.print("connecting to WIFI "); Serial.print(STA_SSID);
//   // while (WiFi.status() != WL_CONNECTED) {
//   //     delay(500);
//   //     //Serial.print(WiFi.status());
//   //     Serial.print(".");
//   // }

//   // mqtt.setServer(MY_MQTT_SERVER, MY_MQTT_PORT);
//   // mqtt.setCallback(callback);

//   // while (!mqtt.connected()) {
//   //     Serial.print("connecting to MQTT "); Serial.println(MY_MQTT_SERVER);

//   //     if (mqtt.connect("ESP32Client", MY_MQTT_USER, MY_MQTT_PASSWORD )) {
//   //       Serial.println("MQTT connected");
//   //     } else {
//   //       Serial.print("failed with state ");
//   //       Serial.print(mqtt.state());
//   //       delay(2000);
//   //     }
//   // }

//   // mqtt.publish(TOPIC_TEST, "=== Hello from ESP32 ===");
// }

// void readMouse(uint8_t i) {
//   odo.readBurst(i);
//   if (odo.mouse[i]->motion != 0)
//   {
//     Serial.print(i); Serial.print(" ---  ");
//     odo.mouse[i]->printAll();
//   } 
// }

// void loop1() {
//     ++nloop;

//   delay(100);

//   // // NCS chip select test
//   // for (int i=0; i<4; ++i) {
//   //   Serial.println(i);
//   //   odo2.select(i);
//   //   delay(1500);
//   // }

//   readMouse(0);
//   delay(5);
//   readMouse(1);
//   delay(5);

//   // // calibration with "mousecam" - send frame data over MQTT
//   // int calib = 0;
//   // odo2.select(calib);
//   // odo2.mouse[calib]->readFrame();
//   // mqtt.publish("adns5020/FRAMEB", odo2.mouse[calib]->frame, ADNS5020_FRAME_LENGTH);
  

//   // if (nloop%50 == 0) {
//   //   odo2.init(ODO2_MASK);

//   //   delay(200);
//   //   odo2.powerDown(1);
//   //   delay(500);
//   // }
// }
