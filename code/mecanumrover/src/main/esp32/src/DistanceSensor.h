// ----------------------------------------------------------------------------
// A&O MECANUMROVER
// ESP32 - sensor data acquisition
// https://bitbucket.org/ospringauf/mecanumrover
// ----------------------------------------------------------------------------

#ifndef DISTANCE_SENSOR_H
#define DISTANCE_SENSOR_H

#include "Arduino.h"

#define DISTANCE_LIMIT 150 // cutoff at 150cm

class DistanceSensor {

    public:
    
    /**
     * coarse-grain rounding with increasing granularity over distance
     * to smoothe-out masurement errors
     */
    static uint8_t myround(uint16_t d, uint8_t limit = DISTANCE_LIMIT) {
        if (d > limit) return DISTANCE_LIMIT;

        if (d <= 20)  return d;
        if (d <= 50)  return 2 * (d/2);
        if (d <= 100) return 3 * (d/3);
        return 5 * (d/5);
        }

};
#endif