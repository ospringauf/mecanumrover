
int readInfrared(int pin) {
  int d = analogRead(pin);
  d = IR_SCALING/d;
  if (d == 0 || d > 0xFF) {
    d = MAX_DISTANCE;
  }
  return d;
}

int readSonar(int n) {
  int d = 0;
#ifdef CLASSIC
  d = readSonarClassic(triggerPins[n], echoPins[n]);
#else
  d = sonar[n]->ping_cm();
#endif
  if (d == 0 || d > 0xFF) {
    d = MAX_DISTANCE;
  }
  return d;
}


int readSonarClassic(int trigPin, int echoPin) {

  digitalWrite(trigPin, LOW);  
  delayMicroseconds(2); 
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  int duration = pulseIn(echoPin, HIGH);
  int distance = (duration/2) / 29.1;


  if (distance >= MAX_DISTANCE || distance <= 0) 
  {
    return 0;
  }
  else 
  {
    return distance;
  }  
}


void sweep(int steps) { 
  if (!myservo.attached()) { 
    myservo.attach(PWM_PIN);    
  }

  int rangeDegrees = MAX_ANGLE-MIN_ANGLE;
  int pwmStep = rangeDegrees / steps;
  int positioningWait = (int) (2.8 * rangeDegrees * POSITIONING_TIME_PER_DEGREE / steps);
  int initialWait = (int) (1.7 * 90 * POSITIONING_TIME_PER_DEGREE);
  myservo.write(MIN_ANGLE);
  delay(initialWait);

  int step = 0;
  for (int pulse=MIN_ANGLE; pulse<=MAX_ANGLE; pulse+=pwmStep) {    
    myservo.write(pulse);
    delay(max(50,positioningWait));

    int d = readSonar(0);
    reg[REG_DATA + (2 * step) + 1] = (byte)d;

    d = readInfrared(IR0_PIN);
    reg[REG_DATA + (2 * step)] = (byte)d;

    step = step+1;
    reg[REG_DATA_READY] = (byte)step;
  }

  myservo.write(90);
  delay(initialWait);
  myservo.detach();
}


void single() {
  byte mask = 1;
  byte devices = reg[REG_DEVICES];
  
  // IR
  for (int i=0; i<4; ++i) {
    mask = 1 << i;
    if (devices & mask) {
      int d = readInfrared(IR0_PIN + i);
      reg[REG_DATA + (2 * i)] = (byte)d;
      reg[REG_DATA_READY] |= mask; // set ready bit
    }
  }


  // SO
  for (int i=0; i<4; ++i) {
    mask = 1 << (i+4);
    if (devices & mask) {
      int d = readSonar(i);
      reg[REG_DATA + (2 * i) + 1] = (byte)d;
      reg[REG_DATA_READY] |= mask;
    }
  }
}


