/*
 
 Arduino as I2C slave,
 controlling IR, SONAR and turret SERVO
 
  http://dsscircuits.com/articles/arduino-i2c-slave-guide
  http://www.gammon.com.au/forum/?id=10896
 
  20170315 
    status: NewPing readings are very unreliable, particularly for longer distances (~1m)  
    fallback to "classic" mode sonar control
  20170318
    NewPing seems ok now
 
    
Config
    continuous sonar rate (millis delay between loops)
    enabled devices (low: 4 bit IR, high: 4 bit SO)
    
Status
    data ready (SINGLE: bitmask of IR/SO sensors; SWEEP: number of steps done)
    
Data
    32 byte (16x IR, 16x SONAR)
    sweep reads IR1/SO1, up to 16 steps
    normal mode reads IR1..IR4, SO1..SO4

Mode
    continuous/single/sweep/sleep
    SWEEP reads IR0 and SO0
    device returns to SLEEP after each SWEEP or SINGLE operation   
    
Ident
    ID=0x23

*/

#include <Servo.h>
#include <NewPing.h>
#include <Wire.h>

// general
#define IR0_PIN         A0
#define IR1_PIN         A1
#define IR2_PIN         A2
#define IR3_PIN         A3

#define PWM_PIN         9

#define MAX_DISTANCE    190
#define I2C_ADR         0x0B

// depending on reference voltage - for 5V: 2376, for 3.3V: 3600
#define IR_SCALING      2376

#define MIN_ANGLE       10
#define MAX_ANGLE       170
#define POSITIONING_TIME_PER_DEGREE (100.0/60.0) // servo speed

#define REG_COUNT       0x33
#define MAX_CMD_BYTES   2
#define MAX_WRITE_REG   3

byte reg[REG_COUNT];
byte cmd[MAX_CMD_BYTES];
byte newreg[MAX_WRITE_REG]; // temp new register values DELAY, DEVICES, MODE
byte upd[MAX_WRITE_REG]; // "to be updated" flag

#define REG_DELAY       0x00 // n x 10ms delay in main loop
#define REG_DEVICES     0x01 // set enabled devices, bitmask ssssiiii (s=sonar, i=infrared)
#define REG_MODE        0x02 // 0x00=SLEEP; 0x10=SINGLE; 0x20=CONT; 0x30+n=SWEEP (n+1 steps)
#define REG_DATA_READY  0x0A
#define REG_DATA        0x0B // 32 bytes of sensor data (cm values)
#define REG_ID          0x32 // always 0x23=53

#define MODE_SLEEP      0x00
#define MODE_SINGLE     0x10
#define MODE_CONT       0x20
#define MODE_SWEEP      0x30 // low nibble: steps-1 (0..15 = 1..16 steps)

int triggerPins[4] = { 10, 12, 10, 10 };
int echoPins[4]    = { 11, 13, 11, 11 };

#define NOCLASSIC

Servo myservo;  // create servo object to control a servo

#ifndef CLASSIC
NewPing sonar0(triggerPins[0], echoPins[0], MAX_DISTANCE);
NewPing sonar1(triggerPins[1], echoPins[1], MAX_DISTANCE);
NewPing sonar2(triggerPins[2], echoPins[2], MAX_DISTANCE);
NewPing sonar3(triggerPins[3], echoPins[3], MAX_DISTANCE);
NewPing* sonar[4] = { &sonar0, &sonar1, &sonar2, &sonar3 };
#endif

int hasUpdate = 0;

void setup() {
  Serial.begin(9600);           // start serial for output

  for (int i=0; i<REG_COUNT; ++i) {
    reg[i] = (byte)0;
  }

  reg[REG_DELAY] = 10;
  reg[REG_DEVICES] = 0x11; // SO0, IR0
  reg[REG_ID] = 0x23;
  reg[REG_MODE] = MODE_SLEEP;

#ifdef CLASSIC
  for (int i=0; i<4; ++i) {
    pinMode(triggerPins[i], OUTPUT);
    pinMode(echoPins[i], INPUT);
  }
#endif  
  
  Serial.println("center servo");
  myservo.attach(PWM_PIN);  // attaches the servo on pin 9 to the servo object
  myservo.write(90);
  delay(300);
  myservo.detach();

  Wire.begin(I2C_ADR);  // join i2c bus with address #11
  //Wire.setClock(400000L); // should not be neccessary for slave
  
  Wire.onReceive(receiveEvent); // register event
  Wire.onRequest(requestEvent); // sendData is funtion called when Pi requests data

  for (int i=0; i<=REG_MODE; ++i) {
    upd[i] = 0;
  }

  Serial.println("setup ready");
}


void updateRegisters() {
  hasUpdate = 0;
    
  // set writable register
  /*
  if ((cmd[0] == REG_DELAY) || (cmd[0] == REG_DEVICES) || (cmd[0] == REG_MODE)) {
    Serial.print("update "); Serial.print(cmd[0]); Serial.print("="); Serial.println(cmd[1]);
    reg[cmd[0]] = cmd[1];
  }
  */
  for (int i=0; i<=REG_MODE; ++i) {
    if (upd[i] == 1) {
      //Serial.print("update "); Serial.print(i); Serial.print("="); Serial.println(upd[i]);
      upd[i] = 0;
      reg[i] = newreg[i];
    }
  }
}


void _loop() {
  int d = readSonar(0);
  //int d = infrared_cm(IR0_PIN);
  Serial.println(d);
  delay(50);
}


void loop() {
  // check for register updates via I2C
  if (hasUpdate == 1) {
    updateRegisters();
  }
  

  // perform measurements according to current MODE
  byte mode = reg[REG_MODE];
  
  if ((mode == MODE_SINGLE) || (mode == MODE_CONT)) {
    single();
  }
  else if ((mode & 0xF0) == MODE_SWEEP) {
    byte steps = (mode & 0x0F) + 1;
    sweep(steps);
  }
  
  if (mode == MODE_CONT) 
  {
    delay(10 * reg[REG_DELAY]);
  } 
  else 
  {
    // sleep after SINGLE or SWEEP
    reg[REG_MODE] = MODE_SLEEP;
  }
  
}


