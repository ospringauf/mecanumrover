// function that executes whenever data is received from master
// this function is registered as an event, see setup()

void receiveEvent(int bytesReceived) {

  for (int a = 0; a < bytesReceived; a++)
  {
    if (a < MAX_CMD_BYTES)
    {
      cmd[a] = Wire.read();
    }
    else
    {
      Wire.read();  // if we receive more data then allowed just throw it away
    }
  }

  if (bytesReceived == 1 && (cmd[0] < REG_COUNT))
  {
    return;
  }

  if (bytesReceived == 1 && (cmd[0] >= REG_COUNT))
  {
    cmd[0] = 0x00;
    return;
  }

  if (bytesReceived == 2 && (cmd[0] < REG_COUNT))
  {
    //Serial.print("write "); Serial.print(cmd[0]); Serial.print("="); Serial.println(cmd[1]);
    hasUpdate=1;
    upd[cmd[0]] = 1; // mark register for update
    newreg[cmd[0]] = cmd[1]; // temp register storage
  }  
}



void requestEvent()
{  
  //Set the buffer up to send all bytes of data (max 32, internal buffer limit)
  int adr = cmd[0];
  Wire.write(reg + adr, min(REG_COUNT - adr, 32));
  reg[REG_DATA_READY] = 0;
}


