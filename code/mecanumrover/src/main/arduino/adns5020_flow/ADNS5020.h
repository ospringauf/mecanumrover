#ifndef __ADNS5020_H__
#define __ADNS5020_H__

#include "Arduino.h"

#define ADNS5020_REG_PRODUCT_ID     0x00
#define ADNS5020_REG_REVISION_ID    0x01
#define ADNS5020_REG_MOTION         0x02
#define ADNS5020_REG_DELTA_X        0x03
#define ADNS5020_REG_DELTA_Y        0x04
#define ADNS5020_REG_SQUAL          0x05
#define ADNS5020_REG_BURST_MODE     0x63
#define ADNS5020_REG_CONTROL        0x0d
#define ADNS5020_REG_PIXEL_GRAB     0x0b
#define ADNS5020_REG_CHIP_RESET     0x3a

#define ADNS5020_FRAME_LENGTH       225
#define ADNS5020_DELAY              100

// Avago ADNS-5020-EN optical mouse sensor
// see http://strofoland.com/arduino-projects/reading-a5020-optical-sensor-using-arduino-part2/
// see https://www.bidouille.org/hack/mousecam

class ADNS5020 {
  public:
    ADNS5020(byte sclk, byte sdio, byte ncs, byte nreset, int cpi);

    char motion;
    char dx;
    char dy;
    byte squal;
    byte shutter_upper;
    byte shutter_lower;
    byte max_pixel;
    byte pixel_sum;
    byte frame[ADNS5020_FRAME_LENGTH];
    int factor;
    int x;
    int y;
    
    void reset();    
    void softReset();
    void powerDown();
    void resolution(int cpi);
    void identify();
    void readDelta();
    void readBurst();
    void readFrame();
    void mousecamOutput();
    void printDelta();
    void printAll();
    
  private:   
    byte _sclk;
    byte _sdio;
    byte _ncs;
    byte _nreset;
    int _cpi;
    
    void pushByte(byte data);
    byte pullByte();
    byte readRegister(byte address);
    void writeRegister(byte address, byte data);
    void printd3(int i);
    void updatePosition();
  
};

#endif  // __ADNS5020_H__
