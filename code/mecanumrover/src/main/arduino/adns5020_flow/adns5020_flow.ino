#include "ADNS5020.h"

/*
to plot data, first close the arduino IDE
https://playground.arduino.cc/Interfacing/LinuxTTY

sudo stty -F /dev/ttyUSB0 cs8 115200 ignbrk -brkint -icrnl -imaxbel -opost -onlcr -isig -icanon -iexten -echo -echoe -echok -echoctl -echoke noflsh -ixon -crtscts

cat /dev/ttyUSB0
*/

// param: SCLK, SDIO, NCS, NRESET
ADNS5020 chip1(2, 3, 4, 5, 500);
ADNS5020 chip2(6, 7, 8, 9, 500);

void setup() {
  Serial.begin(115200);
  Serial.println("RESET");

  //chip1.cpi = chip2.cpi = 1000;

  delay(100);
  chip1.reset();
  chip1.identify();

  chip2.reset();
  chip2.identify();
  chip2.factor = 1;

  /*
  chip2.powerDown();
  delay(1000);
  chip2.reset();
  
  chip1.resolution(1000);
  chip2.resolution(1000);
  */
}

void loop() {

  chip1.readBurst();
  if (chip1.motion != 0)
  {
    Serial.print("[ 1 ]  ");
    chip1.printAll();
  }

  chip2.readBurst();
  if (chip2.motion != 0)
  {
    Serial.print("[ 2 ]  ");
    chip2.printAll();
  }

  if (chip1.motion | chip2.motion) {
    Serial.print("POS:");
    Serial.print(" "); Serial.print(chip1.x);
    Serial.print(" "); Serial.print(chip1.y);
    Serial.print(" "); Serial.print(chip2.x);
    Serial.print(" "); Serial.print(chip2.y);
    Serial.println();
  }
  
  delay(100);
}
