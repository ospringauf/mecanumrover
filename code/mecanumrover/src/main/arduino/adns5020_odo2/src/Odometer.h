// ----------------------------------------------------------------------------
// A&O MECANUMROVER
// Rover-2 Odometer
// https://bitbucket.org/ospringauf/mecanumrover
// ----------------------------------------------------------------------------

#include "ADNS5020.h"

/**
 * This is the driver for the odometer board,
 * which controls multiple (currently up to 4) ADNS-5020-EN optical mouse sensors.
 * Sensors are addressed individually by controlling the NCS lines.
 * 
 */


#define ODO_MODE_SHORT 0
#define ODO_MODE_VERBOSE 1

class Odometer
{
  public:
    ADNS5020 *mouse[4];

    // weighted sum dx/dy over several measurement intervals
    double sdx;
    double sdy;
    // inrementing readings for each sensor
    double dx[4];
    double dy[4];
    int16_t idx[4];
    int16_t idy[4];
    uint16_t sq[4];
    uint16_t updates; // number of updates since last readout/transmission

    bool enabled;
    uint8_t nmice;
    uint8_t selected = 0;
    uint8_t mode = ODO_MODE_VERBOSE;

    Odometer(); 
    void addMouse(ADNS5020 *sensor);

    void init();
    void reset(uint8_t i);
    void identify(uint8_t i);
    void powerUp();
    void powerDown();
    void readBurst(uint8_t i);    
    void read2(uint8_t m1, uint8_t m2);
    void read2Simple(bool print);
    char* printData(char *buf, bool clear=true);

  private:
    int8_t _adr0;
    int8_t _adr1;
    uint8_t _sclk;
    uint8_t _sdio;
};