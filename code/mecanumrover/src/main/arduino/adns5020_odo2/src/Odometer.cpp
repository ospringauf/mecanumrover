// ----------------------------------------------------------------------------
// A&O MECANUMROVER
// Rover-2 Odometer
// https://bitbucket.org/ospringauf/mecanumrover
// ----------------------------------------------------------------------------

#include "ADNS5020.h"
#include "Odometer.h"

/**
 * This is the driver for the odometer board,
 * which controls multiple (currently up to 4) ADNS-5020-EN optical mouse sensors.
 */

#define ODO_MODE_SHORT 0
#define ODO_MODE_VERBOSE 1

Odometer::Odometer() : sdx(0), sdy(0), nmice(0), updates(0), enabled(true)
{
}

void Odometer::addMouse(ADNS5020 *sensor)
{
    int i = nmice;
    mouse[i] = sensor;
    nmice++;
}

void Odometer::init()
{
    powerUp();
    for (int i = 0; i < nmice; ++i)
    {
        identify(i);
        // delay(100);
    }
}

void Odometer::reset(uint8_t i)
{
    mouse[i]->reset();
}

void Odometer::identify(uint8_t i)
{
    mouse[i]->identify();
}

void Odometer::powerUp()
{
    for (int i = 0; i < nmice; ++i)
    {
        mouse[i]->powerUp();
        // delay(100);
    }
    enabled = true;
}

void Odometer::powerDown()
{
    for (int i = 0; i < nmice; ++i)
    {
        mouse[i]->powerDown();
        // delay(100);
    }
    enabled = false;
}

void Odometer::readBurst(uint8_t i)
{
    mouse[i]->readBurst();

    // if (mouse[i]->motion) mouse[i]->printAll();
}

void Odometer::read2(uint8_t m1, uint8_t m2)
{
    readBurst(m1);
    readBurst(m2);

    double dx1 = 0, dx2 = 0, dy1 = 0, dy2 = 0;

    if (mouse[m1]->motion | mouse[m2]->motion)
    {
        double f1 = 0;
        if (mouse[m1]->motion)
        {
            f1 = mouse[m1]->squal == 0 ? 1 : mouse[m1]->squal;
            dx1 = mouse[m1]->transformDx();
            dy1 = mouse[m1]->transformDy();
            sq[m1] += mouse[m1]->squal;
            dx[m1] += dx1;
            dy[m1] += dy1;
        }
        double f2 = 0;
        if (mouse[m2]->motion)
        {
            f2 = mouse[m2]->squal == 0 ? 1 : mouse[m2]->squal;
            dx2 = mouse[m2]->transformDx();
            dy2 = mouse[m2]->transformDy();
            sq[m2] += mouse[m2]->squal;
            dx[m2] += dx2;
            dy[m2] += dy2;
        }

        // weighted sum of both sensors
        double ddx = (f1 / (f1 + f2) * dx1) + (f2 / (f1 + f2) * dx2);
        double ddy = (f1 / (f1 + f2) * dy1) + (f2 / (f1 + f2) * dy2);
        sdx += ddx;
        sdy += ddy;

        updates++;
    }
}

void Odometer::read2Simple(bool print)
{
    int m1 = 0, m2 = 1;

    // read and sum values
    readBurst(m1);
    readBurst(m2);

    if (mouse[m1]->motion | mouse[m2]->motion)
    {
        if (mouse[m1]->motion)
        {
            sq[m1] += mouse[m1]->squal;
            idx[m1] += mouse[m1]->dx;
            idy[m1] += mouse[m1]->dy;
        }
        if (mouse[m2]->motion)
        {
            sq[m2] += mouse[m2]->squal;
            idx[m2] += mouse[m2]->dx;
            idy[m2] += mouse[m2]->dy;
        }

        updates++;
    }

    // output and reset
    if (print && (updates > 0))
    {
        Serial.print("$ODO2,");
        Serial.print(idx[m1], DEC);
        Serial.print(",");
        Serial.print(idy[m1], DEC);
        Serial.print(",");
        Serial.print(sq[m1] / updates, DEC);
        Serial.print(",");
        Serial.print(idx[m2], DEC);
        Serial.print(",");
        Serial.print(idy[m2], DEC);
        Serial.print(",");
        Serial.print(sq[m2] / updates, DEC);
        Serial.println("*");

        idx[m1] = idy[m1] = sq[m1] = 0;
        idx[m2] = idy[m2] = sq[m2] = 0;
        updates = 0;
    }
}

#define DCHARS 7

char buf2a[DCHARS + 2], buf2b[DCHARS + 2], buf2c[DCHARS + 2], buf2d[DCHARS + 2];

char *Odometer::printData(char *buf, bool clear)
{
    if (mode == ODO_MODE_SHORT)
    {
        // weighted sum over both sensors
        uint16_t avgSqual = (sq[0] + sq[1]) / (2 * updates);
        // sprintf(buf, "%3.0f %3.0f %d", sdx, sdy, avgSqual);

        dtostrf(sdx, DCHARS, 1, buf2a);
        dtostrf(sdy, DCHARS, 1, buf2b);

        sprintf(buf, "%s %s %d", buf2a, buf2b, avgSqual);
    }
    else if (mode == ODO_MODE_VERBOSE)
    {
        // separate readings for left/right sensor

        // sprintf(buf, "%3.0f %3.0f %d  %3.0f %3.0f %d",
        // dx[0], dy[0], sq[0]/updates,
        // dx[1], dy[1], sq[1]/updates);
        dtostrf(dx[0], DCHARS, 1, buf2a);
        dtostrf(dy[0], DCHARS, 1, buf2b);
        dtostrf(dx[1], DCHARS, 1, buf2c);
        dtostrf(dy[1], DCHARS, 1, buf2d);

        sprintf(buf, "%s %s %3d  -  %s %s %3d",
                buf2a, buf2b, sq[0] / updates,
                buf2c, buf2d, sq[1] / updates);
    }
    // reset incrementing fields
    if (clear)
    {
        for (uint8_t i = 0; i < 4; ++i)
        {
            dx[i] = dy[i] = 0;
            sq[i] = 0;
        }
        sdx = sdy = 0;
    }
    updates = 0;
    return buf;
}
