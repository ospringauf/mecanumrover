// ----------------------------------------------------------------------------
// A&O MECANUMROVER
// Rover-2 Odometer
// https://bitbucket.org/ospringauf/mecanumrover
// ----------------------------------------------------------------------------

#include <Arduino.h>
#include "ADNS5020.h"
#include "Odometer.h"

#define LED 13

#define MOUSE1_SCLK 5
#define MOUSE1_SDIO 6
#define MOUSE1_NCS 7
#define MOUSE1_NRESET 8

#define MOUSE2_SCLK 9
#define MOUSE2_SDIO 10
#define MOUSE2_NCS 11
#define MOUSE2_NRESET 12

#define CPI 1000

// 2x Avago ADNS-5020-EN mouse sensors (optical flow), bit-banged "SPI"
// param: SCLK, SDIO, NCS, NRESET, CPI
ADNS5020 mouse1(MOUSE1_SCLK, MOUSE1_SDIO, MOUSE1_NCS, MOUSE1_NRESET, CPI);
ADNS5020 mouse2(MOUSE2_SCLK, MOUSE2_SDIO, MOUSE2_NCS, MOUSE2_NRESET, CPI);
Odometer odo;

char buf[80];

// see https://www.instructables.com/id/two-ways-to-reset-arduino-in-software/
void (*resetFunc)(void) = 0; //declare reset function at address 0

void setup()
{
  pinMode(LED, OUTPUT);
  Serial.begin(38400);

  delay(200);

  // rotate sensor coordinates to match rover coordinates
  // mouse1.setTransform(-PI / 2.0, 1.0);
  // mouse2.setTransform(-PI / 2.0, 1.0);

  odo.addMouse(&mouse1);
  odo.addMouse(&mouse2);
  odo.init();
}

long i = 0;

void recvCommand()
{
  if (Serial.available() > 0)
  {
    char c = Serial.read();
    switch (c)
    {
    case 'u':
      Serial.println("power up");
      odo.powerUp();
      break;

    case 'd':
      Serial.println("power down");
      odo.powerDown();
      break;

    case 'h':
      Serial.println("1000cpi");
      mouse1.resolution(1000);
      mouse2.resolution(1000);
      break;

    case 'l':
      Serial.println("500cpi");
      mouse1.resolution(500);
      mouse2.resolution(500);
      break;

    case 'r':
      Serial.println("reset");
      odo.reset(0);
      odo.reset(1);
      odo.powerUp();
      break;

    // reset Arduino
    case 'R':
      // Serial.println("reset");
      resetFunc();
      break;

    default:
      Serial.print("unknown cmd: ");
      Serial.println(c);
      break;
    }
  }
}

void odometerLoop()
{
  if (odo.enabled)
  {
    bool print = i%2==0;
    odo.read2Simple(print);
    i++;

    // mouse1.readBurst();
    // mouse2.readBurst();
    // if (mouse1.motion || mouse2.motion)
    // {
    //   Serial.print("$ODO2,");
    //   mouse1.printShort();
    //   Serial.print(",");
    //   mouse2.printShort();
    //   Serial.println("*");
    // }
  }

  // read command
  recvCommand();
}

void loop()
{
  digitalWrite(LED, HIGH); // LED

  odometerLoop();

  // mouse2.mousecamOutput();

  // odo.read2(0, 1);
  // if (odo.updates > 0) {
  //   Serial.println(odo.printData(buf));
  // }

  // mouse1.readBurst();
  // if (mouse1.motion != 0)
  // {
  //   Serial.print("[ 1 ]  ");
  //   mouse1.printAll();
  // }

  // mouse2.readBurst();
  // if (mouse2.motion != 0)
  // {
  //   Serial.print("[ 2 ]  ");
  //   mouse2.printAll();
  // }

  digitalWrite(LED, LOW); // LED ausschalten

  // i++;
  // if (i%100 == 0) {
  //   // Serial.println(".");
  //   Serial.println(odo.printData(buf, true));
  //   i=0;
  // }

  delay(25);
}