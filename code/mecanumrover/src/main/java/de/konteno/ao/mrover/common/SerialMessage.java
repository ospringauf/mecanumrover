/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.common;

/**
 * NMEA-style message format for serial line communication with ESP32
 *
 */
public class SerialMessage {
	private String content; 
	private String topic;
	private String payload;
	private int checksum;
	
	private static final int NO_CHECKSUM = -1;
	
	public SerialMessage() {}
	
	public SerialMessage(String topic, String payload) {
		this.topic = topic;
		this.payload = payload;
		this.content = (payload != null) ? topic + "," + payload : topic;		
	}
	
	public String toLineFormat() {
		String cc = Integer.toHexString(calcChecksum()).toUpperCase();
		return "$" + content + "*" + cc;
	}
	
	/**
	 * parse NMEA style message (eg. "$TOPIC,arg1,arg2*0E") into topic and payload
	 * @param msg
	 * @return
	 */
	public static SerialMessage parseNmea(String msg) {
		if (!msg.startsWith("$")) {
			return null;
		}
		SerialMessage sm = new SerialMessage();

		String[] f = msg.substring(1).split("\\*");
		sm.content = f[0];
		String[] g = f[0].split(",", 2);
		sm.topic = g[0];
		sm.payload = g.length > 1 ? g[1] : null;
		sm.checksum = f.length > 1 ? Integer.parseInt(f[1], 16) : NO_CHECKSUM;
		return sm;		
	}
	
	// public static String[] parseNmeaMessage(String msg) {
	//// Pattern p = Pattern.compile("\\$([^,]+)\\,([^\\*]*)\\*([0-9a-fA-F]{1,2})");
	//// Matcher m = p.matcher(msg);
	//// return new String[] { m.group(0), m.group(1), m.group(2) };
	//
	// Pattern p = Pattern.compile("\\$([^,]+),([0-9a-zA-Z,\\.])+");
	// Matcher m = p.matcher(msg);
	// if (m.matches()) {
	// return new String[] { m.group(1), m.group(2) /*, m.group(3)*/ };
	// } else {
	// return null;
	// }
	//
	// }

	public int calcChecksum() {
		if (content != null) {
			return RoverUtil.crc8(content.getBytes());
		} else {
			return -1;
		}
	}

	public String getTopic() {
		return topic;
	}

//	public void setTopic(String topic) {
//		this.topic = topic;
//	}

	public String getPayload() {
		return payload;
	}

//	public void setPayload(String payload) {
//		this.payload = payload;
//	}

	public int getChecksum() {
		return checksum;
	}

	public boolean hasChecksum() {
		return checksum != NO_CHECKSUM;
	}
}
