/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.common.cmd;

import de.konteno.ao.mrover.common.RoverTopic;
import de.konteno.ao.mrover.common.unit.Angle;

@RoverTopic(address="cmd.turn")
public class TurnCmd implements IRoverCommand {

	private Angle angle;
	private boolean relative;
	
	// stop turning when within tolerance of target angle
	private double tolerance = 0.75;
	
	private RotateCmd rotation;

	public TurnCmd(Angle angle, boolean relative) {
		setAngle(angle);
		setRelative(relative);
	}

	public Angle getAngle() {
		return angle;
	}

	public TurnCmd setAngle(Angle angle) {
		this.angle = angle;
		return this;
	}

	public boolean isRelative() {
		return relative;
	}

	public TurnCmd setRelative(boolean relative) {
		this.relative = relative;
		return this;
	}

	@Override
	public String toString() {
		return "TURN(" + (relative ? "rel" : "abs") + ", " + angle.toDegree() + ")!";
	}

	public double getTolerance() {
		return tolerance;
	}

	public TurnCmd setTolerance(double tolerance) {
		this.tolerance = tolerance;
		return this;
	}

	public TurnCmd setRotation(RotateCmd cmd) {
		rotation = cmd;
		return this;
	}
	
	public TurnCmd tangential(double steering) {
		rotation = new RotateCmd(1).tangential().steer(steering);
		return this;
	}


	/**
	 * get the actual "rotation" command that will be issued by the TurnService;
	 * use the default "on-the-spot" rotation unless otherwise assigned.
	 * TurnService will assign the speed and direction.
	 * @return
	 */
	public RotateCmd getRotation() {
		return (rotation != null) ? rotation : new RotateCmd(0.75);
	}
	
	public static TurnCmd absolute(Angle a) {
		return new TurnCmd(a, false);
	}

	public static TurnCmd relative(Angle a) {
		return new TurnCmd(a, true);
	}

}
