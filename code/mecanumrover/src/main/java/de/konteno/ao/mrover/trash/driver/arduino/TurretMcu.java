/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.trash.driver.arduino;

import com.pi4j.io.i2c.I2CDevice;

import de.konteno.ao.mrover.driver.common.DistanceSensor;

/**
 * An Atmega328P microcontroller (Arduino ProMini, 5V, 16MHz)
 * controlling the turret servo and IR/SONAR distance sensors.
 * Connected as an I2C slave.
 *
 */
public class TurretMcu /*extends RoverComponent*/ {
	
	public static int REG_DELAY       = 0x00;
	public static int REG_DEVICES     = 0x01;
	public static int REG_MODE        = 0x02; // 0x00=SLEEP; 0x10=SINGLE; 0x20=CONT; 0x30+n=SWEEP (n+1 steps)
	public static int REG_DATA_READY  = 0x0A;
	public static int REG_DATA        = 0x0B;
	public static int REG_ID          = 0x32; // always 0x23=53

	public static byte MODE_SLEEP      = 0x00;
	public static byte MODE_SINGLE     = 0x10;
	public static byte MODE_CONT       = 0x20;
	public static byte MODE_SWEEP      = 0x30; // low nibble: steps-1 (0..15 = 1..16 steps)

	public static int IR0      = 0x01;
	public static int IR1      = 0x02;
	public static int IR2      = 0x04;
	public static int IR3      = 0x08;
	public static int SO0      = 0x10;
	public static int SO1      = 0x20;
	public static int SO2      = 0x40;
	public static int SO3      = 0x80;
	
	// DELAY: in CONT mode, number of millis delay between each main loop (measurement)
	
	// DATA_READY
	// in SIGLE or CONT mode: 8 bits, representing the sensors
	// in SWEEP mode: number of measurements (steps)
	
	// sweep servo (PWM, STEPS) and collect sonar (TRIG/ECHO) and infaŕared (A) measurements. Max STEPS=16!
	// get result of last sweep: return 2*STEPS bytes with ir/sonar readings (max 32 bytes)
	
	byte[] buffer = new byte[32];

	
	private I2CDevice device;

	public TurretMcu(I2CDevice dev) {
		device = dev;		
	}
	
	public int singleSonar() throws Exception {
		config(REG_DEVICES, SO0);
		config(REG_MODE, MODE_SINGLE);
		int ready;
		int dist;
		do {
			Thread.sleep(10);
			device.read(REG_DATA_READY, buffer, 0, 5);
			ready = buffer[0];
			dist = buffer[2] & 0xFF;
		} while (ready == 0);
		return dist;
	}
	
//	public void continuousSonar() throws Exception {
//		device.write(REG_DEVICES, (byte) 0x20);
//		device.write(REG_MODE, (byte) MODE_CONT);
//	}	
	
	public DistanceSensor getSonar0() {
		return new TurretSonar(this);
	}
	
	
	public void demo() throws Exception {
		System.out.println("*** ARDUINO SLAVE DEMO (q to quit) ***");

		boolean repeat = true;
		while (repeat) {

			char input = (char) System.in.read();
			//input = Character.toLowerCase(input);

			switch (input) {
			
			case 'q':
				return;

			case '1':
				int id = device.read(REG_ID);
				System.out.println(String.format("ID=%d", id));
				break;
				
			case '2': // ir-0 Einzelmessung
				config(REG_DEVICES, IR0);
				for (int i=0; i<50; ++i) {
					config(REG_MODE, MODE_SINGLE);
					Thread.sleep(100);
					int r = device.read(REG_DATA_READY);
					int d = device.read(REG_DATA);
					System.out.println(String.format("READY=%3d  DIST=%3d", r, d));
				}
				break;

			case 's': // sonar-0 Einzelmessung
				config(REG_DEVICES, SO0);
				for (int i=0; i<100; ++i) {
					config(REG_MODE, MODE_SINGLE);
					Thread.sleep(100);
					int r = device.read(REG_DATA_READY);
					int d = device.read(REG_DATA+1);
					System.out.println(String.format("READY=%3d  DIST=%3d", r, d));
				}
				break;
			
			case '3': // ir-0 Dauermessung 20ms
				config(REG_DEVICES, IR0);
				config(REG_DELAY, (byte) 0x02);
				config(REG_MODE, MODE_CONT);
				for (int i=0; i<200; ++i) {
					device.read(REG_DATA_READY, buffer, 0, 5);
					int r = buffer[0];
					int d = buffer[1] & 0xFF;
					System.out.println(String.format("READY=%3d  DIST=%3d", r, d));
					Thread.sleep(40);
				}
				break;

			case 'S': // sonar-0 Dauermessung 50ms
				config(REG_DEVICES, SO0);
				config(REG_DELAY, (byte) 0x05);
				config(REG_MODE, MODE_CONT);
				for (int i=0; i<200; ++i) {
					device.read(REG_DATA_READY, buffer, 0, 5);
					int r = buffer[0];
					int d = buffer[2] & 0xFF;
					System.out.println(String.format("READY=%3d  DIST=%3d", r, d));
					Thread.sleep(40);
				}
				break;

			case '4':
				scanFront();
				break;

			}			
			
		}
	}

	public void scanFront() {
		try {
			int steps = 8;
			config(REG_DEVICES, (byte) (IR0 | SO0));
			config(REG_MODE, (byte) (MODE_SWEEP + (steps-1)));
			for (int i=0; i<20; ++i) {
				device.read(REG_DATA_READY, buffer, 0, 20);
				int r = buffer[0];
				int[] dist = getDistancesFromBuffer(2, steps+1);
				System.out.println(String.format("READY=%3d  DIST=%3d %3d %3d %3d %3d %3d %3d %3d %3d", r,
						dist[0], dist[1], dist[2], dist[3], dist[4], dist[5], dist[6], dist[7], dist[8]));
//				EventBroker.updateDistance(dist, 10, 170);
				Thread.sleep(40);
			}
		} 
		catch (Exception e) {
			System.out.println("error in 4: ");
			e.printStackTrace();
		}
	}
	
//	@Override
	public void shutdown() {
		try {
			config(REG_DELAY, 0x0A);
			config(REG_MODE, MODE_SLEEP);
		} catch (Exception e) {
			e.printStackTrace();
		}
//		super.shutdown();
	}

	private void config(int adr, byte value) throws Exception {
		device.write(adr, value);
//		Thread.sleep(5);
	}

	private void config(int adr, int value) throws Exception {
		config(adr, (byte)value);
		
	}

	private int[] getDistancesFromBuffer(int offset, int count) {
		int[] d = new int[count];
		for (int i=0; i<count; ++i) {
			d[i] = buffer[offset + 2*i] & 0xFF;
		}
		return d;
	}
}
