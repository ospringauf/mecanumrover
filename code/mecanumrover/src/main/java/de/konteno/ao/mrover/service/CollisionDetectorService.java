/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.konteno.ao.mrover.common.Rover;
import de.konteno.ao.mrover.common.cmd.StopCmd;
import de.konteno.ao.mrover.common.event.SonarDistanceEvt;

/**
 * Detect obstacles in the direction of travel.
 * Process distance events from the ultrasonic sensors, consider only the sensors directed 
 * in the "relative course" = direction of motion
 *
 */
public class CollisionDetectorService extends MyVerticleBase {

	
	private static final Logger logger = LoggerFactory.getLogger( CollisionDetectorService.class );
	private int lastFrontClearance;
	private int lastLeftClearance;
	private int lastRightClearance;
	private int lastRearClearance;

	
	@Override
	public void start() throws Exception {		
		super.start();
		
		// process distance events
		busListen(SonarDistanceEvt.class, this::distanceUpdate);
	}	
	
	private void distanceUpdate(SonarDistanceEvt evt) {
		if (Rover.currentSpeed == 0) {
			return;
		}
		
		// TODO rewrite, use evtl.distOnCourse
		
		double course = Rover.pose.relativeCourse.absolute().toDegree();
		int minClear = Rover.minClearance();
//		logger.info("got distance, rel course={}, min clear={}, curr speed={}", rc, mc, Rover.currentSpeed);
		
		// left/right: add some more space, wheels are "outside" of the sensor diameter 
		final int sideExtra= 4;
		
		if (course > 270 || course < 90) {
			// forward
			int c = evt.getFrontClearance();
			if (c < lastFrontClearance && c < minClear) {
				logger.warn("front collision warning, rel.course={}, clearance={}", course, c);
				busSend(new StopCmd("obstance in front"));
			}
			lastFrontClearance = c;			
		}
		if (course > 180 && course < 360) {
			// left
			int c = evt.getLeftClearance();
			if (c < lastLeftClearance && c < minClear + sideExtra) {
				logger.warn("left collision warning, rel.course={}, clearance={}", course, c);
				busSend(new StopCmd("obstance to the left"));
			}
			lastLeftClearance = c;			
		}
		if (course > 0 && course < 180) {
			// right
			int c = evt.getRightClearance();
			if (c < lastRightClearance && c < minClear + sideExtra) {
				logger.warn("right collision warning, rel.course={}, clearance={}", course, c);
				busSend(new StopCmd("obstance to the right"));
			}
			lastRightClearance = c;			
		}
		if (course > 90 && course < 270) {
			// rear
			int c = evt.getRearClearance();
			if (c < lastRearClearance && c < minClear) {
				logger.warn("rear collision warning, rel.course={}, clearance={}", course, c);
				busSend(new StopCmd("obstance behind"));
			}
			lastRearClearance = c;			
		}
		
	}
	
	@Override
	public void onShutdown() {
	}

}
