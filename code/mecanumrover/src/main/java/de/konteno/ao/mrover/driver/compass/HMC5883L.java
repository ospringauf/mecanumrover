/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.driver.compass;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import com.pi4j.io.i2c.I2CDevice;

import de.konteno.ao.mrover.common.unit.Angle;

/**
 * compass
 * GY-271 module with Honeywell HMC5883L magnetometer on I²C bus
 * default bus address is 0x1E
 * 
 * datasheet:
 * https://aerocontent.honeywell.com/aero/common/documents/myaerospacecatalog-documents/Defense_Brochures-documents/HMC5883L_3-
 * https://learn.adafruit.com/adafruit-hmc5883l-breakout-triple-axis-magnetometer-compass-sensor/
 * 
 * Lessons learned:
 * - the magnetometer MUST be calibrated! Otherwise the readings are meaningless.
 * - there might be sophisticated calibration methods, but for simple 2D orientation, an offset/scale should do.
 * - calibration must be done in the final robot configuration, otherwise you get wrong offset/scale values.
 * - keep a safe distance (~10cm) to the motors and the raspi, because this is a magnetometer and measures magnetic fields - that means
 *   it also measures the fields of your electronic devices 
 * 
 * see 
 * https://edwardmallon.wordpress.com/2015/05/22/calibrating-any-compass-or-accelerometer-for-arduino/
 * http://www.germersogorb.de/html/kalibrierung_des_hcm5883l.html
 * 
 */
public class HMC5883L {
	// registers
	static final int REG_CONFIG_A = 0x00;
	static final int REG_CONFIG_B = 0x01;
	static final int REG_MODE     = 0x02;
	static final int REG_MSB_X    = 0x03;
	static final int REG_LSB_X    = 0x04;
	static final int REG_MSB_Z    = 0x05;
	static final int REG_LSB_Z    = 0x06;
	static final int REG_MSB_Y    = 0x07;
	static final int REG_LSB_Y    = 0x08;
	static final int REG_STATUS   = 0x09;
	static final int REG_ID_A     = 0x0a;
	static final int REG_ID_B     = 0x0b;
	static final int REG_ID_C     = 0x0c;


	static final int OVERFLOW = -4096;
	
	static final String CALIB_DATA_FILE = "mrover_compass.dat";
	
	private I2CDevice device;

	public int offsetX = -26;
	public int offsetY = 170;
	public double scaleX = 1.4006;
	public double scaleY = 1.4205;
	public double offsetDegree = -170;
	
	public short[] coords = new short[] {0, 0, 0};


	public HMC5883L(I2CDevice device, HMC5883LConfig cfg) throws IOException, InterruptedException {
		this.device = device;
		configure(cfg);
	}
	
	public void saveCalibration() {
		try {
			DataOutputStream s = new DataOutputStream(new FileOutputStream(CALIB_DATA_FILE));
			s.writeInt(offsetX);
			s.writeInt(offsetY);
			s.writeDouble(scaleX);
			s.writeDouble(scaleY);
			s.writeDouble(offsetDegree);
			s.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void loadCalibration() {
		try {
			DataInputStream s = new DataInputStream(new FileInputStream(CALIB_DATA_FILE));
			offsetX = s.readInt();
			offsetY = s.readInt();
			scaleX  = s.readDouble();
			scaleY  = s.readDouble();
			offsetDegree = s.readDouble();
			s.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	

	/**
	 * should return "H43"
	 * @return
	 * @throws IOException
	 */
	public String readId() throws IOException  {
		String id = String.valueOf(
				new char[] {  
					(char)(device.read(REG_ID_A)),
					(char)(device.read(REG_ID_B)),
					(char)(device.read(REG_ID_C)),
					});
		return id;
	}
	
	public void configure(HMC5883LConfig cfg) throws IOException, InterruptedException {
		
		// configuration register A
		device.write(REG_CONFIG_A, (byte) (cfg.getSamples() << 5 | cfg.getDatarate() << 2 | cfg.getMeasurement()));
		
		// configuration register B
		device.write(REG_CONFIG_B, (byte) (cfg.getBias() << 5));

		// mode register - set to continuous measurement for now
		device.write(REG_MODE, (byte) cfg.getMode());
		
		// wait 1000/15(Hz) = 67ms for first measurement
		//int waitMillis = (int)(3100.0 * Math.exp((cfg.getDatarate() + 1) * (-0.765)));
		int waitMillis = 67;
		Thread.sleep(waitMillis);
	}
	
	
	/**
	 * Read three signed 16bit values (x, z, y) in 2's complement from the 6 value registers.
	 * Value range on each axis is 0xF800 (-2048) to 0x7FF (2047). 
	 * @return the magnetic field vector
	 * @throws IOException
	 */
	public short[] readXzy() throws IOException {
		int msb, lsb;
		msb = device.read(REG_MSB_X);
		lsb = device.read(REG_LSB_X);
		short x = (short) (msb << 8 | lsb);
		
		msb = device.read(REG_MSB_Z);
		lsb = device.read(REG_LSB_Z);
		short z = (short) (msb << 8 | lsb);

		msb = device.read(REG_MSB_Y);
		lsb = device.read(REG_LSB_Y);
		short y = (short) (msb << 8 | lsb);

		// TODO detect overflow - any register contains -4096
//		boolean overflow = (x == OVERFLOW) || (y == OVERFLOW) || (z == OVERFLOW);

		coords[0] = x;
		coords[1] = y;
		coords[2] = z;
		return coords;
	}
	
	
	public double readHeading() throws IOException {
		
		readXzy();
		double x = coords[0];
		double y = coords[1];
		double z = coords[2];

		// apply offset/scaling from calibration before calculating the heading angle
		 x = (x + offsetX) * scaleX;
		 y = (y + offsetY) * scaleY;
		
//		System.out.println(String.format("reading: %f %f %f", x, y, z));
		
		// acc. to http://www.germersogorb.de/html/kalibrierung_des_hcm5883l.html
//		double[][] cal_matrix= { 
//				{ 1.321002, 0.067306, -0.007366 },
//                { 0.067306, 1.346573,  0.095366 },
//                {-0.007366, 0.095366,  1.882729 } } ;
//		
//		double cal_offsets[] = {-147.340571, 88.165072, 777.491847}; 
//		
//		x += cal_offsets[0];
//		y += cal_offsets[1];
//		z += cal_offsets[2];
//		x = cal_matrix[0][0]*x + cal_matrix[0][1]*y + cal_matrix[0][2]*z;
//		y = cal_matrix[1][0]*x + cal_matrix[1][1]*y + cal_matrix[1][2]*z;
//		z = cal_matrix[2][0]*x + cal_matrix[2][1]*y + cal_matrix[2][2]*z;
//
//		System.out.println(String.format("reading: %f %f %f", x, y, z));

		double heading = Angle.convertToDegree(x, y, offsetDegree);
		
		return heading;
	}
	
//	public double convertToDegree(double x, double y) {
//		return (Math.atan2(y, x) * (180.0 / Math.PI) + 720 + offsetDegree) % 360.0;
//	}
	

	// TODO löschen?
//	public void calibrate() throws IOException, InterruptedException {
//		System.out.println("Point compass to NORTH, then rotate at least one full turn. You have 30s.");
//		System.out.println("Press Enter key to start");
//		System.in.read();
//		
//		// auto-calibration: collect values of (at least) one full turn ...
//		ArrayList<short[]> points = new ArrayList<short[]>();
//		for (int i=0; i<300; ++i) {
//			short[] coords = readXzy();
//			// TODO for use of magneto.exe, all values should be scaled by *100000/1090
//			System.out.println(String.format("%d\t%d\t%d", coords[0], coords[1], coords[2]));
//			points.add(coords);
//			Thread.sleep(100);
//		}
//		
//		// ... and calc offsets / scaling factors for X and Y axes ("hard iron" calibration)   
//		
//		OptionalInt xmax = points.stream().mapToInt(p -> p[0]).max();
//		OptionalInt xmin = points.stream().mapToInt(p -> p[0]).min();
//		offsetX = (int)((-1) * ((xmax.getAsInt() + xmin.getAsInt()) / 2.0));
//		scaleX = 1000.0/(xmax.getAsInt() - xmin.getAsInt());
//		
//		OptionalInt ymax = points.stream().mapToInt(p -> p[1]).max();
//		OptionalInt ymin = points.stream().mapToInt(p -> p[1]).min();
//		offsetY = (int)((-1) * ((ymax.getAsInt() + ymin.getAsInt()) / 2.0));
//		scaleY = 1000.0/(ymax.getAsInt() - ymin.getAsInt());
//
//		offsetDegree = 0;
//		double north = convertToDegree(
//				(points.get(0)[0] + offsetX) * scaleX, 
//				(points.get(0)[1] + offsetY) * scaleY);
//		offsetDegree = (-1) * north;
//
//		System.out.println(String.format("x = (x + %d) * %3.4f", offsetX, scaleX));
//		System.out.println(String.format("y = (y + %d) * %3.4f", offsetY, scaleY));
//		System.out.println(String.format("north offset = %3.1f", offsetDegree));
//
//		
//		System.out.println("press Enter key to continue");
//		System.in.read();
//	}
	
	/**
	 * this is an attempt to implement the self-test feature from the HMC8553L datasheet,
	 * but I'm not sure if this works as it's supposed to ...
	 */
	public void selfTest() throws IOException, InterruptedException {
		System.out.println("self test");
		device.write(REG_CONFIG_A, (byte) 0x71); // 0111 0001 = 8 samples, 15Hz, pos. bias
		device.write(REG_CONFIG_B, (byte) 0xA0); // 1010 0000 = GAIN_5 4.7Ga
		device.write(REG_MODE, (byte) 0x00);
		
		Thread.sleep(6);
		for (int i=0; i<10; ++i) {
			short[] coords = readXzy();
			System.out.println(String.format("%d\t%d\t%d", coords[0], coords[1], coords[2]));
			device.write((byte) 0x03);

			Thread.sleep(67);
		}
		// TODO for GAIN_5, all values should be in range 243..575
		
		// exit self test mode
		device.write(REG_CONFIG_A, (byte) 0x70);
		
	}

}
