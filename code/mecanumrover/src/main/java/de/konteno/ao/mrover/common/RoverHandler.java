/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.common;

import io.vertx.core.Future;

public interface RoverHandler<T extends IRoverMessage> {

  /**
   * Something has happened, so handle it.
   *
   * @param event  the event to handle
   * @param msg the wrapper message (so that we can reply later)
   */
  Future<Void> process(T event);
  
}
