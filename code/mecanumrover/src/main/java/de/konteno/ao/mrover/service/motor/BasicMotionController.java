/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.service.motor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.konteno.ao.mrover.common.unit.Angle;
import de.konteno.ao.mrover.driver.motor.AdafruitMotor;
import de.konteno.ao.mrover.driver.motor.AdafruitMotorControl;
import de.konteno.ao.mrover.driver.motor.MotorGroups;

/**
 * implements basic movements:
 * - translation (fwd, back, left, right, angles)
 * - rotation (tangential, radial)
 * - stop (brake, release)
 * 
 * for a good description of mecanum motion
 * see http://wiki.seeed.cc/4WD_Mecanum_Wheel_Robot_Kit_Series/#Introduction
 *
 */
public class BasicMotionController implements BasicMotion {
	
	private static final Logger logger = LoggerFactory.getLogger( BasicMotionController.class );

	private AdafruitMotorControl motcontrol;
	private MotorGroups motorgroups;

	public BasicMotionController(AdafruitMotorControl motcontrol) {
		this.motcontrol = motcontrol;
		motorgroups = new MotorGroups(motcontrol);
	}
	
	private double rem(double x) {
		return (100.0-Math.abs(x))/100.0;
	}
	
	/**
	 * Set power scaling factors for individual motors. All factors must be normalized to 0..1
	 * @param lr percent powert shift to left (<0) or right (>0)
	 * @param fb percent powert shift to front (<0) or back (>0)
	 */
	@Override
	public void distributePower(double lr, double fb) {
		double l = (lr <= 0) ? 1.0 : rem(lr);
		double r = (lr >= 0) ? 1.0 : rem(lr);
		double f = (fb <= 0) ? 1.0 : rem(fb);
		double b = (fb >= 0) ? 1.0 : rem(fb);
		logger.debug("motor power factors: {} {} {} {}", f*l, f*r, b*l, b*r);
		motcontrol.getMotor(0).speedScale = f*l;
		motcontrol.getMotor(1).speedScale = f*r;
		motcontrol.getMotor(2).speedScale = b*l;
		motcontrol.getMotor(3).speedScale = b*r;
	}
	
//	/**
//	 * same as rotateRadial(speed, 100)
//	 */
//	public void rotate(double speed) throws Exception {
//		motorgroups.getLeft().go( speed);
//		motorgroups.getRight().go(-speed);
//	}

	public void rotateTangential(double speed, double steering) throws Exception {
		// calc counter-rotation
		//   0 -> opp=speed, forward or back
		//  50 -> opp=0
		// 100 -> opposite speed (-1*speed)
		double opp = speed * (1.0 - 2 * Math.abs(steering) / 100); 
		double left = steering >= 0 ? speed : opp;
		double right = steering >= 0 ? opp : speed;
		motorgroups.getLeft().go(left);
		motorgroups.getRight().go(right);
	}
	

	// looks good in first tests
	public void rotateRadial(double speed, double steering) throws Exception {
		logger.trace("radial speed=" + speed + "  steer=" + steering); 
		// 0   -> front=rear, resulting in straight movement to the right or left
		// 50  -> rear=0 (v>0) or front=0 (v<0)
		// 100 -> opposite speed, front+rear=0, turning on the spot
		// front=rear ==> equivalent to right (v>0) or left (v<0)
		double opp = speed * (1.0 - 2 * Math.abs(steering) / 100); 
		double front = steering >= 0 ? speed : opp;
		double rear= steering >= 0 ? opp : speed;

		motcontrol.getMotor(0).go( front); // front left
		motcontrol.getMotor(1).go(-front); // front right
		motcontrol.getMotor(2).go(-rear);  // rear left
		motcontrol.getMotor(3).go( rear);  // rear right
	}
	
	/**
	 * calculate speed for each diagonal by some rule of thumb.
	 * at 0°, all wheels go forward at 71%
	 */
	public void translate(Angle course, double speed) throws Exception {
		double d03 = Math.sin(Angle.fromDegree(course.toDegree() + 45).toRad());
		double d12 = Math.sin(Angle.fromDegree(course.toDegree() + 135).toRad());

		motorgroups.getD03().go(speed * d03);
		motorgroups.getD12().go(speed * d12);
	}
	
	
	@Override
	public void brake() throws Exception {
		motorgroups.getAll().run(AdafruitMotor.BRAKE);		
	}

	@Override
	public void release() throws Exception {
		motcontrol.stopAllMotors();		
	}

}
