/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.common.cmd;

import de.konteno.ao.mrover.common.Rover;
import de.konteno.ao.mrover.common.RoverTopic;
import de.konteno.ao.mrover.common.unit.Angle;

@RoverTopic(address="cmd.drive")
public class DriveCmd extends TimerCmdBase implements IMotionCommand {

	private Angle course;
	private double speed; // TODO eigener Datentyp für Geschwindigkeit (0..1)
	
	public DriveCmd(Angle a, double v) {
		this(a, v, 0);
	}

	public DriveCmd(Angle a) {
		this(a, Rover.speedLimit, 0);
	}
	
	public static DriveCmd forward(double v) {
		return new DriveCmd(Angle.fromDegree(0), v);
	}
	
	public static DriveCmd right(double v) {
		return new DriveCmd(Angle.fromDegree(90), v);
	}

	public static DriveCmd back(double v) {
		return new DriveCmd(Angle.fromDegree(180), v);
	}
	
	public static DriveCmd left(double v) {
		return new DriveCmd(Angle.fromDegree(270), v);
	}

	
	public DriveCmd(Angle a, double v, long time) {
		course = a;
		speed = v;
		timer = time;
	}

	public DriveCmd(Angle a, long time) {
		this(a, Rover.speedLimit, time);
	}

	
	public Angle getCourse() {
		return course;
	}

	public void setCourse(Angle course) {
		this.course = course;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	@Override
	public String toString() {
		return String.format("DRIVE(%3.0f @ %1.2f for %4d)!", course.toDegree(), speed, timer);
	}
	
	/**
	 * calculate effective relative course for translation
	 * @return direction of travel in the rover coordinate system
	 */
	public Angle relativeCourse() {
		return (speed >= 0) ? course : course.opposite();
	}

	@Override
	public double estimateDx(long t) {
		//return (((39 * speed) - 8) * Math.sin(course.toRad()) * t) / 1000;
		return (((34 * speed) - 8) * Math.sin(course.toRad()) * t) / 1000;
	}

	@Override
	public double estimateDy(long t) {
		return (((39 * speed) - 8) * Math.cos(course.toRad()) * t) / 1000;
	}

	@Override
	public double estimateDalpha(long t) {
		return 0;
	}
}
