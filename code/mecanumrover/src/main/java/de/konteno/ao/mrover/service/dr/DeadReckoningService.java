/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.service.dr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.konteno.ao.mrover.common.cmd.DriveCmd;
import de.konteno.ao.mrover.common.cmd.IMotionCommand;
import de.konteno.ao.mrover.common.cmd.ResetCmd;
import de.konteno.ao.mrover.common.cmd.RotateCmd;
import de.konteno.ao.mrover.common.cmd.StopCmd;
import de.konteno.ao.mrover.common.track.Pose;
import de.konteno.ao.mrover.common.track.TrackType;
import de.konteno.ao.mrover.common.unit.Angle;
import de.konteno.ao.mrover.common.unit.Pos;
import de.konteno.ao.mrover.service.MyVerticleBase;

public class DeadReckoningService extends MyVerticleBase {

	private static final Logger logger = LoggerFactory.getLogger( DeadReckoningService.class );
	private IMotionCommand current = new StopCmd();
	private long timer;
	private Pose last = new Pose(Pos.ZERO, Angle.fromDegree(0), Angle.fromDegree(0), TrackType.DEAD_RECK);
	

	@Override
	public void start() throws Exception {		
		super.start();
		
		// listen to motion commands
		busCmdListen(DriveCmd.class, this::changeMotion);
		busCmdListen(RotateCmd.class, this::changeMotion);
		busCmdListen(StopCmd.class, this::changeMotion);
		busCmdListen(ResetCmd.class, this::reset);
		
		// periodic update
		timer = vertx.setPeriodic(100, v -> update());
	}	
	
	private void reset(ResetCmd cmd) {
		last = new Pose(Pos.ZERO, Angle.fromDegree(0), Angle.fromDegree(0), TrackType.DEAD_RECK);
	}
	
	private void update() {
		if (current instanceof StopCmd) {
			return;
		}
		
		// estimate motion in x/y direction based on elapsed time
		long dt = System.currentTimeMillis() - last.time;
		double dx0 = current.estimateDx(dt);
		double dy0 = current.estimateDy(dt);
		double da = current.estimateDalpha(dt);
		
//		logger.debug("{} - dx={}  dy={}  da={}", current, dx0, dy0, da);
		
		Angle a = last.heading.addDegree(da);
		
		// rotate motion vector by old heading (positive = clockwise!)
		Pose w = last.addRelative(Pos.of(dx0, dy0), a, current.relativeCourse());
		
		busPublish(w);
		last = w;
	}


	private void changeMotion(IMotionCommand cmd) {
		if (cmd != current) {
			update();
		}
		current = cmd;
		last.time = System.currentTimeMillis();
	}
	
	@Override
	public void onShutdown() {
		vertx.cancelTimer(timer);
	}

}
