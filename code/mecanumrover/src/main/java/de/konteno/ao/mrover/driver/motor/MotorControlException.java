/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.driver.motor;

public class MotorControlException extends Exception {

	private static final long serialVersionUID = 2028922919034816217L;

	public MotorControlException() {
		super();
	}
	
	public MotorControlException(String string) {
		super(string);
	}

	public MotorControlException(Throwable e) {
		super(e);
	}

	public MotorControlException(String s, Throwable e) {
		super(s, e);
	}

}
