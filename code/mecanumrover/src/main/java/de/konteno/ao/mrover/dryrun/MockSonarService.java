/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package de.konteno.ao.mrover.dryrun;

import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.konteno.ao.mrover.common.Const;
import de.konteno.ao.mrover.common.event.SonarDistanceEvt;
import de.konteno.ao.mrover.common.track.Pose;
import de.konteno.ao.mrover.common.track.TrackType;
import de.konteno.ao.mrover.common.unit.Angle;
import de.konteno.ao.mrover.common.unit.Pos;
import de.konteno.ao.mrover.service.MyVerticleBase;

/**
 * simulate sonar sensors by finding the intersection of the "beams" with the "walls"
 *
 */
public class MockSonarService extends MyVerticleBase {

	private static final Logger logger = LoggerFactory.getLogger(MockSonarService.class);

	private int currentSensor = 0;

	Pose pose = new Pose(Pos.of(0, 0), Angle.fromDegree(0), Angle.fromDegree(0), TrackType.DEAD_RECK);
	
	int[] dist = new int[] { 150, 150, 150, 150, 150, 150, 150, 150 };
	
	Line2D.Double[] sonarLine = new Line2D.Double[8];
	
	final double a = 180;
	Line2D.Double[] walls = new Line2D.Double[] {
		new Line2D.Double(-a, -a, -a,  a),
		new Line2D.Double(-a,  a,  a,  a),
		new Line2D.Double( a,  a,  a, -a),
		new Line2D.Double( a, -a, -a, -a),
		
		new Line2D.Double( a/2, a/2, a/2, a),
		new Line2D.Double( a/2, a/2, a, a/2)
	};
	
	@Override
	public void start() {
		// periodic sonar readings
		vertx.setPeriodic(25, v -> measure(v));
		
		// publish distances 
		vertx.setPeriodic(60, v -> publish(v));
		
		// monitor DR position
		busListenStream(Pose.class)
		.filter(x -> x.type == TrackType.DEAD_RECK)
		.subscribe(x -> pose = x);
		
		// setup sonar "beams"
		for (int i=0; i<8; ++i) {
			Double p = Const.sonarPosition[i];
			if (i==0||i==3||i==4||i==7) {
				sonarLine[i] = new Line2D.Double(p.x, p.y, p.x + Math.signum(p.x)*Const.SONAR_SENSOR_LIMIT, p.y);
			}
			else {
				sonarLine[i] = new Line2D.Double(p.x, p.y, p.x, p.y + Math.signum(p.y)*Const.SONAR_SENSOR_LIMIT);
			}
		}
	}
	
	
	/**
	 * calculate distance for the current sensor
	 * @param v
	 */
	private void measure_center(Long v) {
		// transform current sonar line according to rover position ...
		AffineTransform t = AffineTransform.getTranslateInstance(pose.pos.x, pose.pos.y);
		// ... and heading
		t.rotate(-pose.heading.toRad());
		
		Line2D.Double l = transformLine(t, sonarLine[currentSensor]);
		
		dist[currentSensor] = Const.SONAR_SENSOR_LIMIT;
		for (Line2D.Double wall : walls) {
			if (l.intersectsLine(wall)) {
				Point2D s = getIntersection(l, wall);
				double d = l.getP1().distance(s);
				d = Math.min(d, Const.SONAR_SENSOR_LIMIT);
				dist[currentSensor] = (int) d;
			}
		}
		
		currentSensor++;
		currentSensor %= 8;
	}
	
	
	private Line2D.Double transformLine(AffineTransform t, Line2D.Double l) {
		Point2D p1 = new Point2D.Double();
		Point2D p2 = new Point2D.Double();
		t.transform(l.getP1(), p1);
		t.transform(l.getP2(), p2);
		Line2D.Double l2 = new Line2D.Double(p1, p2);
		return l2;
	}
	
	private Line2D.Double rotateAtP1(double angle, Line2D.Double l) {
//		AffineTransform t = AffineTransform.getTranslateInstance(-l.getP1().getX(), -l.getP1().getY());
//		l = transformLine(t, l);
		AffineTransform t = AffineTransform.getRotateInstance(angle, l.getP1().getX(), l.getP1().getY());
		return transformLine(t, l);
	}
	
	/**
	 * closest wall point over beam width (heading +/- 15 deg)
	 * TODO does not work yet
	 * @param v
	 */
	private void measure(Long v) {
		// transform current sonar line according to rover position ...
		AffineTransform t = AffineTransform.getTranslateInstance(pose.pos.x, pose.pos.y);
		// ... and heading
		t.rotate(-pose.heading.toRad());
		
		double sbw = Angle.fromDegree(Const.sonarBeamWidth).toRad();

		// left/right beam
		Line2D.Double l1 = rotateAtP1(-sbw/2, transformLine(t, sonarLine[currentSensor]));
		Line2D.Double l2 = rotateAtP1( sbw/2, transformLine(t, sonarLine[currentSensor]));


		double d = Const.SONAR_SENSOR_LIMIT;
		for (Line2D.Double wall : walls) {
			
			double d1 = Const.SONAR_SENSOR_LIMIT;
			double d2 = Const.SONAR_SENSOR_LIMIT;
			if (l1.intersectsLine(wall)) {
				Point2D s = getIntersection(l1, wall);
				d1 = l1.getP1().distance(s);
			}
			if (l2.intersectsLine(wall)) {
				Point2D s = getIntersection(l2, wall);
				d2 = l2.getP1().distance(s);
			}
			
//			if (d1 < Const.DISTANCE_SENSOR_LIMIT || d2 < Const.DISTANCE_SENSOR_LIMIT) {
//				double d = Math.min(d1, d2);
//				dist[currentSensor] = (int) d;
//			}
			d = Math.min(d, Math.min(d1, d2));
		}
		dist[currentSensor] = (int) d;
		
		currentSensor++;
		currentSensor %= 8;
	}
	
	private void publish(Long v) {
		SonarDistanceEvt evt = new SonarDistanceEvt(dist);
		
//		logger.debug(evt.mqttString());
		busPublish(evt);		
	}
	

    public static Point2D getIntersection(final Line2D.Double line1, final Line2D.Double line2) {

        final double x1,y1, x2,y2, x3,y3, x4,y4;
        x1 = line1.x1; y1 = line1.y1; x2 = line1.x2; y2 = line1.y2;
        x3 = line2.x1; y3 = line2.y1; x4 = line2.x2; y4 = line2.y2;
        final double x = (
                (x2 - x1)*(x3*y4 - x4*y3) - (x4 - x3)*(x1*y2 - x2*y1)
                ) /
                (
                (x1 - x2)*(y3 - y4) - (y1 - y2)*(x3 - x4)
                );
        final double y = (
                (y3 - y4)*(x1*y2 - x2*y1) - (y1 - y2)*(x3*y4 - x4*y3)
                ) /
                (
                (x1 - x2)*(y3 - y4) - (y1 - y2)*(x3 - x4)
                );

        return new Point2D.Double(x, y);

    }

	@Override
	public void onShutdown() {
		// TODO Auto-generated method stub

	}

}
