/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.driver.tft;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferUShort;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.spi.SpiDevice;

/**
 * 1.8" TFT display, 160x128 pixels, based on ST7735 controller 
 * driver for 4-wire SPI communication
 *
 * note:
 * - 4-wire SPI mode uses extra command/data signal line (C=low/D=high). 
 *   Arguments to commands must be sent in "data" mode.
 * - This implementation is for 16bit 5-6-5 RGB mode. 
 *   You probably won't see a difference between 64k (16bit) and 262k (18bit) colors anyway. 
 * - use default SPI mode (0)
 * - frame rates depend on SPI clock speed: 1.9@1MHz, 13.8@8MHz, 24.5@16MHz, 40@32MHz
 *   
 * datasheet: http://www.displayfuture.com/Display/datasheet/controller/ST7735.pdf
 * 
 * based on the luma.lcd python code at
 * https://github.com/rm-hull/luma.lcd/blob/master/luma/lcd/device.py
 * https://github.com/rm-hull/luma.core/blob/master/luma/core/device.py
 * https://github.com/rm-hull/luma.core/blob/master/luma/core/interface/serial.py
 * 
 * working test with luma.examples:
 *   python examples/bounce.py -d st7735 -i spi --width=160 --height=128 --gpio-data-command=23 --gpio-reset=24 --spi-bus-speed=32000000
 */
public class ST7735 {

	private SpiDevice device;
	private GpioPinDigitalOutput cd;
	private GpioPinDigitalOutput rst;
	
	public int WIDTH = 160;
	public int HEIGHT = 128;
	
	// CMD_COLMOD (0x3A) pixel formats
	public static final int FORMAT_12BIT_444 = 0x03; // ch 9.8.20
	public static final int FORMAT_16BIT_565 = 0x05; // ch 9.8.21
	public static final int FORMAT_18BIT_666 = 0x06; // ch 9.8.22
	
	public static final int ORDER_RGB = 0x00;
	public static final int ORDER_BGR = 0x08;
	
	public static final int CMD_SWRESET = 0x01; // 10.1.2 SWRESET (01h): Software Reset 
	public static final int CMD_RDDID = 0x04; // 10.1.3 RDDID (04h): Read Display ID  
	public static final int CMD_RDDST = 0x09; // 10.1.4 RDDST (09h): Read Display Status 
	// ... more read commands ...
	public static final int CMD_SLPOUT = 0x11; // 10.1.11 SLPOUT (11h): Sleep Out 
	
	public static final int CMD_PTLON = 0x12; // 10.1.12 PTLON (12h): Partial Display Mode On
	public static final int CMD_NORON = 0x13; // 10.1.13 NORON (13h): Normal Display Mode On 
	public static final int CMD_INVOFF = 0x20; // 10.1.14 INVOFF (20h): Display Inversion Off
	// 10.1.15 INVON (21h): Display Inversion On
	
	public static final int CMD_DISPOFF = 0x28; // 10.1.17 DISPOFF (28h): Display Off 
	public static final int CMD_DISPON = 0x29; // 10.1.18 DISPON (29h): Display On 
	
	public static final int CMD_CASET = 0x2A; // 10.1.19 CASET (2Ah): Column Address Set
	public static final int CMD_RASET = 0x2B; // 10.1.20 RASET (2Bh): Row Address Set
	public static final int CMD_RAMWR = 0x2C; // 10.1.21 RAMWR (2Ch): Memory Write

	public static final int CMD_MADCTL = 0x36; // 10.1.27 MADCTL (36h): Memory Data Access Control
	public static final int CMD_COLMOD = 0x3A; // 10.1.30 COLMOD (3Ah): Interface Pixel Format 

	public static final int CMD_FRMCTR1 = 0xB1; // 10.2.1 Frame Rate Control (In normal mode/ Full colors) 
	public static final int CMD_FRMCTR2 = 0xB2; // 10.2.2 Frame Rate Control (In Idle mode/ 8-colors)  
	public static final int CMD_FRMCTR3 = 0xB3; // 10.2.3 Frame Rate Control (In Partial mode/ full colors)
	public static final int CMD_INVCTR = 0xB4; // 10.2.4 Display Inversion Control 
	
	public static final int CMD_PWCTR1 = 0xC0; // 10.2.5
	public static final int CMD_PWCTR2 = 0xC1; // 10.2.6
	public static final int CMD_PWCTR3 = 0xC2; // 10.2.7
	public static final int CMD_PWCTR4 = 0xC3; // 10.2.8
	public static final int CMD_PWCTR5 = 0xC4; // 10.2.9
	public static final int CMD_VMCTR1 = 0xC5; // 10.2.10

	public static final int CMD_GMCTRP1 = 0xE0; // 10.2.17 GMCTRP1 (E0h): Gamma (‘+’polarity) Correction Characteristics Setting 
	public static final int CMD_GMCTRN1 = 0xE1; // 10.2.18 GMCTRN1 (E1h): Gamma (‘-’polarity) Correction Characteristics Setting 	
	
	
	private BufferedImage img;
	private Graphics2D graphics;
	private DataBufferUShort imagebuffer;
	private ByteBuffer sendbuffer;

	/**
	 * 
	 * @param dev SPI device
	 * @param cd command/data pin (mandatory)
	 * @param rst reset pin (optional)
	 */
	public ST7735(SpiDevice dev, GpioPinDigitalOutput cd, GpioPinDigitalOutput rst, int w, int h) {
		this.device = dev;
		this.cd = cd;
		this.rst = rst;
		this.WIDTH = w;
		this.HEIGHT = h;
		
		setupGraphics();
		try {
			if (rst != null) {
				reset();
			}
			init();
			displayOn();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

//	private void readId() throws IOException {
//		//byte[] d = command(CMD_RDDID);
//		byte[] d = command(CMD_RDDID, new int[] { 0, 0, 0, 0});
//		System.out.println("read id - " + formatBytes(d));
//		
//	}

	private void reset() throws InterruptedException {
		rst.high();		
		Thread.sleep(30);
		rst.low();
		Thread.sleep(30);
		rst.high();		
		Thread.sleep(30);
	}


	/**
	 * 9.4.1
		Command Write Mode
		The write mode of the interface means the micro controller writes commands and data to the LCD driver. 3-lines serial data
		packet contains a control bit D/CX and a transmission byte. In 4-lines serial interface, data packet contains just
		transmission byte and control bit D/CX is transferred by the D/CX pin. If D/CX is “low”, the transmission byte is interpreted
		as a command byte. If D/CX is “high”, the transmission byte is stored in the display data RAM (memory write command), or
		command register as parameter.
	 * @param cmd
	 * @param args
	 * @return
	 * @throws IOException
	 */
	protected void command(int cmd, int[] args) throws IOException {
		byte[] b = new byte[args.length];
		for (int i = 0; i < args.length; ++i) {
			b[i] = (byte) (args[i] & 0xFF);
		}
		command(cmd);
		data(b, args.length);
	}

	protected byte[] command(int cmd) throws IOException {
		cd.low();
//		System.out.println("cmd="+cmd);
		byte[] d = device.write((byte) cmd);
//		System.out.println(" - got " + formatBytes(d));
		return d;
	}

	private String formatBytes(byte[] d) {
		if (d == null) {
			return "null";
		}
		String s = "";
		for (byte b : d) {
			s+=b + " ";
		}
		if (s.length() > 60) {
			s = s.substring(0,  60);
		}
		return "" + d.length + "[" + s + "]";
	}

	protected void data(byte[] data, int count) throws IOException {
		cd.high();
		
		int i = 0;
		while (i < count) {
			int n = Math.min(count - i, SpiDevice.MAX_SUPPORTED_BYTES);
			device.write(data, i, n);
			i += n;
		}
	}
	
	
	protected void setupGraphics() {
		// AWT's 5-6-5 RGB image fits the display's pixel structure in 16bit mode
		img =  new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_USHORT_565_RGB);
		graphics = img.createGraphics();

		
		WritableRaster r = img.getRaster();
		imagebuffer = (DataBufferUShort)r.getDataBuffer();

		sendbuffer = ByteBuffer.allocate(imagebuffer.getData().length * 2);
		sendbuffer.order(ByteOrder.BIG_ENDIAN);
	}
	
	/**
	 * Clears the buffered image.
	 * 
	 * @param display clear display (send empty image to device)
	 * @throws IOException
	 */
	public void clear(boolean display) throws IOException {
		Arrays.fill(imagebuffer.getData(), (short) 0);
		if (display) {
			displayGraphics();
		}
	}
	

	public Graphics2D getGraphics() {
		return graphics;
	}


	/**
	 * Send buffered image data to display.
	 * Currently, the entire image ist transferred. Partial/window mode not (yet) implemented. 
	 * 
	 * @throws IOException
	 */
	public void displayGraphics() throws IOException {
		int left = 0;
		int right = WIDTH;
		int top = 0;
		int bottom = HEIGHT;
		command(CMD_CASET, new int[] { left >> 8, left & 0xFF, (right - 1) >> 8,  (right - 1) & 0xFF }); // Set column addr
		command(CMD_RASET, new int[] { top >> 8,  top & 0xFF,  (bottom - 1) >> 8, (bottom - 1) & 0xFF }); // Set row addr
		command(CMD_RAMWR);
        
		// convert short[] to byte[] and send to display (565 = 2 byte/pixel)
		sendbuffer.asShortBuffer().put(imagebuffer.getData());
		byte[] bytes = sendbuffer.array();				
		data(bytes, bytes.length);
	}
	
	
	public void init() throws Exception {
		command(CMD_SWRESET); // reset
        command(CMD_SLPOUT); // sleep out & booster on
        
        command(CMD_FRMCTR1, new int[] {0x01, 0x2C, 0x2D});   // frame rate control: normal mode
        command(CMD_FRMCTR2, new int[] {0x01, 0x2C, 0x2D});   //frame rate control: idle mode
        command(CMD_FRMCTR3, new int[] {
        			0x01, 0x2C, 0x2D,    // frame rate control: partial mode dot inversion mode
                    0x01, 0x2C, 0x2D});  // frame rate control: line inversion mode
        command(CMD_INVCTR, new int[] {0x07});                // display inversion: none
        
        command(CMD_PWCTR1, new int[] {0xA2, 0x02, 0x84});    // power control 1: -4.6V auto mode
        command(CMD_PWCTR2, new int[] {0xC5});                // power control 2: VGH
        command(CMD_PWCTR3, new int[] {0x0A, 0x00});          // power control 3: OpAmp current small, boost freq
        command(CMD_PWCTR4, new int[] {0x8A, 0x2A});          // power control 4: BCLK/2, Opamp current small & Medium low
        command(CMD_PWCTR5, new int[] {0x8A, 0xEE});          // power control 5: partial mode/full-color
        
        command(CMD_VMCTR1, new int[] {0x0E});                // VCOM Control 1
        command(CMD_MADCTL, new int[] {0x60 | ORDER_RGB});    // memory data access control
        command(CMD_INVOFF);                                  // display inversion off
        command(CMD_COLMOD, new int[] {FORMAT_16BIT_565});    // interface pixel format
        command(CMD_NORON);                                   // partial off (normal)
        
        command(CMD_GMCTRP1, new int[] {                      // gamma adjustment (+ polarity)
                     0x0F, 0x1A, 0x0F, 0x18, 0x2F, 0x28, 0x20, 0x22,
                     0x1F, 0x1B, 0x23, 0x37, 0x00, 0x07, 0x02, 0x10}); 
        command(CMD_GMCTRN1, new int[] {                      // gamma adjustment (- polarity)
                     0x0F, 0x1B, 0x0F, 0x17, 0x33, 0x2C, 0x29, 0x2E,
                     0x30, 0x30, 0x39, 0x3F, 0x00, 0x07, 0x03, 0x10});		
	}

	public void displayOn() throws IOException {
		command(CMD_DISPON);
	}

	public void displayOff() throws IOException {
		command(CMD_DISPOFF);
	}

	public void test1() {
		try {
			Graphics2D g = this.getGraphics();
			this.clear(true);

			g.setColor(Color.RED); g.drawLine(0, 0, WIDTH, 0);
			g.setColor(Color.GREEN); g.drawLine(0, 10, WIDTH, 10);
			g.setColor(Color.BLUE); g.drawLine(0, 15, WIDTH, 15);
			g.setColor(Color.WHITE); g.drawLine(0, 20, WIDTH, 20);
	        this.displayGraphics();
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}			
	}
	
	public void test2() {
		try {
			Graphics2D g = this.getGraphics();
		
			// smile
	        g.setFont(new Font(Font.DIALOG, Font.PLAIN, 84));
			g.drawString("\u263a", 62, 75);
	        this.displayGraphics();
			
			Thread.sleep(2000);
			this.clear(false);
					
	        g.setFont(new Font(Font.DIALOG, Font.PLAIN, 10));
			g.drawRect(0, 0, 127, 63);
			g.drawString("Mecanumrover ***", 2, 9);
			g.drawString("Hi there!", 2, 19);
			//g.drawString((new Date()).toString(), 3, 21);			

	        this.displayGraphics();
		}
		catch (Exception e) {
			e.printStackTrace();
		}			
	}
	
	public void test3() {
		try {
			Graphics2D g = this.getGraphics();
			
			long s = System.currentTimeMillis();
			for (int i=0; i<1000; ++i) {
				this.clear(false);
				
				g.drawLine(0, i % HEIGHT, WIDTH, i % HEIGHT);
				g.drawLine(i % WIDTH, 0, i % WIDTH, HEIGHT);
				
				// current frame rate
				float fr = (float)(1000.0*i) / (System.currentTimeMillis() - s);
				g.drawString(String.format("%3.1f",  fr), 10, 10);
				
		        this.displayGraphics();
			}

		}
		catch (Exception e) {
			e.printStackTrace();
		}			
	}


}
