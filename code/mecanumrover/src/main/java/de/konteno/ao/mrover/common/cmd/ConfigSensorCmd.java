/*********************************************************************
* Copyright (c) 2018-2019 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package de.konteno.ao.mrover.common.cmd;

import de.konteno.ao.mrover.common.RoverTopic;

/**
 * change the configuration of ESP32 sensors
 *
 */
@RoverTopic(address = "cmd.sensorcfg")
public class ConfigSensorCmd implements IRoverCommand {
	
	public final static int MODE_IDLE = 0;
	public final static int MODE_ODO = 1;
	public final static int MODE_SONAR = 2;
	public final static int MODE_LIDAR = 4;
	public final static int MODE_COMPASS = 8;
	
//	public int sonarMask = 0b00000110; // TODO switch on sonars as needed, or always read all sonars
	public int mode;

	public ConfigSensorCmd(int mode) {
		this.mode = mode;
	}

//	public ConfigSensorCmd(int mode, int mask) {
//		this.mode = mode;
//		this.sonarMask = mask;
//	}

}
