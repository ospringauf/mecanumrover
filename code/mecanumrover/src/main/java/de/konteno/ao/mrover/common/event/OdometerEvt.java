/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.common.event;

import de.konteno.ao.mrover.common.Const;
import de.konteno.ao.mrover.common.RoverTopic;

@RoverTopic(address = "evt.odo")
public class OdometerEvt implements IRoverEvent {

	// counts in x/y direction
	public double countX;	
	public double countY;
	
	// signal quality
	public int sigQual;

	// verbose mode -> separate events
	public OdometerEvt left;
	public OdometerEvt right;

	/**
	 * short payload format is "{dx} {dy} {sq}" where dx, dy are signed floats and
	 * sq is the (integer) signal quality
	 * 
	 * verbose payload format is "{dx1} {dy1} {sq1} {dx2} {dy2} {sq2}" with separate
	 * values for each sensor
	 * 
	 * @param payload
	 */
	public static OdometerEvt parse(String payload) {
		OdometerEvt e = new OdometerEvt();
		String[] f = payload.trim().split("\\s+");

		if (f.length == 6) {
			// verbose mode {dx1} {dy1} {sq1} {dx2} {dy2} {sq2}
			// build 2 sub-events
			e.left = OdometerEvt.parse(f[0] + " " + f[1] + " " + f[2]);
			e.right = OdometerEvt.parse(f[3] + " " + f[4] + " " + f[5]);

			// weighted average
			e.setAverage();
		} else {
			// short mode {dx} {dy} {sq}
			e.countX = Double.parseDouble(f[0]);
			e.countY = Double.parseDouble(f[1]);
			e.sigQual = Integer.parseInt(f[2]);
		}
		return e;
	}

	private void setAverage() {
		int ssq = (left.sigQual + right.sigQual);
		if (ssq != 0) {
			countX = (left.sigQual / ssq * left.countX) + (right.sigQual / ssq * right.countX);
			countY = (left.sigQual / ssq * left.countY) + (right.sigQual / ssq * right.countY);
			sigQual = ssq / 2;
		}
	}

	/**
	 * distance in cm
	 */
	public double cmX() {
		return countX / Const.ODO_CPCM;
	}

	public double cmY() {
		return countY / Const.ODO_CPCM;
	}
}
