/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.driver.sonar;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import com.pi4j.wiringpi.Gpio;

/**
 * 
 * uses Pi4J listener to detect rising/falling edges of the ECHO signal
 * 
 * results are unsuable.
 * 
 * @author oliver
 *
 */
public class HCSR04_Async {

	private final class EchoListener implements GpioPinListenerDigital {
		long start = 0;
		public boolean enabled = false;

		@Override
		public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
			if (enabled) {
				if (event.getState() == PinState.HIGH) {
					start = System.nanoTime();
				} else {
					enabled = false;
					long n = System.nanoTime() - start;
			        // note that the sound signal travels 2x the distance (outbound + return)
					double d = n / 1e9 * SOUND_SPEED / 2.0;
					System.out.println(String.format("echo LOW after %dus, d = %3.1f", n/1000, d));
				}
			}
		}
	}



	private GpioPinDigitalInput echoPin;
	private GpioPinDigitalOutput trigPin;
	private EchoListener listener;
	
	private final static float SOUND_SPEED = 34029; // speed of sound in cm/s

	private final static int TRIGGER_MICROS = 10; // trigger duration of 10us
	private final static int TIMEOUT_MILLIS = 60; // wait 60ms

	public HCSR04_Async(Pin trig, Pin echo) {
		GpioController gpio = GpioFactory.getInstance();
		this.echoPin = gpio.provisionDigitalInputPin(echo);
		this.trigPin = gpio.provisionDigitalOutputPin(trig);
		this.trigPin.low();

	
		listener = new EchoListener();
		
    	echoPin.addListener(listener);
}

    /*
     * This method returns the distance measured by the sensor in cm
     */
    public void measureDistance() {
        this.trigger();
        listener.enabled = true;
    }

    /**
     * Put a high on the trig pin for TRIG_DURATION_IN_MICROS
     * 
     * signal measured with piscope:
     * busy wait: typically 40-50us
     * thread sleep: typically 1000-1200us
     */
    private void trigger() {
		trigPin.high();
		
		Gpio.delayMicroseconds(10);
		
		trigPin.low();
	}
    
  
    public void shutdown() {
		GpioController gpio = GpioFactory.getInstance();
		gpio.unprovisionPin(echoPin);
		gpio.unprovisionPin(trigPin);		
	}
    
    
    
	public void demo() throws InterruptedException {
		for (int i=0; i<10; ++i) {
			
			measureDistance();
			//System.out.println(String.format("distance = %3.1fcm [%dms]", d, System.currentTimeMillis() - t0));
			
			Thread.sleep(1000);
		}
	}

}
