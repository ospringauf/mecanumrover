/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.common.cmd;

import de.konteno.ao.mrover.common.RoverTopic;
import de.konteno.ao.mrover.common.unit.Angle;

/**
 * "STOP" command, handled by DriveService
 *
 */
@RoverTopic(address="cmd.stop", timeout=1000)
public class StopCmd implements IMotionCommand {
	
	public StopCmd() {}
	
	public StopCmd(String cause) {
		this.cause = cause;
	}
	
	// optional: reason for stopping
	private String cause;

	public String getCause() {
		return cause;
	}

	public void setCause(String cause) {
		this.cause = cause;
	}

	@Override
	public double estimateDx(long t) {
		return 0;
	}

	@Override
	public double estimateDy(long t) {
		return 0;
	}

	@Override
	public double estimateDalpha(long t) {
		return 0;
	}

	@Override
	public double getSpeed() {
		return 0;
	}

	@Override
	public Angle relativeCourse() {
		return Angle.fromDegree(0);
	}

}
