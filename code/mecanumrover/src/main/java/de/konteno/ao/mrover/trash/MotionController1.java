/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.trash;

import de.konteno.ao.mrover.common.Rover;
import de.konteno.ao.mrover.common.unit.Angle;
import de.konteno.ao.mrover.driver.common.DistanceSensor;
import de.konteno.ao.mrover.service.motor.BasicMotion;

/**
 * advanced motion
 *
 * TODO: shorter sleep times in turn/home for small distances 
 *
 */
public class MotionController1 {

	private DistanceSensor distanceSensor;

//	private Compass compass;

	private BasicMotion control;
	

	/**
	 * go straight for given time and measure distance / calculate speed in cm/s
	 * @param speed
	 * @param time
	 * @throws Exception
	 */
	private void calibrate(double speed, int time) throws Exception {
		
		try {
			double d0 = distanceSensor.measureDistance(3);		
			long t0 = System.currentTimeMillis();
			
			control.translate(Angle.fromDegree(0), speed);		
			Thread.sleep(time);
			control.release();
			
			long t1 = System.currentTimeMillis();
			double d1 = distanceSensor.measureDistance(3);
	
			double cms = Math.abs(1000*(d0-d1)/(t1-t0));
			System.out.println(String.format("%3.1fcm in %dms - %3.1fcm/s", d0-d1, t1-t0, cms));
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			control.release();			
		}
		
	}

	/**
	 * go to given start distance from wall (assuming the rover is facing a wall)
	 * @param start initial distance
	 * @throws Exception
	 */
	private void home(int start) throws Exception {
		try {
			double d = distanceSensor.measureDistance(3);
			Thread.sleep(50);
			
			if (d < start) {
				while (d < start) {
					double s = Rover.bestSpeed(start-d);
					control.translate(Angle.fromDegree(180), s);
					Thread.sleep(50);
					d = distanceSensor.measureDistance(1); 		
				}
			} 
			else {
				while (d > start) {
					double s = Rover.bestSpeed(start-d);
					control.translate(Angle.fromDegree(0), s);
					Thread.sleep(50);
					d = distanceSensor.measureDistance(1);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			control.release();			
		}
	}


}
