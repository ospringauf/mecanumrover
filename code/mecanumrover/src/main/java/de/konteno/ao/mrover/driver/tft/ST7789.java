/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.driver.tft;

import java.awt.Font;
import java.awt.Graphics2D;

import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.spi.SpiDevice;

/**
 * 1.3" IPS display, 240x240 pixels, based on ST7789 controller 
 * driver for 4-wire SPI communication
 *
 * note:
 * - 4-wire SPI mode uses extra command/data signal line (C=low/D=high). 
 *   Arguments to commands must be sent in "data" mode.
 * - This implementation is for 16bit 5-6-5 RGB mode. 
 *   You probably won't see a difference between 64k (16bit) and 262k (18bit) colors anyway. 
 * - use SPI mode 3
 * - frame rates depend on SPI clock speed: 1.9@1MHz, 13.8@8MHz, 24.5@16MHz, 40@32MHz
 *   
 * datasheet: https://www.waveshare.com/w/upload/a/ae/ST7789_Datasheet.pdf
 * 
 */
public class ST7789 extends ST7735 {

	/**
	 * 
	 * @param dev SPI device
	 * @param cd command/data pin (mandatory)
	 * @param rst reset pin (optional)
	 */
	public ST7789(SpiDevice dev, GpioPinDigitalOutput cd, GpioPinDigitalOutput rst) {
		super(dev, cd, rst, 240, 240);
	}

	
	public void test2() {
		try {
			Graphics2D g = this.getGraphics();
		
			// smile
	        g.setFont(new Font(Font.DIALOG, Font.PLAIN, 84));
			g.drawString("\u263a", 62, 75);
	        this.displayGraphics();
			
			Thread.sleep(2000);
			this.clear(false);
					
	        g.setFont(new Font(Font.DIALOG, Font.PLAIN, 10));
			g.drawRect(0, 0, 127, 63);
			g.drawString("Mecanumrover ***", 2, 9);
			g.drawString("Hi there!", 2, 19);
			//g.drawString((new Date()).toString(), 3, 21);			

	        this.displayGraphics();
		}
		catch (Exception e) {
			e.printStackTrace();
		}			
	}
	
	public void test3() {
		try {
			Graphics2D g = this.getGraphics();
			
			long s = System.currentTimeMillis();
			for (int i=0; i<1000; ++i) {
				this.clear(false);
				
				g.drawLine(0, i % HEIGHT, WIDTH, i % HEIGHT);
				g.drawLine(i % WIDTH, 0, i % WIDTH, HEIGHT);
				
				// current frame rate
				float fr = (float)(1000.0*i) / (System.currentTimeMillis() - s);
				g.drawString(String.format("%3.1f",  fr), 10, 10);
				
		        this.displayGraphics();
			}

		}
		catch (Exception e) {
			e.printStackTrace();
		}			
	}


}
