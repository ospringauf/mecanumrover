/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.driver.adc;

import java.io.IOException;

/**
 * convert ADC value (0..range-1) to float (-1 .. 1)
 * @author oliver
 *
 */
public class Poti {

	private Mcp3008 adc;
	private double range;

	public Poti(Mcp3008 adc1, int max) {
		adc = adc1;
		range = max;
	}

	public double getValue() throws IOException {
		int x = adc.readAnalogValue((short) 0);
		return 2.0 * (x - range/2) / range; 
	}

}
