/*********************************************************************
* Copyright (c) 2019 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.dryrun;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.konteno.ao.mrover.common.Rover;
import de.konteno.ao.mrover.common.cmd.DriveCmd;
import de.konteno.ao.mrover.common.cmd.IMotionCommand;
import de.konteno.ao.mrover.common.cmd.ResetCmd;
import de.konteno.ao.mrover.common.cmd.RotateCmd;
import de.konteno.ao.mrover.common.cmd.StopCmd;
import de.konteno.ao.mrover.common.event.CompassEvt;
import de.konteno.ao.mrover.common.unit.Angle;
import de.konteno.ao.mrover.service.MyVerticleBase;

/**
 * Simulated compass based on pure Dead Reckoning
 * @author oliver
 *
 */
public class DrCompassService extends MyVerticleBase {

	private static final Logger logger = LoggerFactory.getLogger( DrCompassService.class );
	private IMotionCommand current = new StopCmd();
	private long timer;
	private Angle lastHeading = Angle.fromDegree(0);
	private long lastTime;
	

	@Override
	public void start() throws Exception {		
		super.start();
		
		// listen to motion commands
		busCmdListen(DriveCmd.class, this::changeMotion);
		busCmdListen(RotateCmd.class, this::changeMotion);
		busCmdListen(StopCmd.class, this::changeMotion);
		busCmdListen(ResetCmd.class, this::reset);
		
		// periodic update
		timer = vertx.setPeriodic(50, v -> update());
	}	
	
	private void reset(ResetCmd cmd) {
		lastHeading = Angle.fromDegree(0);
	}
	
	private void update() {
		if (current instanceof StopCmd) {
			return;
		}
		
		// estimate heading based on rotation / elapsed time
		long dt = System.currentTimeMillis() - lastTime;
		double da = current.estimateDalpha(dt);
		
		lastHeading = lastHeading.addDegree(da);
		
		Rover.pose.heading = lastHeading;	
		busPublish(new CompassEvt(Rover.pose.heading));
	}


	private void changeMotion(IMotionCommand cmd) {
		if (cmd != current) {
			update();
		}
		current = cmd;
		lastTime = System.currentTimeMillis();
	}
	
	@Override
	public void onShutdown() {
		vertx.cancelTimer(timer);
	}

}
