package de.konteno.ao.mrover.common.util;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;

public class MyMessageCodec<T> implements MessageCodec<T,T> {

	
	private Class<T> clazz;

	public MyMessageCodec(Class<T> c) {
		clazz = c;
		
	}
	
	@Override
	public void encodeToWire(Buffer buffer, T s) {
		// TODO see https://github.com/vert-x3/vertx-examples/blob/master/core-examples/src/main/java/io/vertx/example/core/eventbus/messagecodec/util/CustomMessageCodec.java
		JsonObject jso = new JsonObject(Json.encode(s));
		jso.writeToBuffer(buffer);
	}

	@Override
	public T decodeFromWire(int pos, Buffer buffer) {
		// TODO see https://github.com/vert-x3/vertx-examples/blob/master/core-examples/src/main/java/io/vertx/example/core/eventbus/messagecodec/util/CustomMessageCodec.java
		System.out.println("decode " + buffer);
//		int jso = new JsonObject().readFromBuffer(pos, buffer);
		T x = (T) Json.decodeValue(buffer, clazz);
		return x;
	}

	@Override
	public T transform(T s) {
	    // If a message is sent *locally* across the event bus.
		// This example sends message just as is
		return s;
	}

	@Override
	public String name() {
		String n = "codec-" + clazz.getSimpleName();
		return n;
	}

	@Override
	public byte systemCodecID() {
		return -1; // user codec
	}

}
