/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.driver.motor;

/**
 * This class is a wrapper for controlling the PWM channels corresponding to 
 * a single motor driver.
 * 
 * @author oliver
 *
 */
public class AdafruitMotor {

	// DC motor run modes
	public static final int FORWARD = 1;
	public static final int BACKWARD = 2;
	public static final int BRAKE = 3;
	public static final int RELEASE = 4;
	
	public static final int MAXPWM = 16 * 255;

	private int pwm;
	private int pin1;
	private int pin2;
	private Pca9685Pwm mc;
	
	public double speedScale = 1.0; // corresponding to Rover-1 135rpm

	public AdafruitMotor(Pca9685Pwm ctrl, int pwm, int pin2, int pin1) {
		this.pwm = pwm;
		this.pin2 = pin2;
		this.pin1 = pin1;
		this.mc = ctrl;
	}

	public void run(int command) throws MotorControlException {
		switch (command) {
		case FORWARD:
			mc.setPin(pin1, false);
			mc.setPin(pin2, true);
			break;
			
		case BACKWARD:
			mc.setPin(pin1, true);
			mc.setPin(pin2, false);
			break;
			
		case RELEASE:
			mc.setPin(pin1, false);
			mc.setPin(pin2, false);
			break;
			
		case BRAKE:
			mc.setPin(pin1, true);
			mc.setPin(pin2, true);
			break;
		}
	}
	
//	private void setSpeed(int speed) throws MotorControlException {
//		speed = speed < 0 ? 0 : speed;
//		speed = speed > 255 ? 255 : speed;
//		mc.setPWM(pwm, 0, speed*16);
//	}

	public void go(double s) throws MotorControlException {
		if (s == 0) {
			run(RELEASE);
			return;
		}
		
		run(s > 0 ? FORWARD : BACKWARD);
//		setSpeed((int)Math.abs(255 * s * speedScale));		
		
		int speed = (int) Math.abs(s * speedScale * MAXPWM);
		speed = (speed > MAXPWM) ? MAXPWM : speed;
		mc.setPWM(pwm, 0, speed);
	}

}
