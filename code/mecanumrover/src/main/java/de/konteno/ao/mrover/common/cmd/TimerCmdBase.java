/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.common.cmd;

/**
 * command with timer 
 * "success" reply is sent after the timer expires 
 */
public abstract class TimerCmdBase implements IRoverCommand {
	protected long timer = 0;
		

	public long getTimer() {
		return timer;
	}


	public TimerCmdBase time(long t) {
		if (t < 0) {
			throw new IllegalArgumentException("time cannot be negative");
		}
		timer = t;
		return this;
	}

}
