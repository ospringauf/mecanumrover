package de.konteno.ao.mrover.service.turn;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.konteno.ao.mrover.common.Const;
import de.konteno.ao.mrover.common.Rover;
import de.konteno.ao.mrover.common.RoverDirection;
import de.konteno.ao.mrover.common.cmd.RotateCmd;
import de.konteno.ao.mrover.common.cmd.TurnCmd;
import de.konteno.ao.mrover.common.event.ShutdownEvt;
import de.konteno.ao.mrover.common.event.StoppedEvt;
import de.konteno.ao.mrover.common.unit.Angle;
import de.konteno.ao.mrover.service.MyVerticleBase;
import io.vertx.core.Future;
import io.vertx.core.Promise;

/**
 * Second attempt - set a timer after each rotate command, then check again.
 * no direct dependency on the compass.
 * 
 * fails after a max number of steps.
 *
 */
public class TurnService extends MyVerticleBase {

	private static final Logger logger = LoggerFactory.getLogger( TurnService.class );

	private Promise<Void> current;
	
	@Override
	public void start() throws Exception {
		super.start();
		busListen(ShutdownEvt.class, x -> onShutdown());

		// receive turn command and store message (reply later ...)
		busConsume(TurnCmd.class, this::turnCommand);

		// abort turn on STOPPED event
		busListen(StoppedEvt.class, evt -> onStop(evt));
	}

	private void abortPreviousCommand(String cause) {
		// fail the current command
		if (current != null) {
			current.tryFail(cause);
		}		
	}
	
	private void onStop(StoppedEvt evt) {
		abortPreviousCommand(evt != null ? evt.getCause() : null);
	}

	/**
	 * got TURN command: calculate target angle
	 * @param cmd
	 * @param msg bus message, will be replied when turn is complete
	 * @return
	 */
	private Future<Void> turnCommand(TurnCmd cmd) {
		
		abortPreviousCommand("new TURN command");
		
		Angle target = cmd.getAngle();
		if (cmd.isRelative()) {
			// relative Drehung: bestimme Zielwinkel = heading + turn
			target = Angle.remAngle(Rover.pose.heading.toDegree() + cmd.getAngle().toDegree());
			logger.info("TURN relative (" + cmd.getAngle() + ") to " + target);
		}
		else {
			// absolute Drehung: Zielwinkel gegeben
			target = cmd.getAngle();
			logger.info("TURN absolute to " + target);
		}
		
		current = Promise.promise();
		turn(target, cmd, current, 200);
		
		Promise<Void> ret = Promise.promise();
		
		// intercept and propagate result - TODO is there a better way?
		current.future().onSuccess(h -> {
			logger.info("TURN command sequence SUCCEEDED ");
			ret.complete();			
		});
		current.future().onFailure(t -> {
			logger.warn("TURN command sequence CANCELLED: " + t);
			ret.fail(t);
		});

//		current.setHandler(ar -> {
//			if (ar.succeeded()) {
//				logger.info("TURN command sequence SUCCEEDED ");
//				ret.complete();			
//			} else {
//				logger.warn("TURN command sequence CANCELLED: " + ar.cause().getMessage());
//				ret.fail(ar.cause());
//			}});
		return ret.future();
	}
	
	
	private void turn(Angle target, TurnCmd tcmd, Promise<Void> startFuture, int stepsLeft) {
		if (stepsLeft < 0) {
			startFuture.fail("too many steps, target heading not reached");
			sendStop("giving up TURN");
			return;
		}		
		
		try {
			// decreasing angle difference
			Angle delta = Angle.delta(target, Rover.pose.heading);
			double diff = delta.toDegree();
			
			RoverDirection dir = diff >= 0 ? RoverDirection.CW : RoverDirection.CCW;
			double absDiff = Math.abs(diff);

			logger.debug(String.format("TURN current=%3.1f, target=%3.1f, diff=%3.1f", Rover.pose.heading.toDegree(), target.toDegree(), diff));

			if (absDiff <= tcmd.getTolerance()) {
				
				// no more rotation, turn succeeded. DO NOT send STOP, command sequence may go on
				busSend(new RotateCmd(0))
				.map(v -> {	startFuture.complete(); return null; });
				
			} else {
				double v = Rover.bestSpeed(absDiff); // absolute speed
				v = Math.min(Const.MAX_ROTATION_SPEED, v); // limit rotation speed to 75%
				RotateCmd cmd = tcmd.getRotation().adjust(dir, v); // set speed sign depending on CW/CCW direction
				
				logger.trace("sendAndWait " + cmd);
				busSend(cmd);
				
				vertx.setTimer(80, elapsed -> {
					if (startFuture.future().isComplete()) {
						// maybe STOPPED? anyway we're already done
						return;
					}
					if (Rover.isStopped()) {
						// shouldn't get here
						logger.debug("STOPPED before end of command (sequence)");
						startFuture.fail("stopped");
					} else {
						// keep turning if necessary
						turn(target, tcmd, startFuture, stepsLeft-1);
					}
				});
			}

		} catch (Exception e) {
			logger.error("TURN failed", e);
			startFuture.tryFail(e);
		} 
	}

	@Override
	public void onShutdown() {
		abortPreviousCommand("shutting down");
	}

}
