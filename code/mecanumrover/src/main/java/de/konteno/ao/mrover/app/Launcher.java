/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package de.konteno.ao.mrover.app;

public class Launcher {

	public static void main(String[] args) throws Exception {
//		SerialExample.main(args);
		
		if (args.length > 0 && "r2t".equals(args[0])) {
			System.out.println("rover-2 test");
			Rover2TestApp.main(args);
		} else if (args.length > 0 && "r2".equals(args[0])) {
			Rover2App.main(args);
		} else {
			RoverApp.main(args);
		}
	}
}
