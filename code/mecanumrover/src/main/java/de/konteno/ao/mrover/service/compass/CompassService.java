package de.konteno.ao.mrover.service.compass;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CFactory;

import de.konteno.ao.mrover.common.Const;
import de.konteno.ao.mrover.common.Rover;
import de.konteno.ao.mrover.common.cmd.CalibrateCompassCmd;
import de.konteno.ao.mrover.common.cmd.RotateCmd;
import de.konteno.ao.mrover.common.cmd.StopCmd;
import de.konteno.ao.mrover.common.event.CompassEvt;
import de.konteno.ao.mrover.common.event.StoppedEvt;
import de.konteno.ao.mrover.common.unit.Angle;
import de.konteno.ao.mrover.driver.compass.HMC5883L;
import de.konteno.ao.mrover.driver.compass.HMC5883LConfig;
import de.konteno.ao.mrover.service.MyVerticleBase;
import io.vertx.core.Future;
import io.vertx.core.Promise;

public class CompassService extends MyVerticleBase {

	private static final Logger LOGGER = LoggerFactory.getLogger(CompassService.class);
	private HMC5883L compass;

	static final int freqLo = 5; // Hz 1/200ms
	static final int freqHi = 20; // Hz 1/50ms
	int freq = freqLo;
	boolean highSpeed = false;
	long timerId;
	private HMC5883LCalibration calibrator;
	
	// first reading; normalize initial heading to 0 degrees
	private Angle initial = null;


	@Override
	public void start() throws Exception {
		super.start();

		I2CBus bus = I2CFactory.getInstance(Const.I2C_BUS_NUMBER);

		compass = new HMC5883L(bus.getDevice(Const.COMPASS_ADR), HMC5883LConfig.Default);
		compass.loadCalibration();
		
		busCmdListen(RotateCmd.class, cmd -> onRotate(cmd)); // just want to know when we're rotating...
		busListen(StoppedEvt.class, cmd -> { onRotate(new RotateCmd(0)); });
		busConsume(CalibrateCompassCmd.class, this::calibrateCompass);

		// periodic compass readings
		timerId = vertx.setPeriodic(1000 / freq, v -> measure(v));
	}
	
	/**
	 * rotate for 6 seconds, while measuring x/y/z data. finally, scale and shift the measurements
	 * and update the compass' calibration.
	 * @param cmd
	 * @return
	 */
	private Future<Void> calibrateCompass(CalibrateCompassCmd cmd) {
		Promise<Void> prom = Promise.promise();
		LOGGER.info("start compass calibration");
		adjustTimer(true);
		calibrator = new HMC5883LCalibration();
		
		busSend(new RotateCmd(0.75));
		vertx.setTimer(4000, h -> {
		
			calibrator.update(compass);
			calibrator = null;
			Rover.clearTracks();
			busSend(new StopCmd());
			prom.complete();
		});
		
		return prom.future();
	}


	private void onRotate(RotateCmd cmd) {
		// high measurement frequency during turns
		adjustTimer(Math.abs(cmd.getSpeed()) > 0.01);
	}

	private void adjustTimer(boolean hi) {
		if (hi == highSpeed) {
			return;
		}
		vertx.cancelTimer(timerId);
		highSpeed = hi;
		freq = hi ? freqHi : freqLo;
		timerId = vertx.setPeriodic(1000 / freq, v -> measure(v));
		LOGGER.debug("adjust timer to {} Hz", freq);
	}


	private void measure(Long v) {
		try {
			double h = compass.readHeading();
			Angle heading = Angle.fromDegree(h);

			if (initial == null) {
				initial = heading;
			}
			heading = heading.minus(initial);

			
			Rover.pose.heading = heading;
			
			if (calibrator != null) {
				calibrator.add(compass.coords);
			}
			
			LOGGER.trace("COMPASS publish {}", heading);
			busPublish(new CompassEvt(heading));
		} catch (IOException e) {
			LOGGER.error("error reading compass", e);
		}
	}

	@Override
	public void onShutdown() {
		try {
			compass.configure(HMC5883LConfig.buildIdleConfig());
		} catch (IOException | InterruptedException e) {
		}

	}

}
