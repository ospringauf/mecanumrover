/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.common.cmd;

import de.konteno.ao.mrover.common.RoverTopic;

@RoverTopic(address="cmd.demo")
public class DemoCmd implements IRoverCommand {

	public static final String square = "square";
	public static final String squareTurn = "squareTurn";
	public static final String wiggle = "wiggle";
	public static final String dance = "dance";
	public static final String follow = "follow";

	private String demoName;

	public DemoCmd(String demoName) {
		this.setDemoName(demoName);
	}

	public String getDemoName() {
		return demoName;
	}

	public void setDemoName(String demoName) {
		this.demoName = demoName;
	}
}
