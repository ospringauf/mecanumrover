package de.konteno.ao.mrover.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pi4j.io.serial.SerialFactory;

public class ShutdownService extends MyVerticleBase {

	private static final Logger logger = LoggerFactory.getLogger( ShutdownService.class );

	@Override
	public void start() throws Exception {
		super.start();
	}
	
	@Override
	public void onShutdown() {
		logger.info("SHUTDOWN vert.x in 2 seconds ...");
		vertx.setTimer(2000, id -> {
			SerialFactory.shutdown();
			vertx.close();
		});
	}

}
