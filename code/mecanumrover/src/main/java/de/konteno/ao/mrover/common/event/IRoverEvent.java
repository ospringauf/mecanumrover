/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.common.event;

import de.konteno.ao.mrover.common.IRoverMessage;

/**
 * marker interface for events
 * 
 * events are "published" on the event bus, meaning there can be multiple consumers,
 * but an event cannot return a result
 *
 */
public interface IRoverEvent extends IRoverMessage {

}
