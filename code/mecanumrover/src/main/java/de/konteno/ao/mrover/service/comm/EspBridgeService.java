/*********************************************************************
* Copyright (c) 2018-2019 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.service.comm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pi4j.io.serial.Baud;
import com.pi4j.io.serial.SerialPort;

import de.konteno.ao.mrover.common.Rover;
import de.konteno.ao.mrover.common.SerialMessage;
import de.konteno.ao.mrover.common.cmd.ConfigSensorCmd;
import de.konteno.ao.mrover.common.cmd.SendSerialMessageCmd;
import de.konteno.ao.mrover.common.event.CompassEvt;
import de.konteno.ao.mrover.common.event.LaserDistanceEvt;
import de.konteno.ao.mrover.common.event.OdometerEvt;
import de.konteno.ao.mrover.common.event.SonarDistanceEvt;
import de.konteno.ao.mrover.driver.serial.RoverSerialPort;
import de.konteno.ao.mrover.service.MotionDetBase;
import io.vertx.core.Future;
import io.vertx.core.Promise;

/**
 * handle communication with ESP32 module, sensor configuration
 * - process incoming messages from ESP32 via serial port, translate sensor data into bus events
 * - relay bus messages to ESP32 via serial port
 * - switch sonar sensors on/off when moving/stopped
 *
 */
public class EspBridgeService extends MotionDetBase {

	private static final Logger logger = LoggerFactory.getLogger( EspBridgeService.class );

	private long lastPing = 9999;
	private RoverSerialPort port;

	private long sonarOffTimer;
	

	@Override
	public void start(Promise<Void> startFuture) throws Exception {
		super.start();

		// relay messages from event bus to ESP32 
		busConsume(SendSerialMessageCmd.class, this::sendToEsp);

		// configure ESP32 sensors 
		busConsume(ConfigSensorCmd.class, this::configureEsp);

		// detect motion/stop -> activate sonar sensors
		onMotionChange(this::motionChange);
		
		// listen for data from ESP32 on serial port
		port = new RoverSerialPort("esp32");
		// set handler for incoming messages (already parsed)
		port.setHandler(this::messageReceived);
		port.start(SerialPort.getDefaultPort(), Baud._115200);
		startFuture.complete();
	}

	private void messageReceived(String topic, String payload) {

		logger.trace("received: topic: {}, payload: {}", topic, payload);
		
		// translate MQTT messages into vert.x events
		switch (topic) {

		case "PING":
		case "esp/out/ping":
			int ping = Integer.parseInt(payload.trim());
			if (ping < lastPing) {
				// first contact, or ESP32 has been reset
				logger.info("(re-)connected to ESP32 (got ping)");
				configureEsp(new ConfigSensorCmd(Rover.sensorMode));
			}
			lastPing = ping;
			break;
			
		case "SON":
		case "esp/out/sonar":
//			logger.debug("received: topic: {}, payload: {}", topic, payload);
			busPublish(SonarDistanceEvt.parse(payload));
			break;

		case "LRF":
		case "esp/out/lrf":
			busPublish(LaserDistanceEvt.parseEsp(payload));
			break;

		case "ODO":
		case "esp/out/odo":
			busPublish(OdometerEvt.parse(payload));
			break;

		case "COMP":
//			logger.debug("received: topic: {}, payload: {}", topic, payload);
			CompassEvt ce = CompassEvt.parseEsp(payload);
			Rover.pose.heading = ce.getHeading();
			busPublish(ce);
			break;

		case "esp/info":
			logger.info("ESP32 says: " + payload) ;
			break;
		
		default:
			break;
		}		
	}
	
	private Future<Void> configureEsp(ConfigSensorCmd cmd) {
		configureEsp(cmd.mode);
		return Future.succeededFuture();
	}

	protected void configureEsp(int mode) {
		// send current/initial configuration commands to ESP32
		logger.info("set ESP32 sensor mode {}", mode);
		Rover.sensorMode = mode;

//		sendToEsp("esp/cmd/sonarmask", Integer.toString(cmd.sonarMask)); 
		sendToEsp("esp/cmd/mode", Integer.toString(mode));
	}

	private Future<Void> sendToEsp(SendSerialMessageCmd cmd) {
		port.send(cmd);
		return Future.succeededFuture();
	}
	
	private void sendToEsp(String topic, String message) {
		port.send(new SerialMessage(topic, message));
	}
	
	/**
	 * switch sonar sensors on/off when moving/stopped (off after 10s)
	 * @param cmd
	 */
	private void motionChange(Boolean moving) {
		int x = ConfigSensorCmd.MODE_SONAR | ConfigSensorCmd.MODE_ODO;
		
		if (moving) {
			// moving -> enable sonar
			vertx.cancelTimer(sonarOffTimer);
			sonarOffTimer = 0;
			if ((Rover.sensorMode & x) != x) {
				configureEsp(Rover.sensorMode | x);
			}
		}
		else if (!moving && (Rover.sensorMode & x) == x && sonarOffTimer == 0) {
			// stopped -> disable sonar
//			logger.info("turn off sensors in 10s due to {}", cmd);
			sonarOffTimer = vertx.setTimer(10000, t -> configureEsp(Rover.sensorMode & ~x));			
		}
	}

	@Override
	public void onShutdown() {
		// pi4j.Serial has its own shutdown hook, so the serial port is probably already closed now.
		// Too bad, this message will not get through...
		// ... unless we have removed the hook (see RoverApp)
		sendToEsp("esp/cmd/mode", Integer.toString(ConfigSensorCmd.MODE_IDLE));

		port.shutdown();
	}

}
