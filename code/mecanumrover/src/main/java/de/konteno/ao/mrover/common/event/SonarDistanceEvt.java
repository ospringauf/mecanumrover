/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.common.event;

import java.util.Arrays;

import de.konteno.ao.mrover.common.Const;
import de.konteno.ao.mrover.common.RoverTopic;
import de.konteno.ao.mrover.common.unit.Angle;

@RoverTopic(address="evt.sonar")
public class SonarDistanceEvt implements IRoverEvent {

	public static final int LEFT_FRONT = 0;
	public static final int FRONT_LEFT = 1;
	public static final int FRONT_RIGHT = 2;
	public static final int RIGHT_FRONT = 3;
	public static final int RIGHT_REAR = 4;
	public static final int REAR_RIGHT = 5;
	public static final int REAR_LEFT = 6;
	public static final int LEFT_REAR = 7;
			
	
	public int distance[];
	
	public SonarDistanceEvt(int[] dist) {
		distance = Arrays.copyOf(dist, 8);
	}
	
	private SonarDistanceEvt() {
	}

	public static SonarDistanceEvt parse(String payload) {
		SonarDistanceEvt e = new SonarDistanceEvt();
		String[] f = payload.trim().split("\\s+");
		e.distance = new int[8];
		for (int i=0; i<f.length; ++i) {
			e.distance[i] = Integer.parseInt(f[i]);
		}
		return e;
	}
	
	public String mqttString() {
		return String.format("%3d %3d %3d %3d %3d %3d %3d %3d", distance[0], distance[1], distance[2], distance[3], distance[4], distance[5], distance[6], distance[7]);
	}
	
	public int getDistance(int i) {
		return distance[i];
	}
	
	/**
	 * ignore "rear" sonar sensors (relative to current couse) for collision detection  
	 * @param course relative course in rover system
	 * @return
	 */
	public static boolean[] ahead(Angle course) {
		boolean[] d = new boolean[8];
//		double relCourse = course.absolute().toRad();
//		for (int i=0; i<8; ++i) {
//			d[i] = (Math.abs(Const.sonarDirection[i].toRad() - relCourse) < (0.75*Math.PI));
//		}
		for (int i=0; i<8; ++i) {
			d[i] = (Math.abs(Angle.delta(course, Const.sonarDirection[i]).toDegree()) < 135);
		}

		return d;
	}
	
	public int getFrontClearance() {
		return Math.min(distance[FRONT_LEFT], distance[FRONT_RIGHT]);
	}

	public int getLeftClearance() {
		return Math.min(distance[LEFT_FRONT], distance[LEFT_REAR]);
	}

	public int getRearClearance() {
		return Math.min(distance[REAR_LEFT], distance[REAR_RIGHT]);
	}
	
	public int getRightClearance() {
		return Math.min(distance[RIGHT_FRONT], distance[RIGHT_REAR]);
	}

}
