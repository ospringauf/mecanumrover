/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.driver.serial;

/**
 * functional interface: handler method for incoming (serial) message from ESP32/Arduino
 *
 */
public interface SerialMessageHandler {
	
	/**
	 * handle incoming message (MQTT style)
	 * @param topic
	 * @param payload
	 */
	void handle(String topic, String payload);

}
