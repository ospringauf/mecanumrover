/*********************************************************************
* Copyright (c) 2018-2019 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package de.konteno.ao.mrover.service.odo;

import static de.konteno.ao.mrover.common.cmd.ConfigSensorCmd.MODE_ODO;

import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pi4j.io.serial.Baud;

import de.konteno.ao.mrover.common.Const;
import de.konteno.ao.mrover.common.Rover;
import de.konteno.ao.mrover.common.event.DisplacementEvt;
import de.konteno.ao.mrover.common.track.Pose;
import de.konteno.ao.mrover.common.unit.Angle;
import de.konteno.ao.mrover.common.unit.Pos;
import de.konteno.ao.mrover.driver.odo.IOdo2Driver;
import de.konteno.ao.mrover.driver.odo.OdoSensor;
import de.konteno.ao.mrover.driver.odo.Odometer2Driver;
import de.konteno.ao.mrover.service.MotionDetBase;
import io.reactivex.Observable;
import io.vertx.core.Promise;

/**
 * Process x/y displacement measurements from the "optical flow odometer"
 * (2 ADNS-5020-EN optical mouse sensors)
 * 
 * Communicates directly with Arduino via USB serial port.
 * 
 * - update current Rover position
 * - generate waypoints in the Rover track
 *
 */
public class Odometer2Service extends MotionDetBase {

	private static final Logger LOGGER = LoggerFactory.getLogger(Odometer2Service.class);
	private long odoOffTimer;
	
	private IOdo2Driver odo;
	
	// builder function for the driver
	public Supplier<IOdo2Driver> buildDriver = () -> new Odometer2Driver("/dev/ttyUSB0", Baud._38400);

	
	@Override
	public void start(Promise<Void> startFuture) throws Exception {
		super.start();

		// build the odometer driver
		odo = buildDriver.get();
		
		// listen to data stream from the driver, calculate and publish displacement
		Observable<OdoSensor[]> p = Observable.fromPublisher(odo);
		p.map(this::linearDisplacement) // emits displacement events
			.filter(x -> x != null)
			.subscribe(this::busPublish); 

		// update current waypoint / Rover pose for every ODO event,
		Observable<DisplacementEvt> s = busListenStream(DisplacementEvt.class);
		s.map(this::updatePose) // emits Waypoints!
//			.sample(333, TimeUnit.MILLISECONDS)
			.subscribe(this::busPublish); 
//
//		busConsume(CalibrateOdometerCmd.class, this::calibrate);

		onMotionChange(this::motionChange);

		startFuture.complete();
	}


	/**
	 * based on 
	 * Elements of Robotics (Ben-Ari, Mondada; Springer 2018)
	 * chapter 5.6 Odometry with Turns
	 * https://link.springer.com/chapter/10.1007/978-3-319-62533-1_5
	 * 
	 * TODO only works for tangential motion because of approximation (x = sin x for small angles)
	 * TODO sensors must be calibrated (rotation/scaling)!
	 */
	DisplacementEvt odometryWithTurns(OdoSensor[] data) {
		// TODO consider squal?
		
		double b = Const.ODO_SENSOR_DISTANCE; // distance between sensors
		double dl = data[0].transform().distance(); // left distance
		double dr = data[1].transform().distance();
		
		double theta = (dr - dl)/b; // turn angle in radians, cartesian; APPOXIMATION!
		
		double dc = (dl + dr)*0.5; // center displacement
		
		double dx = -dc * Math.sin(theta);
		double dy = dc * Math.cos(theta);
		
		return new DisplacementEvt(dx, dy, Angle.fromRad(-theta));
	}
	
	/**
	 * another approach: calc turn angle from the difference between left and 
	 * right displacement (and distance between sensors)
	 */
	DisplacementEvt myOdometry(OdoSensor[] data) {
		// TODO consider squal?
		
		DisplacementEvt evt = linearDisplacement(data);
//		if (evt == null) {
//			return null;
//		}
		double b = Const.ODO_SENSOR_DISTANCE; // distance between sensors
		Pos dl = data[0].transform();
		Pos dr = data[1].transform();
		
		Pos dd = Pos.of(b + dr.x - dl.x, dr.y - dl.y);
		Angle da = dd.toAngle0().negative();
//		logger.debug("myodo: " + dl + "  /  " + dr + "  /  " + dd + "  /  " + da);
				
		evt.da = da;
		return evt;
	}
	
	
	/**
	 * merge data from left+right sensor into single dx/dy displacement.
	 * (weighted average by signal quality) 
	 * no heading/angle is derived from the sensors --> needs compass for heading update
	 */
	DisplacementEvt linearDisplacement(OdoSensor[] data) {
//		logger.debug("in  :" + data[0] + " / " + data[1]);
		OdoSensor m1 = data[0];
		OdoSensor m2 = data[1];
		Pos d1 = Pos.ZERO;
		Pos d2 = Pos.ZERO;

	    if (m1.motion() || m2.motion())
	    {
	        double w1 = 0;
	        if (m1.motion())
	        {
	            w1 = m1.squal == 0 ? 1 : m1.squal;
	            d1 = m1.transform();
	        }
	        double w2 = 0;
	        if (m2.motion())
	        {
	            w2 = m2.squal == 0 ? 1 : m2.squal;
	            d2 = m2.transform();
	        }

	        // weighted sum of both sensors
	        double w = w1 + w2;
			double dx = (d1.x * (w1 / w)) + (d2.x * (w2 / w));
	        double dy = (d1.y * (w1 / w)) + (d2.y * (w2 / w));
	    
	        DisplacementEvt evt = new DisplacementEvt(dx, dy, Angle.ZERO);
	        LOGGER.trace("lin displace: " + evt.toString());
	        return evt;
	    }
	    return DisplacementEvt.ZERO;
	}
	

//	/**
//	 * update Rover position relative to initial heading and current position
//	 * @param odo odometer data (x/y displacement)
//	 * @return a waypoint representing the updated Rover position
//	 */
//	private Waypoint updatePose(DisplacementEvt odo) {
//		if (Rover.getInitialHeading() == null) {
//			Waypoint w = new Waypoint(odo.delta, Angle.fromDegree(0), Angle.fromDegree(0), TrackType.ODO);
//			return w;
//		}
//		Angle relHeading = Rover.getHeading().minus(Rover.getInitialHeading());
//		Waypoint w = Rover.getPosition().addRelative(odo.delta, relHeading);
//		Rover.setPosition(w);
//		return w;
//	}
	
	
	/**
	 * update Rover position relative to initial heading and current position
	 * @param odo odometer data (x/y displacement)
	 * @return a waypoint representing the updated Rover position
	 */
	private Pose updatePose(DisplacementEvt odo) {
		Angle heading = Rover.pose.heading;
		Rover.pose = Rover.pose.addRelative(odo.delta, heading);
		// if displacement data contain a rotation (delta-angle), update rover heading now
		// _after_ updating x/y position 
		Rover.pose.heading = Rover.pose.heading.plus(odo.da);
//		w.heading = Rover.getHeading(); 
//		logger.debug("odo2s update-2 to " + w);
		return Rover.pose;
	}
	
	/**
	 * switch odometer LEDs on/off when moving/stopped (off after 10s)
	 * @param cmd
	 */
	private void motionChange(Boolean moving) {
//		LOGGER.info("odo2 motionChange " + moving);
		if (moving) {
			// moving -> enable odo
			vertx.cancelTimer(odoOffTimer);
			odoOffTimer = 0;
			if (!Rover.hasMode(MODE_ODO)) {
				odo.powerUp();
			}
		}
		else if (Rover.hasMode(MODE_ODO) && odoOffTimer == 0) {
			// stopped -> disable odo
//			LOGGER.info("turn off sensors in 10s");
			odoOffTimer = vertx.setTimer(10000, t -> odo.powerDown());			
		}
	}


	@Override
	public void onShutdown() {
		odo.shutdown();
	}

}
