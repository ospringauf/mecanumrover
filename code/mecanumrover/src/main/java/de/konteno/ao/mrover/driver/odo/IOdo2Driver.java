package de.konteno.ao.mrover.driver.odo;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public interface IOdo2Driver extends Publisher<OdoSensor[]> {
	
    static final Logger logger = LoggerFactory.getLogger(IOdo2Driver.class);
    static final Path configPath = Paths.get("odometer2driver.json");

    OdoSensor[] sensors = new OdoSensor[] {new OdoSensor(), new OdoSensor()};

    default OdoSensor left() { return sensors[0]; }
    default OdoSensor right() { return sensors[1]; }
    
	void powerUp();

	void powerDown();

	void shutdown();

	void subscribe(Subscriber<? super OdoSensor[]> sub);

	default void readConfiguration() {
        try {
            if (Files.exists(configPath)) {
                String j = new String(Files.readAllBytes(configPath));
                OdoSensor[] s = OdoSensor.fromJsonArray(j);                
                sensors[0] = s[0];
                sensors[1] = s[1];
                logger.info("odometer configuration loaded");
            } else {
                logger.warn("no odometer configuration file found at " + configPath.toAbsolutePath());
            }
        } catch (Exception e) {
            logger.error("failed to read odometer configuration", e);
        }
    }
	
	default void saveConfiguration() {
        try {
            String j = OdoSensor.toJsonArray(sensors).toString();
            Files.write(configPath, j.getBytes());
            logger.info("odometer configuration saved");
        } catch (Exception e) {
            logger.error("failed to save odometer configuration", e);
        }
    }

}