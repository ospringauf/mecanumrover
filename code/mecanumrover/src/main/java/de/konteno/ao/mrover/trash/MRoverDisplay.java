/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.trash;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Arc2D;
import java.io.IOException;

import de.konteno.ao.mrover.driver.oled.SH1106;

public class MRoverDisplay /*implements DistanceListener*/ {

	private static final boolean RADAR_LOGARITHMIC = true;
	private SH1106 oled;
	
	private int x = 0;

	public MRoverDisplay(SH1106 sh1106) {
		oled = sh1106;
//		EventBroker.addDistanceListener(this);
	}

	
	public void distanceChanged(int[] dist, int minAngle, int maxAngle) {
		try {
			oled.clear(false);
			updateRadar2(dist, oled.getGraphics(), SH1106.WIDTH, SH1106.HEIGHT, minAngle, maxAngle);
			oled.displayGraphics();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	private static float scale(int sample) {
		if (sample > 100) {
			return 2;
		} else if (sample < 1) {
			return 0;
		} else {
			return (float) (RADAR_LOGARITHMIC ? Math.log10(sample)/2.0f : sample/100.0f);
		}
	}
	
	/**
	 * paint "radar style" pie diagram, representing the distance measured
	 * 
	 * @param dist array of measurements, right-to-left, in [cm]
	 * @param g
	 */
	public static void updateRadar2(int[] dist, Graphics2D g, int width, int height, int startAngle, int endAngle) {		
        Point p0 = new Point(width/2, height);

        g.setColor(Color.WHITE);
		//g.fillRect(0, 0, width, height);
        
//		int startAngle = 10;
//		int endAngle = 160;
		float r0 = p0.x * 1.1f; // 100% = 100cm radius
		
		Arc2D.Float arc = new Arc2D.Float();
		
		// pre-fill full range
		arc.setArcByCenter(p0.x, p0.y, 2*r0, startAngle, endAngle-startAngle, Arc2D.PIE);
		g.fill(arc);

		// delta-angle
		float da = (endAngle-startAngle) / (float)dist.length;
		
		for (int i=0; i<dist.length; ++i)  {
			// samples are right-to-left
			int sample = dist[i]; //d[d.length - i - 1];
			
			float r = r0 * scale(sample);
			
			g.setColor(Color.BLACK);
			arc.setArcByCenter(p0.x, p0.y, r, startAngle+i*da, da, Arc2D.PIE);
			g.fill(arc);
			
			// radial lines
			g.setColor(Color.WHITE);
			arc.setArcByCenter(p0.x, p0.y, 2*r0, startAngle+i*da, da, Arc2D.PIE);
			g.draw(arc);
		}
		
		// concentric arcs
		g.setColor(Color.BLACK);
		for (int i=1; i<10; ++i) {
			float r = r0 * scale(i*10);
			arc.setArcByCenter(p0.x, p0.y, r, startAngle, endAngle-startAngle, Arc2D.OPEN);
			g.draw(arc);
		}
	}

	
	public void testOled() {
		try {
			Graphics2D g = oled.getGraphics();

			// smile
	        g.setFont(new Font(Font.DIALOG, Font.PLAIN, 84));
			g.drawString("\u263a", 62, 75);
	        oled.displayGraphics();
			
			Thread.sleep(2000);
			oled.clear(false);
					
	        g.setFont(new Font(Font.DIALOG, Font.PLAIN, 10));
			g.drawRect(0, 0, 127, 63);
			g.drawString("Mecanumrover ***", 2, 9);
			g.drawString("Hi there!", 2, 19);
			//g.drawString((new Date()).toString(), 3, 21);			

	        oled.displayGraphics();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
	}

	public void off() {
		try {
			oled.displayOff();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public void displayText(String[] text) {
		try {
			Graphics2D g = oled.getGraphics();
			oled.clear(false);
	        g.setFont(new Font(Font.DIALOG, Font.PLAIN, 10));
	        for (int i=0; i<text.length; ++i) {
	        	g.drawString(text[i], 0, 10+10*i);
	        }
	        oled.displayGraphics();
		}
		catch (Exception e) {
			e.printStackTrace();
		}				
	}


	public void displayText(String string) {
		displayText(new String[] { string });		
	}


	
	public void toggle() {
		try {
			switch(x) {
			case 0:
				oled.scanDir(false);
				oled.segmentMap(false);
				displayText("toggle:" + x);
				break;
			case 1:
				oled.scanDir(false);
				oled.segmentMap(true);
				displayText("toggle:" + x);
				break;
			case 2:
				oled.scanDir(true);
				oled.segmentMap(false);
				displayText("toggle:" + x);
				break;
			case 3:
				oled.scanDir(true);
				oled.segmentMap(true);
				displayText("toggle:" + x);
				break;
			}
			x+=1;
			x%=4;
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	

}
