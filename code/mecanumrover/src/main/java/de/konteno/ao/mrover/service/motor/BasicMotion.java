/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.service.motor;

import de.konteno.ao.mrover.common.unit.Angle;

/**
 * elementary Rover movement
 * 
 * TODO Steering/Radius in "echten" Radius (cm) umrechnen
 *
 */
public interface BasicMotion {


	/**
	 * Hard/soft turn along tangent, front pointing in the direction of motion (tangent) --> curve
	 * Left/right wheels turn in the same direction if |steering| < 50 or in opposite directions if |steering| > 50
	 * 
	 * @param speed speed
	 * 	> 0 : clockwise (right turn)
	 *  < 0 : counter-clockwise (left turn)
	 *   
	 * @param steering (-100 .. 100)
	 * 	relative steering angle; determines rotation radius
	 * 	  0 : no turn; go straight
	 *   50 : half turn right; only left wheels move
	 *  100 : full turn right; rotate on the spot
	 *  negative values = likewise for left turn
	 *  
	 * @throws Exception
	 */
	void rotateTangential(double speed, double steering) throws Exception;

	/**
	 * Turn around a central point, front pointing towards/away from the central point --> slide sideways
	 * Left/right wheels turn in opposite directions.
	 * If front>back, the center of rotation is behind the rover. If speed>0, rotate right (clockwise).
	 * 
	 * @param speed speed
	 * 	> 0 : nose moves clockwise (right turn)
	 *  < 0 : counter-clockwise (left turn)

	 * @param steering (-100 .. 100)
	 *  >0  : nose points outwards, rotation around the read end
	 *  <0  : nose points inwards, rotation around the front end
	 *   0  : front=rear, right or left 
	 *  50  : rear=0 (v>0) or front=0 (v<0)
	 * 100  : opposite speed, front+rear=0
	 * front=rear ==> equivalent to straight movement right (v>0) or left (v<0)
	 * 
	 * @throws Exception
	 */
	public void rotateRadial(double speed, double steering) throws Exception;
	
	/**
	 * "Translation" motion (font points always in the same direction)
	 * forward (0), backward (180), left (270), right (90)
	 * @param course
	 * @param v speed
	 * @throws Exception
	 */
	void translate(Angle course, double v) throws Exception;

	void release() throws Exception;

	void brake() throws Exception;

	void distributePower(double lr, double fb);

}