/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.service.compass;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.konteno.ao.mrover.common.unit.Angle;
import de.konteno.ao.mrover.driver.compass.HMC5883L;

public class HMC5883LCalibration {

	private static final Logger logger = LoggerFactory.getLogger(HMC5883LCalibration.class);

	short[] startPoint = null;
	int xmin = 0, xmax = 0, ymin = 0, ymax = 0;

	/**
	 * add a single measurement (x/y/z point)
	 * 
	 * @param p
	 */
	public void add(short[] p) {
		if (startPoint == null) {
			startPoint = p.clone();
			xmin = xmax = p[0];
			ymin = ymax = p[1];
		}

		xmin = Math.min(xmin, p[0]);
		xmax = Math.max(xmax, p[0]);
		ymin = Math.min(ymin, p[1]);
		ymax = Math.max(ymax, p[1]);

		logger.debug(String.format("%3d %3d %3d", p[0], p[1], p[2]));
	}

	/**
	 * scale an shift the measures points and update the compass
	 * 
	 * @param compass
	 */
	public void update(HMC5883L compass) {

		compass.offsetX = (int) ((-1) * ((xmax + xmin) / 2.0));
		compass.scaleX = 1000.0 / (xmax - xmin);

		compass.offsetY = (int) ((-1) * ((ymax + ymin) / 2.0));
		compass.scaleY = 1000.0 / (ymax - ymin);

		compass.offsetDegree = 0;
		double north = Angle.convertToDegree(
				(startPoint[0] + compass.offsetX) * compass.scaleX,
				(startPoint[1] + compass.offsetY) * compass.scaleY, 
				compass.offsetDegree);
		compass.offsetDegree = (-1) * north;

		logger.info(String.format("x = (x + %d) * %3.4f", compass.offsetX, compass.scaleX));
		logger.info(String.format("y = (y + %d) * %3.4f", compass.offsetY, compass.scaleY));
		logger.info(String.format("north offset = %3.1f", compass.offsetDegree));

		compass.saveCalibration();

	}

}
