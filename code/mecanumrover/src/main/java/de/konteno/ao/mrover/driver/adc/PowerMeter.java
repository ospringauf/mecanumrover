/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************
*/

package de.konteno.ao.mrover.driver.adc;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pi4j.io.i2c.I2CDevice;

public class PowerMeter {

	private static final Logger logger = LoggerFactory.getLogger( PowerMeter.class );

	private Ads1115 adc;
	private double adcGain = 4.096; // max input voltage ~ 100%
	private int adcScale;

	public PowerMeter(I2CDevice dev) throws IOException {
		this.adc = new Ads1115(dev);
		adc.setGain(Ads1115.GAIN_4V096);
		adc.setDataRate(Ads1115.DATARATE_128SPS);
		adc.configSingle(0);
		adcScale = 1 << (adc.getResolution() - 1);
	}
	
	
	public String readPower() {
		
		try {
			short a0 = adc.readSingleValue(0);
//			Gpio.delay(adc.getConversationDelay());
			short a1 = adc.readSingleValue(1);
			
			
			double voltage = a0 * (adcGain / adcScale) * 5;
			double current = a1 * (adcGain / adcScale);
			String info = String.format("%3.1fV  %3.1fA", voltage, current);
			System.out.println(String.format("PowerMeter:  %3.1fV (%d), %3.1fA (%d)", voltage, a0, current, a1));
			return info;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "power?";
		}
	}
}
