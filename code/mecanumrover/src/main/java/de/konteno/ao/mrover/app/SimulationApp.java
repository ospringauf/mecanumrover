package de.konteno.ao.mrover.app;

import java.nio.file.Paths;

import de.konteno.ao.mrover.common.cmd.DemoCmd;
import de.konteno.ao.mrover.common.cmd.DriveCmd;
import de.konteno.ao.mrover.common.cmd.RotateCmd;
import de.konteno.ao.mrover.common.cmd.TurnCmd;
import de.konteno.ao.mrover.common.event.ShutdownEvt;
import de.konteno.ao.mrover.common.unit.Angle;
import de.konteno.ao.mrover.dryrun.MockCompassService;
import de.konteno.ao.mrover.dryrun.MockDriveService;
import de.konteno.ao.mrover.dryrun.MockSonarService;
import de.konteno.ao.mrover.dryrun.ReplayOdometer;
import de.konteno.ao.mrover.service.CollisionDetectorService;
import de.konteno.ao.mrover.service.DemoService;
import de.konteno.ao.mrover.service.MyVerticleBase;
import de.konteno.ao.mrover.service.ShutdownService;
import de.konteno.ao.mrover.service.comm.MqttBridgeService;
import de.konteno.ao.mrover.service.dr.DeadReckoningService;
import de.konteno.ao.mrover.service.motor.DriveService;
import de.konteno.ao.mrover.service.odo.Odometer2Service;
import de.konteno.ao.mrover.service.turn.TurnService;
import io.vertx.core.Vertx;

public class SimulationApp extends RoverApp {

	public static void main(String[] args) {
		
		initLogging();

		// AWT graphics are used for "painting" on the OLED display
//		System.setProperty("java.awt.headless", "true");
		
		// -- vert.x configuration --------------------------------------------

		Vertx vertx = Vertx.vertx();
		
		registerCodecs(vertx);
		
		// -- start services --------------------------------------------------

//		vertx.deployVerticle(new DisplayService());

		DriveService drive = new MockDriveService();
		vertx.deployVerticle(drive);
		vertx.deployVerticle(new TurnService());
//		vertx.deployVerticle(new CompassService());
		vertx.deployVerticle(new MockCompassService());
		vertx.deployVerticle(new MqttBridgeService());
		vertx.deployVerticle(new DemoService());
		vertx.deployVerticle(new DeadReckoningService());
		vertx.deployVerticle(new MockSonarService());
		vertx.deployVerticle(new CollisionDetectorService());
		vertx.deployVerticle(new ShutdownService());

		Odometer2Service odos = new Odometer2Service();
//		odos.buildDriver = () -> new MockOdometer();
		odos.buildDriver = () -> new ReplayOdometer(Paths.get("src/test/resources/data/odometer.data"), vertx);
		vertx.deployVerticle(odos);
		
//		PathPanel.open();
		
		fixShutdownHooks();
		
		 // simulate commands
//		 new Thread(new Runnable() { 
//			 public void run() { demo4Stimuli(drive);
//		 }}).start();;
	}

	private static void demoStimuli(MyVerticleBase sender) {
		try {
			Thread.sleep(1000);
			// sender.busSend(new DriveCmd(Angle.fromDegree(0)));
			sender.busSend(new DemoCmd(DemoCmd.square));
			Thread.sleep(1740);
			//sender.busSend(new StopCmd("collision?"));
			sender.sendStop("collision?");

			Thread.sleep(1000);
			sender.busSend(new DemoCmd(DemoCmd.square));
			
			Thread.sleep(8000);
			sender.busPublish(new ShutdownEvt());

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void demo2Stimuli(MyVerticleBase sender) {
		try {
			Thread.sleep(500);
			sender.busSend(new DemoCmd(DemoCmd.squareTurn));
			
			Thread.sleep(30000);
			sender.busPublish(new ShutdownEvt());

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void demo3Stimuli(MyVerticleBase sender) {
		try {
			Thread.sleep(500);
			sender.busSend(new DriveCmd(Angle.fromDegree(0)));			
			Thread.sleep(1500);
			sender.busSend(new DriveCmd(Angle.fromDegree(90)));			
			Thread.sleep(1500);
			sender.busSend(new RotateCmd(0.4).tangential().steer(20));
			Thread.sleep(6000);
			sender.busSend(new RotateCmd(0.7).tangential().steer(70));
			Thread.sleep(4000);
			sender.busPublish(new ShutdownEvt());

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void demo4Stimuli(MyVerticleBase sender) {
		try {
			Thread.sleep(500);
			sender.busSend(new DemoCmd(DemoCmd.wiggle));			
			Thread.sleep(60000);
			sender.busPublish(new ShutdownEvt());

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void turnStimuli(MyVerticleBase sender) {
		try {
			Thread.sleep(2000);
			sender.busSend(new TurnCmd(Angle.fromDegree(60), false));
			Thread.sleep(900);
			sender.busSend(new TurnCmd(Angle.fromDegree(20), false));
			Thread.sleep(10000);
			sender.busSend(new TurnCmd(Angle.fromDegree(-5), false));

			Thread.sleep(5000);
			sender.busPublish(new ShutdownEvt());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
