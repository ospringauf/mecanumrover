package de.konteno.ao.mrover.app;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import de.konteno.ao.mrover.common.unit.Angle;
import de.konteno.ao.mrover.driver.odo.OdoSensor;

public class OdoCalibration {

	public OdoSensor left = new OdoSensor();
	public OdoSensor right = new OdoSensor();

	public OdoSensor sumleft = new OdoSensor();
	public OdoSensor sumright = new OdoSensor();
	
	public static void main(String[] args) throws Exception {
		(new OdoCalibration()).eval(Paths.get("src/test/resources/data/odometer1.data"));
	}


	private void eval(Path path) throws Exception {
//		left.rot = Angle.fromDegree(90.065);
//		right.rot = Angle.fromDegree(90.066);
		left.rot = Angle.fromDegree(90.13);
		right.rot = Angle.fromDegree(90.0);

		left.scaleY = left.scaleX = 1.0/77.9;
		right.scaleY = right.scaleX = 1.0/82;
		
		Files.lines(path).forEach(line -> {
			String payload = line.split("\t")[1];
			OdoSensor.updateMulti(payload, left, right);
				
			sumleft.delta = sumleft.delta.translate(left.transform());
			sumright.delta = sumright.delta.translate(right.transform());
		});
		
		System.out.println(sumleft);
		System.out.println(sumright);
		
		System.out.println(OdoSensor.toJsonArray(left, right));
	}
}
