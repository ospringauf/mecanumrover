/*********************************************************************
* Copyright (c) 2019 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package de.konteno.ao.mrover.service.lidar;

import static de.konteno.ao.mrover.common.cmd.ConfigSensorCmd.MODE_LIDAR;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.konteno.ao.mrover.common.Rover;
import de.konteno.ao.mrover.common.event.LidarScanEvt;
import de.konteno.ao.mrover.lidar.YdLidarX4;
import de.konteno.ao.mrover.service.MotionDetBase;
import io.vertx.core.Promise;

/**
 * uses YdLidar X4 JNI wrapper at https://github.com/ospringauf/ydlidar-jni
 * 
 * usb 1-1.1.2: cp210x converter now attached to ttyUSB1 internal fixed baud
 * rate at 128_000
 *
 */
public class LidarService extends MotionDetBase {

	private static final Logger LOGGER = LoggerFactory.getLogger(LidarService.class);

	private YdLidarX4 lidar;

	private String port;

	private long lidarOffTimer = 0;

	private long lidarPeriodic = 0;

	private final long scanFreq = 400; // millis between scans

	public LidarService(String port) {
		this.port = port;
	}

	@Override
	public void start(Promise<Void> startFuture) throws Exception {
		super.start();

		try {
			lidar = new YdLidarX4();
			lidar.init(port, 10, 500, -180, 180);

			startLidar();
		} catch (Exception e) {
			LOGGER.error("failed to initialize LIDAR", e);
		}

		onMotionChange(this::motionChange);

		startFuture.complete();
	}

	private void motionChange(Boolean moving) {
//		LOGGER.info("lidar motionChange " + moving);
		if (moving) {
			// moving -> enable lidar
			vertx.cancelTimer(lidarOffTimer);
			lidarOffTimer = 0;
			startLidar();
		} else if (Rover.hasMode(MODE_LIDAR) && lidarOffTimer == 0) {
			// stopped -> disable lidar
//			logger.info("turn off sensors in 10s due to {}", cmd);
			lidarOffTimer = vertx.setTimer(10000, t -> stopLidar());
		}
	}

	private void startLidar() {
		if (!Rover.hasMode(MODE_LIDAR)) {
			lidar.turnOn();
			Rover.sensorMode |= MODE_LIDAR;
			lidarPeriodic = vertx.setPeriodic(scanFreq, l -> scan());
		}
	}

	private void stopLidar() {
		vertx.cancelTimer(lidarPeriodic);
		lidar.turnOff();
		Rover.sensorMode &= ~MODE_LIDAR;
	}

	void scan() {
		// execute as blocking code; scan might take some time
		vertx.executeBlocking(future -> {
			int[] dist = lidar.scan();
			LidarScanEvt lse = new LidarScanEvt(dist, Rover.pose.clone()).reduce(2);
			future.complete(lse);
		}, res -> {
			busPublish((LidarScanEvt)res.result());
		});
	}

	@Override
	public void onShutdown() {
		stopLidar();
	}

}
