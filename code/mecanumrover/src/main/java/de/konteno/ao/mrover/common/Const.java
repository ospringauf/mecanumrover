package de.konteno.ao.mrover.common;

import java.awt.geom.Point2D;

import com.pi4j.io.i2c.I2CBus;

import de.konteno.ao.mrover.common.unit.Angle;
import de.konteno.ao.mrover.driver.motor.AdafruitMotorControl;

public class Const {
	public static final int MOTCONTROL_ADR = AdafruitMotorControl.DEFAULT_I2C_ADR;
	
	// use 100Hz if you need servos
	public static final int PWM_FREQ = 1100;

	public static final int OLED_ADR = 0x3C;

	public static final int COMPASS_ADR = 0x1E;

	public static final int ADC_ADR = 0x48;

	public static final int I2C_BUS_NUMBER = I2CBus.BUS_1;
	
	/**
	 * ESP32 distance sensors cut off at ...
	 */
	public final static int SONAR_SENSOR_LIMIT = 150;
	
	public final static int LIDAR_SENSOR_LIMIT = 380;
	
	public final static double MAX_ROTATION_SPEED = 0.7;
	
	/**
	 * Odometer counts per cm
	 */
//	public final static double ODO_CPCM = 44.5;
	public final static double ODO_CPCM = 39.2;

	/**
	 * distance between odometer sensors [cm]
	 */
	public final static double ODO_SENSOR_DISTANCE = 14.37;
	
	
	/**
	 * sonar sensor positions
	 */
	public static final Point2D.Double[] sonarPosition = { 
			new Point2D.Double( -8,  10),
			new Point2D.Double( -6,  14),
			new Point2D.Double(  6,  14),
			new Point2D.Double(  8,  10),
			new Point2D.Double(  8, -10),
			new Point2D.Double(  6, -14),
			new Point2D.Double( -6, -14),
			new Point2D.Double( -8, -10)
	};
	
	/**
	 * sonar sensor tilt
	 */
	public static final int sonarTilt = 0;
	public static final Angle[] sonarDirection = {
			Angle.fromDegree(-90+sonarTilt), 
			Angle.fromDegree(0-sonarTilt), 
			Angle.fromDegree(0+sonarTilt), 
			Angle.fromDegree(90-sonarTilt), 
			Angle.fromDegree(90+sonarTilt), 
			Angle.fromDegree(180-sonarTilt), 
			Angle.fromDegree(180+sonarTilt), 
			Angle.fromDegree(-90-sonarTilt)
		};
	
	public static final int sonarBeamWidth = 30; //55;
	public static final double lidarBeamWidth = 0.5; 
	
//	private static final int DISPLAY_ADR = 0x3F; // 1602 display, not used

}
