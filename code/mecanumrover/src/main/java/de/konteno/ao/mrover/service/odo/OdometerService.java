/*********************************************************************
* Copyright (c) 2018-2019 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package de.konteno.ao.mrover.service.odo;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.konteno.ao.mrover.common.Const;
import de.konteno.ao.mrover.common.Rover;
import de.konteno.ao.mrover.common.cmd.CalibrateOdometerCmd;
import de.konteno.ao.mrover.common.cmd.ConfigSensorCmd;
import de.konteno.ao.mrover.common.cmd.SendSerialMessageCmd;
import de.konteno.ao.mrover.common.event.LaserDistanceEvt;
import de.konteno.ao.mrover.common.event.OdometerEvt;
import de.konteno.ao.mrover.common.event.StoppedEvt;
import de.konteno.ao.mrover.common.track.Pose;
import de.konteno.ao.mrover.common.unit.Angle;
import de.konteno.ao.mrover.common.unit.Pos;
import de.konteno.ao.mrover.service.MyVerticleBase;
import io.reactivex.Observable;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.MessageConsumer;

/**
 * process x/y displacement measurements (events) from the "optical flow odometer"
 * (2 ADNS-5020-EN optical mouse sensors)
 * 
 * - update current Rover position
 * - generate waypoints in the Rover track
 *
 */
public class OdometerService extends MyVerticleBase {

	private static final Logger logger = LoggerFactory.getLogger(OdometerService.class);

	@Override
	public void start() throws Exception {
		super.start();

		// Rx stream experiment: update current waypoint for every ODO event,
		// but add max 3 waypoints per second to the Rover track 
		Observable<OdometerEvt> s = busListenStream(OdometerEvt.class);
		s.map(x -> updatePosition(x))
		.sample(333, TimeUnit.MILLISECONDS)
		.subscribe(this::busPublish); // emits Waypoints to Telemetry Client

		busConsume(CalibrateOdometerCmd.class, this::calibrate);
	}

	/**
	 * update Rover position relative to initial heading and current position
	 * @param odo odometer data (x/y displacement)
	 * @return a waypoint representing the updated Rover position
	 */
	private Pose updatePosition(OdometerEvt odo) {
		Pos d = Pos.of(odo.cmX(), odo.cmY());
//		if (Rover.getInitialHeading() == null) {
//			return new Waypoint(d, Angle.fromDegree(0), Angle.fromDegree(0), TrackType.ODO);
//		}
//		Angle relHeading = Rover.getHeading().minus(Rover.getInitialHeading());
		Angle relHeading = Rover.pose.heading;
		Rover.pose = Rover.pose.addRelative(d, relHeading);
		return Rover.pose;
	}

	/**
	 * Calibrate odometer sensitivity (counts/cm) using LIDAR distance readings.
	 * Listen for "ODO" and "LRF" events and collect distance and mouse counter
	 * data. On "STOP", the data will be evaluated.
	 * 
	 * Does not start any motion (drive/rotate)!
	 * 
	 */
	private Future<Void> calibrate(CalibrateOdometerCmd cmd) {
		logger.info("start odometer calibration");
		final boolean separate = false;

		Promise<Object> currentCalibration = Promise.promise();
		Promise<Void> ret = Promise.promise();

		int mode0 = Rover.sensorMode;
		// switch on LRF and ODO
		busSend(new ConfigSensorCmd(Rover.sensorMode | ConfigSensorCmd.MODE_LIDAR | ConfigSensorCmd.MODE_ODO));

		setSeparateOdometer(separate);

		// containers for collected data (combined or separate mouse sensors)
		final OdometerCalibrationData data = new OdometerCalibrationData();
		final OdometerCalibrationData data2 = new OdometerCalibrationData();

		// on "LRF" event: track min/max distance from LIDAR sensor (-45deg turret
		// direction -> y=RIGHT, -x=LEFT)
		final MessageConsumer<LaserDistanceEvt> onLrf = busListen(LaserDistanceEvt.class, lrf -> {
			double y = lrf.getDistance(LaserDistanceEvt.RIGHT);
			double x = lrf.getDistance(LaserDistanceEvt.LEFT);
			// logger.debug("LRF y={} x={}", y, x);
			if (y != Const.SONAR_SENSOR_LIMIT) {
				data.ymin = (data.ymin == 0) ? y : Math.min(y, data.ymin);
				data.ymax = (data.ymax == 0) ? y : Math.max(y, data.ymax);
			}
			if (x != Const.SONAR_SENSOR_LIMIT) {
				data.xmin = (data.xmin == 0) ? x : Math.min(x, data.xmin);
				data.xmax = (data.xmax == 0) ? x : Math.max(x, data.xmax);
			}
		});

		// on "ODO" event: collect odometer messages, sum counts
		final MessageConsumer<OdometerEvt> onOdo = busListen(OdometerEvt.class, odo -> {
			if (separate) {
				data.collect(odo.left);
				data2.collect(odo.right);
			} else {
				data.collect(odo);
			}
		});

		// on "STOP" (command interceptor) -> end calibration
//		final Handler<SendContext> onStop = busCmdListen(StopCmd.class, stopCmd -> {
//			currentCalibration.tryComplete();
//		});
		
		// on "STOPPED" event -> end calibration
		final MessageConsumer<StoppedEvt> onStop = busListen(StoppedEvt.class, evt -> {
			currentCalibration.tryComplete();
		});


		// after "stop" (collision?), evaluate the collected data
		currentCalibration.future().onComplete(ar -> {
			onLrf.unregister(); // stop listening to distance events
			onOdo.unregister(); // stop listening to odometer events
			//vertx.eventBus().removeInterceptor(onStop); // remove STOP interceptor
			onStop.unregister();
			busSend(new ConfigSensorCmd(mode0)); // back to original ESP32 configuration
			setSeparateOdometer(false); // standard: combine mouse sensors

			evalCalibration(data, data2);

			ret.complete();
		});

		return ret.future();
	}

	protected void setSeparateOdometer(final boolean separate) {
		// TODO turn into ConfigSensorCmd?
		busSend(new SendSerialMessageCmd("esp/cmd/odomode", separate ? "1" : "0"));
	}

	/**
	 * log calibration results
	 * 
	 * @param data
	 * @param data2
	 */
	private void evalCalibration(OdometerCalibrationData data, OdometerCalibrationData data2) {
		logger.info("odometer calibration results:");
		double y = Math.abs(data.ymax - data.ymin);
		double x = Math.abs(data.xmax - data.xmin);
		logger.info("traveled distance (LRF): y={}cm  x={}cm", y, x);

		logger.info("odometer-1 counts: dy={}  dx={}, samples={}, avg.sq={}", data.cy, data.cx, data.samples,
				data.sq / data.samples);
		logger.info("odometer-1 resolution (y): {}/cm", data.cy / y);
		logger.info("odometer-1 resolution (x): {}/cm", data.cx / x);

		if (data2 != null) {
			logger.info("odometer-2 counts: dy={}  dx={}, samples={}, avg.sq={}", data2.cy, data2.cx, data2.samples,
					data2.sq / data2.samples);
			logger.info("odometer-2 resolution (y): {}/cm", data2.cy / y);
			logger.info("odometer-2 resolution (x): {}/cm", data2.cx / x);
		}
	}

	@Override
	public void onShutdown() {
	}
}
