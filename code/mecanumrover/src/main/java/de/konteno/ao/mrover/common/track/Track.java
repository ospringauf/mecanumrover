/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package de.konteno.ao.mrover.common.track;

import java.util.ArrayList;

import io.vertx.core.Handler;

@SuppressWarnings("serial")
public class Track extends ArrayList<Pose> {

	public String mqttTopic;
	public Pose last;
	public long minTime;
	public long maxTime;
	public boolean telemetry;
	public Handler<Track> onAdd = null;
		
	public Track(String mqttTopic) {
		super();
		this.mqttTopic = mqttTopic;
	}
	
	@Override
	public boolean add(Pose p) {
		last = p;
		minTime = (minTime == 0) ? p.time : Math.min(minTime, p.time);
		maxTime = (maxTime == 0) ? p.time : Math.max(maxTime, p.time);

		if (onAdd != null) {
			onAdd.handle(this);
		}

		return super.add(p);
	}
	
	@Override
	public void clear() {
		super.clear();
		
		last = null;
		minTime = maxTime = 0;
	}
	

}
