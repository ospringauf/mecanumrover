/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.driver.sonar;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigital;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinMode;
import com.pi4j.wiringpi.Gpio;

import de.konteno.ao.mrover.driver.common.DistanceSensor;
import de.konteno.ao.mrover.driver.common.SensorException;

/**
 * HC-SR04 ultrasonic distance sensor (sonar)
 * 
 * When triggered, the device sends an ultrasonic sound and measures the round-trip time of the echo.
 * The ECHO signal is pulled HIGH when the sound is emitted and LOW when the echo is received. By measuring
 * the width of the HIGH signal, we get the round-trip time and thus the distance to the reflecting object. 
 * 
 * 1) set TRIG high for min. 10us to start measurement
 *            ______
 * TRIG  ____| 10us |___________________________________________
 * 
 * 2) device waits 250us after trigger, then sends 8 x 40kHz pulse (200us)
 * 
 *       ___________:__250us__:||||||||_______________________
 * 
 * 3) ECHO signal goes high after sending pulse, low after receiving echo.
 * measure high time to get sound pulse runtime (outbound+return), then multiply with speed of sound 
 *                                     __________________________
 * ECHO  _____________________________|      1m ~ 5800us         |_______ 
 * 
 * sources:
 * https://gist.github.com/rutgerclaes/6713672
 * 
 * experiences:
 * * try GPIO interrupt/listener instead of busy waiting
 * --> the Pi4J listeners cannot hande such short intervals, measured times are more or less random
 * --> using wiringPi native callbacks does not help either, results are just as bad
 * 
 * * control TRIG and ECHO with a single GPIO (change OUT/IN mode)
 * --> looks good, switching pin mode to OUTPUT for trigger, then to INPUT for reading the echo
 * 
 * 
 * TODO 1: mehrere Messungen (Mittelwert) zur Verbesserung der Genauigkeit
 * TODO 2: kontinuierliche Messung - Idee:
 * - verwende einen PWM-Pin als Trigger, z.B. mit 20-50 fallenden Flanken / s
 * - Trigger-Logik kann also entfallen, Timeout (teilweise) auch
 * - messe ECHO genau, oder durch undersampling/statistisches Sampling
 * - singlePinMode ist damit nicht möglich
 * TODO 3: externe, interrupt-gesteuerte Zeitmessung
 * 	http://www.forum-raspberrypi.de/Thread-sensor-signal-per-interrupt-abfragen
 *  http://www.raspberry-pi-geek.de/Magazin/2013/05/Tricks-zum-Programmieren-der-GPIO-Schnittstelle
 *  https://www.codeproject.com/Articles/1032794/Simple-I-O-device-driver-for-RaspberryPi
 *  http://cs.smith.edu/dftwiki/index.php/Tutorial:_Interrupt-Driven_Event-Counter_on_the_Raspberry_Pi
 * 
 * @author oliver
 *
 */
public class HCSR04 implements DistanceSensor {

	private GpioPinDigital echoPin;
	private GpioPinDigitalOutput trigPin;
	
	public final static float SOUND_SPEED = 34029; // speed of sound in cm/s

	private final static int TRIGGER_MICROS = 10; // trigger duration of 10us
	private final static int TIMEOUT_MILLIS = 60; // wait 60ms

	private boolean singlePinMode = false;
	
	/**
	 * Two-pin mode, use separate GPIOs for TRIG and ECHO
	 * @param trig
	 * @param echo
	 */
	public HCSR04(Pin trig, Pin echo) {
		singlePinMode = false;
		GpioController gpio = GpioFactory.getInstance();
		this.trigPin = gpio.provisionDigitalOutputPin(trig);
		this.trigPin.low();
		this.echoPin = gpio.provisionDigitalInputPin(echo);
		Gpio.delay(30);
	}

	/**
	 * Constructor for single-GPIO operation.
	 * Connect ECHO and TRIG to the same GPIO (attention: the voltage divider is still neccessary!)
	 * @param pin
	 */
	public HCSR04(Pin pin) {
		singlePinMode = true;
		GpioController gpio = GpioFactory.getInstance();
		this.trigPin = gpio.provisionDigitalOutputPin(pin);
		this.trigPin.low();
		this.echoPin = trigPin;
		Gpio.delay(30);
	}

	public static float micros2cm(long micros) {
        // note that the sound signal travels 2x the distance (outbound + return)
		return micros / 1e6f * SOUND_SPEED / 2.0f;
	}

	public static float nanos2cm(long nanos) {
        // note that the sound signal travels 2x the distance (outbound + return)
		return nanos / 1e9f * SOUND_SPEED / 2.0f;
	}

	/**
	 * This method returns the distance measured by the sensor in cm
	 */
    public float measureDistance() {
        long duration;
		try {
			duration = this.measureEcho();
		} catch (InterruptedException | SensorException e) {
			System.out.println(e.getMessage());
			return -1;
		}
        
        return nanos2cm(duration);
    }

    
    /**
     * Put a high on the trig pin for TRIG_DURATION_IN_MICROS
     * 
     * signal measured with piscope:
     * busy wait: typically 40-50us
     * thread sleep: typically 1000-1200us
     * wiringPi native delay: 30-40us
     * 
     * Then wait for ECHO to go HIGH and measure time until falling edge
     * 
     * @return the duration of the signal in nano seconds
     * @throws InterruptedException 
     * @throws SensorException 
     * @throws DistanceMonitor.TimeoutException if no low appears in time
     */
    private long measureEcho() throws InterruptedException, SensorException {

        long start = System.nanoTime();
        long timeout = start + TIMEOUT_MILLIS * 1000 * 1000;  
        
    	// --- TRIGGER -------------------------------------------------
        
    	if (singlePinMode) {
    		trigPin.setMode(PinMode.DIGITAL_OUTPUT);
    	}
		trigPin.high();

//		Thread.sleep(0, TRIGGER_MICROS * 1000);
		
//		long trigOff = System.nanoTime() + (TRIGGER_MICROS * 1000); // now + 10us
//		while (System.nanoTime() < trigOff) { }
		
		Gpio.delayMicroseconds(TRIGGER_MICROS);
		
		trigPin.low();
    	if (singlePinMode) {
    		trigPin.setMode(PinMode.DIGITAL_INPUT);
    	}

    	// --- ECHO ----------------------------------------------------    	
        
		// warte auf steigende Flanke, typisch 450us nach Trigger
        while (echoPin.isLow() && (start = System.nanoTime()) < timeout) { 
        }
        
        if (start > timeout) {
        	throw new SensorException("sonar - no echo");
        }        
        
        long end = start;
        timeout = start + TIMEOUT_MILLIS * 1000 * 1000;

        // warte auf fallende Flanke
        while (echoPin.isHigh() && (end = System.nanoTime()) < timeout) {
        }
        
        if (end > timeout) {
        	throw new SensorException("sonar - echo too late");
        }
        
        // debug output
//    	System.out.println(String.format("signal duration: %dus, loops: %d / %d, total %dms", 
//    			(end-start)/1000, loop1, loop2,
//    			System.currentTimeMillis() - t0));
        
		return end - start;
	}
    
    public void shutdown() {
    	try {
			GpioController gpio = GpioFactory.getInstance();
			gpio.unprovisionPin(trigPin);		
			if (!singlePinMode) {
				gpio.unprovisionPin(echoPin);
			}
    	} catch (Exception e) {
			e.printStackTrace();
		}
	}
        
    
	public void demo() throws InterruptedException {
		for (int i=0; i<10; ++i) {
			
	    	long t0 = System.currentTimeMillis();
			double d = measureDistance();
			System.out.println(String.format("distance = %3.1fcm [%dms]", d, System.currentTimeMillis() - t0));
			
			Thread.sleep(1000);
		}
	}

	@Override
	public float measureDistance(int samples) throws SensorException {
		int success = 0;
		float sum = 0;
		for (int i=0; i<samples; ++i) {
			float d = measureDistance();
			if (d >= 0) {
				success++;
				sum += d;
			}
			Gpio.delay(50); // recommended cycle time; wait for echo to fade away
		}
		
		return (success > 0) ? sum/success : -1;
	}

	@Override
	public void fastMode() {
		// TODO Auto-generated method stub
		
	}

}
