package de.konteno.ao.mrover.dryrun;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.stream.Stream;

import org.reactivestreams.Subscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.konteno.ao.mrover.driver.odo.IOdo2Driver;
import de.konteno.ao.mrover.driver.odo.OdoSensor;
import io.vertx.core.Vertx;

/**
 * Replay recorded odometer data with the original timing (intervals between measurements).
 * Mocks an odometer driver from simulation.
 * Use LoggingOdometer2Driver to gather the data in the first place.
 */
public class ReplayOdometer implements IOdo2Driver {

	private static final Logger logger = LoggerFactory.getLogger(ReplayOdometer.class);
	
	Subscriber<? super OdoSensor[]> subscriber;

	private String payload;
	private long timestamp;

	private Vertx vertx;
	private Stream<String> stream;

	public ReplayOdometer(Path dataFile, Vertx vertx) {
		this.vertx = vertx;
		try {
			stream = Files.lines(dataFile); 
			logger.info("replaying odometer data from " + dataFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		readConfiguration();
	}

	private long readNextData(Iterator<String> input) {
		if (input.hasNext()) {
			String data = input.next();
			long nextTimestamp = Long.parseLong(data.split("\t")[0]);
			long wait = nextTimestamp - timestamp;
			payload = data.split("\t")[1];
			timestamp = nextTimestamp;
			return wait;
		}
		return -1;
	}

	@Override
	public void powerUp() {
	}

	@Override
	public void powerDown() {
	}

	@Override
	public void shutdown() {
		stream.close();
	}

	@Override
	public void subscribe(Subscriber<? super OdoSensor[]> sub) {
		this.subscriber = sub;
		
		Iterator<String> input = stream.iterator();
		readNextData(input);
		replayData(input);
	}

	private void replayData(Iterator<String> input) {
		OdoSensor.updateMulti(payload, sensors);

		// pass data to subscriber (service)
		subscriber.onNext(sensors);

		// read next line and wait until it can be sent
		long wait = readNextData(input);
		if (wait >= 0) {
			// let vert.x call me when it's time
			vertx.setTimer(wait, (x) -> replayData(input));
		} else {
			// no more data in input
			logger.info("odometer data exhausted");
		}
	}

	@Override
	public void saveConfiguration() {
		// TODO Auto-generated method stub
		
	}

}
