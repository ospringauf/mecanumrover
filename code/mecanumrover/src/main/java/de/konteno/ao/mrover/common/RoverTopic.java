package de.konteno.ao.mrover.common;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * annotation for all command/event POJOs that are transmitted over vert.x event bus
 * @author oliver
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface RoverTopic {

	String address();

	int timeout() default 0;

//	boolean needReply() default false;

}
