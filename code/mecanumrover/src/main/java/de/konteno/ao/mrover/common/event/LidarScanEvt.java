/*********************************************************************
* Copyright (c) 2019 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.common.event;

import de.konteno.ao.mrover.common.RoverTopic;
import de.konteno.ao.mrover.common.track.Pose;

/**
 * Lidar scan event
 * 
 */
@RoverTopic(address="evt.lidar")
public class LidarScanEvt implements IRoverEvent {

	public int scan[];
	public Pose pose;

	public LidarScanEvt(int scan[], Pose pose) {
		this.scan = scan;
		this.pose = pose;
	}

	private LidarScanEvt() {}
	
	public static LidarScanEvt parseMqtt(String s) {
		LidarScanEvt e = new LidarScanEvt();
		String[] f = s.split("\\|");
		e.pose = Pose.parseMqtt(f[0]);		
		f = f[1].split(" ");
		e.scan = new int[f.length];
		for (int i=0; i<e.scan.length; ++i) {
			e.scan[i] = Integer.parseInt(f[i]);
		}
		return e;
	}
	
	/**
	 * reduce the number of samples by factor n. 
	 * calculate the minimum of each n samples, ignoring "0" values.
	 * @param n
	 * @return reduced scan
	 */
//	public LidarScanEvt reduce(int n) {
//		LidarScanEvt e = new LidarScanEvt();
//		e.pose = pose;
//		e.scan = new int[scan.length / n];
//		
//		int ei = 0;
//		for (int i=0; i<=scan.length-n; i+=n) {
//			int nn = 0;
//			int sum = 0;
//			for (int j=i; j<i+n; ++j) {
//				if (scan[j] > 0) {
//					nn++;
//					sum += scan[j];
//				}
//			}
//			e.scan[ei++] = (nn>1) ? sum/nn : sum;
//		}
//		
//		return e;
//	}
	public LidarScanEvt reduce(int n) {
		LidarScanEvt e = new LidarScanEvt();
		e.pose = pose;
		e.scan = new int[scan.length / n];
		
		int ei = 0;
		for (int i=0; i<=scan.length-n; i+=n) {
			int min = 10000;
			for (int j=i; j<i+n; ++j) {
				if (scan[j] > 0) {
					min = (scan[j] < min) ? scan[j] : min;
				}
			}
			e.scan[ei++] = (min < 10000) ? min : 0;
		}
		return e;
	}

	public String mqttString() {
		StringBuilder b = new StringBuilder();
		b.append(pose.mqttString());
		b.append("|");
		for (int d : scan) {
			b.append(d);
			b.append(" ");
		}
		return b.toString();
	}

	public double beamWidth() {
		return 360.0/scan.length;
	}

}
