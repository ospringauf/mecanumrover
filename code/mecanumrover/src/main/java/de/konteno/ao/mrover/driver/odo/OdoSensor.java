/*********************************************************************
* Copyright (c) 2019 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package de.konteno.ao.mrover.driver.odo;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.konteno.ao.mrover.common.unit.Angle;
import de.konteno.ao.mrover.common.unit.Pos;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

/**
 * current state (last measurement) of a single optical flow sensor such as ADNS-5020
 *
 */
public class OdoSensor {
    // displacement
    public transient Pos delta = Pos.of(0, 0);

    // signal quality
    public transient int squal;

    // configuration:
    // scaling converts from counts (CPI) to cm
    public double scaleX = 1.0 / 79.0;
    public double scaleY = 1.0 / 82.0;

    // rotation: compensate angle between sensor orientation and rover coordinates
    public Angle rot = Angle.fromRad(-Math.PI / 2.0);

    public void update(int dx, int dy, int sq) {
        delta.x = dx;
        delta.y = dy;
        squal = sq;
    }

    public boolean motion() {
        return delta.x != 0 || delta.y != 0;
    }

    public Pos transform() {
        return delta.rotate(rot).scale(scaleX, scaleY);
    }

    public Pos inverseTransform() {
        return inverseTransform(delta);
    }

    public Pos inverseTransform(Pos p) {
        return p.scale(1.0 / scaleX, 1.0 / scaleY).rotate(rot.negative());
    }
    
    
    // parsing and JSON serialization ---------------------------- 

    public static void updateMulti(String payload, OdoSensor... sensors) {
        String[] f = payload.split(",");
        int i = 0;
        for (OdoSensor s : sensors) {
            s.update(Integer.parseInt(f[i++]), Integer.parseInt(f[i++]), Integer.parseInt(f[i++]));
        }
    }

    public JsonObject toJson() {
        return JsonObject.mapFrom(this);
//        return (new ObjectMapper()).writeValueAsString(this);
    }

    public static JsonArray toJsonArray(OdoSensor... sensors) {
        JsonArray a = new JsonArray();
        for (OdoSensor s : sensors) {
            a.add(JsonObject.mapFrom(s));
        }
        return a;
    }

    public static OdoSensor fromJson(String j) {
        return new JsonObject(j).mapTo(OdoSensor.class);
    }
    
    public static OdoSensor[] fromJsonArray(String j) throws IOException {
        return (new ObjectMapper()).readValue(j, OdoSensor[].class);
    }

    @Override
    public String toString() {
        return String.format("%s sq=%d", delta.toString(), squal);
    }

}
