/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.common.util;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.reflections.Reflections;

import de.konteno.ao.mrover.common.RoverTopic;

/**
 * parts from:
 * https://stackoverflow.com/questions/862106/how-to-find-annotated-methods-in-a-given-package#862130
 *
 */
public class ReflectionHelper {

	/**
	 * find all classes in "common" having the "RoverTopic" annotation
	 */
	public static Iterable<Class<?>> getRoverTopics() {
		// // FIXME is there any other option than "org.reflections"?
		 Reflections reflections = new Reflections("de.konteno.ao.mrover");
		 Set<Class<?>> topics = reflections.getTypesAnnotatedWith(RoverTopic.class);
		 return topics;
	}
		

	/**
	 * find all classes in "common" having the "RoverTopic" annotation
	 */
	public static Iterable<Class<?>> getRoverTopics_alt() {
		try {
			return getClasses("de.konteno.ao.mrover.common")
					.stream()
					.filter(c -> c.isAnnotationPresent(RoverTopic.class))
					.collect(Collectors.toList());
		} catch (ClassNotFoundException | IOException | URISyntaxException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * try to find current shutdown hooks with some dirty reflection
	 */
    @SuppressWarnings("unchecked")
	public static Set<Thread> findShutdownHooks() {
		try {
		    Class<?> clazz = Class.forName("java.lang.ApplicationShutdownHooks");
		    Field field = clazz.getDeclaredField("hooks");
		    field.setAccessible(true);
			Map<Thread, Thread> hooks = (Map<Thread, Thread>) field.get(null);
		    return hooks.keySet();
		} catch (ClassNotFoundException | NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
			return null;
		}	
	}

	/**
	 * Scans all classes accessible from the context class loader which belong to
	 * the given package and subpackages.
	 * 
	 * @param packageName
	 *            The base package
	 * @return The classes
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	public static List<Class<?>> getClasses(String packageName)
			throws ClassNotFoundException, IOException, URISyntaxException {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		String path = packageName.replace('.', '/');
		Enumeration<URL> resources = classLoader.getResources(path);
		List<File> dirs = new ArrayList<File>();
		while (resources.hasMoreElements()) {
			URL resource = resources.nextElement();
			URI uri = new URI(resource.toString());
			dirs.add(new File(uri.getPath()));
		}
		List<Class<?>> classes = new ArrayList<Class<?>>();
		for (File directory : dirs) {
			classes.addAll(findClasses(directory, packageName));
		}

		return classes;
	}

	/**
	 * Recursive method used to find all classes in a given directory and subdirs.
	 * 
	 * @param directory
	 *            The base directory
	 * @param packageName
	 *            The package name for classes found inside the base directory
	 * @return The classes
	 * @throws ClassNotFoundException
	 */
	private static List<Class<?>> findClasses(File directory, String packageName) throws ClassNotFoundException {
		List<Class<?>> classes = new ArrayList<Class<?>>();
		if (!directory.exists()) {
			return classes;
		}
		File[] files = directory.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				classes.addAll(findClasses(file, packageName + "." + file.getName()));
			} else if (file.getName().endsWith(".class")) {
				try {
					String cname = packageName + '.' + file.getName().substring(0, file.getName().length() - 6);
					Class<?> c = Class.forName(cname);
					classes.add(c);
				} catch (Exception e) {
					System.err.println(e.getMessage());
				}
			}
		}
		return classes;
	}
}
