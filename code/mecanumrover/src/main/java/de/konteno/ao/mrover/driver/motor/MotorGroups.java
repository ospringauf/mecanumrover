/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.driver.motor;

/**
 * motor layout:
 * 
 * 	+-------------+
 *  | 0    ^    1 |
 *  |             |
 *  |             |
 *  |             |
 *  |             |
 *  | 2         3 |
 * 	+-------------+
 * 
 *
 */
public class MotorGroups {

	private MotorGroup left;
	private MotorGroup right;
	private MotorGroup front;
	private MotorGroup rear;
	private MotorGroup d03; // diagonal \
	private MotorGroup d12; // diagonal /
	private MotorGroup all;

	public MotorGroups(AdafruitMotorControl motcontrol) {
		AdafruitMotor mot0 = motcontrol.getMotor(0);
		AdafruitMotor mot1 = motcontrol.getMotor(1);
		AdafruitMotor mot2 = motcontrol.getMotor(2);
		AdafruitMotor mot3 = motcontrol.getMotor(3);

		left = new MotorGroup(new AdafruitMotor[] {mot0, mot2});
		right = new MotorGroup(new AdafruitMotor[] {mot1, mot3});
		front = new MotorGroup(new AdafruitMotor[] {mot0, mot1});
		rear = new MotorGroup(new AdafruitMotor[] {mot2, mot3});
		d03 = new MotorGroup(new AdafruitMotor[] {mot0, mot3});
		d12 = new MotorGroup(new AdafruitMotor[] {mot1, mot2});
		all = new MotorGroup(new AdafruitMotor[] {mot0, mot1, mot2, mot3});
	}


	public MotorGroup getLeft() {
		return left;
	}

	public MotorGroup getRight() {
		return right;
	}

	public MotorGroup getFront() {
		return front;
	}

	public MotorGroup getRear() {
		return rear;
	}

	public MotorGroup getD03() {
		return d03;
	}

	public MotorGroup getD12() {
		return d12;
	}

	public MotorGroup getAll() {
		return all;
	}

}
