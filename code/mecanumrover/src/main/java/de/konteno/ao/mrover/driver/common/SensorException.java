/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.driver.common;

public class SensorException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SensorException(String string) {
		super(string);
	}

	public SensorException(Exception e) {
		super(e);
	}

}
