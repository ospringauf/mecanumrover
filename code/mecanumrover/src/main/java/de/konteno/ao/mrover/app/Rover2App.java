/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.app;

import java.nio.file.Paths;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pi4j.io.serial.Baud;

import de.konteno.ao.mrover.common.event.ShutdownEvt;
import de.konteno.ao.mrover.common.util.MyMessageCodec;
import de.konteno.ao.mrover.common.util.ReflectionHelper;
import de.konteno.ao.mrover.driver.motor.AdafruitMotorControl;
import de.konteno.ao.mrover.driver.odo.LoggingOdometer2Driver;
import de.konteno.ao.mrover.service.DemoService;
import de.konteno.ao.mrover.service.ShutdownService;
import de.konteno.ao.mrover.service.comm.MqttBridgeService;
import de.konteno.ao.mrover.service.compass.MyImuService;
import de.konteno.ao.mrover.service.dr.DeadReckoningService;
import de.konteno.ao.mrover.service.info.DisplayService;
import de.konteno.ao.mrover.service.lidar.LidarService;
import de.konteno.ao.mrover.service.motor.DriveService;
import de.konteno.ao.mrover.service.odo.Odometer2Service;
import de.konteno.ao.mrover.service.turn.TurnService;
import io.vertx.core.Vertx;

/**
 * ┌─┐ ┬ ┌─┐  ╔╦╗╔═╗╔═╗╔═╗╔╗╔╦ ╦╔╦╗╦═╗╔═╗╦  ╦╔═╗╦═╗
 * ├─┤┌┼─│ │  ║║║║╣ ║  ╠═╣║║║║ ║║║║╠╦╝║ ║╚╗╔╝║╣ ╠╦╝ 
 * ┴ ┴└┘ └─┘  ╩ ╩╚═╝╚═╝╩ ╩╝╚╝╚═╝╩ ╩╩╚═╚═╝ ╚╝ ╚═╝╩╚═
 * with vert.x microservices
 * 
 * font: http://patorjk.com/software/taag/#p=display&f=Calvin%20S&t=MECANUMROVER / "Calvin S"
 * 
 * check dmesg for USB ports, e.g. 
 * Odometer/Arduino: usb 1-1.1.3: ch341-uart converter now attached to ttyUSB0
 * YdLidarX4: usb 1-1.1.2: cp210x converter now attached to ttyUSB1
 *
 */
public class Rover2App {
	static final String LIDAR_PORT = "/dev/ttyUSB0";
	static final String ODOMETER_PORT = "/dev/ttyUSB1";
	private static final Logger logger = LoggerFactory.getLogger( Rover2App.class );

	public static void main(String[] args) {
		// Rover 2 specific settings
		//AdafruitMotor.speedScale = 0.675; // 135rpm/200rpm
		AdafruitMotorControl.motorMapping = new int[] { 3, 2, 1, 0 };
		
		initLogging();
		logger.info("starting ROVER-2");
		
		// AWT graphics are used for "painting" on the OLED display
		System.setProperty("java.awt.headless", "true");

		// -- vert.x configuration --------------------------------------------

		Vertx vertx = Vertx.vertx();
		
		registerCodecs(vertx);
		

		// -- start services --------------------------------------------------

		vertx.deployVerticle(new DisplayService());
		DriveService drive = new DriveService();
		vertx.deployVerticle(drive);
		vertx.deployVerticle(new TurnService());
//		vertx.deployVerticle(new CompassService());  // HMC5883L via I2C or BNO055 via ESP32/serial
		vertx.deployVerticle(new MqttBridgeService());
//		vertx.deployVerticle(new EspBridgeService());
//		vertx.deployVerticle(new CollisionDetectorService());
		vertx.deployVerticle(new DemoService());
//		vertx.deployVerticle(new FollowService());
//		vertx.deployVerticle(new OdometerService());
		vertx.deployVerticle(new DeadReckoningService());
		
//		vertx.deployVerticle(new DrCompassService()); // simulated compass based on DR
		
		Odometer2Service odos = new Odometer2Service();
		odos.buildDriver = () -> new LoggingOdometer2Driver(ODOMETER_PORT, Baud._38400, Paths.get("odometer.data"));
		vertx.deployVerticle(odos);	
		
		vertx.deployVerticle(new MyImuService());
		vertx.deployVerticle(new LidarService(LIDAR_PORT));
		
		ShutdownService shut;
		vertx.deployVerticle(shut = new ShutdownService());

		// wait until all services are started, then try to remove Pi4J serial shutdown hook
		vertx.setTimer(3000, id -> { fixShutdownHooks(); });
		
		
		// -- shutdown handling -----------------------------------------------
		// Shutdown the motors on exit. From Adafruit tutorial:
		// The PWM driver is 'free running' - that means that even if the python
		// code or Pi linux kernel crashes, the PWM driver will still continue to work. This is good because it
		// lets the Pi focus on linuxy things while the PWM driver does its PWMy things. But it means that the
		// motors DO NOT STOP when the python code quits. For that reason, we strongly recommend this 'at exit' code when using
		// DC motors, it will do its best to shut down all the motors.
		Runtime.getRuntime().addShutdownHook(new Thread("mrover shutdown") {
			@Override
			public void run() {
				try {
					shut.busPublish(new ShutdownEvt());

					logger.info("shutdown all motors");
					if (drive != null) {
						drive.stopMotors(null);
					}

					Thread.sleep(2500);
					vertx.close();
					
//					// what's still running?
//					Set<Thread> threads = Thread.getAllStackTraces().keySet();
//					 
//					for (Thread t : threads) {
//					    String name = t.getName();
//					    Thread.State state = t.getState();
//					    int priority = t.getPriority();
//					    String type = t.isDaemon() ? "Daemon" : "Normal";
//					    System.out.printf("%-20s \t %s \t %d \t %s\n", name, state, priority, type);
//					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}

	protected static void registerCodecs(Vertx vertx) {
//		vertx.eventBus().registerDefaultCodec(DriveCmd.class, new MyMessageCodec<DriveCmd>(DriveCmd.class));
//		vertx.eventBus().registerDefaultCodec(RotateCmd.class, new MyMessageCodec<RotateCmd>(RotateCmd.class));
//		... and so on

		// find all classes with "@RoverTopic" annotation and register a bus codec for each
		Iterable<Class<?>> topics = ReflectionHelper.getRoverTopics();
		
		topics.forEach(x -> {
			Class c = x;
			vertx.eventBus().registerDefaultCodec(c, new MyMessageCodec<>(c));
		});
	}
	
	
	protected static void initLogging() {
		// see http://vertx.io/docs/apidocs/io/vertx/core/logging/Logger.html
		System.setProperty("vertx.logger-delegate-factory-class-name", "io.vertx.core.logging.SLF4JLogDelegateFactory");
	}

	protected static void fixShutdownHooks() {
		try {
			Set<Thread> hooks = ReflectionHelper.findShutdownHooks();
		    
		    // Pi4J registers a shutdown hook for the serial interface that fires before we get a chance
		    // to say goodbye via serial.
		    // try to find and remove that shutdown thread:

//			System.out.println("shutdown threads");
//			hooks.stream().forEach(t -> System.out.println(t));
//			System.out.println("shutdown thread classes");
//			hooks.stream().forEach(t -> System.out.println(t.getClass().getName()));
		    Optional<Thread> serialShutdown = hooks.stream().filter(t -> t.getClass().getName().startsWith("com.pi4j.io.serial")).findFirst();
		    if (serialShutdown.isPresent()) {
		    	logger.debug("removing PI4J serial shutdown hook: {}", serialShutdown.get().getClass().getName());
		    	Runtime.getRuntime().removeShutdownHook(serialShutdown.get());
		    }
		    
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
