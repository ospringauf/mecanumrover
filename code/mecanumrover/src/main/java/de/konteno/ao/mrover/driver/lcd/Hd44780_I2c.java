/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.driver.lcd;

import java.io.IOException;

import com.pi4j.io.i2c.I2CDevice;

/**
 * Standard 1602/2004 LCD (HD44780 compatible) with I2C adapter based on 
 * PCF8574 GPIO extender chip.
 * I2C address is usually 0x27 or 0x3F.
 * 
 * see http://www.circuitbasics.com/raspberry-pi-i2c-lcd-set-up-and-programming/
 * https://tutorials-raspberrypi.de/hd44780-lcd-display-per-i2c-mit-dem-raspberry-pi-ansteuern/
 * http://www.netzmafia.de/skripten/hardware/RasPi/Projekt-LCD/
 *
 */
public class Hd44780_I2c  {

	// commands
	public static final int LCD_CLEARDISPLAY = 0x01;
	public static final int LCD_RETURNHOME = 0x02;
	public static final int LCD_ENTRYMODESET = 0x04;
	public static final int LCD_DISPLAYCONTROL = 0x08;
	public static final int LCD_CURSORSHIFT = 0x10;
	public static final int LCD_FUNCTIONSET = 0x20;
	public static final int LCD_SETCGRAMADDR = 0x40;
	public static final int LCD_SETDDRAMADDR = 0x80;

	// flags for display entry mode
	public static final int LCD_ENTRYRIGHT = 0x00;
	public static final int LCD_ENTRYLEFT = 0x02;
	public static final int LCD_ENTRYSHIFTINCREMENT = 0x01;
	public static final int LCD_ENTRYSHIFTDECREMENT = 0x00;

	// flags for display on/off control
	public static final int LCD_DISPLAYON = 0x04;
	public static final int LCD_DISPLAYOFF = 0x00;
	public static final int LCD_CURSORON = 0x02;
	public static final int LCD_CURSOROFF = 0x00;
	public static final int LCD_BLINKON = 0x01;
	public static final int LCD_BLINKOFF = 0x00;

	// flags for display/cursor shift
	public static final int LCD_DISPLAYMOVE = 0x08;
	public static final int LCD_CURSORMOVE = 0x00;
	public static final int LCD_MOVERIGHT = 0x04;
	public static final int LCD_MOVELEFT = 0x00;

	// flags for function set
	public static final int LCD_8BITMODE = 0x10;
	public static final int LCD_4BITMODE = 0x00;
	public static final int LCD_2LINE = 0x08;
	public static final int LCD_1LINE = 0x00;
	public static final int LCD_5x10DOTS = 0x04;
	public static final int LCD_5x8DOTS = 0x00;

	// flags for backlight control
	public static final int LCD_BACKLIGHT = 0x08;
	public static final int LCD_NOBACKLIGHT = 0x00;

	public static final int En = 0b00000100; // Enable bit
	public static final int Rw = 0b00000010; // Read/Write bit
	public static final int Rs = 0b00000001; // Register select bit

	private I2CDevice device;

	public Hd44780_I2c(I2CDevice dev) throws InterruptedException, IOException {
		device = dev;
		lcdWrite(0x03);
		lcdWrite(0x03);
		lcdWrite(0x03);
		lcdWrite(0x02);

		lcdWrite(LCD_FUNCTIONSET | LCD_2LINE | LCD_5x8DOTS | LCD_4BITMODE);
		lcdWrite(LCD_DISPLAYCONTROL | LCD_DISPLAYON);
		lcdWrite(LCD_CLEARDISPLAY);
		lcdWrite(LCD_ENTRYMODESET | LCD_ENTRYLEFT);

		Thread.sleep(200);
	}

	/**
	 * clocks EN to latch command
	 * @param data
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private void lcdStrobe(int data) throws IOException, InterruptedException {
		device.write((byte) (data | En | LCD_BACKLIGHT));
		Thread.sleep(1);
		// sleep(.0005)
		device.write((byte) ((data & ~En) | LCD_BACKLIGHT));
		// sleep(.0001);
		Thread.sleep(1);
	}

	private void lcdWriteFourBits(int data) throws IOException, InterruptedException {
		device.write((byte) (data | LCD_BACKLIGHT));
		lcdStrobe(data);
	}

	/**
	 *  write a command to lcd
	 * @param cmd
	 * @param mode
	 */
	private void lcdWrite(int cmd, int mode) {
		try {
			lcdWriteFourBits(mode | (cmd & 0xF0));
			lcdWriteFourBits(mode | ((cmd << 4) & 0xF0));
			
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void lcdWrite(int cmd) throws IOException, InterruptedException {
		lcdWrite(cmd, 0);
	}

	/**
	 * turn on/off the lcd backlight
	 * 
	 * @param state
	 * @throws IOException
	 */
	public void backlight(boolean state) throws IOException {
		device.write((byte) (state ? LCD_BACKLIGHT : LCD_NOBACKLIGHT));
	}

	/**
	 * Display text in line. 
	 * If text is shorter than the current content, remaining characters will not be cleared. 
	 * 
	 * @param text
	 * @param line 1,2,(3,4)
	 * @param offset column in line
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void displayString(String text, int line) throws IOException, InterruptedException {

		displayString(text, line, 0);
	}

	public void displayString(String text, int line, int offset) throws IOException, InterruptedException {

		// line address in HD44780 display register
		int[] lineAdr = new int[] {0x00, 0x40, 0x14, 0x54};
		lcdWrite(LCD_SETDDRAMADDR + lineAdr[line - 1] + offset);

		text.chars().forEach(c -> lcdWrite(c, Rs));
	}

	/**
	 * clear lcd and set to home
	 * 
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void clear() throws IOException, InterruptedException {
		lcdWrite(LCD_CLEARDISPLAY);
		lcdWrite(LCD_RETURNHOME);
	};

}
