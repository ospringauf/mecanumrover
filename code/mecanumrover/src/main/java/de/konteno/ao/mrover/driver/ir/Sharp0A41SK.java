/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.driver.ir;

import java.io.IOException;

import de.konteno.ao.mrover.driver.adc.ADConverter;
import de.konteno.ao.mrover.driver.common.DistanceSensor;
import de.konteno.ao.mrover.driver.common.SensorException;

/**
 * Sharp GP2Y0A41SK0F analog infrared distance sensor
 * datasheet: http://image.dfrobot.com/image/data/SEN0143/GP2Y0A41SK%20datasheet.pdf
 * or https://www.pololu.com/file/download/GP2Y0A41SK0F.pdf?file_id=0J713
 *
 */
public class Sharp0A41SK implements DistanceSensor
{
    private ADConverter adc; // the AD converter
	private short analogChannel;
	private int scale; // was 3600 for 12bit MCP3208

    public Sharp0A41SK(ADConverter ad, short analogChan)
    {
        this.adc = ad;
        this.setAnalogChannel(analogChan);
        scale = (1 << ad.getResolution());
    }

    public int getRawValue() throws IOException
    {
        return adc.readSingleValue(getAnalogChannel());
    }

    public float measureDistance() throws SensorException
    {
    	try {
	    	int r = getRawValue();
	    	if (r < 0) {
	    		return -1;
	    	}
	    	else {
	    		float d = scale / (float)r;
	    		return (d > 100) ? 101 : d;
	    	}
    	} catch (Exception e) {
			throw new SensorException(e);
		}
    }

	public short getAnalogChannel() {
		return analogChannel;
	}

	public void setAnalogChannel(short analogChannel) {
		this.analogChannel = analogChannel;
	}

	@Override
	public float measureDistance(int samples) throws SensorException {
		try {
			int success = 0;
			float sum = 0;
			for (int i = 0; i < samples; ++i) {
				float d = measureDistance();
				System.out.print(String.format("%5.1f ", d));
				if (d >= 0) {
					success++;
					sum += d;
				}
				Thread.sleep(15); // average sample rate of 0A41SK is 16.5+/-4ms
			}
			
			float avg = (success > 0) ? sum / success : -1;
			System.out.println(String.format("  >> %5.1f", avg));
			return avg;
		} catch (Exception e) {
			throw new SensorException(e);
		}
	}

	@Override
	public void fastMode() {
		adc.continuousMode(analogChannel);		
	}

}
