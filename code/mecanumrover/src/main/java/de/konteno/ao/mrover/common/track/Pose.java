/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.common.track;

import de.konteno.ao.mrover.common.RoverTopic;
import de.konteno.ao.mrover.common.event.IRoverEvent;
import de.konteno.ao.mrover.common.unit.Angle;
import de.konteno.ao.mrover.common.unit.Pos;

// TODO update time when angle changes (IMU/Compass event)

@RoverTopic(address = "evt.pose")
public class Pose implements IRoverEvent {
	static final long t0 = System.currentTimeMillis();

	/**
	 * x/y position
	 */
	public Pos pos;
	
	/**
	 * Heading (in global system)
	 * This is the rover's orientation relative to the global system,
	 * ie. the direction where the "nose" to pointing.
	 * 
	 * If we go sideways or backwards, the heading stays the same.
	 */
	public Angle heading;
	
	/**
	 * Course in the rover system (forward=0, right=90)
	 * useful for selecting the right collision sensors
	 */
	public Angle relativeCourse;
	
	public long time;
	public TrackType type;

	
	public Pose clone() {
		Pose p = new Pose();
		p.pos = pos;
		p.heading = heading;
		p.relativeCourse = relativeCourse;
		p.time = time;
		p.type = type;
		
		return p;
	}
	
	private Pose() {}
	
	public Pose(Pos p, Angle h, Angle rc, TrackType ty) {
		this.pos = p;
		this.heading = h;
		this.relativeCourse = rc;
		this.time = System.currentTimeMillis();
		this.type = ty;
	}
	
	public static Pose parseMqtt(String payload) {
		String[] f = payload.trim().replace(',', '.').split("\\s+");
		Pose p = new Pose();
		p.time = Long.parseLong(f[0]);
		p.pos = Pos.of(f[1], f[2]);
		p.heading = Angle.fromDegree(Double.parseDouble(f[3]));
		p.relativeCourse = Angle.fromDegree(Double.parseDouble(f[4]));
		return p;
	}


	@Override
	public String toString() {
		return String.format("[%3.3f] x=%3.1f y=%3.1f  head=%3.1f relCourse=%3.1f", 
				(time - t0)/1000.0, pos.x, pos.y, heading.toDegree(), relativeCourse.toDegree());
	}

	/**
	 * rotate delta motion vector by new heading (positive = clockwise!) and 
	 * create a new incremented waypoint
	 * 
	 * TODO use (heading + h)/2 for rotation?
	 * 
	 * @param dx relative x motion in heading direction 
	 * @param dy relative y motion in heading direction
	 * @param h new heading
	 * @return next waypoint
	 */
	public Pose addRelative(Pos delta, Angle h, Angle rel) {
		delta = delta.rotate(h.negative()); // mathematically, positive angles are CCW
		return new Pose(pos.translate(delta), h, rel, type);
	}

	public Pose addRelative(Pos delta, Angle h) {
		// calc relative course from dx/dy
		return addRelative(delta, h, delta.toAngle());
	}

	public String mqttString() {	
		return String.format("%d %3.2f %3.2f %3.2f %3.2f", time, pos.x, pos.y, heading.toDegree(), (relativeCourse != null) ? relativeCourse.toDegree() : 0.0);
	}
}
