package de.konteno.ao.mrover.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.konteno.ao.mrover.common.IRoverMessage;
import de.konteno.ao.mrover.common.RoverHandler;
import de.konteno.ao.mrover.common.RoverTopic;
import de.konteno.ao.mrover.common.cmd.IRoverCommand;
import de.konteno.ao.mrover.common.cmd.StopCmd;
import de.konteno.ao.mrover.common.event.IRoverEvent;
import de.konteno.ao.mrover.common.event.ShutdownEvt;
import io.reactivex.Observable;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.DeliveryContext;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.reactivex.ObservableHelper;

/**
 * common functions for all my verticles
 *
 */
public abstract class MyVerticleBase extends AbstractVerticle {
	private static final Logger baselogger = LoggerFactory.getLogger(MyVerticleBase.class);

	/**
	 * every service must (should) implement a "shutdown" method to clean up its own
	 * resources
	 */
	@Override
	public void start() throws Exception {
		busListen(ShutdownEvt.class, x -> onShutdown());
	}

	/**
	 * register a consumer with a handler that takes only the message payload
	 * (event/command POJO)
	 * 
	 * @param address
	 * @param h
	 * @param c
	 * @return
	 */
	protected <T extends IRoverEvent> MessageConsumer<T> busListen(Class<T> c, Handler<T> h) {
		RoverTopic topic = getClassTopic(c);
		MessageConsumer<T> consumer = vertx.eventBus().consumer(topic.address(), message -> {
			h.handle(message.body());
		});
		return consumer;
	}

	/**
	 * TODO how to unsubscribe the observable from event bus?
	 */
	protected <T extends IRoverEvent> Observable<T> busListenStream(Class<T> c) {
		RoverTopic topic = getClassTopic(c);
		MessageConsumer<T> consumer = vertx.eventBus().consumer(topic.address());
		
		Observable<T> obs = ObservableHelper.toObservable(consumer.bodyStream());
		return obs;
	}

	/**
	 * "consume" with a handler that also gets the received message
	 * 
	 * @param address
	 * @param h
	 * @param c
	 */
	protected <T extends IRoverCommand> MessageConsumer<T> busConsume(Class<T> c, RoverHandler<T> rh) {
		RoverTopic topic = getClassTopic(c);
		MessageConsumer<T> consumer = vertx.eventBus().consumer(topic.address(), message -> {

			Future<Void> fut = rh.process(message.body());

			fut.onComplete(ar -> {
				if (ar.succeeded()) {
					message.reply(null);
				} else {
					message.fail(-1, ar.cause().getMessage()); // TODO error code?
				}
			});
		});
		return consumer;
	}

//	/**
//	 * "listen" only to commands Useful if you want to know that a command is sent,
//	 * but you are not the one who's processing it. A "sent" command will still be
//	 * delivered to its real consumer.
//	 * 
//	 * @param c
//	 *            the command class
//	 * @param h
//	 *            the listener
//	 * @return the interceptor (can be removed when no longer needed)
//	 */
//	@SuppressWarnings("unchecked")
//	protected <T extends IRoverCommand> Handler<SendContext> busCmdListen(Class<T> c, Handler<T> h) {
//		RoverTopic topic = getClassTopic(c);
//		Handler<SendContext> interceptor = ctx -> {
//			if (topic.address().equals(ctx.message().address())) {
//				T body = (T) ctx.message().body();
//				h.handle(body);
//			}
//			ctx.next(); // continue processing the message
//		};
//		vertx.eventBus().addInterceptor(interceptor);
//		return interceptor;
//	}
	/**
	 * "listen" only to commands. Useful if you want to know that a command is sent,
	 * but you are not the one who's processing it. A "sent" command will still be
	 * delivered to its real consumer.
	 * 
	 * @param c the command class
	 * @param h the listener
	 * @return the interceptor (can be removed when no longer needed)
	 */
	@SuppressWarnings("unchecked")
	protected <T extends IRoverCommand> Handler<DeliveryContext<T>> busCmdListen(Class<T> c, Handler<T> h) {
		RoverTopic topic = getClassTopic(c);
		Handler<DeliveryContext<T>> interceptor = ctx -> {
			if (topic.address().equals(ctx.message().address())) {
				T body = ctx.message().body();
				h.handle(body);
			}
			ctx.next(); // continue processing the message
		};
		vertx.eventBus().addOutboundInterceptor(interceptor);
		return interceptor;
	}

	/**
	 * get the bus topic for any command/event class from the class' annotation
	 * 
	 * @param c
	 * @return
	 */
	private <T extends IRoverMessage> RoverTopic getClassTopic(Class<T> c) {
		RoverTopic ann = c.getAnnotation(RoverTopic.class);
		if (ann == null || ann.address() == null || ann.address().isEmpty()) {
			throw new IllegalArgumentException("class " + c.getName() + " needs topic annotation");
		}
		return ann;
	}

	/**
	 * publish an event/command POJO to the bus
	 * 
	 * @param ev
	 */
	public <T extends IRoverEvent> void busPublish(T ev) {
		RoverTopic topic = getClassTopic(ev.getClass());
		vertx.eventBus().publish(topic.address(), ev);
	}

	/**
	 * send a command message
	 * 
	 * @return a future that completes when we receive the reply to the message
	 */
	public <T extends IRoverCommand> Future<Void> busSend(T cmd) {
		return busSendP(cmd).future();
	}
	
	public <T extends IRoverCommand> Promise<Void> busSendP(T cmd) {
		Promise<Void> p = Promise.promise();
		RoverTopic topic = getClassTopic(cmd.getClass());
		DeliveryOptions dop = new DeliveryOptions();
		if (topic.timeout() != 0) {
			dop.setSendTimeout(topic.timeout());
		}
		vertx.eventBus().request(topic.address(), cmd, dop, ar -> {
			// may be already completed by something else (probably a STOPPED event handler)
			if (ar.failed()) {
				p.tryFail(ar.cause());
			} else {
				p.tryComplete();
			}
		});
		return p;
	}

	// /**
	// * send without reply handler
	// */
	// public <T extends IRoverCommand> void busSend(T cmd) {
	// RoverTopic topic = getClassTopic(cmd.getClass());
	// vertx.eventBus().send(topic.address(), cmd);
	//// busSend(cmd, null);
	// }

	/**
	 * shutdown handler, every service must implement this (stop devices, set
	 * hardware into sleep mode, ...)
	 */
	public abstract void onShutdown();

	/**
	 * fail-safe "STOP" Send a stop command with timeout. If there is no reply
	 * before the timeout, shut down the rover.
	 * 
	 * @param cause
	 *            why do we stop?
	 * @return a future that completes when the STOP is processed, or fails on
	 *         timeout
	 */
	public Future<Void> sendStop(String cause) {
		Promise<Void> p = Promise.promise();
		Future<Void> stop = busSend(new StopCmd(cause));
		
		stop.onSuccess((x) -> p.complete());
		stop.onFailure(t -> {
			baselogger.error("STOP command timed out - check your DriveService", t);
			p.fail(t);
			busPublish(new ShutdownEvt());
		});
		return p.future();
	}
	
	
}
