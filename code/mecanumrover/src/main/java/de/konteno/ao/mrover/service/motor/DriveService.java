package de.konteno.ao.mrover.service.motor;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CFactory;

import de.konteno.ao.mrover.common.Const;
import de.konteno.ao.mrover.common.Rover;
import de.konteno.ao.mrover.common.cmd.ConfigPowerDistributionCmd;
import de.konteno.ao.mrover.common.cmd.DriveCmd;
import de.konteno.ao.mrover.common.cmd.RotateCmd;
import de.konteno.ao.mrover.common.cmd.StopCmd;
import de.konteno.ao.mrover.common.cmd.TimerCmdBase;
import de.konteno.ao.mrover.common.event.StoppedEvt;
import de.konteno.ao.mrover.driver.motor.AdafruitMotorControl;
import de.konteno.ao.mrover.service.MyVerticleBase;
import io.vertx.core.Future;
import io.vertx.core.Promise;

/**
 * Handle "DRIVE" (speed, course), "ROTATE" (speed, steering) and "STOP" commands.
 * These commands may contain a "timer"; so the success message (future) is delayed
 * until the timer has elapsed. The rover does not stop when a timer has elapsed!
 *
 */
public class DriveService extends MyVerticleBase {
	
	private static final Logger logger = LoggerFactory.getLogger( DriveService.class );

	private BasicMotion control;

	private Promise<Void> currentMotion; // assigned in "buildFuture" for timed cmds only

	
	public DriveService() {
	}

	@Override
	public void start() throws Exception {		
		super.start();
		
		I2CBus bus = I2CFactory.getInstance(Const.I2C_BUS_NUMBER);

		AdafruitMotorControl motcontrol = new AdafruitMotorControl(bus.getDevice(Const.MOTCONTROL_ADR), Const.PWM_FREQ);
		control = new BasicMotionController(motcontrol);

		// process commands
		busConsume(DriveCmd.class, this::drive);
		busConsume(RotateCmd.class, this::rotate);
		busConsume(StopCmd.class, this::stopMotors);
		busConsume(ConfigPowerDistributionCmd.class, this::distributePower);
	}	

	private Future<Void> distributePower(ConfigPowerDistributionCmd cmd) {
		control.distributePower(cmd.lr, cmd.fb);
		return Future.succeededFuture();
	}
	
	/**
	 * Rotate (radially or tangentially) in the given direction. The rotation radius is
	 * determined by the "steering" parameter (0 = no rotation, +/-100 = max steering)
	 */
	private Future<Void> rotate(RotateCmd cmd) {
		abortPreviousCommand("new ROTATE command");
		try {
			Rover.currentSpeed =  Math.abs(cmd.getSpeed());
			Rover.pose.relativeCourse = cmd.relativeCourse();
			if (cmd.getSpeed() == 0) {
				control.release();
				return Future.succeededFuture();
			} 
			
			if (cmd.isRadial()) {
				control.rotateRadial(cmd.getSpeed(), cmd.getSteering());
			}
			else {
				control.rotateTangential(cmd.getSpeed(), cmd.getSteering());
			}
			
			return buildFuture(cmd);
			
		} catch (Exception e) {
			logger.error(cmd.toString() + " failed", e);
			return Future.failedFuture(e);
		}
	}


	/**
	 * Drive (translate) in the given direction.
	 * 
	 * @param cmd
	 * @return
	 */
	private Future<Void> drive(DriveCmd cmd) {
		logger.debug(cmd.toString());
		abortPreviousCommand("new DRIVE command");
		try {
			Rover.currentSpeed = cmd.getSpeed();
			Rover.pose.relativeCourse = cmd.relativeCourse();
			if (cmd.getSpeed()== 0) {
				control.release();
			} else {
				control.translate(cmd.getCourse(), cmd.getSpeed());
			}
			return buildFuture(cmd);
		} catch (Exception e) {
			logger.error("DRIVE failed", e);
			return Future.failedFuture(e);
		}
	}
	
	/**
	 * stop all motors and send a reply to the command
	 * @param cmd
	 * @param msg
	 */
	public Future<Void> stopMotors(StopCmd cmd) {
		String cause = (cmd != null) ? cmd.getCause() : null;
		logger.debug("STOP ({})", cause);
		try {
			control.release();
			Rover.currentSpeed = 0;
			
			abortPreviousCommand(cause);
			
			busPublish(new StoppedEvt(cause));
			
			return Future.succeededFuture();
		} catch (Exception e) {
			logger.error("STOP failed", e);
			return Future.failedFuture(e);
		}
	}
	
	/**
	 * this is only useful/necessary for "timed" commands that get interrupted by a STOP
	 * (manual command, collision, ...) before the timer has expired
	 * @param cause
	 */
	private void abortPreviousCommand(String cause) {
		// fail the current command
		if (currentMotion != null) {
			currentMotion.tryFail(cause);
			currentMotion = null;
		}		
	}

	/**
	 * delayed result
	 * @param cmd a command that has a built-in "timer"
	 * @return a future that succeeds immediately (timer=0) or after the timer has elapsed
	 */
	protected Future<Void> buildFuture(TimerCmdBase cmd) {
		if (cmd.getTimer() == 0) {
			return Future.succeededFuture();
		} else {
			Promise<Void> p = Promise.promise();
			vertx.setTimer(cmd.getTimer(), id -> {
				p.tryComplete();
				currentMotion = null;
			});
			currentMotion = p;
			return p.future();
		}
	}


	@Override
	public void onShutdown() {
		stopMotors(null);
	}

}
