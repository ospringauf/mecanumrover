/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.driver.motor;

import java.io.IOException;

import com.pi4j.io.i2c.I2CDevice;

/**
 * Adafruit PCA9685 16-Channel PWM Servo Driver
 * (16-channel, 12-bit PWM Fm+ I2C-bus LED controller).
 * based on Adafruit example code
 * 
 * The PWM controller is driving 2 TB6612 motor controllers (2 channels each).
 * 
 * based on sample python code
 * 
 * datasheet
 * https://cdn-shop.adafruit.com/datasheets/PCA9685.pdf
 *
 */
public class Pca9685Pwm {

	public static final int REG_MODE1 = 0x00;
	public static final int REG_MODE2 = 0x01;
	public static final int REG_SUBADR1 = 0x02;
	public static final int REG_SUBADR2 = 0x03;
	public static final int REG_SUBADR3 = 0x04;
	public static final int REG_PRESCALE = 0xFE;
	
	public static final int REG_LED0_ON_L = 0x06;
	public static final int REG_LED0_ON_H = 0x07;
	public static final int REG_LED0_OFF_L = 0x08;
	public static final int REG_LED0_OFF_H = 0x09;
	
	public static final int REG_ALL_LED_ON_L = 0xFA;
	public static final int REG_ALL_LED_ON_H = 0xFB;
	public static final int REG_ALL_LED_OFF_L = 0xFC;
	public static final int REG_ALL_LED_OFF_H = 0xFD;

	// Bits
	public static final int __RESTART = 0x80;
	public static final int __SLEEP = 0x10;
	public static final int __ALLCALL = 0x01;
	public static final int __INVRT = 0x10;
	public static final int __OUTDRV = 0x04;
	
	private I2CDevice device;
	private int frequency;

	public Pca9685Pwm(I2CDevice dev) throws MotorControlException  {
		device = dev;

		try {
	//		System.out.println("Reseting PCA9685 MODE1 (without SLEEP) and MODE2");
	
			setAllPWM(0, 0);
			device.write(REG_MODE2, (byte) __OUTDRV);
			device.write(REG_MODE1, (byte) __ALLCALL);
			Thread.sleep(5); // wait for oscillator
	
			int mode1 = device.read(REG_MODE1);
			mode1 = mode1 & ~__SLEEP; // wake up (reset sleep)
			device.write(REG_MODE1, (byte) mode1);
			Thread.sleep(5); // wait for oscillator
			
		} catch (IOException | InterruptedException e) {
			throw new MotorControlException(e);
		}
	}

	/**
	 * ON/OFF logic mapped to PWM values for on/off registers
	 * @param channel PWM channel number
	 * @param value high/low value 
	 * @throws MotorControlException
	 */
	public void setPin(int channel, boolean value) throws MotorControlException {
		if (value) {
			setPWM(channel, 4096, 0);
		}
		else {
			setPWM(channel, 0, 4096);
		}
	}

	/**
	 * Sets a all PWM channels
	 * @param on
	 * @param off
	 * @throws IOException
	 */
	public void setAllPWM(int on, int off) throws MotorControlException {
		try {
			device.write(REG_ALL_LED_ON_L,  (byte) (on & 0xFF));
			device.write(REG_ALL_LED_ON_H,  (byte) (on >> 8));
			device.write(REG_ALL_LED_OFF_L, (byte) (off & 0xFF));
			device.write(REG_ALL_LED_OFF_H, (byte) (off >> 8));
		} catch (IOException e) {
			throw new MotorControlException(e);
		}
	}
  
	/**
	 * Sets a single PWM channel
	 * @throws IOException 
	 * @throws MotorControlException 
	 */
	public void setPWM(int channel, int on, int off) throws MotorControlException {
		if ((channel < 0) || (channel > 15)) {
			throw new MotorControlException("PWM channel must be between 0 and 15 inclusive");
		}

		// based on LED0 register address 
		try {
			device.write(REG_LED0_ON_L  + 4*channel, (byte) (on & 0xFF));
			device.write(REG_LED0_ON_H  + 4*channel, (byte) (on >> 8));
			device.write(REG_LED0_OFF_L + 4*channel, (byte) (off & 0xFF));
			device.write(REG_LED0_OFF_H + 4*channel, (byte) (off >> 8));		
		} catch (IOException e) {
			throw new MotorControlException(e);
		}
	}

	/**
	 * Sets the PWM frequency
	 * @param freq
	 * @throws MotorControlException 
	 */
	public void setPWMFreq(int freq) throws MotorControlException  {
		try {
			frequency = freq;
		    double prescaleval = 25000000.0; // 25MHz
		    prescaleval /= 4096.0;        // 12-bit
		    prescaleval /= (float)freq;
		    prescaleval -= 1.0;
		    
	//	    System.out.println(String.format("Setting PWM frequency to %d Hz", freq));
	//	    System.out.println(String.format("Estimated pre-scale: %f", prescaleval));
		    
		    double prescale = Math.floor(prescaleval + 0.5);
	
	//	    System.out.println(String.format("Final pre-scale: %f", prescale));
		
		    int oldmode = device.read(REG_MODE1);
		    int newmode = (oldmode & 0x7F) | 0x10; // sleep
		    
		    device.write(REG_MODE1, (byte) newmode); // go to sleep
		    device.write(REG_PRESCALE, (byte) Math.floor(prescale));
		    device.write(REG_MODE1, (byte) oldmode);
		    Thread.sleep(5);
		    device.write(REG_MODE1, (byte) (oldmode | 0x80));
		} catch (IOException | InterruptedException e) {
			throw new MotorControlException(e);
		}
	}

	public int getPWMFreq() {
		return frequency;
	}

	
	
//	class PWM :
//		  # Registers/etc.
//		  __MODE1              = 0x00
//		  __MODE2              = 0x01
//		  __SUBADR1            = 0x02
//		  __SUBADR2            = 0x03
//		  __SUBADR3            = 0x04
//		  __PRESCALE           = 0xFE
//		  __LED0_ON_L          = 0x06
//		  __LED0_ON_H          = 0x07
//		  __LED0_OFF_L         = 0x08
//		  __LED0_OFF_H         = 0x09
//		  __ALL_LED_ON_L       = 0xFA
//		  __ALL_LED_ON_H       = 0xFB
//		  __ALL_LED_OFF_L      = 0xFC
//		  __ALL_LED_OFF_H      = 0xFD
//
//		  # Bits
//		  __RESTART            = 0x80
//		  __SLEEP              = 0x10
//		  __ALLCALL            = 0x01
//		  __INVRT              = 0x10
//		  __OUTDRV             = 0x04
//
//		  general_call_i2c = Adafruit_I2C(0x00)
//
//		  @classmethod
//		  def softwareReset(cls):
//		    "Sends a software reset (SWRST) command to all the servo drivers on the bus"
//		    cls.general_call_i2c.writeRaw8(0x06)        # SWRST
//
//		  def __init__(self, address=0x40, debug=False):
//		    self.i2c = Adafruit_I2C(address)
//		    self.i2c.debug = debug
//		    self.address = address
//		    self.debug = debug
//		    if (self.debug):
//		      print "Reseting PCA9685 MODE1 (without SLEEP) and MODE2"
//		    self.setAllPWM(0, 0)
//		    self.i2c.write8(self.__MODE2, self.__OUTDRV)
//		    self.i2c.write8(self.__MODE1, self.__ALLCALL)
//		    time.sleep(0.005)                                       # wait for oscillator
//
//		    mode1 = self.i2c.readU8(self.__MODE1)
//		    mode1 = mode1 & ~self.__SLEEP                 # wake up (reset sleep)
//		    self.i2c.write8(self.__MODE1, mode1)
//		    time.sleep(0.005)                             # wait for oscillator
//
//		  def setPWMFreq(self, freq):
//		    "Sets the PWM frequency"
//		    prescaleval = 25000000.0    # 25MHz
//		    prescaleval /= 4096.0       # 12-bit
//		    prescaleval /= float(freq)
//		    prescaleval -= 1.0
//		    if (self.debug):
//		      print "Setting PWM frequency to %d Hz" % freq
//		      print "Estimated pre-scale: %d" % prescaleval
//		    prescale = math.floor(prescaleval + 0.5)
//		    if (self.debug):
//		      print "Final pre-scale: %d" % prescale
//
//		    oldmode = self.i2c.readU8(self.__MODE1);
//		    newmode = (oldmode & 0x7F) | 0x10             # sleep
//		    self.i2c.write8(self.__MODE1, newmode)        # go to sleep
//		    self.i2c.write8(self.__PRESCALE, int(math.floor(prescale)))
//		    self.i2c.write8(self.__MODE1, oldmode)
//		    time.sleep(0.005)
//		    self.i2c.write8(self.__MODE1, oldmode | 0x80)
//
//		  def setPWM(self, channel, on, off):
//		    "Sets a single PWM channel"
//		    self.i2c.write8(self.__LED0_ON_L+4*channel, on & 0xFF)
//		    self.i2c.write8(self.__LED0_ON_H+4*channel, on >> 8)
//		    self.i2c.write8(self.__LED0_OFF_L+4*channel, off & 0xFF)
//		    self.i2c.write8(self.__LED0_OFF_H+4*channel, off >> 8)
//
//		  def setAllPWM(self, on, off):
//		    "Sets a all PWM channels"
//		    self.i2c.write8(self.__ALL_LED_ON_L, on & 0xFF)
//		    self.i2c.write8(self.__ALL_LED_ON_H, on >> 8)
//		    self.i2c.write8(self.__ALL_LED_OFF_L, off & 0xFF)
//		    self.i2c.write8(self.__ALL_LED_OFF_H, off >> 8)

}
