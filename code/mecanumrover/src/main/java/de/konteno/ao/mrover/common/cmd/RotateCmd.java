/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.common.cmd;

import de.konteno.ao.mrover.common.RoverDirection;
import de.konteno.ao.mrover.common.RoverTopic;
import de.konteno.ao.mrover.common.unit.Angle;


@RoverTopic(address = "cmd.rotate")
public class RotateCmd extends TimerCmdBase implements IMotionCommand {

	private double speed; // TODO eigener Datentyp für Geschwindigkeit (0..1)
	private boolean radial = true; // or tangential
	private double steering = 100;


	public RotateCmd(double speed) {
		this.speed = speed;
	}

	public double getSpeed() {
		return speed;
	}

	public RotateCmd setSpeed(double speed) {
		this.speed = speed;
		return this;
	}
	
	@Override
	public String toString() {
		String shape = isRadial() ? "RADIAL" : "TANGENTAL";
		return String.format("ROTATE(%s, steer=%3.1f, v=%2.3f)!", shape, getSteering(), speed);
	}

	public double getSteering() {
		return steering;
	}

	public RotateCmd steer(double steering) {
		this.steering = steering;
		return this;
	}

	public boolean isRadial() {
		return radial;
	}

	public void setRadial(boolean r) {
		this.radial = r;
	}
	
	public RotateCmd radial() {
		this.radial = true;
		return this;
	}

	public RotateCmd tangential() {
		this.radial = false;
		return this;
	}

	/**
	 * Adjust rotation direction (positive/negative speed) in relation to the target angle.
	 * This method is called by TurnService and does not change the path defined by "steering"
	 * @param dir clockwise or counter-clockwise rotation
	 * @param absSpeed absolute rotation speed
	 * @return
	 */
	public RotateCmd adjust(RoverDirection dir, double absSpeed) {
		if (steering > 0 && dir == RoverDirection.CCW) {
			setSpeed(-1 * absSpeed);
		} 
		else if (steering < 0 && dir == RoverDirection.CW) {
			setSpeed(-1 * absSpeed);
		}
		else {
			setSpeed(absSpeed);
		}
		return this;
	}
	
	
	/**
	 * calculate effective relative course for rotation (approx.)
	 * so that the correspondig distance sensors are used for collision detection
	 * @return direction of travel in the rover coordinate system
	 */
	public Angle relativeCourse() {
		if (isRadial()) {
			// RADIAL
			if (speed >= 0) {
				// forward (v>=0): course is 90 +/- 45deg, depending on steering and speed
				//return Angle.fromDegree(90 + (steering/100.0) * 45.0);
				return Angle.fromDegree(90.0 + (steering/(speed*100.0)) * 45.0);
			} else {
				// back (v<0): course is 270 -/+ 45deg
				//return Angle.fromDegree(280.0 - (steering/100.0) * 45.0);
				return Angle.fromDegree(270.0 + (steering/(speed*100.0)) * 45.0);
			}			
			
		} else {
			// TANGENTIAL
			if (speed >= 0) {
				// forward (v>=0): course is 0 +/- 45deg, depending on steering and speed
				//return Angle.fromDegree(0 + (steering/100.0) * 45.0);
				return Angle.fromDegree(0 + (steering/(speed*100.0)) * 45.0);
			} else {
				// back (v<0): course is 180 -/+ 45deg
//				return Angle.fromDegree(180.0 - (steering/100.0) * 45.0);
				return Angle.fromDegree(180.0 + (steering/(speed*100.0)) * 45.0);
			}			
		}
	}

	/**
	 * The following motion equations have been interpolated from measurements
	 * (test drives). A multi-linear regression yielded worse results for low speeds,
	 * so we stick with the simple interpolation. 
	 */
	
	@Override
	public double estimateDx(long t) {
		// only radial; tangential has no x-speed
//		return isRadial() ? (speed * (70 - (0.7 * steering)) * t) / 1000 : 0;
		return isRadial() ? Math.signum(steering)*(speed * (70 - (0.7 * Math.abs(steering))) * t) / 1000 : 0;
//		return isRadial() ? ((25.5*speed - 0.54*steering + 34.5) * t) / 1000 : 0;
	}

	@Override
	public double estimateDy(long t) {
		// only tangential; radial has no y-speed
		//return isRadial() ? 0 : (speed * (80 - (0.8 * steering)) * t) / 1000;
		return isRadial() ? 0 : Math.signum(steering)*(speed * (80 - (0.8 * Math.abs(steering))) * t) / 1000;
//		return isRadial() ? 0 : ((51*speed - 0.53*steering + 20.8) * t) / 1000;
	}

	@Override
	public double estimateDalpha(long t) {
		double da = isRadial() ? 
				(speed * ((1.2 * steering) /*+ 8.2*/) * t) / 1000 : 
				(speed * ((1.1 * steering) /*+ 9.4*/) * t) / 1000;
//		double da = isRadial() ? 
//				((121.3*speed + 0.98*steering - 80) * t) / 1000.0 : 
//				((82.7*speed + 0.77*steering - 45) * t) / 1000.0;
		
		return da;	
		}


}
