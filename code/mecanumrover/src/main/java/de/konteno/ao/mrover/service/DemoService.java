package de.konteno.ao.mrover.service;

import java.util.ArrayList;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.konteno.ao.mrover.common.Rover;
import de.konteno.ao.mrover.common.cmd.DemoCmd;
import de.konteno.ao.mrover.common.cmd.DriveCmd;
import de.konteno.ao.mrover.common.cmd.FollowCmd;
import de.konteno.ao.mrover.common.cmd.IRoverCommand;
import de.konteno.ao.mrover.common.cmd.RotateCmd;
import de.konteno.ao.mrover.common.cmd.TurnCmd;
import de.konteno.ao.mrover.common.event.StoppedEvt;
import de.konteno.ao.mrover.common.unit.Angle;
import io.vertx.core.Future;
import io.vertx.core.Promise;

/**
 * TODO have a look at http://vertx.io/docs/vertx-sync/java/
 */
public class DemoService extends MyVerticleBase {

	private static final Logger logger = LoggerFactory.getLogger(DemoService.class );
	
//	private Subscription subscription;
//	private TimeoutStream periodicStream;
	Promise<Void> currentCmd;

	@Override
	public void start() throws Exception {
		super.start();

		busConsume(DemoCmd.class, this::demo);
		busListen(StoppedEvt.class, this::onStop);
	}

	private Future<Void> demo(DemoCmd cmd) {
		logger.info("got command: " + cmd.getDemoName());
		switch (cmd.getDemoName())
		{
			case DemoCmd.square:	
				return square_2c(cmd);
				
			case DemoCmd.squareTurn:	
				return squareTurn_1(cmd);
				
			case DemoCmd.wiggle:
				return wiggle(cmd);

			case DemoCmd.dance:
				return dance(cmd);

			case DemoCmd.follow:
				return busSend(new FollowCmd()); // delegate to follow service

			default:
				logger.warn( "no such demo: {}", cmd.getDemoName());
				return Future.failedFuture("unknown demo");
				
		}
	}

	private void onStop(StoppedEvt evt) {
		// fail the current command
		if (currentCmd != null) {
			currentCmd.tryFail(evt != null ? evt.getCause() : null);
		}

//		if (periodicStream != null) {
//			periodicStream.cancel();
//		}

//		if (subscription != null) {
//			subscription.unsubscribe();
//		}
	}
	
	// ------------------------------------------------------------------------

	/**
	 * "classic" style
	 * 
	 * @param cmd
	 */
	private void square_0(DemoCmd cmd) {

		busSend(new DriveCmd(Angle.fromDegree(0)));
		vertx.setTimer(2000, ar1 -> {
			busSend(new DriveCmd(Angle.fromDegree(90)));
			vertx.setTimer(2000, ar2 -> {
				busSend(new DriveCmd(Angle.fromDegree(180)));
				vertx.setTimer(2000, ar3 -> {
					busSend(new DriveCmd(Angle.fromDegree(270)));
					vertx.setTimer(2000, ar4 -> {
						sendStop("demo1 complete");
					});

				});

			});
		});
	}

//	/**
//	 * using RX timer stream
//	 * 
//	 * @param cmd
//	 */
//	private void square_1(DemoCmd cmd) {
//		periodicStream = vertx.periodicStream(2000);
//		Observable<Long> rxps = RxHelper.toObservable(periodicStream);
//		subscription = rxps.subscribe(new Subscriber<Long>() {
//			int a = 0;
//
//			@Override
//			public void onCompleted() {
//			}
//
//			@Override
//			public void onError(Throwable arg0) {
//			}
//
//			@Override
//			public void onNext(Long arg0) {
//				if (a == 360) {
//					sendStop("demo1 complete");
//				} else {
//					busSend(new DriveCmd(Angle.fromDegree(a)));
//					a += 90;
//				}
//			}
//		});
//	}

	// -- with future composition ---------------------------------------------

	/**
	 * send a (drive) command
	 * 
	 * @returns a future that succeeds after a given time or fails if the rover has
	 *          been stopped before the time has elapsed (preventing following
	 *          commands from being executed)
	 */
	private <T extends IRoverCommand> Future<Void> sendAndWait(final T cmd, final long time) {
		logger.debug("sendAndWait " + cmd);
		currentCmd = Promise.promise();
		if (cmd != null) {
			busSend(cmd);
		}
		vertx.setTimer(time, elapsed -> {
			// STOP might already have been processed
			if (!currentCmd.future().isComplete()) {
				if (Rover.isStopped()) {
					logger.debug("STOPPED before end of command (sequence)");
					currentCmd.fail("stopped");
				} else {
					currentCmd.complete();
				}
			}
		});
		return currentCmd.future();
	}

	/**
	 * using composed futures
	 * 
	 * @param cmd
	 */
	private void square_2(DemoCmd cmd) {
		Future<Void> f1 = sendAndWait(new DriveCmd(Angle.fromDegree(0)), 1000);
		f1
		.compose(h -> sendAndWait(new DriveCmd(Angle.fromDegree(90)), 2000))
		.compose(h -> sendAndWait(new DriveCmd(Angle.fromDegree(180)), 1001))
		.compose(h -> sendAndWait(new DriveCmd(Angle.fromDegree(270)), 2000))
		.compose(h -> {
			sendStop("demo1 complete");
			return Future.succeededFuture();
		});
	}

	/**
	 * build command sequence using composed futures, then trigger the sequence
	 * 
	 * @param cmd
	 */
	private void square_2a(DemoCmd cmd) {
		logger.debug("square_2a");

		IRoverCommand[] commands = new IRoverCommand[] { 
				new DriveCmd(Angle.fromDegree(0)),
				new DriveCmd(Angle.fromDegree(90)), 
				new DriveCmd(Angle.fromDegree(180)),
				new DriveCmd(Angle.fromDegree(270)), };

		Promise<Void> trigger = Promise.promise();
		Promise<Void> success = Promise.promise();

		// build a composed future

		// // stream syntax
		// Future<Void> x = Stream.of(commands)
		// .reduce(
		// trigger,
		// (f, c) -> f.compose(h -> timedCommand(c, 1000)),
		// (f1, f2) -> null /* no combiner !? */);

		// classic syntax
		Future<Void> x = trigger.future();
		for (IRoverCommand c : commands) {
			x = x.compose(h -> sendAndWait(c, 300));
		}
		// final step: stop and propagate the result to "success"
		x = x.compose(h -> {
			sendStop("demo1 complete");
			logger.info("----------");
			success.complete();
			return success.future();
		});

		success.future().map(v -> {
			logger.info("demo command sequence COMPLETE ");
			return null;
		})
			.otherwise(err -> {
				logger.warn("demo command sequence FAILED: " + err.getMessage());
				return null;
			});

		// start
		trigger.complete();

	}

	/**
	 * send a (drive) command
	 * 
	 * @returns a future that succeeds after receiving a reply or fails if the rover has
	 *          been stopped in the meantime (preventing following
	 *          commands from being executed)
	 */
	private <T extends IRoverCommand> Future<Void> sendAndWait2(final T cmd) {
		logger.debug("sendAndWait2 " + cmd);
		if (cmd == null) {
			return Future.succeededFuture();
		}
		currentCmd = busSendP(cmd);
//		currentCmd = Future.future();
//		busSend(cmd, ar -> {			
//			// STOP might already have been processed
//			if (!currentCmd.isComplete()) {
//				// we're done
//				currentCmd.complete();
//			}
//		});
		return currentCmd.future();
	}

	/**
	 * build command sequence using composed futures, then trigger the sequence 
	 * - more readable version
	 * 
	 * @param demoCmd
	 */
	private void square_2b(DemoCmd demoCmd) {
		logger.debug("square_2b");

		IRoverCommand[] plan = new IRoverCommand[] { 
				new DriveCmd(Angle.fromDegree(0)),
				new DriveCmd(Angle.fromDegree(90)), 
				new DriveCmd(Angle.fromDegree(180)),
				new DriveCmd(Angle.fromDegree(270)), 
				};

		// build a composed future, chaining the commands (classic syntax)
		Promise<Void> trigger = Promise.promise();
		Future<Void> commands = trigger.future();

		for (IRoverCommand cmd : plan) {
			commands = commands.compose(h -> sendAndWait(cmd, 500));
		}
		// final step: stop
		commands = commands.compose(h -> sendStop("demo1 complete"));
		
		commands.onComplete(ar -> {
			if (ar.succeeded()) {
				logger.info("demo command sequence SUCCEEDED ");
			} else {
				logger.warn("demo command sequence CANCELLED: " + ar.cause().getMessage());
			}
		});

		// start the command sequence by firing (completing) the trigger
		trigger.complete();
	}

	/**
	 * build command sequence using composed futures, then trigger the sequence -
	 * more readable version
	 * 
	 * @param demoCmd
	 */
	private Future<Void> square_2c(DemoCmd demoCmd) {
		logger.debug("square_2c - " + demoCmd.getDemoName());
		Promise<Void> ret = Promise.promise();

		IRoverCommand[] plan = new IRoverCommand[] { 
				new DriveCmd(Angle.fromDegree(0)).time(1500),
				new DriveCmd(Angle.fromDegree(90)).time(2000), 
				new DriveCmd(Angle.fromDegree(180)).time(1500),
				new DriveCmd(Angle.fromDegree(270)).time(2000), 
				};

		// build a composed future, chaining the commands (classic syntax)
		Promise<Void> trigger = Promise.promise();
		Future<Void> commands = trigger.future();

		for (IRoverCommand cmd : plan) {
			commands = commands.compose(h -> (currentCmd = busSendP(cmd)).future());
		}
		// final step: stop
		commands = commands.compose(h -> sendStop("demo " + demoCmd.getDemoName() + " complete"));

		commands
		.onSuccess(h -> {
			logger.info("demo command sequence SUCCEEDED ");
			ret.tryComplete();
		})
		.onFailure(t -> {
			logger.warn("demo command sequence CANCELLED: " + t + " / " + t.getCause());
			ret.tryFail(t.getCause());
		});

		// start the command sequence by firing (completing) the trigger
		trigger.complete();
		return ret.future();
	}

	/**
	 * TODO use rxSend/Stream to issue command
	 * 
	 * @param cmd
	 */
	private void square_3(DemoCmd cmd) {
		logger.debug("square_3");

		// IRoverCommand[] commands = new IRoverCommand[] {
		// new DriveCmd(Angle.fromDegree(0)),
		// new DriveCmd(Angle.fromDegree(90)),
		// new DriveCmd(Angle.fromDegree(180)),
		// new DriveCmd(Angle.fromDegree(270)),
		// };
		//
		// Stream<IRoverCommand> s1 = Stream.of(commands);
		// Observable<IRoverCommand> s2 = Observable.from(commands);
		//
		// Vertx rvx = io.vertx.rxjava.core.Vertx.vertx();
		//
		// s2.flatMap(c -> rvx.eventBus().rxSend("a", c));
	}

	// ------------------------------------------------------------------------

	private Future<Void> squareTurn_1(DemoCmd demoCmd) {
		logger.debug("demo2_1 - " + demoCmd.getDemoName());

		IRoverCommand[] plan = new IRoverCommand[] { 
				new DriveCmd(Angle.fromDegree(0)).time(1500),
				new TurnCmd(Angle.fromDegree(90), true), 
				new DriveCmd(Angle.fromDegree(0)).time(1500),
				new TurnCmd(Angle.fromDegree(90), true), 
				new DriveCmd(Angle.fromDegree(0)).time(1500),
				new TurnCmd(Angle.fromDegree(90), true), 
				new DriveCmd(Angle.fromDegree(0)).time(1500),
				new TurnCmd(Angle.fromDegree(90), true), 
				};

		return executePlan(Arrays.asList(plan));
		
//		// build a composed future, chaining the commands (classic syntax)
//		Future<Void> trigger = Future.future();
//		Future<Void> commands = trigger;
//		Future<Void> ret = Future.future();
//
//		for (IRoverCommand cmd : plan) {
//			commands = commands.compose(h -> currentCmd = busSend(cmd));
//		}
//		// final step: stop
//		commands = commands.compose(h -> sendStop("demo " + demoCmd.getDemoName() + " complete"));
//
//		commands.setHandler(ar -> {
//			if (ar.succeeded()) {
//				LOGGER.info("demo command sequence SUCCEEDED ");
//				ret.tryComplete();
//			} else {
//				LOGGER.warning("demo command sequence CANCELLED: " + ar.cause().getMessage());
//				ret.tryFail(ar.cause());
//			}
//		});
//
//		// start the command sequence by firing (completing) the trigger
//		trigger.complete();		
//		
//		return ret;
	}


	// ------------------------------------------------------------------------
	
	private Future<Void> wiggle(DemoCmd cmd) {
		ArrayList<IRoverCommand> plan = new ArrayList<IRoverCommand>();

		double vmax = Rover.speedLimit;
		// use high tolerance to avoid ugly counter-steering
		plan.add(TurnCmd.relative(Angle.fromDegree(45)).tangential(40).setTolerance(3));

		plan.add(TurnCmd.relative(Angle.fromDegree(-90)).tangential(-40).setTolerance(3));
		plan.add(TurnCmd.relative(Angle.fromDegree( 90)).tangential( 40).setTolerance(3));

		plan.add(TurnCmd.relative(Angle.fromDegree(-90)).tangential(-40).setTolerance(3));
		plan.add(TurnCmd.relative(Angle.fromDegree( 90)).tangential( 40).setTolerance(3));

		plan.add(TurnCmd.relative(Angle.fromDegree(-45)).tangential(-40).setTolerance(3));

		return executePlan(plan);
	}

	// ------------------------------------------------------------------------

	private Future<Void> dance(DemoCmd cmd) {
		int delay = 1600;
		double vmax = Rover.speedLimit;
		Angle startAngle = Rover.pose.heading;
		
		//north();
		
		IRoverCommand[] plan = new IRoverCommand[] { 
				
			// rectangle forward - right - back - left
			DriveCmd.forward(vmax).time(delay),
			DriveCmd.right(vmax).time(delay),
			DriveCmd.back(vmax).time(delay),
			DriveCmd.left(vmax).time(delay),
			
			//north
			TurnCmd.absolute(startAngle),
			
			// -------------------------------------------
			// diagonal triangle  45 - left - 135 
			new DriveCmd(Angle.fromDegree(45), vmax).time(2*delay),
			TurnCmd.absolute(startAngle),

			DriveCmd.left(vmax).time((long) (1.5*delay)),

			new DriveCmd(Angle.fromDegree(135), vmax).time(2*delay),
			
			//north
			TurnCmd.absolute(startAngle),
			
			// back to start
			TurnCmd.relative(Angle.fromDegree(-90)),
			DriveCmd.forward(vmax).time(delay),
			TurnCmd.absolute(startAngle),
			
			// -------------------------------------------
			// rounded rect fwd - right - fwd - right - fwd
			// back to starting point
			DriveCmd.forward(vmax).time(delay/2),
			
			TurnCmd.relative(Angle.fromDegree(90)).tangential(65).setTolerance(2),
//				turn( 90, (double v, int d) -> control.rotateTangential(Rover.speedLimit, 65), 2,

			DriveCmd.forward(vmax).time(delay/2),

			TurnCmd.relative(Angle.fromDegree(90)).tangential(65).setTolerance(2),

			DriveCmd.forward(vmax).time(delay/2),

			// -------------------------------------------
			// a 180deg sweep right 
			
			TurnCmd.relative(Angle.fromDegree(180)).setTolerance(2).setRotation(new RotateCmd(vmax).radial().steer(30)),
			//north
			TurnCmd.absolute(startAngle),
			
			// and back home
			DriveCmd.back(vmax).time(delay),
			//north
			TurnCmd.absolute(startAngle),
			DriveCmd.left(vmax).time(delay),
			
			TurnCmd.relative(Angle.fromDegree(270)),
			TurnCmd.absolute(startAngle),
		};

		//plan.add(new StopCmd());
		return executePlan(Arrays.asList(plan));
	}

	// ------------------------------------------------------------------------


	/**
	 * build a composed future, chaining the commands (classic syntax)
	 * @param plan command sequence
	 * @return a future that succeeds after the last command (stop) is complete
	 */
	private Future<Void> executePlan(Iterable<IRoverCommand> plan) {
		
		Promise<Void> trigger = Promise.promise();
		Future<Void> commands = trigger.future();
		Promise<Void> ret = Promise.promise();

		for (IRoverCommand cmd : plan) {
			commands = commands.compose(h -> (currentCmd = busSendP(cmd)).future());
		}
		// final step: stop
		commands = commands.compose(h -> sendStop("command sequence complete"));

		commands
		.onSuccess(h -> {
			logger.info("command sequence SUCCEEDED ");
			ret.tryComplete();
		})
		.onFailure(t -> {
			logger.warn("command sequence CANCELLED: " + t + " / " + t.getCause());
			ret.tryFail(t.getCause());
		});
		
		// start the command sequence by firing (completing) the trigger
		trigger.complete();		
		
		return ret.future();	
	}

	// ------------------------------------------------------------------------

	@Override
	public void onShutdown() {
		onStop(null);
	}

}
