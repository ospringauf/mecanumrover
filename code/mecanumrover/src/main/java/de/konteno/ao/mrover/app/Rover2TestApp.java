/*********************************************************************
* Copyright (c) 2019 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package de.konteno.ao.mrover.app;

import java.io.IOException;

import org.jooq.lambda.Seq;

import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CFactory;
import com.pi4j.io.i2c.I2CFactory.UnsupportedBusNumberException;
import com.pi4j.io.serial.Baud;

import de.konteno.ao.mrover.common.Const;
import de.konteno.ao.mrover.driver.motor.AdafruitMotorControl;
import de.konteno.ao.mrover.driver.motor.MotorControlException;
import de.konteno.ao.mrover.driver.serial.RoverSerialPort;

public class Rover2TestApp {

	public static void main(String[] args) throws Exception {
		// Rover 2 specific settings
//		AdafruitMotor.speedScale = 0.675; // 135rpm/200rpm
		AdafruitMotorControl.motorMapping = new int[] { 3, 2, 1, 0 };
		
		testMotors();
//		testOdoSerial();
		
	}

	private static void testOdoSerial() throws Exception {
		RoverSerialPort port = new RoverSerialPort("odo");
		port.setHandler((t,p) -> {System.out.println(t + "/" + p);});
		port.start(Rover2App.ODOMETER_PORT, Baud._38400);		

		Thread.sleep(10000);
		port.send("d"); // power down
		Thread.sleep(5000);
		port.send("u"); // power up
		Thread.sleep(10000);
		port.send("d"); // power down
		port.shutdown();
	}

	protected static void testMotors()
			throws UnsupportedBusNumberException, IOException, MotorControlException, InterruptedException {
		I2CBus bus = I2CFactory.getInstance(Const.I2C_BUS_NUMBER);

		AdafruitMotorControl motcontrol = new AdafruitMotorControl(bus.getDevice(Const.MOTCONTROL_ADR), Const.PWM_FREQ);
		
		System.out.println("test motors");
		Seq.rangeClosed(0, 3).forEach(m -> {
			try {
				System.out.println("motor: " + m);
				motcontrol.getMotor(m).go(0.85);
				Thread.sleep(10000);			
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		
		Thread.sleep(120000);

		System.out.println("release all");
		Seq.rangeClosed(0, 3).forEach(m -> {
			try {
				motcontrol.getMotor(m).go(0);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}
	
	
}
