/*********************************************************************
* Copyright (c) 2019 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.driver.serial;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pi4j.io.serial.Baud;
import com.pi4j.io.serial.DataBits;
import com.pi4j.io.serial.FlowControl;
import com.pi4j.io.serial.Parity;
import com.pi4j.io.serial.Serial;
import com.pi4j.io.serial.SerialConfig;
import com.pi4j.io.serial.SerialDataEvent;
import com.pi4j.io.serial.SerialDataEventListener;
import com.pi4j.io.serial.SerialFactory;
import com.pi4j.io.serial.StopBits;

import de.konteno.ao.mrover.common.RoverUtil;
import de.konteno.ao.mrover.common.SerialMessage;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.parsetools.RecordParser;

/**
 * Serial line communication with Arduino Nano Odometer 
 *
 */
public class RoverSerialPort {

	private static final Logger logger = LoggerFactory.getLogger(RoverSerialPort.class);

	private SerialMessageHandler handler;

	// parser is fed with incoming data blocks (buffers) from UART and emits
	// individual lines
	RecordParser recparser = RecordParser.newDelimited("\r\n", this::processIncomingMessage);

	private Serial serial;
	private String device;
	public String name;

	public RoverSerialPort(String name) {
		this.name = name;
	}
	
	/**
	 * gets called with parsed data
	 * 
	 * @param h
	 */
	public void setHandler(SerialMessageHandler h) {
		this.handler = h;
	}

	public void start(String device, Baud baud) throws Exception {
		// create an instance of the serial communications class
		serial = SerialFactory.createInstance();
		this.device = device;
		
		// create and register the serial data listener
		serial.addListener(new SerialDataEventListener() {
			@Override
			public void dataReceived(SerialDataEvent event) {

				// NOTE! - It is extremely important to read the data received from the
				// serial port. If it does not get read from the receive buffer, the
				// buffer will continue to grow and consume memory.

				try {
					// Pass data block to the record parser. This buffer may contain
					// more or less than one line, the parser will take care of line splitting!
					recparser.handle(Buffer.buffer(event.getBytes()));
					
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});

		// create serial config object
		SerialConfig config = new SerialConfig();

		// set default serial settings (device, baud rate, flow control, etc)
		//
		// by default, use the DEFAULT com port on the Raspberry Pi (exposed on GPIO header)
		// NOTE: this utility method will determine the default serial port for the
		// detected platform and board/model. For all Raspberry Pi models
		// except the 3B, it will return "/dev/ttyAMA0". For Raspberry Pi
		// model 3B may return "/dev/ttyS0" or "/dev/ttyAMA0" depending on
		// environment configuration.
		config.device(device);
		config.baud(baud);
		config.dataBits(DataBits._8);
		config.parity(Parity.NONE);
		config.stopBits(StopBits._1).flowControl(FlowControl.NONE);
		
        // open the default serial device/port with the configuration settings
		logger.info(name + " connecting to " + config.toString());
        serial.open(config);
	}
	
	public void send(SerialMessage m) {
		send(m.toLineFormat() + "\n");
	}
	
	public void send(CharSequence m) {
		try {
			serial.write(m);
		} catch (IllegalStateException | IOException e) {
			logger.error(name + " failed to send {} ({})", m, e.getMessage());
		}
	}
	
	public void flush() {
		try {
			serial.flush();
		} catch (IllegalStateException | IOException e) {
			logger.error(name + " failed to flush serial port ({})", e.getMessage());
		}
	}

	private void processIncomingMessage(Buffer b) {
		String msg = b.toString();
		logger.trace(name + " processing incoming message [{}]", msg);
		
		// try parse message
		try {
			SerialMessage m = SerialMessage.parseNmea(msg);
			if (m == null) {
				logger.warn(name + " ignored [{}]", RoverUtil.last(msg, 40));
				return;
			}
			// try validate checksum
			if (m.hasChecksum() && m.getChecksum() != m.calcChecksum()) {
				logger.error(name + " checksum mismatch for message [{}], expected {}", msg, m.calcChecksum());
				return;
			}
			// try call handler
			if (handler != null) {
				handler.handle(m.getTopic(), m.getPayload());
			}
		} catch (Exception e) {
			logger.error(name + " failed to process incoming message: " + b.toString(), e);
		}
	}

	public void shutdown() {
		if (!serial.isClosed()) {
			try {
				logger.info(name + " closing serial port " + device);
				serial.close();
			} catch (IllegalStateException | IOException e) {
				logger.error(name + " failed to close serial port ({})", e.getMessage());
			}
		}
		
	}

}
