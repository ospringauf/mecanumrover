/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.common.event;

import de.konteno.ao.mrover.common.RoverTopic;
import de.konteno.ao.mrover.common.unit.Angle;

/**
 * distance measurement from laser range finder
 * 
 * TODO vertical angle
 *
 */
@RoverTopic(address="evt.laser")
public class LaserDistanceEvt implements IRoverEvent {


	public static final int LEFT = 0;
	public static final int RIGHT = 1;
			
	
	public int distance[] = new int[2];
	public Angle angle[] = new Angle[2];
	
	private LaserDistanceEvt() {}
	
	/**
	 * payload format is "{angle} {d0} {d1}"
	 * where "angle" is the current direction of the horizontal servo in degrees (LRF direction is then +/-45°)
	 * @param payload
	 */
	public static LaserDistanceEvt parseEsp(String payload) {
		LaserDistanceEvt e = new LaserDistanceEvt();
		String[] f = payload.trim().split("\\s+");
		
		double a = Double.parseDouble(f[0]);
		
		e.distance[0] = Integer.parseInt(f[1]);
		e.angle[0] = Angle.fromDegree(a - 45);

		e.distance[1] = Integer.parseInt(f[2]);
		e.angle[1] = Angle.fromDegree(a + 45);
		return e;
	}
	
	public int getDistance(int i) {
		return distance[i];
	}

	public Angle getAngle(int i) {
		return angle[i];
	}
}
