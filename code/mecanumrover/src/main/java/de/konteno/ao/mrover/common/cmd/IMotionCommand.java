/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package de.konteno.ao.mrover.common.cmd;

import de.konteno.ao.mrover.common.unit.Angle;

public interface IMotionCommand extends IRoverCommand {

	double estimateDx(long t);
	double estimateDy(long t);
	double estimateDalpha(long t);
	public double getSpeed();
	public Angle relativeCourse();
}
