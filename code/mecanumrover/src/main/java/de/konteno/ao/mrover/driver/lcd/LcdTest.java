/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.driver.lcd;

import java.io.IOException;

import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;
import com.pi4j.io.i2c.I2CFactory.UnsupportedBusNumberException;

public class LcdTest {

	public void go() throws UnsupportedBusNumberException, IOException, InterruptedException {
		
		I2CBus iic1 = I2CFactory.getInstance(I2CBus.BUS_1);
		I2CDevice dev = iic1.getDevice(0x3F);

		Hd44780_I2c lcd = new Hd44780_I2c(dev);
		
		lcd.clear();
		Thread.sleep(1000);
		
		lcd.displayString("....zeile1", 1);
		Thread.sleep(1000);

		lcd.displayString("ZEILE 2", 2);
		Thread.sleep(1000);
		
		lcd.displayString("x", 1, 2);
		Thread.sleep(1000);

		lcd.displayString("zzz", 1, 15);
		Thread.sleep(1000);

		lcd.displayString("AAAAAAAAAAAAAAAA", 1);
		lcd.displayString("BBBBBBBBBBBBBBBB", 1);
		lcd.displayString("CCCCCCCCCCCCCCCC", 1);
		Thread.sleep(1000);
		
//		lcd.backlight(false);
	}
}
