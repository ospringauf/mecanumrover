package de.konteno.ao.mrover.service.info;

import java.awt.Font;
import java.awt.Graphics2D;
import java.io.IOException;

import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CFactory;

import de.konteno.ao.mrover.common.Const;
import de.konteno.ao.mrover.driver.oled.SH1106;
import de.konteno.ao.mrover.service.MyVerticleBase;

public class DisplayService extends MyVerticleBase {

	private SH1106 oled;

	@Override
	public void start() throws Exception {
		super.start();

		I2CBus bus = I2CFactory.getInstance(Const.I2C_BUS_NUMBER);
		oled = new SH1106(bus.getDevice(Const.OLED_ADR));

		vertx.setTimer(100, id -> {
			displayText(new String[] { "", "M E C A N U M R O V E R", "", "with vert.x" });
		});

	}

	@Override
	public void onShutdown() {
		displayText(new String[] { "goodbye", ""/* , powerm.readPower() */ });

	}

	public void off() {
		try {
			oled.displayOff();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void displayText(String[] text) {
		try {
			Graphics2D g = oled.getGraphics();
			oled.clear(false);
			g.setFont(new Font(Font.DIALOG, Font.PLAIN, 10));
			for (int i = 0; i < text.length; ++i) {
				g.drawString(text[i], 0, 10 + 10 * i);
			}
			oled.displayGraphics();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void displayText(String string) {
		displayText(new String[] { string });
	}
	
	public void testOled() {
		try {
			Graphics2D g = oled.getGraphics();

			// smile
	        g.setFont(new Font(Font.DIALOG, Font.PLAIN, 84));
			g.drawString("\u263a", 62, 75);
	        oled.displayGraphics();
			
			Thread.sleep(2000);
			oled.clear(false);
					
	        g.setFont(new Font(Font.DIALOG, Font.PLAIN, 10));
			g.drawRect(0, 0, 127, 63);
			g.drawString("Mecanumrover ***", 2, 9);
			g.drawString("Hi there!", 2, 19);
			//g.drawString((new Date()).toString(), 3, 21);			

	        oled.displayGraphics();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
	}


	//
	// public void toggle() {
	// try {
	// switch(x) {
	// case 0:
	// oled.scanDir(false);
	// oled.segmentMap(false);
	// displayText("toggle:" + x);
	// break;
	// case 1:
	// oled.scanDir(false);
	// oled.segmentMap(true);
	// displayText("toggle:" + x);
	// break;
	// case 2:
	// oled.scanDir(true);
	// oled.segmentMap(false);
	// displayText("toggle:" + x);
	// break;
	// case 3:
	// oled.scanDir(true);
	// oled.segmentMap(true);
	// displayText("toggle:" + x);
	// break;
	// }
	// x+=1;
	// x%=4;
	// } catch (IOException e1) {
	// // TODO Auto-generated catch block
	// e1.printStackTrace();
	// }
	// }
}
