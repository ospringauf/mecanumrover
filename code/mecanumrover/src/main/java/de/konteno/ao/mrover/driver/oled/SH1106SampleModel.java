/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.driver.oled;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.RasterFormatException;
import java.awt.image.SampleModel;



/***
 * This is an AWT <code>SampleModel</code> subclass
 * using a packed (> 1 pixel/byte), byte-based data buffer for a monochrome bitmap (=indexed color) image.
 *  
 * The pixel layout in the data buffer matches the screen layout of the SH1106 OLED display,
 * so that the buffer can be written directly to the display.
 * 
 *          	0	 1	 2	 3	... 	127
 *             ____________________________
 *   page0  0 | A0 | B0
 *          1 | A1 | B1
 *          2 | A2 | B2 
 *          3 | A3 | B3  ...
 *          4 | A4 | B4
 *          5 | A5 | B5
 *          6 | A6 | B6
 *          7 | A7 | B7
 *   page1  8 | 
 *   		9 |
 *   	   10 |
 *   	   11 |
 *    	  ...
 *    	   63 |
 *   
 * [A7 .. A0] is the first byte of the data buffer and the first byte of 
 * page0 on the display. It contains the first 8-pixel column.
 * 
 * A pixel (x,y) is located at byte offset (x + 128 * [y/8]), bit (x%8) in the buffer.
 * 
 * Ths approach is similar to AWT's <code>MultiPixelPackedSampleModel</code>, except that the buffer's
 * bytes contain pixel columns, not pixel rows (scanlines). The code is based on <code>MultiPixelPackedSampleModel</code>.
 * 
 * @author oliver
 *
 */
public class SH1106SampleModel extends SampleModel
{
    /** The number of bits from one pixel to the next. */
    int pixelBitStride;

    /** Bitmask that extracts the rightmost pixel of a data element. */
    int bitMask;

    /**
      * The number of pixels that fit in a data element.  Also used
      * as the number of bits per pixel.
      */
    int pixelsPerDataElement;

    /** The size of a data element in bits. */
    int dataElementSize;
  
    int pageHeight = 8;

    public SH1106SampleModel(int w, int h) {
    	super(DataBuffer.TYPE_BYTE, w, h, 1);
    	
    	int bitsPerPixel = 1;
    	this.dataType = DataBuffer.TYPE_BYTE;
        this.pixelBitStride = 1;
        
        this.dataElementSize = DataBuffer.getDataTypeSize(dataType);
        this.pixelsPerDataElement = dataElementSize/bitsPerPixel;
        this.bitMask = (1 << bitsPerPixel) - 1;    	
    }

    public SampleModel createCompatibleSampleModel(int w, int h) {
      return new SH1106SampleModel(w, h);
    }

    public DataBuffer createDataBuffer() {
        int size = width*height / dataElementSize;
        return new DataBufferByte(size);
    }

    public int getNumDataElements() {
        return 1;
    }

    /**
     * Returns the number of bits per sample for all bands.
     * @return the number of bits per sample.
     */
    public int[] getSampleSize() {
        int sampleSize[] = {pixelBitStride};
        return sampleSize;
    }

    /**
     * Returns the number of bits per sample for the specified band.
     * @param band the specified band
     * @return the number of bits per sample for the specified band.
     */
    public int getSampleSize(int band) {
        return pixelBitStride;
    }

    /**
     * Returns the offset of pixel (x,&nbsp;y) in data array elements.
     * @param x the X coordinate of the specified pixel
     * @param y the Y coordinate of the specified pixel
     * @return the offset of the specified pixel.
     */
    public int getOffset(int x, int y) {
//    	return x + (width * (y / pageHeight));
    	return x + (width * (y >> 3));
    }
    
    public int getBit(int x, int y) {
//    	return y % 8;
    	return y & 0b111; // y % 8
    }
    
    protected int getElement(DataBuffer data, int x, int y) {
		return data.getElem(x + (width * (y >> 3)));
//    	return data.getElem(getOffset(x, y));
	}

    /**
     * Returns the pixel bit stride in bits.  This value is the same as
     * the number of bits per pixel.
     * @return the <code>pixelBitStride</code> of this
     * <code>MultiPixelPackedSampleModel</code>.
     */
    public int getPixelBitStride() {
        return pixelBitStride;
    }

//    /**
//     * Returns the data bit offset in bits.
//     * @return the <code>dataBitOffset</code> of this
//     * <code>MultiPixelPackedSampleModel</code>.
//     */
//    public int getDataBitOffset() {
//        return dataBitOffset;
//    }

    /**
     *  Returns the TransferType used to transfer pixels by way of the
     *  <code>getDataElements</code> and <code>setDataElements</code>
     *  methods. The TransferType is always DataBuffer.TYPE_BYTE.
     *  @return the transfertype.
     */
    public int getTransferType() {
        return DataBuffer.TYPE_BYTE;
    }

    public SampleModel createSubsetSampleModel(int bands[]) {
        if (bands != null) {
           if (bands.length != 1)
            throw new RasterFormatException("SH1106SampleModel has only one band.");
        }
        SampleModel sm = createCompatibleSampleModel(width, height);
        return sm;
    }

    /**
     * Returns as <code>int</code> the sample in a specified band for the
     * pixel located at (x,&nbsp;y).  An
     * <code>ArrayIndexOutOfBoundsException</code> is thrown if the
     * coordinates are not in bounds.
     * @param x         the X coordinate of the specified pixel
     * @param y         the Y coordinate of the specified pixel
     * @param b         the band to return, which is assumed to be 0
     * @param data      the <code>DataBuffer</code> containing the image
     *                  data
     * @return the specified band containing the sample of the specified
     * pixel.
     * @exception ArrayIndexOutOfBoundsException if the specified
     *          coordinates are not in bounds.
     * @see #setSample(int, int, int, int, DataBuffer)
     */
    public int getSample(int x, int y, int b, DataBuffer data) {
//        // 'b' must be 0
        if ((x < 0) || (y < 0) || (x >= width) || (y >= height) || (b != 0)) {
            throw new ArrayIndexOutOfBoundsException("Coordinate out of bounds!");
        }
        int element = getElement(data, x, y);
        int bitnum = getBit(x, y); 
        return (element >> bitnum) & bitMask;
    }

    /**
     * Returns the specified single band pixel in the first element
     * of an <code>int</code> array.
     * <code>ArrayIndexOutOfBoundsException</code> is thrown if the
     * coordinates are not in bounds.
     * @param x the X coordinate of the specified pixel
     * @param y the Y coordinate of the specified pixel
     * @param iArray the array containing the pixel to be returned or
     *  <code>null</code>
     * @param data the <code>DataBuffer</code> where image data is stored
     * @return an array containing the specified pixel.
     * @exception ArrayIndexOutOfBoundsException if the coordinates
     *  are not in bounds
     * @see #setPixel(int, int, int[], DataBuffer)
     */
    public int[] getPixel(int x, int y, int iArray[], DataBuffer data) {
//        if ((x < 0) || (y < 0) || (x >= width) || (y >= height)) {
//            throw new ArrayIndexOutOfBoundsException("Coordinate out of bounds!");
//        }
        int pixels[];
        if (iArray != null) {
           pixels = iArray;
        } else {
           pixels = new int [numBands];
        }
        
        pixels[0] = getSample(x, y, 0, data);
        
//        int element = getElement(data, x, y);
//        int bitnum = getBit(x, y); 
//        pixels[0] = (element >> bitnum) & bitMask;
        return pixels;
    }

    
    /**
     * Sets a sample in the specified band for the pixel located at
     * (x,&nbsp;y) in the <code>DataBuffer</code> using an
     * <code>int</code> for input.
     * An <code>ArrayIndexOutOfBoundsException</code> is thrown if the
     * coordinates are not in bounds.
     * @param x the X coordinate of the specified pixel
     * @param y the Y coordinate of the specified pixel
     * @param b the band to return, which is assumed to be 0
     * @param s the input sample as an <code>int</code>
     * @param data the <code>DataBuffer</code> where image data is stored
     * @exception ArrayIndexOutOfBoundsException if the coordinates are
     * not in bounds.
     * @see #getSample(int, int, int, DataBuffer)
     */
    public void setSample(int x, int y, int b, int s, DataBuffer data) {
//        // 'b' must be 0
        if ((x < 0) || (y < 0) || (x >= width) || (y >= height) || (b != 0)) {
            throw new ArrayIndexOutOfBoundsException("Coordinate out of bounds!");
        }
        
        int index = getOffset(x, y);
        int element = data.getElem(index);
        int bitnum = getBit(x, y); 

        element &= ~(bitMask << bitnum); // clear bit
        element |= (s & bitMask) << bitnum; // set bit
        data.setElem(index,element);
    }

    /**
     * Returns data for a single pixel in a primitive array of type
     * TransferType.  
     * @param x the X coordinate of the specified pixel
     * @param y the Y coordinate of the specified pixel
     * @param obj a primitive array in which to return the pixel data or
     *          <code>null</code>.
     * @param data the <code>DataBuffer</code> containing the image data.
     * @return an <code>Object</code> containing data for the specified
     *  pixel.
     * @exception ClassCastException if <code>obj</code> is not a
     *  primitive array of type TransferType or is not <code>null</code>
     * @exception ArrayIndexOutOfBoundsException if the coordinates are
     * not in bounds, or if <code>obj</code> is not <code>null</code> or
     * not large enough to hold the pixel data
     * @see #setDataElements(int, int, Object, DataBuffer)
     */
    public Object getDataElements(int x, int y, Object obj, DataBuffer data) {
        if ((x < 0) || (y < 0) || (x >= width) || (y >= height)) {
            throw new ArrayIndexOutOfBoundsException("Coordinate out of bounds!");
        }

        int bitnum = getBit(x, y); 
        int element = 0;

        byte[] bdata;

        if (obj == null)
            bdata = new byte[1];
        else
            bdata = (byte[])obj;

        element = data.getElem(getOffset(x, y));
        bdata[0] = (byte)((element >> bitnum) & bitMask);

        obj = (Object)bdata;
        return obj;
    }


    /**
     * Sets the data for a single pixel in the specified
     * <code>DataBuffer</code> from a primitive array of type
     * TransferType.  
     * @param x the X coordinate of the pixel location
     * @param y the Y coordinate of the pixel location
     * @param obj a primitive array containing pixel data
     * @param data the <code>DataBuffer</code> containing the image data
     * @see #getDataElements(int, int, Object, DataBuffer)
     */
    public void setDataElements(int x, int y, Object obj, DataBuffer data) {
//        if ((x < 0) || (y < 0) || (x >= width) || (y >= height)) {
//            throw new ArrayIndexOutOfBoundsException("Coordinate out of bounds!");
//        }

        byte[] barray = (byte[])obj;
        int s = (int)(barray[0]) & 0xff;

        setSample(x, y, 0, s, data);
//        
//        int index = getOffset(x, y);
//        int element = data.getElem(index);
//        int bitnum = getBit(x, y); 
//        
//        element &= ~(bitMask << bitnum); // clear bit
//		element |= (s & bitMask) << bitnum; // set bit
//        data.setElem(index, element);
    }

    /**
     * Sets a pixel in the <code>DataBuffer</code> using an
     * <code>int</code> array for input.
     * <code>ArrayIndexOutOfBoundsException</code> is thrown if
     * the coordinates are not in bounds.
     * @param x the X coordinate of the pixel location
     * @param y the Y coordinate of the pixel location
     * @param iArray the input pixel in an <code>int</code> array
     * @param data the <code>DataBuffer</code> containing the image data
     * @see #getPixel(int, int, int[], DataBuffer)
     */
    public void setPixel(int x, int y, int[] iArray, DataBuffer data) {
//        if ((x < 0) || (y < 0) || (x >= width) || (y >= height)) {
//            throw new ArrayIndexOutOfBoundsException("Coordinate out of bounds!");
//        }

    	setSample(x, y, 0, iArray[0], data);
        
//        int index = getOffset(x, y);
//        int element = data.getElem(index);
//        int bitnum = getBit(x, y); 
//
//        element &= ~(bitMask << bitnum); // clear bit
//        element |= (iArray[0] & bitMask) << bitnum;  // set bit
//        data.setElem(index,element);
    }

    public boolean equals(Object o) {
        if ((o == null) || !(o instanceof SH1106SampleModel)) {
            return false;
        }

        SH1106SampleModel that = (SH1106SampleModel)o;
        return this.width == that.width &&
            this.height == that.height &&
            this.numBands == that.numBands &&
            this.dataType == that.dataType &&
            this.pixelBitStride == that.pixelBitStride &&
            this.bitMask == that.bitMask &&
            this.pixelsPerDataElement == that.pixelsPerDataElement &&
            this.dataElementSize == that.dataElementSize;
//            this.dataBitOffset == that.dataBitOffset;
    }

    // If we implement equals() we must also implement hashCode
	    public int hashCode() {
	        int hash = 0;
	        hash = width;
	        hash <<= 8;
	        hash ^= height;
	        hash <<= 8;
	        hash ^= numBands;
	        hash <<= 8;
	        hash ^= dataType;
	        hash <<= 8;
	        hash ^= pixelBitStride;
	        hash <<= 8;
	        hash ^= bitMask;
	        hash <<= 8;
	        hash ^= pixelsPerDataElement;
	        hash <<= 8;
	        hash ^= dataElementSize;
//	        hash <<= 8;
//	        hash ^= dataBitOffset;
	        return hash;
	    }
	}
