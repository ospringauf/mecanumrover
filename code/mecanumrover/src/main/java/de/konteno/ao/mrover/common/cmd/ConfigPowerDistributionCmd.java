/*********************************************************************
* Copyright (c) 2018-2021 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package de.konteno.ao.mrover.common.cmd;

import de.konteno.ao.mrover.common.RoverTopic;

/**
 * configure power distribution among motors (left/right, front/back)
 *
 */
@RoverTopic(address = "cmd.powerdist")
public class ConfigPowerDistributionCmd implements IRoverCommand {
	
	public final double lr;
	public final double fb;

	public ConfigPowerDistributionCmd(double lr, double fb) {
		this.lr = lr;
		this.fb = fb;
	}

}
