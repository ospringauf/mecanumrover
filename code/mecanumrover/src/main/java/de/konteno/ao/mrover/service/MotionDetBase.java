/*********************************************************************
* Copyright (c) 2019 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.service;

import de.konteno.ao.mrover.common.cmd.DriveCmd;
import de.konteno.ao.mrover.common.cmd.IMotionCommand;
import de.konteno.ao.mrover.common.cmd.RotateCmd;
import de.konteno.ao.mrover.common.cmd.StopCmd;
import io.vertx.core.Handler;

/**
 * provides methods for detecting change in motion
 *
 */
public abstract class MotionDetBase extends MyVerticleBase {

	// motion detection

	private boolean moving = false;
	private Handler<Boolean> motionChangeHandler;

	protected void motionChange(IMotionCommand cmd) {
		boolean m = cmd.getSpeed() > 0;
		if (m != moving) {
			motionChangeHandler.handle(m);
		}
		moving = m;
	}

	protected void onMotionChange(Handler<Boolean> motionChangeHandler) {
		this.motionChangeHandler = motionChangeHandler;

		busCmdListen(DriveCmd.class, this::motionChange);
		busCmdListen(RotateCmd.class, this::motionChange);
		busCmdListen(StopCmd.class, this::motionChange);
	}

}
