package de.konteno.ao.mrover.driver.odo;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;

import com.pi4j.io.serial.Baud;

/**
 * wrap the odometer driver, log incoming data to a file for later replay
 *
 */
public class LoggingOdometer2Driver extends Odometer2Driver {

	private BufferedWriter writer;

	public LoggingOdometer2Driver(String device, Baud baud, Path logFileName)  {
		super(device, baud);
		
		 try {
			writer = Files.newBufferedWriter(logFileName, Charset.forName("UTF-8"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	protected void messageReceived(String topic, String payload) {
		try {
			writer.write(String.format("%d\t%s%n", System.currentTimeMillis(), payload));
		} catch (IOException e) {
			e.printStackTrace();
		}
		super.messageReceived(topic, payload);
	}
	
	@Override
	public void shutdown() {
		super.shutdown();
		try {
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
