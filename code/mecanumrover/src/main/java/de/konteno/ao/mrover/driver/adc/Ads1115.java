/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.driver.adc;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.wiringpi.Gpio;

/**
 * Texas Instruments ADS1115 A/D converter 
 * 16bit, 4 input (2 differential), I²C interface (default address 0x48)
 * 
 * Note that there also is a GPIO-emulation for ADS1115 in Pi4J. 
 * This class is not complete. For now, only single-ended input is implemented (differential might come later) 
 * 
 * see
 * http://www.ti.com/lit/ds/symlink/ads1115.pdf
 * https://learn.adafruit.com/raspberry-pi-analog-to-digital-converters
 * 
 *
 */
public class Ads1115 implements ADConverter {

	private static final Logger logger = LoggerFactory.getLogger(Ads1115.class);

	public static final int REG_CONVERSION =0x00;
	public static final int REG_CONFIG =0x01;
	
	// only needed for comparator mode
	public static final int REG_LO_THRESH =0x02;
	public static final int REG_HI_THRESH =0x03;
	
	// configuration 
	public static final int OS_SINGLE = 1;

	public static final int GAIN_6V144 = 0b000;
	public static final int GAIN_4V096 = 0b001;
	public static final int GAIN_2V048 = 0b010;
	public static final int GAIN_1V024 = 0b011;
	public static final int GAIN_0V512 = 0b100;
	public static final int GAIN_0V256 = 0b101;
	
	public static final int MODE_CONTINUOUS = 0;
	public static final int MODE_SINGLESHOT = 1;

	// delay should depend on data rate, 
//	public static final int CONVERSATION_DELAY = 8; // 125 @8sps

	public static final int DATARATE_8SPS = 0;
	public static final int DATARATE_16SPS = 1;
	public static final int DATARATE_32SPS = 2;
	public static final int DATARATE_64SPS = 3;
	public static final int DATARATE_128SPS = 4;
	public static final int DATARATE_250SPS = 5;
	public static final int DATARATE_475SPS = 6;
	public static final int DATARATE_860SPS = 7;
	
	private I2CDevice device;
	private int mode = MODE_SINGLESHOT;

	private int gain = GAIN_6V144;
	private int dataRate = DATARATE_128SPS;
	private int conversationDelay = 10;
	
	private byte[] buf = new byte[2];
	private ByteBuffer bb = ByteBuffer.wrap(buf).order(ByteOrder.BIG_ENDIAN);

	public Ads1115(I2CDevice dev) {
		device = dev;
		setDataRate(DATARATE_128SPS);
	}
	
	protected void configure(int os, int mux, int pga, int mode, int dr, int compmode, int comppol, int complat, int compque) throws IOException {
		int cfg =
				((os & 0x1) << 15) |
				((mux & 0x7) << 12) |
				((pga & 0x7) << 9) |
				((mode & 0x1) << 8) |
				((dr & 0b111) << 5) |
				((compmode & 0x1) << 4) |
				((comppol & 0x1) << 3) |
				((complat & 0x1) << 2) |
				((compque & 0x3) << 0);
		bb.putShort(0, (short) cfg);
		byte[] a = bb.array();
		//System.out.println(String.format("ADS1115 configure (%02X %02X)", a[0], a[1]));
		device.write(REG_CONFIG, a);
	}
	
	public void configSingle(int channel) throws IOException {
		mode = MODE_SINGLESHOT;
		// 1100 0001 0000 0000 = 0xC100 for single read on channel 0
		int mux = 4 + (channel & 0x3); // 0..3 are differential mode
		configure(OS_SINGLE, mux, gain, MODE_SINGLESHOT, dataRate, 0, 0, 0, 0);		
	}
	
	public void configContinuous(int channel) throws IOException {
		mode = MODE_CONTINUOUS;
		int mux = 4 + (channel & 0x3); // 0..3 are differential mode
		configure(0, mux, gain, MODE_CONTINUOUS, dataRate, 0, 0, 0, 0);				
	}
	
	/* (non-Javadoc)
	 * @see de.konteno.ao.mrover.adc.ADConverter#readSingleValue(int)
	 */
	@Override
	public short readSingleValue(int channel) throws IOException {
		if (mode == MODE_SINGLESHOT) {
			configSingle(channel);
			Gpio.delay(conversationDelay);
		}
		return readValue();
	}

	private short readValue() throws IOException {
		device.read(REG_CONVERSION, buf, 0, 2);		
		short val = bb.getShort(0);		
//		System.out.println(String.format("ADS1115 read(%d) = %02X %02X (%04X = %d)", x, buf[0], buf[1], val, val));
		return val;
	}

	public int getGain() {
		return gain;
	}

	public void setGain(int gain) {
		this.gain = gain;
	}

	@Override
	public int getResolution() {
		return 16;
	}

	@Override
	public void continuousMode(short analogChannel) {
		try {
			configContinuous(analogChannel);
		} catch (IOException e) {
			logger.error( "failed to configure ADS1115", e);
		}		
	}
	
	
	public void shutdown() {
		try {
			for (int i = 0; i < 4; ++i) {
				// auto power save mode
				configSingle(i);
			}
		} catch (IOException e) {
			logger.error("failed to configure ADS1115 for shutdown", e);
		}
	}

	public int getDataRate() {
		return dataRate;
	}

	public void setDataRate(int dataRate) {
		this.dataRate = dataRate;
		
		// adjust conversation delay: 8ms @ 128sps; 125ms @ 8sps ...
		conversationDelay = (int) ((1000.0 / (1 << (dataRate + 3))) + 1);
		//System.out.println(String.format("ADS1115 dataRate = %d, delay = %d", dataRate, conversationDelay));

	}

	public int getConversationDelay() {
		return conversationDelay;
	}

	public void setConversationDelay(int conversationDelay) {
		this.conversationDelay = conversationDelay;
	}
}
