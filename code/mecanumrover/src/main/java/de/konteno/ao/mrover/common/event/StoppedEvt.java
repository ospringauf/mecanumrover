/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.common.event;

import de.konteno.ao.mrover.common.RoverTopic;

/**
 * Occurs after a "stop" command to notify everybody that we have stopped (for any reason).
 * Note that this is not the STOP command!
 *
 */
@RoverTopic(address="evt.stopped")
public class StoppedEvt implements IRoverEvent {
	
	public StoppedEvt() {}
	
	public StoppedEvt(String cause) {
		this.cause = cause;
	}
	
	private String cause;

	public String getCause() {
		return cause;
	}

	public void setCause(String cause) {
		this.cause = cause;
	}
}
