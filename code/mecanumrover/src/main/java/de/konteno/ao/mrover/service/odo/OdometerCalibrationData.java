/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package de.konteno.ao.mrover.service.odo;

import de.konteno.ao.mrover.common.event.OdometerEvt;

public class OdometerCalibrationData {
	
	// distance sensor measurements
	public double xmin;
	public double xmax;
	public double ymin;
	public double ymax;
	
	// odometer readings
	public double cx;
	public double cy;
	public double sq;

	// number of odometer events (for avg SQ)
	public int samples;

	public void collect(OdometerEvt evt) {
		samples++;
		cx += evt.countX;
		cy += evt.countY;
		sq += evt.sigQual;
	}
}