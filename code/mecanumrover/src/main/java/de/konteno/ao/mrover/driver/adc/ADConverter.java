/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.driver.adc;

import java.io.IOException;

public interface ADConverter {

	short readSingleValue(int channel) throws IOException;
	
	/**
	 * number of bits (10, 12, 16, ...)
	 * @return
	 */
	int getResolution();

	void continuousMode(short analogChannel); 

}