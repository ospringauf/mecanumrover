/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.driver.oled;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.IndexColorModel;
import java.awt.image.SampleModel;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.util.Arrays;

import com.pi4j.io.i2c.I2CDevice;

/**
 * 
 * SH1106
 * 132 X 64 Dot Matrix OLED/PLED
 * Segment/Common Driver with Controller
 * 
 * This wrapper offers an AWT <code>Graphics</code> object that can be used for
 * drawing on the display. It is based on a special <code>SampleModel</code> that
 * stores the bitmap in a byte[] layout that matches the SH1106 display memory layout.
 * 
 * based on 
 * https://github.com/ondryaso/pi-ssd1306-java
 * https://luma-oled.readthedocs.io/en/latest/index.html
 * https://github.com/rm-hull/luma.core/tree/master/luma/core
 * https://github.com/rm-hull/luma.oled/blob/master/luma/oled
 * 
 * TODO add code for SPI communication?
 * TODO extract common OLED data/logic into a base class
 */
public class SH1106 {

	public static final int COMMAND_MODE = 0x00;
	public static final int DATA_MODE = 0x40;

	// common constants
	public static final int DISPLAYOFF = 0xAE;
	public static final int DISPLAYON = 0xAF;
	public static final int DISPLAYALLON = 0xA5;
	public static final int DISPLAYALLON_RESUME = 0xA4;
	public static final int NORMALDISPLAY = 0xA6;
	public static final int INVERTDISPLAY = 0xA7;
	public static final int SETREMAP = 0xA0;
	public static final int SETMULTIPLEX = 0xA8;
	public static final int SETCONTRAST = 0x81;

	// SSD1306 / SH1106 constants
	public static final int CHARGEPUMP = 0x8D;
	public static final int COLUMNADDR = 0x21;
	public static final int COMSCANDEC = 0xC8;
	public static final int COMSCANINC = 0xC0;
	public static final int EXTERNALVCC = 0x1;
	public static final int MEMORYMODE = 0x20;
	public static final int PAGEADDR = 0x22;
	public static final int SETCOMPINS = 0xDA;
	public static final int SETDISPLAYCLOCKDIV = 0xD5;
	public static final int SETDISPLAYOFFSET = 0xD3;
	public static final int SETHIGHCOLUMN = 0x10;
	public static final int SETLOWCOLUMN = 0x00;
	public static final int SETPRECHARGE = 0xD9;
	public static final int SETSEGMENTREMAP = 0xA1;
	public static final int SETSTARTLINE = 0x40;
	public static final int SETVCOMDETECT = 0xDB;
	public static final int SWITCHCAPVCC = 0x2;
	
	protected I2CDevice device;
	public static final int WIDTH = 128;
	public static final int HEIGHT = 64;
	protected int pages;
	
	private BufferedImage img;
	private Graphics2D graphics;
	private DataBufferByte databuffer;

	/**
	 * Encapsulates the serial interface to the monochrome SH1106 OLED display
	 * hardware. An initialization sequence is pumped to the display to properly configure it. 
	 * Further control commands can then be called to affect the brightness and other settings.
	 *     
	 * @param dev
	 * @throws IOException
	 */
	public SH1106(I2CDevice dev) throws IOException {
		device = dev;
		pages = HEIGHT / 8;

		setupGraphics();
        
		command(new int[] { 
				DISPLAYOFF, 
				MEMORYMODE, 
				SETHIGHCOLUMN, 0xB0, 0xC8, 
				SETLOWCOLUMN, 0x10, 0x40,
				SETSEGMENTREMAP, 
				NORMALDISPLAY, 
				SETMULTIPLEX, 0x3F, 
				DISPLAYALLON_RESUME, 
				SETDISPLAYOFFSET, 0x00,
				SETDISPLAYCLOCKDIV, 0xF0, 
				SETPRECHARGE, 0x22, 
				SETCOMPINS, 0x12, 
				SETVCOMDETECT, 0x20, 
				CHARGEPUMP,	0x14 });

		contrast(0x7F);
		clear(true);
		//upsideDown(true);
		displayOn();
	}

	public void setupGraphics() {
		// build AWT Graphics -> BufferedImage -> Raster -> SampleModel -> DataBuffer chain,
		// using a SampleModel that mimics the SH1106 pixel layout
		SampleModel sm = new SH1106SampleModel(WIDTH, HEIGHT);
		databuffer = (DataBufferByte)(sm.createDataBuffer());
		
		WritableRaster r = WritableRaster.createWritableRaster(sm, databuffer, new Point(0, 0));

		// 1-bit, black/white indexed color model
		byte[] blackwhite = new byte[] {0, (byte) 255};
		IndexColorModel cm = new IndexColorModel(1, 2, blackwhite, blackwhite, blackwhite);
		
		img = new BufferedImage(cm, r, false, null);		
		graphics = img.createGraphics();
	}

	/**
	 * Clears the buffered image.
	 * 
	 * @param display clear display (send empty image to device)
	 * @throws IOException
	 */
	public void clear(boolean display) throws IOException {
		Arrays.fill(databuffer.getData(), (byte) 0);
		if (display) {
			displayGraphics();
		}
	}
	
	
	/**
	 * Send buffered image data to display.
	 * The display area consists of 8 pages. Each page is 8 pixels high and {WIDTH} pixels wide.
	 * Image data consist of one byte for each 8-pixel-column in the page, so each page is {WIDTH} bytes.
	 * Each page is sent separately to the display.
	 * 
	 * @throws IOException
	 */
	public void displayGraphics() throws IOException {
		for (int page = 0; page < pages; page += 1) {
			int pageAddress = 0xB0 + page;
			command(new int[] { pageAddress, 0x02, 0x10 });
			
			// the databuffer's layout already matches the display layout, so we just 
			// have to select the right page and send it  
			byte[] buf = Arrays.copyOfRange(databuffer.getData(), page * WIDTH, (page+1) * WIDTH);
			data(buf, WIDTH);
		}		
	}

	
    /**
     * Sets if the display should be inverted
     * @param invert Invert state
     * @throws IOException 
     */
    public void invertDisplay(boolean invert) throws IOException {
    	command(invert ? INVERTDISPLAY : NORMALDISPLAY);
    }
    
    public void scanDir(boolean invert) throws IOException {
    	command(invert ? COMSCANDEC : COMSCANINC);
    }

    public void segmentMap(boolean invert) throws IOException {
    	command(invert ? SETREMAP : SETSEGMENTREMAP);
    }
	
	public void upsideDown(boolean flag) throws IOException {
		command(flag ? COMSCANINC : COMSCANDEC);
		command(flag ? SETREMAP : SETSEGMENTREMAP);
	}

	/**
	 * Sends a command or sequence of commands through to the I2C address -
	 * maximum allowed is 32 bytes in one go.
	 * 
	 * @param cmd
	 * @throws IOException
	 */
	protected void command(byte[] cmd) throws IOException {
		device.write(COMMAND_MODE, cmd);
	}

	protected void command(int[] cmd) throws IOException {
		byte[] b = new byte[cmd.length];
		for (int i = 0; i < cmd.length; ++i) {
			b[i] = (byte) (cmd[i] & 0xFF);
		}
		command(b);
	}

	protected void command(int cmd) throws IOException {
		device.write(COMMAND_MODE, (byte) cmd);
	}

	/**
	 * Sends a data byte or sequence of data bytes through to the I2C address -
	 * maximum allowed in one transaction is 32 bytes, so if data is larger than
	 * this, it is sent in chunks.
	 * 
	 * @throws IOException
	 */
	protected void data(byte[] data, int count) throws IOException {
		int i = 0;
		while (i < count) {
			device.write((byte) DATA_MODE, data, i, 32);
			i += 32;
		}
	}

	/**
	 * Sets the display mode ON, waking the device out of a prior low-power
	 * sleep mode.
	 * 
	 * @throws IOException
	 */
	public void displayOn() throws IOException {
		command(DISPLAYON);
	}

	/**
	 * Switches the display mode OFF, putting the device in low-power sleep
	 * mode.
	 * 
	 * @throws IOException
	 */
	public void displayOff() throws IOException {
		command(DISPLAYOFF);
	}

	/**
	 * Switches the display contrast to the desired level, in the range 0-255.
	 * Note that setting the level to a low (or zero) value will not necessarily
	 * dim the display to nearly off. In other words, this method is **NOT**
	 * suitable for fade-in/out animation.
	 * 
	 * @param level
	 *            Desired contrast level in the range of 0-255.
	 * @throws IOException
	 */
	public void contrast(int level) throws IOException {
		command(new byte[] { (byte) SETCONTRAST, (byte) level });
	}

	public Graphics2D getGraphics() {
		return graphics;
	}

}
