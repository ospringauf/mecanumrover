/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.konteno.ao.mrover.common.Rover;
import de.konteno.ao.mrover.common.cmd.ConfigSensorCmd;
import de.konteno.ao.mrover.common.cmd.DriveCmd;
import de.konteno.ao.mrover.common.cmd.FollowCmd;
import de.konteno.ao.mrover.common.cmd.StopCmd;
import de.konteno.ao.mrover.common.event.LaserDistanceEvt;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.MessageConsumer;

/**
 * "follow me" demo service
 *
 */
public class FollowService extends MyVerticleBase {
	
	private static final Logger logger = LoggerFactory.getLogger( FollowService.class );

	private Promise<Void> current;

	@Override
	public void start() throws Exception {
		super.start();

		busConsume(FollowCmd.class, this::follow);
		
		// cancel follow on "stop" command
		busCmdListen(StopCmd.class, this::onStop);
	}

	private Future<Void> follow(FollowCmd cmd) {
		final int lidarWasOn = Rover.sensorMode & ConfigSensorCmd.MODE_LIDAR;
		busSend(new ConfigSensorCmd(Rover.sensorMode | ConfigSensorCmd.MODE_LIDAR));
		
		// adjust speed & direction for distance
		final MessageConsumer mc = busListen(LaserDistanceEvt.class, this::onDistance);
		
		Promise<Void> ret = Promise.promise();
		
		current = Promise.promise();
		current.future().onComplete(ar -> {
			logger.debug("follow complete");
			mc.unregister(); // stop listening to distance events
			busSend(new ConfigSensorCmd((Rover.sensorMode & ~ConfigSensorCmd.MODE_LIDAR) | lidarWasOn));
			ret.complete();
		});
		
		return ret.future();
	}

	/**
	 * keep a given distance (17cm) to a (moving) object in front of the distance
	 * sensor
	 * 
	 * @throws Exception
	 */
	private void onDistance(LaserDistanceEvt evt) {
		if (current == null) {
			return;
		}

		int center = 15;
		int minrange = 0;
		int maxrange = 60;

		double d = evt.getDistance(LaserDistanceEvt.RIGHT);		

		if ((Math.abs(d - center) < 2) || (d >= maxrange)) {
			logger.trace("dist={} - on spot or out of range - stop", d);
			busSend(DriveCmd.forward(0)); // stop
			
		} else if (d > minrange && d < maxrange) {
			
			double s = Rover.bestSpeed(3 * (center - d));
			DriveCmd drv = (d < center) ? DriveCmd.back(s) : DriveCmd.forward(s);
			logger.trace("dist={} - drive to {} @ {}", d, drv.getCourse(), s);
			busSend(drv);
		}
	}

	private void onStop(StopCmd evt) {
		// complete the current command
		if (current != null) {
			current.tryComplete();
			current = null;
		}
	}

	@Override
	public void onShutdown() {
	}

}
