/*********************************************************************
* Copyright (c) 2019 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package de.konteno.ao.mrover.driver.imu;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.serial.Baud;

import de.konteno.ao.mrover.driver.serial.RoverSerialPort;
import io.vertx.core.Vertx;

/**
 * Communication with the MCU9255/STM32 IMU via serial (onboard UART)
 * see RTIMULib code
 *
 */
public class MyImuDriver implements Publisher<Double> {
	private static final Logger logger = LoggerFactory.getLogger(MyImuDriver.class);

	RoverSerialPort port;
	Subscriber<? super Double> subscriber;

	// GPIO-28 (BCM-20) connected to STM32 reset; reset IMU on startup
	private GpioPinDigitalOutput nrst;

	private long messageCount;

	public MyImuDriver(String device, Baud baud) {
		// listen for data from Arduino on serial port
		// and set handler for incoming messages (already parsed)
		port = new RoverSerialPort("myimu");
		port.setHandler(this::messageReceived);
		try {
			port.start(device, baud);
			port.flush();
		} catch (Exception e) {
			logger.error("failed to open serial port " + device, e);
		}

		nrst = GpioFactory.getInstance().provisionDigitalOutputPin(RaspiPin.GPIO_28); // BCM 20
		Vertx.currentContext().owner().setTimer(2000, t -> reset());
	}

	// message format: $GYR,53.7*
	protected void messageReceived(String topic, String payload) {
		// log only 1st message?
		if (messageCount == 0)
			logger.info("received: topic: {}, payload: {}", topic, payload);
		messageCount++;

		if ("YAW".equals(topic)) {
			double heading = Double.parseDouble(payload);

			// pass data to subscriber (service)
			if (subscriber != null) {
				subscriber.onNext(heading);
			}
		}
	}

	public void reset() {
		try {
			port.flush();
			nrst.setState(false); // LOW = reset
			Thread.sleep(50);
			nrst.setState(true);
			logger.info("reset MYIMU");
		} catch (InterruptedException e) {
			logger.error("failed to reset MYIMU: ", e);
		}
	}

	public void subscribe(Subscriber<? super Double> sub) {
		if (this.subscriber != null)
			logger.warn("already has a subscriber: " + this.subscriber);
		this.subscriber = sub;
	}

	public void shutdown() {
		try {
			port.flush();
			port.shutdown();
		} catch (Exception e) {
			logger.warn("failed to close IMU port: {}", e.getMessage());
		}
	}
}
