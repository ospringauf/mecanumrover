/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.driver.common;

public interface DistanceSensor {
	
	
		
	/**
	 * take single sample
	 * @return
	 * @throws SensorException
	 */
	float measureDistance() throws SensorException;

	/**
	 * take n samples and return average
	 * @param samples
	 * @return
	 * @throws SensorException
	 */
	float measureDistance(int samples) throws SensorException;

	void fastMode();

}
