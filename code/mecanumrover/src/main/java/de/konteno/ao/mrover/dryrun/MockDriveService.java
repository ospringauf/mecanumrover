package de.konteno.ao.mrover.dryrun;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.konteno.ao.mrover.common.Rover;
import de.konteno.ao.mrover.common.cmd.DriveCmd;
import de.konteno.ao.mrover.common.cmd.RotateCmd;
import de.konteno.ao.mrover.common.cmd.StopCmd;
import de.konteno.ao.mrover.common.event.ShutdownEvt;
import de.konteno.ao.mrover.common.event.StoppedEvt;
import de.konteno.ao.mrover.service.motor.DriveService;
import io.vertx.core.Future;

public class MockDriveService extends DriveService {

	private static final Logger logger = LoggerFactory.getLogger( MockDriveService.class );

	public MockDriveService() {
	}

	@Override
	public void start() throws Exception {		
		busListen(ShutdownEvt.class, x -> onShutdown());

		// receive commands
		busConsume(DriveCmd.class, this::drive);
		busConsume(RotateCmd.class, this::rotate);
		busConsume(StopCmd.class, this::stopMotors);
		
	}	

	public Future<Void> stopMotors(StopCmd x) {
		Rover.currentSpeed = 0;
		String cause = x != null ? x.getCause() : null;
		busPublish(new StoppedEvt(cause));
		logger.debug("stopping(" + cause + ")");
		return Future.succeededFuture();
	}

	private Future<Void> drive(DriveCmd cmd) {
		Rover.currentSpeed =  Math.abs(cmd.getSpeed());
		Rover.pose.relativeCourse = cmd.relativeCourse();
		logger.trace("driving(sim) " + cmd.getCourse().toDegree());
		return buildFuture(cmd);
	}
	

	private Future<Void> rotate(RotateCmd cmd) {
		Rover.currentSpeed =  Math.abs(cmd.getSpeed());
		Rover.pose.relativeCourse = cmd.relativeCourse();
		logger.trace(String.format("rotating(sim) %3.2f",cmd.getSpeed()));
		return Future.succeededFuture();
	}


	@Override
	public void onShutdown() {
		logger.debug("onShutdown");
		stopMotors(new StopCmd("shutdown"));
	}

}
