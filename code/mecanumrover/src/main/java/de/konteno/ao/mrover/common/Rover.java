/*********************************************************************
* Copyright (c) 2018-2019 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.common;

import de.konteno.ao.mrover.common.cmd.ConfigSensorCmd;
import de.konteno.ao.mrover.common.track.Pose;
import de.konteno.ao.mrover.common.track.TrackType;
import de.konteno.ao.mrover.common.unit.Angle;
import de.konteno.ao.mrover.common.unit.Pos;

/**
 * current state / position / configuration
 *
 */
public class Rover {

	private Rover() {}
	
	public static double speedLimit = 0.99;
	public static double currentSpeed = 0;
	public static boolean telemetry = true;
	
	/**
	 * pose contains position (x,y) and heading
	 * 
	 * most recent waypoint, updated on every odometer event
	 * (not necessarily contained in a "track")
	 */
	public static Pose pose;
	
	public static int sensorMode = ConfigSensorCmd.MODE_COMPASS;
	
	public static boolean isStopped() {
		return currentSpeed == 0;
	}
	
	static {
		clearTracks();
	}
	
	
	/**
	 * maneuvering speed: linear between dmin and dmax distance, vmin below dmin
	 * vmax above dmax
	 * 
	 * can also be used for angles!
	 * 
	 * @param d
	 * @return
	 */
	public static double bestSpeed(double d) {
		d = Math.abs(d);
		double dmin = 6;
		double dmax = 40;
		double vmin = 0.20f;
		double vmax = speedLimit;
		if (d < dmin) {
			return vmin;
		}
		if (d > dmax) {
			return vmax;
		}
		return vmin + (vmax - vmin) * (d - dmin) / (dmax - dmin);
	}
	
	
	/**
	 * collision warning limit
	 * 13cm @ 100%
	 * 7cm @ 50% or less
	 * note: CollisionService adds extra left/right clearance depending on relative course 
	 * @return min clearance to avoid collision
	 */
	public static int minClearance() {
		double s = Math.abs(currentSpeed);
		return (int) Math.max(7.0, s*13.0);
	}

	public static void clearTracks() {
		pose = new Pose(Pos.ZERO, Angle.ZERO, Angle.ZERO, TrackType.ODO);
	}

	public static boolean hasMode(int mode) {
		return (sensorMode & mode) == mode;
	}
}
