/*********************************************************************
* Copyright (c) 2019 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package de.konteno.ao.mrover.common.unit;

public class Pos {
	public double x, y;
	public static final Pos ZERO = Pos.of(0, 0);

	private Pos(double dx, double dy) {
		x = dx;
		y = dy;
	}

	/**
	 * cartesian, pos angle = ccw
	 */
	public Pos rotate(Angle a) {
		return Pos.of(
				x * a.cos() - y * a.sin(), 
				x * a.sin() + y * a.cos());
	}

	public Pos scale(double sx, double sy) {
		return Pos.of(x * sx, y * sy);
	}

	public Pos translate(Pos delta) {
		return Pos.of(x + delta.x, y + delta.y);
	}

	public static Pos of(double dx, double dy) {
		return new Pos(dx, dy);
	}

	public static Pos of(String s1, String s2) {
		return Pos.of(Double.parseDouble(s1), Double.parseDouble(s2));
	}

	public Angle toAngle0() {
		// cartesian system
		return Angle.fromRad(Math.atan2(y, x)); 
	}
	
	public Angle toAngle() {
		// robot system
		return Angle.fromRad(Math.atan2(y, x)).negative().plus(Angle.fromDegree(90));
	}
	
	/**
	 * length of the hypothenuse
	 */
	public double distance() {
		return Math.sqrt(x*x + y*y);
	}

	@Override
	public String toString() {
		return String.format("x=%3.3f y=%3.3f", x, y);
	}

}
