/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package de.konteno.ao.mrover.common.cmd;

import de.konteno.ao.mrover.common.RoverTopic;

@RoverTopic(address="cmd.follow")
public class FollowCmd implements IRoverCommand {

}
