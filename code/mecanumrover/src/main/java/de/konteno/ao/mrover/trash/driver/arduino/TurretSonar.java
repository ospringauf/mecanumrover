/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.trash.driver.arduino;

import de.konteno.ao.mrover.driver.common.DistanceSensor;
import de.konteno.ao.mrover.driver.common.SensorException;

/**
 * "sonar distance" adapter for arduino 
 * @author oliver
 *
 */
public class TurretSonar implements DistanceSensor {

	private TurretMcu device;

	public TurretSonar(TurretMcu ardu) {
		this.device = ardu;
	}

	@Override
	public float measureDistance() throws SensorException {
		try {
			return device.singleSonar();
		} catch (Exception e) {
			throw new SensorException(e);
		}
	}

	@Override
	public float measureDistance(int samples) throws SensorException {
		try {
			int success = 0;
			float sum = 0;
			for (int i = 0; i < samples; ++i) {
				float d = measureDistance();
				if (d >= 0) {
					success++;
					sum += d;
				}
				// recommended cycle time; wait for echo to fade away
				Thread.sleep(50); 
			}

			return (success > 0) ? sum / success : -1;
		} catch (Exception e) {
			throw new SensorException(e);
		}
	}

	@Override
	public void fastMode() {
		// TODO Auto-generated method stub
		
	}
}
