/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.common;

public enum RoverDirection {

	CW,
	CCW,

//	FORWARD,
//	BACKWARD,
//	LEFT,
//	RIGHT;

//	private final int value;
//
//	private RoverDirection(int value) {
//		this.value = value;
//	}
//
//	public int value() {
//		return value;
//	}

}
