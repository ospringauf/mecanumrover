/*********************************************************************
* Copyright (c) 2018-2019 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.common.event;

import de.konteno.ao.mrover.common.RoverTopic;
import de.konteno.ao.mrover.common.unit.Angle;

@RoverTopic(address="evt.compass")
public class CompassEvt implements IRoverEvent {

	private CompassEvt() {}
	
	/**
	 * payload format is "{angle} {calib}" eg. "$COMP,70.1 3333*64"
	 * where "angle" is the current heading from the BNO055 compass in NDOF mode
	 * and "calib" is the SYS/MAG/ACCEL/GYRO calibration level (0-3). Ideally this should be 3333. 
	 * @param payload
	 */
	public static CompassEvt parseEsp(String payload) {
		CompassEvt e = new CompassEvt();
		String[] f = payload.trim().split("\\s+");
		
		double a = Double.parseDouble(f[0]);		
		e.setHeading(Angle.fromDegree(a));		
		e.setQuality(Integer.parseInt(f[1]));
		return e;
	}
	
	private Angle heading;
	private int quality;
		
	public CompassEvt(Angle heading) {
		this.heading = heading;
	}
	
	public Angle getHeading() {
		return heading;
	}

	public void setHeading(Angle heading) {
		this.heading = heading;
	}

	public int getQuality() {
		return quality;
	}

	public void setQuality(int quality) {
		this.quality = quality;
	}

	public String mqttString() {
		return String.format("%s %s", heading.degString(), quality);
	}
	
	public static CompassEvt parseMqtt(String s) {
		String[] f = s.trim().replace(',', '.').split("\\s+");
		return new CompassEvt(Angle.fromDegree(Double.parseDouble(f[0])));
	}

}
