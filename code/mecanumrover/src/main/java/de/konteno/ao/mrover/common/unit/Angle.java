/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.common.unit;


public class Angle {
	public static final Angle ZERO = Angle.fromDegree(0);
	
	private double degree;

	Angle() {}
	
	public Angle(double deg) {
		setDegree(deg);
	}

    public double getDegree() {
        return degree;
    }

    public void setDegree(double degree) {
        this.degree = degree;
    }

	public double toDegree() {
		return getDegree();
	}

	public double toRad() {
		return Math.PI * getDegree() / 180;
	}

	public static Angle fromDegree(double deg) {
		// return new Angle(remDegree(deg));
		return new Angle(deg);
	}

	public static Angle fromRad(double rad) {
		return new Angle(180 * rad / Math.PI); // 360′ = 2pi
	}

	/**
	 * circle angle remainder (modulo 360) function, always returns a positive value
	 */
	public static Angle remAngle(double a) {
		return Angle.fromDegree(remDegree(a));
	}

	public Angle opposite() {
		return remAngle(getDegree() + 180);
	}

	public Angle absolute() {
		return remAngle(getDegree());
	}

	@Override
	public String toString() {
		return degString();
	}
	
	public static Angle parseDeg(String s) {
		return fromDegree(Double.parseDouble(s));
	}

	public String degString() {
		return String.format("%3.1f", getDegree());
	}

	/**
	 * circle angle remainder (modulo 360) function, always returns a positive value
	 */
	public static double remDegree(double a) {
		double r = a % 360;
		return (r < 0) ? r + 360 : r;
	}

	public static double normalizeDeg(double d) {
		while (d < 0.0) d += 360;
		while (d > 360.0) d -= 360;
		return d;
	}

	/*
	 * Winkeldifferenz relativ (-180 ... 180 Grad) zum Zielwinkel see
	 * https://math.stackexchange.com/questions/1366869/calculating-rotation-
	 * direction-between-two-angles
	 */
	public static Angle delta(Angle target, Angle current) {
		double delta = target.toDegree() - current.toDegree();
		double t = 0;

		// decide whether to turn left or right for target heading
		if ((-180 < delta) && (delta <= 180)) {
			t = delta;
		} else if (delta > 180) {
			t = delta - 360;
		} else if (delta <= -180) {
			t = delta + 360;
		}
		return fromDegree(t);
	}

	public static double convertToDegree(double x, double y, double offsetDegree) {
		return (Math.atan2(y, x) * (180.0 / Math.PI) + 720 + offsetDegree) % 360.0;
	}

	public Angle minus(Angle h0) {		
		return Angle.fromDegree(normalizeDeg(toDegree() - h0.toDegree()));
	}


	public Angle plus(Angle h0) {
		return addDegree(h0.toDegree());
	}

	public Angle addDegree(double da) {
		 return Angle.fromDegree(normalizeDeg(toDegree() + da));
	}

	public double cos() {
		return Math.cos(toRad());
	}

	public double sin() {
		return Math.sin(toRad());	
	}

	public Angle negative() {
		return Angle.fromDegree(-getDegree());
	}

	public Pos toPos(double radius) {
		return Pos.of(radius * cos(), radius * sin());
	}

}
