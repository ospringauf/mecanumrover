package de.konteno.ao.mrover.dryrun;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.konteno.ao.mrover.common.Rover;
import de.konteno.ao.mrover.common.cmd.RotateCmd;
import de.konteno.ao.mrover.common.event.CompassEvt;
import de.konteno.ao.mrover.common.event.StoppedEvt;
import de.konteno.ao.mrover.common.unit.Angle;
import de.konteno.ao.mrover.service.MyVerticleBase;

public class MockCompassService extends MyVerticleBase {

	private static final Logger logger = LoggerFactory.getLogger(MockCompassService.class);

	static final int freqLo = 3; // Hz
	static final int freqHi = 10; // Hz
	int freq = freqLo;
	boolean highSpeed = false;
	long timerId;

	// double rotSpeed = 0;

	private RotateCmd current = null;

	@Override
	public void start() throws Exception {
		super.start();

		busCmdListen(RotateCmd.class, cmd -> onRotate(cmd)); // just want to know when we're rotating...
		busListen(StoppedEvt.class, cmd -> {
			onRotate(new RotateCmd(0));
		});

		// periodic compass readings
		timerId = vertx.setPeriodic(1000 / freq, v -> measure(v));
	}

	private void onRotate(RotateCmd cmd) {
		// LOGGER.fine("listen: " + cmd);
		current = cmd;
		// rotSpeed = cmd.getSpeed(); // * (cmd.getDirection() == RoverDirection.RIGHT ?
		// 1 : -1);

		// high measurement frequency during turns
		adjustTimer(Math.abs(cmd.getSpeed()) > 0.01);
	}

	private void adjustTimer(boolean hi) {
		if (hi == highSpeed) {
			return;
		}
		vertx.cancelTimer(timerId);
		highSpeed = hi;
		freq = hi ? freqHi : freqLo;
		timerId = vertx.setPeriodic(1000 / freq, v -> measure(v));
		logger.info(String.format("adjust timer to %d Hz", freq));
	}

	private void measure(Long v) {
		if (current != null) {
			double b = Rover.pose.heading.toDegree();

			// b += (360.0 / (5.0*freq)) * rotSpeed; // full turn at full speed = 5s
			double da = current.estimateDalpha(1000 / freq);
			b += da;
			b *= (1 + 0.015 * (Math.random()-0.5)); // max 1.5% error

			logger.trace(String.format("heading = %3.1f", b));
			Rover.pose.heading = Angle.fromDegree(b);
		}
		busPublish(new CompassEvt(Rover.pose.heading));
	}

	@Override
	public void onShutdown() {
	}

}
