/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.driver.motor;

public class MotorGroup {

	private AdafruitMotor[] motors;

	public MotorGroup(AdafruitMotor[] adafruitMotors) {		
		motors = adafruitMotors;
	}

	public void go(double d) throws MotorControlException {
		for (AdafruitMotor m : motors) {
			m.go(d);
		}
	}
	
	public void run(int command) throws MotorControlException {
		for (AdafruitMotor m : motors) {
			m.run(command);
		}
	}

}
