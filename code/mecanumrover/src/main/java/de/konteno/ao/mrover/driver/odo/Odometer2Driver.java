/*********************************************************************
* Copyright (c) 2019 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package de.konteno.ao.mrover.driver.odo;

import static de.konteno.ao.mrover.common.cmd.ConfigSensorCmd.MODE_ODO;

import org.reactivestreams.Subscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pi4j.io.serial.Baud;

import de.konteno.ao.mrover.common.Rover;
import de.konteno.ao.mrover.driver.serial.RoverSerialPort;

/**
 * Communication with the Arduino Odometer board over USB-serial interface. Receive sensor data and send commands.
 *
 */
public class Odometer2Driver implements IOdo2Driver {
    private static final Logger logger = LoggerFactory.getLogger(Odometer2Driver.class);

    RoverSerialPort port;
    Subscriber<? super OdoSensor[]> subscriber;

    public Odometer2Driver(String device, Baud baud) {
        
        readConfiguration();
        
        // listen for data from Arduino on serial port
        // and set handler for incoming messages (already parsed)
        port = new RoverSerialPort("odo");
        port.setHandler(this::messageReceived);
        try {
            port.start(device, baud);
        } catch (Exception e) {
            logger.error("failed to open serial port " + device, e);
        }
    }


    // message format: $ODO2,-12,-36,68,50,-29,48*
    // payload: -12,-36,68,50,-29,48
    protected void messageReceived(String topic, String payload) {
        logger.trace("received: topic: {}, payload: {}", topic, payload);

        OdoSensor.updateMulti(payload, sensors);

        // pass data to subscriber (service)
        if (subscriber != null) {
            subscriber.onNext(sensors);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.konteno.ao.mrover.driver.odo.IOdo2Driver#powerUp()
     */
    @Override
    public void powerUp() {
        Rover.sensorMode |= MODE_ODO;
        port.send("u");
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.konteno.ao.mrover.driver.odo.IOdo2Driver#powerDown()
     */
    @Override
    public void powerDown() {
        Rover.sensorMode &= ~MODE_ODO;
        port.send("d");
    }

    public void reset() {
        port.send("r");
    }

    public void hiRes() {
        port.send("h");
    }

    public void loRes() {
        port.send("l");
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.konteno.ao.mrover.driver.odo.IOdo2Driver#shutdown()
     */
    @Override
    public void shutdown() {
    	try {
    		powerDown();
    		port.flush();
    		port.shutdown();
    	} catch (Exception e) {
    		logger.warn("error during odometer shutdown", e);
    	}
        saveConfiguration();
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.konteno.ao.mrover.driver.odo.IOdo2Driver#subscribe(org.reactivestreams.Subscriber)
     */
    @Override
    public void subscribe(Subscriber<? super OdoSensor[]> sub) {
        this.subscriber = sub;
    }
}
