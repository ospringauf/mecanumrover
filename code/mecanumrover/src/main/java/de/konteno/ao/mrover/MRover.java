/*
 * Copyright (c) 2017-2018 Angelika Wittek, Oliver Springauf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Authors:
 *    Angelika Wittek, Oliver Springauf
 * Contributors:
*/
package de.konteno.ao.mrover;

//import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
//import org.eclipse.paho.client.mqttv3.MqttCallback;
//import org.eclipse.paho.client.mqttv3.MqttMessage;

import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CFactory;
import com.pi4j.io.i2c.I2CFactory.UnsupportedBusNumberException;
import com.pi4j.io.spi.SpiChannel;
import com.pi4j.io.spi.SpiDevice;
import com.pi4j.io.spi.SpiFactory;
import com.pi4j.io.spi.SpiMode;

import de.konteno.ao.mrover.common.Const;
import de.konteno.ao.mrover.common.Rover;
import de.konteno.ao.mrover.common.RoverDirection;
import de.konteno.ao.mrover.driver.adc.Ads1115;
import de.konteno.ao.mrover.driver.adc.PowerMeter;
import de.konteno.ao.mrover.driver.common.DistanceSensor;
import de.konteno.ao.mrover.driver.compass.HMC5883L;
import de.konteno.ao.mrover.driver.compass.HMC5883LConfig;
import de.konteno.ao.mrover.driver.motor.AdafruitMotor;
import de.konteno.ao.mrover.driver.motor.AdafruitMotorControl;
import de.konteno.ao.mrover.driver.motor.MotorControlException;
import de.konteno.ao.mrover.driver.motor.MotorGroups;
import de.konteno.ao.mrover.driver.oled.SH1106;
import de.konteno.ao.mrover.driver.tft.ST7789;
import de.konteno.ao.mrover.trash.MRoverDisplay;
import de.konteno.ao.mrover.trash.MotionController1;
import de.konteno.ao.mrover.trash.driver.arduino.TurretMcu;

/**
 * ┌─┐ ┬ ┌─┐  ╔╦╗╔═╗╔═╗╔═╗╔╗╔╦ ╦╔╦╗╦═╗╔═╗╦  ╦╔═╗╦═╗
 * ├─┤┌┼─│ │  ║║║║╣ ║  ╠═╣║║║║ ║║║║╠╦╝║ ║╚╗╔╝║╣ ╠╦╝ 
 * ┴ ┴└┘ └─┘  ╩ ╩╚═╝╚═╝╩ ╩╝╚╝╚═╝╩ ╩╩╚═╚═╝ ╚╝ ╚═╝╩╚═
 * 
 * note: we are using "WiringPi" pin numbering schema (default)
 *
 * current pin use:
 * GPIO.0  - sonar TRIGGER
 * GPIO.1  - sonar ECHO
 * GPIO.29 - reserved (shutdownbutton script)
 */
public class MRover 
{

	private AdafruitMotorControl motcontrol;
	private MotorGroups motorGroups;
	private RoverDirection direction;

	private static final int TURRET_MCU_ADR = 0x0B;
	
	public static boolean exit = false;
	
	// other rover components
	// private Hd44780_I2c lcd;
	private HMC5883L compass;
	private Ads1115 adc;
	private DistanceSensor ir1;
	private DistanceSensor sonar1;
//	private SensorTurret stur;
	private MRoverDisplay display;
	private TurretMcu ardu1;
	private MotionController1 mc1;
	private PowerMeter powerm;

	public static void main(String[] args) {
		// AWT graphics are used for "painting" on the OLED display
		System.setProperty("java.awt.headless", "true");

		// System.out.println("GPIO provider: " +
		// GpioFactory.getDefaultProvider().getName());

		MRover mrover;
		try {
			mrover = new MRover();

			if ((args.length > 0) && "remote".equals(args[0])) {
//				System.out.println("MQTT remote control mode");
//				MqttRemoteController remoteMc1 = new MqttRemoteController("move/#", mrover.mc1);
//				MqttRemoteController remoteRover = new MqttRemoteController("rover/#", mrover);

//				EventBroker.addInfoListener(remoteMc1);

				while (!exit) {
					Thread.sleep(1000);
				}
//				remoteMc1.shutdown();
//				remoteRover.shutdown();
//				mrover.shutdown();

			} else {
				mrover.demo();
//				mrover.shutdown();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		// ReschedulableTimer.cancelAll();
	}

	public MRover() throws Exception {
		I2CBus iic1 = I2CFactory.getInstance(I2CBus.BUS_1);

		// use 100Hz, servo does not like 1600Hz
		motcontrol = new AdafruitMotorControl(iic1.getDevice(Const.MOTCONTROL_ADR), Const.PWM_FREQ);
		motorGroups = new MotorGroups(motcontrol);
//		shutdownComponents.add(motcontrol);

		// --- 1602 LCD display
		// lcd = new Hd44780_I2c(iic1.getDevice(DISPLAY_ADR));
		// lcd.displayString(" MECANUMROVER", 1);
		// shutdownComponents.add(lcd);

		// --- compass
		compass = new HMC5883L(iic1.getDevice(Const.COMPASS_ADR), HMC5883LConfig.Default);
		compass.loadCalibration();

		// --- distance / ADC
		/*
		 * adc = new Ads1115(iic1.getDevice(ADC_ADR)); ir1 = new
		 * Sharp0A41SK(adc, (short) 1); shutdownComponents.add(adc);
		 */

		powerm = new PowerMeter(iic1.getDevice(Const.ADC_ADR));
//		shutdownComponents.add(powerm);

		ardu1 = new TurretMcu(iic1.getDevice(TURRET_MCU_ADR));
//		shutdownComponents.add(ardu1);

		// sonar1 = new HCSR04_Extern(RaspiPin.GPIO_00, RaspiPin.GPIO_01); //
		// BCM 17/18
		sonar1 = ardu1.getSonar0();
		// shutdownComponents.add(sonar1);

//		stur = new SensorTurret(motcontrol.servo0, ir1, sonar1);
//		stur.getServo().setRange(570 - 310, 570 + 310);
//		stur.neutralPosition();
//		shutdownComponents.add(stur);

		display = new MRoverDisplay(new SH1106(iic1.getDevice(Const.OLED_ADR)));
		Thread.sleep(100);
		display.displayText(new String[] { "", "M E C A N U M R O V E R", "", "@ Eclipse DemoCamp"});
//		shutdownComponents.add(display);

//		mc1 = new MotionController1(new BasicMotionController(motcontrol));
//		mc1.setDistanceSensor(sonar1);
//		mc1.setCompass(compass);
//
//		// Shutdown the motors on exit. From Adafruit tutorial:
//		// The PWM driver is 'free running' - that means that even if the python
//		// code or Pi linux kernel crashes,
//		// the PWM driver will still continue to work. This is good because it
//		// lets the Pi focus on linuxy things
//		// while the PWM driver does its PWMy things. But it means that the
//		// motors DO NOT STOP when the python code quits
//		// For that reason, we strongly recommend this 'at exit' code when using
//		// DC motors, it will do its best
//		// to shut down all the motors.
//		Runtime.getRuntime().addShutdownHook(new Thread() {
//			@Override
//			public void run() {
//				try {
////					shutdown();
//
//					System.out.println("shutdown all motors");
//					motcontrol.stopAllMotors();
//
//				} catch (MotorControlException e) {
//					// can't help it
//					e.printStackTrace();
//				}
//			}
//		});
	}

//	@Override
//	public void shutdown() {
//		try {
//			// lcd.clear();
//			display.displayText(new String[] { "goodbye", "", powerm.readPower() });
//			motcontrol.stopAllMotors();
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		super.shutdown();
//	}

	/**
	 * basic movements:
	 * http://wiki.seeed.cc/4WD_Mecanum_Wheel_Robot_Kit_Series/#Introduction
	 * 
	 * @throws Exception
	 * @throws UnsupportedBusNumberException
	 */
	public void demo() throws Exception {
		double speed0 = Rover.speedLimit;

		// ir1.measureDistance();
		printCommands();
		boolean repeat = true;
		while (repeat) {
			System.out.print("MRover > ");
			char input = (char) System.in.read();

			if (Character.isUpperCase(input)) {
				speed0 = -Rover.speedLimit;
//				direction = RoverDirection.BACKWARD;
			} else {
				speed0 = Rover.speedLimit;
//				direction = RoverDirection.FORWARD;
			}
			input = Character.toLowerCase(input);

			SpiDevice spi;
			switch (input) {

//			case '1':
//				mc1.demo();
//				printCommands();
//				break;

			case '9':
				display.toggle();
				break;

			case 'a':
				ardu1.demo();
				break;

			case 'o':
				display.testOled();
				break;

			case 'p':
//				spi = SpiFactory.getInstance(SpiChannel.CS0, 16000000, SpiDevice.DEFAULT_SPI_MODE); // default spi mode 0
//				GpioPinDigitalOutput cd = GpioFactory.getInstance().provisionDigitalOutputPin(RaspiPin.GPIO_04); // BCM 23
//				GpioPinDigitalOutput rst = GpioFactory.getInstance().provisionDigitalOutputPin(RaspiPin.GPIO_05); // BCM 24
//				// GpioPinDigitalOutput rst = null;
//				ST7735 tft = new ST7735(spi, cd, rst);
				
				spi = SpiFactory.getInstance(SpiChannel.CS0, 16000000, SpiMode.MODE_3); 
				GpioPinDigitalOutput cd = GpioFactory.getInstance().provisionDigitalOutputPin(RaspiPin.GPIO_06); // BCM 25
				GpioPinDigitalOutput rst = GpioFactory.getInstance().provisionDigitalOutputPin(RaspiPin.GPIO_02); // BCM 27
				// GpioPinDigitalOutput rst = null;
				ST7789 tft = new ST7789(spi, cd, rst);
				
				tft.test3();
				break;

			// normal movement
			case 'm':
				motorGroups.getAll().go(speed0);
				break;

//			case 'i':
//				hardTurn(speed0);
//				break;
//
//			case 'j':
//				softTurn(speed0);
//				break;

			// rotate
			case 'r':
				motorGroups.getLeft().go(-speed0);
				motorGroups.getRight().go(speed0);
				break;

			// lateral movement
			case 'l':
				motorGroups.getD03().go(-speed0);
				motorGroups.getD12().go(speed0);
				break;

			// diagonal
			case 'd':
				motorGroups.getD03().go(speed0);
				motorGroups.getD12().go(0);
				break;

			// case 'a':
			// turnByAngle(speed, 90.0);
			// break;
			//
			// case 'n':
			// goNorth(speed);
			// break;

			// experimental "slide"
			case 'x':
				slide(Rover.speedLimit);
				break;

			// experimental "slide"
			case 'y':
				slide(-1 * Rover.speedLimit);
				break;

//			// compass
//			case 'c':
//				calibrateCompass();
//				break;

			case 'b':
				double b = compass.readHeading();
				String text = String.format("DIR %3.1f    ", b);
				System.out.println(text);
				display.displayText(text);
				break;

//			// distance
//			case 'e':
//				for (int i = 0; i < 20; ++i) {
//					float x = ir1.measureDistance();
//					EventBroker.updateDistance((int) x);
//					System.out.println(String.format("ir: %3.1f", x));
//					Thread.sleep(100);
//				}
//				break;

//			// turret / servo test
//			case 'v':
//				stur.demo();
//				break;

//			// ultrasonic distance test
//			case 'u':
//				for (int i = 0; i < 20; ++i) {
//					float x1 = sonar1.measureDistance(1);
//					float x2 = sonar1.measureDistance(2);
//					float x4 = sonar1.measureDistance(4);
//					EventBroker.updateDistance((int) x1);
//					System.out.println(String.format("sonar: %4.1f - %4.1f - %4.1f", x1, x2, x4));
//					Thread.sleep(100);
//				}
//				break;

//			case 'g':
//				System.out.println("dance");
//				dance();
//				break;

			case 's':
				System.out.println("stop");
				motcontrol.stopAllMotors();
				break;

			case 't':
				System.out.println("stop hard");
				motorGroups.getAll().run(AdafruitMotor.BRAKE);
				break;

			case 'q':
				System.out.println("quit");
				repeat = false;
				break;

			}
		}
	}

	public void slide(double speed0) {
		try {
			motcontrol.getMotor(0).go(0.7 * speed0);
			motcontrol.getMotor(1).go(-0.7 * speed0);
			motcontrol.getMotor(2).go(-2.0 * speed0);
			motcontrol.getMotor(3).go(2.0 * speed0);
		} catch (MotorControlException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

//	public void slide1(double speed0) {
//		try {
//			motcontrol.getMotor(0).go(2.0 * speed0);
//			motcontrol.getMotor(1).go(-2.0 * speed0);
//			motcontrol.getMotor(2).go(-0.3 * speed0);
//			motcontrol.getMotor(3).go(0.3 * speed0);
//		} catch (MotorControlException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//
//	public void turnTang(double left, double right) throws Exception {
//		motorGroups.getLeft().go(left);
//		motorGroups.getRight().go(right);
//	}


//	private void dance() throws MotorControlException, InterruptedException, IOException {
//		double speed = 0.7;
//		int sleepfactor = 20;
//
//		direction = RoverDirection.FORWARD;
//
//		// motorGroups.getAll().go(-speed);
//		// Thread.sleep(130 * sleepfactor/2);
//		// motorGroups.getD14().go(speed);
//		// motorGroups.getD23().go(-speed);
//		// Thread.sleep(100 * sleepfactor/2);
//		//
//		// testIt(speed);
//
//		// rectangle
//		motorGroups.getAll().go(speed);
//		Thread.sleep(100 * sleepfactor);
//		motorGroups.getD14().go(-speed);
//		motorGroups.getD23().go(speed);
//		Thread.sleep(130 * sleepfactor);
//		motorGroups.getAll().go(-speed);
//		Thread.sleep(130 * sleepfactor);
//		motorGroups.getD14().go(speed);
//		motorGroups.getD23().go(-speed);
//		Thread.sleep(100 * sleepfactor);
//
//		// testIt(speed);
//
//		// diagonal
//		motorGroups.getD14().run(AdafruitMotor.BRAKE);
//		motorGroups.getD23().go(speed);
//		Thread.sleep(150 * sleepfactor);
//		motorGroups.getD14().go(speed);
//		motorGroups.getD23().go(-speed);
//		Thread.sleep(130 * sleepfactor);
//		motorGroups.getD14().go(-speed);
//		motorGroups.getD23().run(AdafruitMotor.BRAKE);
//		Thread.sleep(150 * sleepfactor);
//		motorGroups.getD14().go(speed);
//		motorGroups.getD23().go(-speed);
//		Thread.sleep(130 * sleepfactor);
//
//		// testIt(speed);
//
//		motorGroups.getAll().go(speed);
//		Thread.sleep(130 * sleepfactor / 2);
//
//		// testIt(speed);
//
//		// circle
//		softTurn(-speed);
//		Thread.sleep(500 * sleepfactor);
//
//		// testIt(speed);
//
//		// goNorth(speed);
//		// turnByAngle(speed, 180);
//		// turnByAngle(speed, 180);
//
//		motcontrol.stopAllMotors();
//	}
//
//	private void testIt(double speed) throws MotorControlException, IOException {
//
//		motcontrol.stopAllMotors();
//		// goNorth(speed);
//		System.in.read();
//	}

	// private void arduinoTest() throws Exception {
	// I2CBus iic1 = I2CFactory.getInstance(I2CBus.BUS_1);
	// I2CDevice dev = iic1.getDevice(0x0B);
	//
	// for (byte b=1; b<25; ++b) {
	// dev.write(b);
	// int c = dev.read();
	// System.out.println(String.format("write %d - read %d", b, c));
	// Thread.sleep(500);
	// }
	// }

//	private void calibrateCompass() throws InterruptedException, MotorControlException {
//		motcontrol.stopAllMotors();
//
//		System.out.println("start calibration");
//		HMC5883LCalibration calib = new HMC5883LCalibration(compass);
//		Thread t = new Thread(calib);
//		t.start();
//
//		hardTurn(0.6);
//
//		int delay = 6000;
//		System.out.println(String.format("turning for %dms", delay));
//		Thread.sleep(delay);
//
//		calib.setDone(true);
//		motcontrol.stopAllMotors();
//		t.join();
//		System.out.println("end calibration");
//
//		compass.saveCalibration();
//	}

//	private void hardTurn(double speed) throws MotorControlException {
//		motorGroups.getLeft().go(-speed);
//		motorGroups.getRight().go(speed);
//	}
//
//	private void softTurn(double speed) throws MotorControlException {
//
//		double halfspeed = speed;
//		if (speed != 0) {
//			halfspeed = speed / 3;
//		}
//
//		if (speed >= 0 && RoverDirection.FORWARD.equals(direction)) {
//			motorGroups.getLeft().go(halfspeed);
//			motorGroups.getRight().go(speed);
//		} else if (speed < 0 && RoverDirection.FORWARD.equals(direction)) {
//			motorGroups.getLeft().go(-speed);
//			motorGroups.getRight().go(-halfspeed);
//		} else if (speed >= 0 && RoverDirection.BACKWARD.equals(direction)) {
//			motorGroups.getLeft().go(-speed);
//			motorGroups.getRight().go(-halfspeed);
//		} else if (speed < 0 && RoverDirection.BACKWARD.equals(direction)) {
//			motorGroups.getLeft().go(halfspeed);
//			motorGroups.getRight().go(speed);
//		}
//	}

	
	private static void printCommands() {
		System.out.println("---------------------------");
		System.out.println("Hi - I am the Mecanum-Rover!");
		System.out.println("---------------------------");
		System.out.println("Try the following comands:");
		System.out.println("    1   -> MC1 demo");
		System.out.println("    a   -> Arduino demo");
		System.out.println("    m/M -> drive forward/backward");
		System.out.println("    i/I -> hard turn left/right");
		System.out.println("    j/J -> soft left/right");
		// System.out.println(" a/A -> turn by angle left/right");
		// System.out.println(" n/N -> go noth");
		System.out.println("    r/R -> rotate left/right");
		System.out.println("    l/L -> lateral left/right");
		System.out.println("    x/X -> slide right/left");
		System.out.println("    y/Y -> slide and brake");
		System.out.println("    c   -> calibrate compass");
		System.out.println("    e   -> read IR distance sensor");
		System.out.println("    u   -> read SONAR distance sensor");
		System.out.println("    f/F -> run, distance and brake");
		System.out.println("    v -  > sensor turret demo");
		System.out.println("    s   -> stop");
		System.out.println("    t   -> brake");
		System.out.println("    q   -> quit");
		System.out.println("---------------------------");
	}

//	@Override
//	public void connectionLost(Throwable arg0) {
//		// TODO Auto-generated method stub
//
//	}
//
//	@Override
//	public void deliveryComplete(IMqttDeliveryToken arg0) {
//		// TODO Auto-generated method stub
//
//	}

//	@Override
	public void messageArrived(String topic, String/*MqttMessage*/ msg) throws Exception {
		String msgs = msg.toString();
		System.out.println("MRover message arrived: " + topic + ":" + msg);

		switch (msgs) {

		case "exit":
			MRover.exit = true;
			return;

//		case "calibratecompass":
//			calibrateCompass();
//			return;

		case "scan":
			ardu1.scanFront();
			return;

//		case "dance":
////			mc1.schlangenlinie();
//			mc1.dance();
//			return;

//		case "dance-old":
////			dance();
//			return;

		case "slide":
			slide(Rover.speedLimit);
			return;

		case "slide-back":
			slide(-Rover.speedLimit);
			return;

//		case "slide1":
//			slide1(-speed);
//			return;

//		case "follow":
//			mc1.follow();
//			return;

		case "power":
			powerm.readPower();
			return;

		}
	}

}
