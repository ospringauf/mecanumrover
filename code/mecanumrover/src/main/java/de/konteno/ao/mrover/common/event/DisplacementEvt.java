package de.konteno.ao.mrover.common.event;

import de.konteno.ao.mrover.common.RoverTopic;
import de.konteno.ao.mrover.common.unit.Angle;
import de.konteno.ao.mrover.common.unit.Pos;

@RoverTopic(address = "evt.odo2")
public class DisplacementEvt implements IRoverEvent {
	public static final DisplacementEvt ZERO = new DisplacementEvt(0, 0, Angle.ZERO);
	
	public Pos delta;
	public Angle da;

	public DisplacementEvt(double dx, double dy, Angle da) {
		this.delta = Pos.of(dx, dy);
		this.da = da;
	}

	@Override
	public String toString() {
		return String.format("%s a=%3.2f", delta.toString(), da.toDegree());
	}
}
