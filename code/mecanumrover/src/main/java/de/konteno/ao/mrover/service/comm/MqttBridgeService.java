/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.service.comm;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.konteno.ao.mrover.common.Rover;
import de.konteno.ao.mrover.common.RoverUtil;
import de.konteno.ao.mrover.common.cmd.CalibrateCompassCmd;
import de.konteno.ao.mrover.common.cmd.CalibrateOdometerCmd;
import de.konteno.ao.mrover.common.cmd.ConfigPowerDistributionCmd;
import de.konteno.ao.mrover.common.cmd.DemoCmd;
import de.konteno.ao.mrover.common.cmd.DriveCmd;
import de.konteno.ao.mrover.common.cmd.ResetCmd;
import de.konteno.ao.mrover.common.cmd.RotateCmd;
import de.konteno.ao.mrover.common.cmd.SendSerialMessageCmd;
import de.konteno.ao.mrover.common.cmd.TurnCmd;
import de.konteno.ao.mrover.common.event.CompassEvt;
import de.konteno.ao.mrover.common.event.LidarScanEvt;
import de.konteno.ao.mrover.common.event.ShutdownEvt;
import de.konteno.ao.mrover.common.event.SonarDistanceEvt;
import de.konteno.ao.mrover.common.track.Pose;
import de.konteno.ao.mrover.common.track.TrackType;
import de.konteno.ao.mrover.common.unit.Angle;
import de.konteno.ao.mrover.service.MyVerticleBase;
import io.netty.handler.codec.mqtt.MqttQoS;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.mqtt.MqttClient;
import io.vertx.mqtt.MqttClientOptions;
import io.vertx.mqtt.messages.MqttConnAckMessage;
import io.vertx.mqtt.messages.MqttPublishMessage;

/**
 * subscribe on MQTT and Vert.x eventbus and pass messages between the two
 * 
 * periodically send speed/heading/track telemetry over MQTT
 */
public class MqttBridgeService extends MyVerticleBase {

	private static final Logger logger = LoggerFactory.getLogger(MqttBridgeService.class);

	// commands/configuration sent from the "MQTT Dash" app
	private static final String TopicRemoteCmd = "rover/cmd"; // +"/#";
	private static final String TopicRemoteSetting = "rover/set"; // + "/#";

	// commands to be relayed from MQTT to ESP32 (via serial), for test only
	private static final String TopicEspCmd = "esp/cmd"; // + "/#";

	public static final String TopicTelemetryOdo = "rover/tm/odo";
	public static final String TopicTelemetryDr = "rover/tm/dr";
	public static final String TopicTelemetrySonar = "rover/tm/sonar";
	public static final String TopicTelemetryLidar = "rover/tm/lidar";
	public static final String TopicTelemetryImu = "rover/tm/imu";

	private MqttClient sendClient;
	private MqttClient recvClient;

	private String lastMaxSpeed = null;

	private long telemetryTimer;

	private Map<String, Consumer<String>> actions = new HashMap<>();

	@Override
	public void start(Promise<Void> startFuture) throws Exception {
		super.start();

		initActions();

		// separate clients for sending and receiving; might fix blocking errors?
		MqttClientOptions op1 = new MqttClientOptions();
		op1.setClientId("rover-data");
		sendClient = MqttClient.create(vertx, op1);

		MqttClientOptions op2 = new MqttClientOptions();
		op2.setClientId("rover-control");
		recvClient = MqttClient.create(vertx, op2);

		Future<MqttConnAckMessage> f1 = sendClient.connect(1883, "localhost");
		Future<MqttConnAckMessage> f2 = recvClient.connect(1883, "localhost");

		CompositeFuture.all(f1, f2).onComplete(ar -> {
			clientConnected();
		});

		if (Rover.telemetry) {
			// listen to waypoints and publish max 3 per second and type to MQTT

			busListenStream(Pose.class).filter(x -> x.type == TrackType.ODO)
					.sample(333, TimeUnit.MILLISECONDS)
					.subscribe(x -> publishMqtt(TopicTelemetryOdo, x.mqttString()));

			busListenStream(Pose.class).filter(x -> x.type == TrackType.DEAD_RECK)
					.sample(200, TimeUnit.MILLISECONDS)
					.subscribe(x -> publishMqtt(TopicTelemetryDr, x.mqttString()));

			busListenStream(SonarDistanceEvt.class).subscribe(x -> publishMqtt(TopicTelemetrySonar, x.mqttString()));

			busListenStream(LidarScanEvt.class).subscribe(x -> publishMqtt(TopicTelemetryLidar, x.mqttString()));

			busListenStream(CompassEvt.class).subscribe(x -> publishMqtt(TopicTelemetryImu, x.mqttString()));

		}

		startFuture.complete();
	}

	private void publishMqtt(String topic, String payload) {
//		logger.debug("mqtt send: " + topic + "/" + payload);
		sendClient.publish(topic, Buffer.buffer(payload), MqttQoS.AT_LEAST_ONCE, false, false);
	}

	private void clientConnected() {

		recvClient.publishHandler(p -> {
			mqttMessageReceived(p);
//			logger.debug("message handled");
		});

		publishMqtt("rover-info", "hello from vert.x");

		recvClient.subscribe(TopicRemoteCmd + "/#", MqttQoS.EXACTLY_ONCE.value());
		recvClient.subscribe(TopicRemoteSetting + "/#", MqttQoS.EXACTLY_ONCE.value());
		recvClient.subscribe(TopicEspCmd + "/#", MqttQoS.EXACTLY_ONCE.value());
	}

	private void initActions() {
		// translate incoming MQTT messages (from telemetry client) into Rover (vert.x) events or commands
		actions.put("rover/cmd/stop", p -> sendStop("stopped by user"));
		actions.put("rover/cmd/drive", p -> busSend(new DriveCmd(Angle.fromDegree(Integer.parseInt(p)))));
		actions.put("rover/cmd/info", this::cmdInfo);
		actions.put("rover/set/maxspeed", this::setMaxSpeed);
		actions.put("rover/set/powerdist", this::setPowerDistribution);
		actions.put("rover/cmd/rotate", this::cmdRotate);
		actions.put("rover/cmd/turn", this::cmdTurn);
		actions.put("rover/cmd/reset", this::cmdReset);
		actions.put("rover/cmd/demo", p -> busSend(new DemoCmd(p)));
		actions.put("rover/cmd/calibratecompass", p -> busSend(new CalibrateCompassCmd()));
		actions.put("rover/cmd/calibrateodo", p -> busSend(new CalibrateOdometerCmd()));
		actions.put("rover/cmd/demo", p -> busSend(new DemoCmd(p)));
		actions.put("rover/cmd/shutdown", p -> busPublish(new ShutdownEvt()));

		// calibration
		// TODO remove when no longer needed
		actions.put("rover/cmd/d", this::cmdCalibrateDrive);
		actions.put("rover/cmd/rr", this::cmdCalibrateRotRad);
		actions.put("rover/cmd/rt", this::cmdCalibrateRotTang);

	}

	private void mqttMessageReceived(MqttPublishMessage p) {
		String payload = p.payload().toString();

		logger.debug("received: topic: " + p.topicName() + ", payload: " + payload);
		// System.out.println("QoS: " + p.qosLevel());

		if (p.topicName().startsWith(TopicEspCmd)) {
			logger.debug("passing message to ESP32");
			busSend(new SendSerialMessageCmd(p.topicName(), payload));
			return;
		}

		Consumer<String> action = actions.getOrDefault(p.topicName(), null);

		if (action != null) {
			action.accept(payload);
		} else {
			logger.warn("unknown command via mqtt - topic: " + p.topicName() + ", payload: " + payload);
		}
	}

	private void cmdCalibrateRotTang(String payload) {
		String[] args = payload.split(" ");
		double v = Double.parseDouble(args[0]);
		double steer = Double.parseDouble(args[1]);
		long time = Long.parseLong(args[2]);
		busSend(new RotateCmd(v).tangential().steer(steer).time(time)).compose(x -> sendStop("done"));
	}

	private void cmdCalibrateRotRad(String payload) {
		String[] args = payload.split(" ");
		double v = Double.parseDouble(args[0]);
		double steer = Double.parseDouble(args[1]);
		long time = Long.parseLong(args[2]);
		busSend(new RotateCmd(v).radial().steer(steer).time(time)).compose(x -> sendStop("done"));
	}

	private void cmdCalibrateDrive(String payload) {
		String[] args = payload.split(" ");
		double v = Double.parseDouble(args[0]);
		double angle = Double.parseDouble(args[1]);
		long time = Long.parseLong(args[2]);
		busSend(new DriveCmd(Angle.fromDegree(angle), v).time(time)).compose(x -> sendStop("done"));
	}

	private void cmdReset(String payload) {
		Rover.clearTracks();
		busSend(new ResetCmd());
	}

	private void cmdTurn(String payload) {
		// payload is "+60", "-90" (for relative turn) or "60", "270" (for absolute turn)
		Angle angle = "north".equals(payload) ? Angle.ZERO : Angle.fromDegree(Integer.parseInt(payload));
		boolean relative = payload.startsWith("+") || payload.startsWith("-");
		TurnCmd cmd = new TurnCmd(angle, relative);
		busSend(cmd);
	}

	private void cmdRotate(String payload) {
		// payload is "left" or "right"
		boolean clockwise = "RIGHT".equalsIgnoreCase(payload);
		busSend(new RotateCmd(Rover.speedLimit).steer(clockwise ? 100 : -100));
	}

	private void setMaxSpeed(String payload) {
		if (payload.equals(lastMaxSpeed)) {
			// this is the output of "sendInfo", ignore
		} else {
			Rover.speedLimit = Double.parseDouble(payload) / 100.0;
			logger.debug("set max speed to {}", Rover.speedLimit);
			sendInfo();
		}
	}
	
	private void setPowerDistribution(String payload) {
		String[] a = payload.replace(',', '.').split(" ");
		double lr = Double.parseDouble(a[0]);
		double fb = Double.parseDouble(a[1]);
		logger.debug("set power distribution to {}/{}", lr, fb);
		busSend(new ConfigPowerDistributionCmd(lr, fb));
	}

	private void cmdInfo(String payload) {
		sendInfo();
		if (!RoverUtil.isNullOrEmpty(payload)) {
			int interval = Integer.parseInt(payload);
			startTelemetry(interval);
		}
	}

	private void startTelemetry(int interval) {
		if (telemetryTimer != 0) {
			vertx.cancelTimer(telemetryTimer);
		}
		if (interval != 0) {
			telemetryTimer = vertx.setTimer(interval, h -> {});
		}
	}

	private void sendInfo() {
		lastMaxSpeed = String.format("%3.0f", 100 * Rover.speedLimit);
		publishMqtt("rover/info/heading", String.format("%3.0f", Rover.pose.heading.toDegree()));
		publishMqtt("rover/set/maxspeed", lastMaxSpeed);
	}

	@Override
	public void onShutdown() {
		publishMqtt("rover-info", "shutting down");

		sendClient.disconnect();
		recvClient.disconnect();
	}

}
