/*********************************************************************
* Copyright (c) 2019 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package de.konteno.ao.mrover.service.compass;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pi4j.io.serial.Baud;
import com.pi4j.io.serial.SerialPort;

import de.konteno.ao.mrover.common.Rover;
import de.konteno.ao.mrover.common.event.CompassEvt;
import de.konteno.ao.mrover.common.unit.Angle;
import de.konteno.ao.mrover.driver.imu.MyImuDriver;
import de.konteno.ao.mrover.service.MyVerticleBase;
import io.reactivex.Observable;
import io.vertx.core.Promise;

/**
 * Process heading measurements from IMU
 * (MUC9255 + STM32 + RTIMULIB)
 * 
 * Communicates directly with myIMU via onboard serial port (/dev/ttyS0)
 * 
 * - update current Rover heading
 *
 */
public class MyImuService extends MyVerticleBase {

	private static final Logger LOGGER = LoggerFactory.getLogger(MyImuService.class);
	
	private MyImuDriver imu;
	
	// first reading; normalize initial heading to 0 degrees
	private Angle initial = null;
	
	@Override
	public void start(Promise<Void> startp) throws Exception {
		super.start();

		// build the odometer driver
		imu = new MyImuDriver(SerialPort.getDefaultPort(), Baud._115200);
		
		// listen to data stream from the driver, calculate and publish displacement
		Observable.fromPublisher(imu).subscribe(this::headingUpdate);
		
		startp.complete();
	}
	
	/**
	 * update rover heading and publish a compass event in case 
	 * some other service is listening
	 * @param h current heading in degrees from IMU
	 */
	void headingUpdate(double h) {
		Angle heading = Angle.fromDegree(h);
		if (initial == null) {
			initial = heading;
		}
		heading = heading.minus(initial);
		
		Rover.pose.heading = heading;
		LOGGER.trace("heading = {}", heading);
		busPublish(new CompassEvt(heading));
	}

	
	@Override
	public void onShutdown() {
		imu.shutdown();
	}

}
