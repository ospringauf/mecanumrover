package de.konteno.ao.mrover.dryrun;

import org.reactivestreams.Subscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.konteno.ao.mrover.common.unit.Angle;
import de.konteno.ao.mrover.common.unit.Pos;
import de.konteno.ao.mrover.driver.odo.IOdo2Driver;
import de.konteno.ao.mrover.driver.odo.OdoSensor;
import io.vertx.reactivex.core.Vertx;

/**
 * simulated odometer data
 */
public class MockOdometer implements IOdo2Driver {
	
	private static final Logger logger = LoggerFactory.getLogger(MockOdometer.class);

	long deltat = 50; // sampling frequency (ms)
	double b = 15.0; // distance between sensors
	
	public OdoSensor left = new OdoSensor();
	public OdoSensor right = new OdoSensor();

	Subscriber<? super OdoSensor[]> subscriber;
	private long start;

	@Override
	public void powerUp() {
		// TODO Auto-generated method stub

	}

	@Override
	public void powerDown() {
		// TODO Auto-generated method stub

	}

	@Override
	public void shutdown() {
		// TODO Auto-generated method stub

	}

	private void setup() {
		start = System.currentTimeMillis();
	}

	@Override
	public void subscribe(Subscriber<? super OdoSensor[]> sub) {
		this.subscriber = sub;

		setup();
		Vertx.vertx().setPeriodic(deltat, t -> rightAndLeft());
	}
	
	
	

//	private void circleTangential() {
//		long now = System.currentTimeMillis();
//		Angle a0 = Angle.fromDegree(360.0 * (now-start-deltat)/30000.0);
//		Angle a = Angle.fromDegree(360.0 * (now-start)/30000.0);
//		
//		Pos pr0 = a0.toPos(100 - b/2);
//		Pos pl0 = a0.toPos(100 + b/2);
//		Pos pr = a.toPos(100 - b/2);
//		Pos pl = a.toPos(100 + b/2);
//
//		left.delta = left.inverseTransform(Pos.of(pl.x - pl0.x, pl.y - pl0.y));
//		left.squal = 40;
//
//		right.delta = right.inverseTransform(Pos.of(pr.x - pr0.x, pr.y - pr0.y));
//		right.squal = 40;
//
//		subscriber.onNext(new OdoSensor[] { left, right });
//	}
	
	private void circleTangential2() {
		left.delta = left.inverseTransform(Pos.of(-0.01, 1.13));
		left.squal = 40;

		right.delta = right.inverseTransform(Pos.of(-0.01, 0.97));
		right.squal = 40;

		subscriber.onNext(new OdoSensor[] { left, right });
	}

	private void circleRadial() {
		long now = System.currentTimeMillis();
		Angle a0 = Angle.fromDegree(360.0 * (now-start-deltat)/30000.0);
		Angle a = Angle.fromDegree(360.0 * (now-start)/30000.0);
		
		Pos pr0 = Pos.of(100, -b/2).rotate(a0);
		Pos pl0 = Pos.of(100,  b/2).rotate(a0);
		Pos pr = Pos.of(100, -b/2).rotate(a);
		Pos pl = Pos.of(100,  b/2).rotate(a);

		left.delta = left.inverseTransform(Pos.of(pl.x - pl0.x, pl.y - pl0.y));
		left.squal = 40;

		right.delta = right.inverseTransform(Pos.of(pr.x - pr0.x, pr.y - pr0.y));
		right.squal = 40;

		subscriber.onNext(new OdoSensor[] { left, right });
	}
	
	private void circleRadial2() {
		left.delta = left.inverseTransform(Pos.of(-1, -0.05));
		left.squal = 40;

		right.delta = right.inverseTransform(Pos.of(-1, 0.05));
		right.squal = 40;

		subscriber.onNext(new OdoSensor[] { left, right });
	}
	
	private void rightAndLeft() {
		long now = System.currentTimeMillis();
		long dir = ((now - start) / 5000) % 2;
		dir = dir == 0 ? 1 : -1;

		left.delta = left.inverseTransform(Pos.of(dir * 0.3, 0));
		left.squal = 40;

		right.delta = right.inverseTransform(Pos.of(dir * 0.3, 0));
		right.squal = 30;

		subscriber.onNext(new OdoSensor[] { left, right });
	}

	@Override
	public void saveConfiguration() {
		// TODO Auto-generated method stub
		
	}

}
