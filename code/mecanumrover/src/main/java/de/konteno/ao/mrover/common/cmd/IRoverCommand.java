/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package de.konteno.ao.mrover.common.cmd;

import de.konteno.ao.mrover.common.IRoverMessage;

/**
 * marker interface for commands
 * 
 * commands are "sent" over the event bus, meaning each command is processed only once
 * and can return a result
 * 
 * @author SPRINO
 *
 */
public interface IRoverCommand extends IRoverMessage {

}
