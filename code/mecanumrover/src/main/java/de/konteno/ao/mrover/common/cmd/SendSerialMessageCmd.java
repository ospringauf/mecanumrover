/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.common.cmd;

import de.konteno.ao.mrover.common.RoverTopic;
import de.konteno.ao.mrover.common.SerialMessage;

/**
 * send a message (topic, payload) to ESP32
 * EspCommService acts as a relay between event bus and serial interface
 *
 */
@RoverTopic(address="cmd.sendsermsg")
public class SendSerialMessageCmd extends SerialMessage implements IRoverCommand {

	public SendSerialMessageCmd(String topic, String payload) {
		super(topic, payload);
	}

}
