/*********************************************************************
* Copyright (c) 2018-2019 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.driver.motor;

import java.io.IOException;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pi4j.io.i2c.I2CDevice;

/**
 * Adafruit DC & Stepper Motor HAT
 * based on Adafruit example code  
 * https://www.adafruit.com/product/2348
 *
 * controls 4 DC motors or 2 stepper motors via I²C
 * consists of one PCA9685 16-channel I2C PWM controller
 * and two TB6612FNG dual motor drivers
 * 
 * The board still has some unused PWM lines that could be used to control servos.
 * 
 * This class builds/configures the PWM controller and motor wrappers 
 */
public class AdafruitMotorControl {

	private static final Logger logger = LoggerFactory.getLogger( AdafruitMotorControl.class );

	public static final int DEFAULT_I2C_ADR = 0x60;
	public static final int BROADCAST_I2C_ADR = 0x70;
	
	public static int[] motorMapping = { 0, 1, 2, 3 };
	
	// default 1600Hz PWM freq
	public static final int FREQ_DEFAULT = 1600;
	
	private AdafruitMotor[] motors;
	private Pca9685Pwm pwm;
		
	/**
	 * Build PWM controller and 4 DC motors 
	 * @param device I2C device of PWM controller
	 * @param freq PWM frequency
	 * @throws MotorControlException 
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public AdafruitMotorControl(I2CDevice device, int freq) throws MotorControlException  {
        pwm =  new Pca9685Pwm(device);
        pwm.setPWMFreq(freq);

		// see the Adafruit schematic for pin connections between PWM outputs and motor driver inputs
		motors = new AdafruitMotor[] {
				new AdafruitMotor(pwm,  7,  6,  5),			
				new AdafruitMotor(pwm,  2,  3,  4),
				new AdafruitMotor(pwm, 13, 12, 11),
				new AdafruitMotor(pwm,  8,  9, 10),
		};
		
		remapMotors(motorMapping);
		
//		servo0 = new Servo(pwm, 0);

        //self.steppers = [ Adafruit_StepperMotor(self, 1), Adafruit_StepperMotor(self, 2) ]

	}
	
	private void remapMotors(int[] remap) {
		AdafruitMotor[] tmp = Arrays.copyOf(motors, 4);
		motors[0] = tmp[remap[0]];
		motors[1] = tmp[remap[1]];
		motors[2] = tmp[remap[2]];
		motors[3] = tmp[remap[3]];
	}
	
	public AdafruitMotorControl(I2CDevice dev) throws MotorControlException {
		this(dev, FREQ_DEFAULT);
	}

	public AdafruitMotor getMotor(int n) {
		return motors[n];		
	}
	
	public void stopAllMotors() throws MotorControlException {
		for (AdafruitMotor m : motors) {
			m.run(AdafruitMotor.RELEASE);			
		}		
	}

	public void shutdown() {
		try {
			stopAllMotors();
		} catch (MotorControlException e) {
			logger.error("motor controller error", e);
		}
	}
	
}
