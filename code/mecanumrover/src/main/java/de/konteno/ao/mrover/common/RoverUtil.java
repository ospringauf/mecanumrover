/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.common;

public class RoverUtil {


	public static int crc8(byte[] data) {
		assert data != null;

		int tmp;
		int res = 0;
		for (int i = 0; i < data.length; i++) {
			tmp = res << 1;
			tmp += 0xff & data[i];
			res = ((tmp & 0xff) + (tmp >> 8)) & 0xff;
		}
		return res;
	}
	
	public static boolean isNullOrEmpty(String s) {
		return (s == null) || s.isEmpty();
	}

	public static String last(String msg, int i) {
		return (msg != null && msg.length() > i) ? "..." + msg.substring(msg.length()-i-1) : msg;
	}
}
