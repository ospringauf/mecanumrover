#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <jni.h>
#include <wiringPi.h>
#include "de_konteno_ao_mrover_extern_Sonar.h"

// compile with
// gcc -shared -fpic -I/usr/lib/jvm/jdk-8-oracle-arm32-vfp-hflt/include/ -I/usr/lib/jvm/jdk-8-oracle-arm32-vfp-hflt/include/linux -lwiringPi -o libmrover.so de_konteno_ao_mrover_extern_Sonar.c

#define SONAR_TIMEOUT_MICROS (100*1000)

/*
 * Class:     de_konteno_ao_mrover_extern_Sonar
 * Method:    setup
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_de_konteno_ao_mrover_extern_Sonar_setup
  (JNIEnv *env, jobject jo) 
{
        wiringPiSetup();
/*
        pinMode(TRIG, OUTPUT);
        pinMode(ECHO, INPUT);
 
        // TRIG pin must start LOW
        digitalWrite(TRIG, LOW);
        delay(30);
*/
}

/*
 * Class:     de_konteno_ao_mrover_extern_Sonar
 * Method:    triggerEchoAvg
 * Signature: (III)I
 */
JNIEXPORT jint JNICALL Java_de_konteno_ao_mrover_extern_Sonar_triggerEchoAvg
  (JNIEnv *env, jobject jo, jint trigger_pin, jint echo_pin, jint n)
{
	int i;
	int sum = 0;
	int sample = 0;
	int succ = 0;
	for (i=0; i<n; i=i+1) {
		sample = Java_de_konteno_ao_mrover_extern_Sonar_triggerEcho(env, jo, trigger_pin, echo_pin);
		if (sample >= 0) {
			succ = succ+1;
			sum = sum+sample;
		}
		delay(50); // recommended cycle time; wait for echo to fade away
	}
	if (succ > 0) {
		return sum/succ;
	} else {
		return -1;
	}
}



/*
 * Class:     de_konteno_ao_mrover_extern_Sonar
 * Method:    triggerEcho
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_de_konteno_ao_mrover_extern_Sonar_triggerEcho
  (JNIEnv *env, jobject jo, jint trigger_pin, jint echo_pin)
{
	// printf("in Java_de_konteno_ao_mrover_extern_Sonar_triggerEcho %d %d\n", trigger_pin, echo_pin);

        // Send trig pulse
        digitalWrite(trigger_pin, HIGH);
        delayMicroseconds(10);
        digitalWrite(trigger_pin, LOW);
 
	int c = 0;
	long timeout = micros() + SONAR_TIMEOUT_MICROS;
	// printf("trig sent, wait for echo start\n");
        while(digitalRead(echo_pin) == LOW) {
		c = c+1;
		if (c%1000==0 && micros()>timeout) {
			//printf("echo start timeout\n");
			return -1;
		}
	}
 
	// printf("wait for echo end\n");
        long startTime = micros();
	timeout = startTime + SONAR_TIMEOUT_MICROS;
	c = 0;
        while(digitalRead(echo_pin) == HIGH) {
		c = c+1;
		if (c%1000==0 && micros()>timeout) {
			//printf("echo end timeout\n");
			return -1;
		}
	}

        long travelTime = micros() - startTime;
 
        // Get distance in cm
        // int distance = travelTime / 58;
 
        return travelTime;
}
 

  

