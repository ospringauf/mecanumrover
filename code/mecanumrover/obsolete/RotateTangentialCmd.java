package de.konteno.ao.mrover.common.cmd;

import de.konteno.ao.mrover.common.RoverDirection;
import de.konteno.ao.mrover.common.RoverTopic;

@RoverTopic(address = "cmd.rotate_tangential")
public class RotateTangentialCmd extends RotateCmd  implements IRoverCommand {

	private double steering;

	public RotateTangentialCmd(double speed, double steering) {
		super(speed);
		this.steering = steering;
	}
	
	public double getSteering() {
		return steering;
	}

	public void setSteering(double steering) {
		this.steering = steering;
	}
	
	@Override
	public String toString() {
		return String.format("ROTATE_TANGENTIAL(%2.3f, %2.3f)!", getSpeed(), steering);
	}

	// TODO this is ugly
	@Override
	public RotateCmd setSpeed(RoverDirection dir, double absSpeed) {
		if (dir == RoverDirection.LEFT && steering > 0) {
			setSpeed(-1 * absSpeed);			
		}
		if (dir == RoverDirection.RIGHT && steering < 0) {
			setSpeed(-1 * absSpeed);			
		}
		setSpeed(absSpeed);
		//this.speed = (dir == RoverDirection.RIGHT) ? absSpeed : -1 * absSpeed;
		return this;
	}

}
