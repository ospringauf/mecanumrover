/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.service.motor;

public interface MotionMethod {
	/**
	 * template for motion lamdba
	 * @param speed
	 * @param direction +1=cw/right, -1=ccw/left
	 * @throws Exception
	 */
	public void move(double speed, int direction) throws Exception;
}
