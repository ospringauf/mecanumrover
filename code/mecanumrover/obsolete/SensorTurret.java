/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.trash;


import de.konteno.ao.mrover.driver.motor.MotorControlException;
import de.konteno.ao.mrover.service.comm.EventBroker;

/**
 * 
 * TODO IR+Sonar kombinieren und an Broker schicken
 * 
 * @author oliver
 *
 */
public class SensorTurret extends RoverComponent {

	private Servo servo;
	private DistanceSensor infrared;
	private DistanceSensor sonar;

	public SensorTurret(Servo s, DistanceSensor ir, DistanceSensor sonar) {
		setServo(s);
		s.autoOff();
		this.infrared = ir;
		this.sonar = sonar;
		
		shutdownComponents.add(servo);
	}

	public Servo getServo() {
		return servo;
	}

	public void setServo(Servo servo) {
		this.servo = servo;
	}
	
	public void neutralPosition() throws Exception {
		servo.center();
		Thread.sleep(300);
		servo.off();		
	}
	
	
	public void demo() throws Exception {
		System.out.println("*** TURRET/SERVO DEMO (q to quit) ***");

		boolean repeat = true;
		while (repeat) {

			char input = (char) System.in.read();
			input = Character.toLowerCase(input);

			float x;
			switch (input) {
			
			case 'q':
				return;

			case 'n':
				servo.setPwm(servo.min);
				break;
			
			case 'x':
				servo.setPwm(servo.max);
				break;

			case 'c':
				neutralPosition();
				break;

			case '+':
				servo.setPwm(servo.getPwm() + 10);
				break;

			case '-':
				servo.setPwm(servo.getPwm() - 10);
				break;

			case 'm':
				float d1 = infrared.measureDistance();
				float d2 = sonar.measureDistance();
				System.out.println(String.format("infrared = %3.1f", d1));
				System.out.println(String.format("sonar    = %3.1f", d2));				
				break;

			case 's':
				servo.off();
				break;
			
			case 'd':
				EventBroker.updateDistance(new int[] {20, 16, 30, 43, 50, 80, 102, 130});
				break;
				
			case 'i':
				x = infrared.measureDistance();
				EventBroker.updateDistance((int)x);
				System.out.println(String.format("adc: %3.1f", x));
				break;

			case 'u':
				x = sonar.measureDistance();
				EventBroker.updateDistance((int)x);
				System.out.println(String.format("adc: %3.1f", x));
				break;

			case '1':
				System.out.println("UP");
				servo.setPwm(servo.min, 400);
				for (int pulse = servo.min; pulse <= servo.max; pulse = pulse+5) {
					System.out.print(" " + pulse);
					servo.setPwm(pulse, 5);			
				}

				Thread.sleep(500);
				System.out.println("DOWN");
				for (int pulse = servo.max; pulse >= servo.min; pulse = pulse-5) {
					servo.setPwm(pulse, 5);
				}
				Thread.sleep(500);

				System.out.println("NEUTRAL");
				neutralPosition();
				break;

			case '2':
				lookAround(8);
				break;
				
			case '3':
				lookAround2(16);
				break;
				
			
			}
			
			if ("nx+-c".indexOf(input)>=0) System.out.println("pwm = " + servo.getPwm());
		}
	}

	public void lookAround(int steps) throws MotorControlException, InterruptedException, Exception {
		int pwmStep = (servo.max - servo.min) / steps;
		long time = System.currentTimeMillis();
		int samples = 3; // 3 samples per step seems to give stable values
		int rangeDegrees = 160;
		
		// wait until servo has reached target position
		int positioningWait = (int) (1.7 * rangeDegrees * Servo.POSITIONING_TIME_PER_DEGREE / steps);
		int initialWait = (int) (1.3 * 90 * Servo.POSITIONING_TIME_PER_DEGREE);
//		System.out.println(initialWait);
//		System.out.println(positioningWait);
		
		// go to rightmost position
		servo.setPwm(servo.min, initialWait);

		int[] dist = new int[steps+1];
		int i = 0;
		
		// step to leftmost position
		for (int pulse = servo.min; pulse <= servo.max; pulse += pwmStep) {
			servo.setPwm(pulse, positioningWait); // wait for servo to reach position			
			
			// take average of n samples 
			float dinfra = infrared.measureDistance(samples);			
			float dsonar = sonar.measureDistance(samples);
//			int successfulSonar = 0;
//			for (int s = 0; s < samples; ++s) {				
//				float ds0 = sonar.measureDistance(); // handle invalid measurement (exception / -1)
//				if (ds0 >= 0) {
//					dsonar += ds0;
//					successfulSonar++;
//				}
//				dinfra += infrared.measureDistance();
//			}
//			dinfra /= samples;
//			dsonar = successfulSonar > 0 ? dsonar/successfulSonar : 100;
			
			System.out.print(String.format("[%3.0f|%3.0f]  ", dinfra, dsonar));
			dist[i++] = (int) dsonar;
		}
		System.out.println(String.format(" - ready (%dms)", System.currentTimeMillis() - time));
		EventBroker.updateDistance(dist, 10, 160);

		// home
		neutralPosition();
	}
	
	// IR only
	public void lookAround2(int steps) throws MotorControlException, InterruptedException, Exception {
		infrared.fastMode();
		int pwmStep = (servo.max - servo.min) / steps;
		long time = System.currentTimeMillis();
		int samples = 8; // 3 samples per step seems to give stable values
		int rangeDegrees = 160;
		
		// wait until servo has reached target position
		int positioningWait = (int) (1.7 * rangeDegrees * Servo.POSITIONING_TIME_PER_DEGREE / steps);
		int initialWait = (int) (1.3 * 90 * Servo.POSITIONING_TIME_PER_DEGREE);
		
		// go to rightmost position
		servo.setPwm(servo.min, initialWait);

		int[] dist = new int[steps+1];
		int i = 0;
		
		// step to leftmost position
		for (int pulse = servo.min; pulse <= servo.max; pulse += pwmStep) {
			servo.setPwm(pulse, positioningWait); // wait for servo to reach position		
			
			// discard 1st reading
			//infrared.measureDistance();
			
			// take average of n samples 
			float dinfra = infrared.measureDistance(samples);			
//			float dsonar = sonar.measureDistance(samples);
			
			System.out.print(String.format("%3.0f ", dinfra));
			dist[i++] = (int) dinfra;
		}
		System.out.println(String.format(" - ready (%dms)", System.currentTimeMillis() - time));
		EventBroker.updateDistance(dist, 10, 160);

		// home
		neutralPosition();
	}
	
}
