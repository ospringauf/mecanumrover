/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.trash;

import java.util.LinkedList;

import de.konteno.ao.mrover.service.comm.InfoListener;

public class EventBroker {

	static int[] distances;
	private static LinkedList<DistanceListener> distList = new LinkedList<>();
	private static LinkedList<InfoListener> infoList = new LinkedList<>();
	
	public static void addDistanceListener(DistanceListener l) {
		distList.add(l);
	}
	
	public static void removeDistanceListener(DistanceListener l) {
		distList.remove(l);		
	}

	public static void addInfoListener(InfoListener l) {
		infoList.add(l);
	}
	
	public static void removeInfoListener(InfoListener l) {
		infoList.remove(l);		
	}

	public static void updateDistance(int[] d, int minAngle, int maxAngle) {
		distances = d;
		distList.forEach(l -> l.distanceChanged(d, minAngle, maxAngle));
	}

	public static void updateDistance(int[] d) {
		updateDistance(d, 0, 180);
	}

	public static void updateDistance(int x) {
		updateDistance(new int[] {x}, 0, 180);
	}

	public static void info(String topic, String message) {
		infoList.forEach(l -> l.info(topic, message));
	}
}
