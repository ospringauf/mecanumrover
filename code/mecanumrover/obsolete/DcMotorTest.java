/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.trash;

import java.io.IOException;

import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;
import com.pi4j.io.i2c.I2CFactory.UnsupportedBusNumberException;

import de.konteno.ao.mrover.driver.adc.Poti;
import de.konteno.ao.mrover.driver.motor.AdafruitMotor;
import de.konteno.ao.mrover.driver.motor.AdafruitMotorControl;
import de.konteno.ao.mrover.driver.motor.MotorControlException;

/**
 * Tests for motor control
 * @author oliver
 *
 */
public class DcMotorTest {
	
	private AdafruitMotor mot;

	public DcMotorTest() throws UnsupportedBusNumberException, MotorControlException, IOException {
		I2CBus iic1 = I2CFactory.getInstance(I2CBus.BUS_1);
		I2CDevice dev = iic1.getDevice(AdafruitMotorControl.DEFAULT_I2C_ADR);

		AdafruitMotorControl ctrl = new AdafruitMotorControl(dev);


		mot = ctrl.getMotor(2);
	}


	public void brakeTest() throws MotorControlException, InterruptedException {
		for (int i=0; i<5; ++i) {
			mot.setSpeed(255);
			mot.run(AdafruitMotor.FORWARD);
			Thread.sleep(500);
			mot.run(AdafruitMotor.RELEASE);
			
			Thread.sleep(1000);

			mot.setSpeed(255);
			mot.run(AdafruitMotor.FORWARD);
			Thread.sleep(500);
			mot.run(AdafruitMotor.BRAKE);
//			Thread.sleep(100);
//			mot.run(mot.RELEASE);

			Thread.sleep(1000);
		}
		
		mot.run(AdafruitMotor.RELEASE);
	}

	public void upDown() throws MotorControlException, InterruptedException {
		mot.setSpeed(150);
		mot.run(AdafruitMotor.FORWARD);
		mot.run(AdafruitMotor.RELEASE);
		
		
		System.out.println("forward");
		mot.run(AdafruitMotor.FORWARD);
		for (int i=0; i<256; ++i) {
			mot.setSpeed(i);
			Thread.sleep(10);
		}
		
		System.out.println("slow down");
		for (int i=255; i>=0; --i) {
			mot.setSpeed(i);
			Thread.sleep(10);
		}
		
		mot.run(AdafruitMotor.RELEASE);
	}


	public void control(Poti poti) throws MotorControlException, IOException, InterruptedException {
		while (true) {
			double val = poti.getValue();
			System.out.println(String.format("poti=%3.1f %%", val * 100));

			mot.run(val > 0 ? AdafruitMotor.FORWARD : AdafruitMotor.BACKWARD);			
			mot.setSpeed((int) Math.abs(val * 255));
			
			Thread.sleep(100);
		}
		
	}
	
	

}
