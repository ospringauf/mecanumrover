package others;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.Timer;

import de.konteno.ao.mrover.common.Rover;
import de.konteno.ao.mrover.common.track.Waypoint;

@SuppressWarnings("serial")
public class PathPanel extends Canvas implements ActionListener {

	private boolean init = true;
	private final Timer timer = new Timer(500, this);
	private int lastTrack = 0;

	@Override
	public void actionPerformed(ActionEvent e) {
		if (Rover.track0.size() != lastTrack)
		{
			this.repaint();
			lastTrack = Rover.track0.size();
		}
	}

	public static void open() {
		PathPanel pp = new PathPanel(700, 600);
		pp.setBackground(Color.WHITE);

		JFrame frame = new JFrame("rover map");
		frame.setPreferredSize(new Dimension(800, 500));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.add(pp, BorderLayout.CENTER);
		frame.pack();
		frame.setVisible(true);

		pp.createBufferStrategy(2);
		pp.timer.start();

	}

	private ZoomAndPanListener zoomAndPanListener;

	public Dimension getPreferredSize() {
		return new Dimension(600, 500);
	}

	public PathPanel(int width, int height) {

		this.zoomAndPanListener = new ZoomAndPanListener(this);
		this.addMouseListener(zoomAndPanListener);
		this.addMouseMotionListener(zoomAndPanListener);
		this.addMouseWheelListener(zoomAndPanListener);
	}

	@Override
	public void paint(Graphics g0) {
		Graphics2D g = (Graphics2D) g0;

		RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHints(rh);

		if (init) {
			// Initialize the viewport by moving the origin to the center of the window,
			// and inverting the y-axis to point upwards.
			init = false;
			Dimension d = getSize();
			int xc = d.width / 2;
			int yc = d.height / 2;
			g.translate(xc, yc);
			g.scale(1, -1);
			// Save the viewport to be updated by the ZoomAndPanListener
			zoomAndPanListener.setCoordTransform(g.getTransform());
		} else {
			// Restore the viewport after it was updated by the ZoomAndPanListener
			g.setTransform(zoomAndPanListener.getCoordTransform());
		}
		myPaint(g);
	}

	private void myPaint(Graphics2D g) {

		// bounding box in mouse coords
		Rectangle bounds = new Rectangle(0, 0, 0, 0);
		// Stream.concat(points1.stream(), points2.stream()).forEach(p ->
		// bounds.add(p));
		Rover.track0.stream().forEach(p -> bounds.add(p.x, p.y));
		bounds.grow(50, 50);

		// // scale isometric
		// double h = getHeight();
		// double w = getWidth();
		// double s = Math.min(w/bounds.width, h/bounds.height);

		// AffineTransform tx = new AffineTransform();
		// tx.translate(-1 * bounds.x, 1 * bounds.y);
		// tx.scale(s, s);
		// g.transform(tx);
		// bounds.grow(-30, -30);
		// g.setColor(Color.WHITE);
		// g.fill(bounds);
		

		drawGrid(g, bounds);

		g.setColor(Color.BLACK);
		g.drawLine(0, bounds.y, 0, bounds.y + bounds.height);
		g.drawLine(bounds.x, 0, bounds.x + bounds.width, 0);

		g.setColor(Color.LIGHT_GRAY);
		Waypoint p = Rover.track0.get(Rover.track0.size() - 1);
		drawRover(g, p);

		g.setColor(Color.RED);
		drawRoverPath(g);
	}

	private void drawGrid(Graphics2D g, Rectangle r) {
		g.setColor(Color.LIGHT_GRAY);
		for (int x = 20; x < r.x + r.width; x = x + 20) {
			g.drawLine(x, r.y, x, r.y + r.height);
		}
		for (int x = -20; x > r.x; x = x - 20) {
			g.drawLine(x, r.y, x, r.y + r.height);
		}
		for (int y = 20; y < r.y + r.height; y = y + 20) {
			g.drawLine(r.x, y, r.x + r.width, y);
		}
		for (int y = -20; y > r.y; y = y - 20) {
			g.drawLine(r.x, y, r.x + r.width, y);
		}
	}

	private void drawRover(Graphics2D g, Waypoint p) {
		Rectangle r = new Rectangle(0, 0, 0, 0);
		r.grow(11, 15); // rover is 22x30cm

		AffineTransform tx = g.getTransform(); // save current transformation

		AffineTransform tx2 = (AffineTransform) tx.clone();
		tx2.translate(p.x, p.y); // rover center
		tx2.rotate(-p.heading.toRad()); // rover heading
		g.setTransform(tx2);

		g.fill(r);
		g.setTransform(tx); // restore transformation

	}

	private void drawRoverPath(Graphics2D g) {
		// map mouse points to screen
		ArrayList<Waypoint> l = Rover.track0;
		int[] x = l.stream().mapToInt(p -> (int) (p.x)).toArray();
		int[] y = l.stream().mapToInt(p -> (int) (p.y)).toArray();

		g.drawPolyline(x, y, l.size());
	}

}
