/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.trash;

import java.util.LinkedList;

public abstract class RoverComponent {
	
	protected LinkedList<RoverComponent> shutdownComponents = new LinkedList<>();
	
	public void shutdown() {
		shutdownComponents.forEach(c -> {
			System.out.println("shutdown " + c);
			c.shutdown();
			});
		shutdownComponents.clear();
	}

}
