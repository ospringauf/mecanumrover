package de.konteno.ao.mrover.service.turn;

import java.util.logging.Level;
import java.util.logging.Logger;

import de.konteno.ao.mrover.common.Rover;
import de.konteno.ao.mrover.common.RoverDirection;
import de.konteno.ao.mrover.common.RoverUtil;
import de.konteno.ao.mrover.common.cmd.RotateCmd;
import de.konteno.ao.mrover.common.cmd.TurnCmd;
import de.konteno.ao.mrover.common.event.CompassEvt;
import de.konteno.ao.mrover.common.event.StoppedEvt;
import de.konteno.ao.mrover.common.unit.Angle;
import de.konteno.ao.mrover.service.MyVerticleBase;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.MessageConsumer;

/**
 * 1st attempt - turn steps are triggered by the compass messages 
 *
 */
public class TurnService1 extends MyVerticleBase {

	private static final Logger LOGGER = Logger.getLogger( TurnService1.class.getSimpleName() );

	private Message<Object> activeCommand;
	private Angle target;

	private MessageConsumer<Object> compass;
	
	@Override
	public void start() throws Exception {
		super.start();

		// receive turn command and store message (reply later ...)
		busConsume(TurnCmd.class, this::turnCommand);

		// abort turn on STOPPED event
		busConsume(StoppedEvt.class, cmd -> onStop());

		// new compass data -> set turn direction and speed
		compass = busConsume(CompassEvent.class, evt -> compassEvent(evt));
		compass.setMaxBufferedMessages(1); 
		compass.pause();
	}

	private void onStop() {
		if (activeCommand != null)
		{
			activeCommand.fail(0, "TURN stopped before complete");
			activeCommand = null;
			target = null;
			compass.pause();
		}
	}

	/**
	 * got TURN command: calculate target angle
	 * @param cmd
	 * @param msg bus message, will be replied when turn is complete
	 * @return
	 */
	private void turnCommand(TurnCmd cmd) {
		compass.resume();
		target = cmd.getAngle();
		activeCommand = msg;
		
		
		if (cmd.isRelative()) {
			// relative Drehung: bestimme Zielwinkel = heading + turn
			target = RoverUtil.remAngle(Rover.heading.toDegree() + cmd.getAngle().toDegree());
			LOGGER.info("TURN relative (" + cmd.getAngle() + ") to " + target);
		}
		else {
			// absolute Drehung: Zielwinkel gegeben
			target = cmd.getAngle();
			LOGGER.info("TURN absolute to " + target);
		}
	}
	
	private void compassEvent(CompassEvent evt) {
		Rover.heading = evt.getHeading();
		
		if (activeCommand == null) {
			return;
		}

		turn(0.75);
	}

	private void turn(/*MotionMethod move,*/ double tolerance) {
		try {
			// decreasing angle difference
			Angle delta = Angle.delta(target, Rover.heading);
			double diff = delta.toDegree();
			
			int dir = (int) Math.signum(diff); // +1=right/cw, -1=left/ccw
			double absDiff = Math.abs(diff);

			LOGGER.fine(String.format("TURN current=%3.1f, target=%3.1f, diff=%3.1f", Rover.heading.toDegree(), target.toDegree(), diff));

			if ((absDiff > tolerance)) {
				double v = Rover.bestSpeed(absDiff);
//				move.move(v, dir);
				RotateCmd cmd = new RotateCmd(dir == 1 ? RoverDirection.RIGHT : RoverDirection.LEFT, v);
				busSend(cmd);
			} else {
				// done				
				target = null;
				LOGGER.info("target heading reached, error=" + diff);
				
				// Antwort auf das "TurnCmd" senden
				activeCommand.reply("TURN complete");
				activeCommand = null;
				
				compass.pause();

				sendStop("TURN complete");
			}

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "TURN failed", e);
			activeCommand.fail(0, e.getMessage());
			activeCommand = null;
		} 
	}
	

	// TODO mit anderen "MotionMethods" kann auch auf einer Kreisbahn gedreht werden ...
	
//	public void xrotate(double speed) throws Exception {
//		// send move command to motor control
////		publishBusEvent(new RotateCmd(speed));
//	}

//	public void turn(double angle) throws Exception {
//		turn(angle, (double v, int dir) -> xrotate(v * dir), 1);
//	}


	


	@Override
	public void onShutdown() {
		// nothing to do
	}

}
