/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.trash;

import com.pi4j.wiringpi.Gpio;

import de.konteno.ao.mrover.driver.motor.MotorControlException;
import de.konteno.ao.mrover.driver.motor.Pca9685Pwm;

/**
 * SG09 micro servo
 * 
 * see https://github.com/adafruit/Adafruit_Python_PCA9685
 * https://learn.adafruit.com/adafruit-16-channel-servo-driver-with-raspberry-pi/using-the-adafruit-library
 * 
 * @author oliver
 *
 */
public class Servo extends RoverComponent {

	public final static int ABS_MIN = 120;
	public final static int ABS_MAX = 1300;

	public static final int AUTO_OFF_DELAY = 1000;

	// 100ms per 60°
	public static final double POSITIONING_TIME_PER_DEGREE = 100.0 / 60.0; 

	private Pca9685Pwm ctrl;
	private int channel;
	public int min = 330;
	public int max = 1060;
	private int pwm;
	private ReschedulableTimer offTimer;

	public Servo(Pca9685Pwm ctrl, int channel) {
		this.ctrl = ctrl;
		this.channel = channel;
	}

	public Servo(Pca9685Pwm ctrl, int channel, int min, int max) {
		this(ctrl, channel);
		setRange(min, max);
	}

	public void setRange(int min, int max) {
		this.min = min;
		this.max = max;
	}

	public void setPwm(int p) throws MotorControlException {
		if (p < ABS_MIN || p > ABS_MAX) {
			throw new IllegalArgumentException("PWM value " + p + " not allowed");
		}
		pwm = p;
		ctrl.setPWM(channel, 0, pwm);

		if (offTimer != null) {
			offTimer.reschedule(AUTO_OFF_DELAY);
		}
	}

	/**
	 * set position (pwm) and wait for specified millis
	 * 
	 * @param p
	 * @param wait
	 * @throws MotorControlException
	 * @throws InterruptedException
	 */
	public void setPwm(int p, int wait) throws MotorControlException, InterruptedException {
		setPwm(p);
		//Thread.sleep(wait);
		Gpio.delay(wait);
	}

	public int getPwm() {
		return pwm;
	}

	public void center() throws InterruptedException, MotorControlException {
		setPwm(min + (max - min) / 2);
	}

	public void off() throws MotorControlException {
		int offPwm = 0;
		ctrl.setPWM(channel, 0, offPwm);
	}

	public void autoOff() {
		offTimer = new ReschedulableTimer();
		offTimer.schedule(new Runnable() {

			@Override
			public void run() {
				try {
					off();
				} catch (MotorControlException e) {
					e.printStackTrace();
				}
			}
		}, AUTO_OFF_DELAY);
	}

	@Override
	public void shutdown() {
		try {
			if (pwm > 0) {
				off();
			}
			if (offTimer != null) {
				offTimer.cancel();
			}
		} catch (MotorControlException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.shutdown();
	}

	// # Configure min and max servo pulse lengths
	// servo_min = 150 # Min pulse length out of 4096
	// servo_max = 600 # Max pulse length out of 4096
	//
	// # Helper function to make setting a servo pulse width simpler.
	// def set_servo_pulse(channel, pulse):
	// pulse_length = 1000000 # 1,000,000 us per second
	// pulse_length //= 60 # 60 Hz
	// print('{0}us per period'.format(pulse_length))
	// pulse_length //= 4096 # 12 bits of resolution
	// print('{0}us per bit'.format(pulse_length))
	// pulse *= 1000
	// pulse //= pulse_length
	// pwm.set_pwm(channel, 0, pulse)
}
