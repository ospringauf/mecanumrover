package de.konteno.ao.mrover.common.cmd;

import de.konteno.ao.mrover.common.RoverTopic;

@RoverTopic(address = "cmd.rotate_radial_2")
public class RotateRadial2Cmd extends RotateCmd  implements IRoverCommand {

	private double steering;

	public RotateRadial2Cmd(double speed, double steering) {
		super(speed);
		this.steering = steering;
	}
	
	public double getSteering() {
		return steering;
	}

	public void setSteering(double steering) {
		this.steering = steering;
	}
	
	@Override
	public String toString() {
		return String.format("ROTATE_RADIAL_2(%2.3f, %2.3f)!", getSpeed(), steering);
	}

	

}
