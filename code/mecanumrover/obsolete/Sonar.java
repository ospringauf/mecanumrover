/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.extern;

import java.io.IOException;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.wiringpi.Gpio;

import de.konteno.ao.mrover.trash.NativeUtils;

/**
 * Native (C) functions for reading sonar 
 * 
 * generate header file for JNI:
 * cd /home/oliver/projekte/rover/git/mecanumrover/code/mecanumrover/target/classes
 * javah de.konteno.ao.mrover.extern.Sonar
 * mv de_konteno_ao_mrover_extern_Sonar.h ../../src/main/resources/
 * 
 * compile shared library (libmrover.so) on RasPi:
 * gcc -shared -fpic -I/usr/lib/jvm/jdk-8-oracle-arm32-vfp-hflt/include/ -I/usr/lib/jvm/jdk-8-oracle-arm32-vfp-hflt/include/linux -o libmrover.so de_konteno_ao_mrover_extern_Sonar.c
 * 
 * place SO into resources directory so that it can be loaded from JAR (using NativeUtils)
 */
@Deprecated
public class Sonar {
	public native void setup();

	/**
	 * Single measurement
	 * @param triggerPin
	 * @param echoPin
	 * @return echo run time in microseconds
	 */
	public native int triggerEcho(int triggerPin, int echoPin);
	
	/**
	 * Average of n measurements
	 * @param triggerPin
	 * @param echoPin
	 * @param n number of samples
	 * @return average echo run time in microseconds
	 */
	public native int triggerEchoAvg(int triggerPin, int echoPin, int n);

	static {
		try {
			System.loadLibrary("mrover");
		} catch (UnsatisfiedLinkError e) {
			try {
				// during runtime. .SO within .JAR
				NativeUtils.loadLibraryFromJar("/libmrover.so");
				System.out.println("loaded libmrover.so from jar");
			} catch (IOException e1) {
				throw new RuntimeException(e1);
			}
		}
	}

	
	public void test() throws InterruptedException {
		System.out.println("call setup");
		setup();
		
		//Gpio.wiringPiSetup();
		
		GpioController gpio = GpioFactory.getInstance();
		GpioPinDigitalOutput trigPin = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_00);
		trigPin.low();
		GpioPinDigitalInput echoPin = gpio.provisionDigitalInputPin(RaspiPin.GPIO_01);
		Gpio.delay(30);
		
//		trigPin.export(PinMode.DIGITAL_OUTPUT, PinState.LOW);
//		echoPin.export(PinMode.DIGITAL_INPUT, PinState.LOW);

		
		System.out.println("triggerEcho");
		long t = System.currentTimeMillis();
		int d = triggerEcho(trigPin.getPin().getAddress(), echoPin.getPin().getAddress());		
		System.out.println("single read: " + d + " [" + (System.currentTimeMillis() - t) + "ms]");
		
		for (int i=0; i<1000; ++i) {
			t = System.currentTimeMillis();
			d = triggerEchoAvg(0, 1, 3);
			//d = triggerEcho(0, 1);
			System.out.println("d=" + d/58 + " [" + (System.currentTimeMillis() - t) + "ms]");
			
			Thread.sleep(30);
		}
	}
}
