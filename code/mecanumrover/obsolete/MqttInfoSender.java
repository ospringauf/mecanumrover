/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.service.comm;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;

public class MqttInfoSender implements InfoListener {

	private MqttClient client;
	public MqttInfoSender() throws MqttException {
		client = new MqttClient("tcp://localhost:1883", "mrover-info");
		client.connect();

	}
	
	@Override
	public void info(String topic, String message) {
		try {
			client.publish(topic, message.getBytes(), 0, false);
		} catch (MqttPersistenceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void shutdown() throws MqttException {
		System.out.println("MqttInfoSender.shutdown");
		//client.unsubscribe(topic);
		client.disconnect(1000);
	}


}
