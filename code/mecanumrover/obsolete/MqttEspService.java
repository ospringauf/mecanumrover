/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.service.comm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.konteno.ao.mrover.common.event.SonarDistanceEvt;
import de.konteno.ao.mrover.service.MyVerticleBase;
import io.netty.handler.codec.mqtt.MqttQoS;
import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;
import io.vertx.mqtt.MqttClient;
import io.vertx.mqtt.messages.MqttConnAckMessage;
import io.vertx.mqtt.messages.MqttPublishMessage;

/**
 * handle communication with ESP32 module
 *
 */
public class MqttEspService extends MyVerticleBase {

	private static final Logger logger = LoggerFactory.getLogger( MqttEspService.class );
	private static final String TopicEspData = "esp/out/#";

	private MqttClient client;
	private long lastSeen = 0;
	
	public final static int MODE_IDLE = 0;
	public final static int MODE_ODO = 1;
	public final static int MODE_SONAR = 2;
	public final static int MODE_LIDAR = 4;

	@Override
	public void start(Future<Void> startFuture) throws Exception {
		super.start();

		client = MqttClient.create(vertx);
		client.connect(1883, "localhost", s -> {
			clientConnected(s.result());
		});

		startFuture.complete();
	}

	private void clientConnected(MqttConnAckMessage m) {

		client.publishHandler(p -> {
			mqttMessageReceived(p);
		});

		client.subscribe(TopicEspData, MqttQoS.EXACTLY_ONCE.value());
	}

	private void mqttMessageReceived(MqttPublishMessage p) {
		String payload = p.payload().toString();

		logger.trace("received: topic: {}, payload: {}", p.topicName(), payload);
		// System.out.println("QoS: " + p.qosLevel());

		
		// translate MQTT messages into vert.x events
		switch (p.topicName()) {

		case "esp/out/ping":
			if (lastSeen == 0) {
				lastSeen = System.currentTimeMillis();
				logger.info("connected to ESP-32");

				sendToEsp("esp/cmd/sonarmask", Integer.toString(0b00000110)); 
				sendToEsp("esp/cmd/mode", Integer.toString(MODE_SONAR));
			}
			break;
			
		case "esp/out/sonar":
			busPublish(new SonarDistanceEvt(payload));
			break;
			
		
		default:
			break;
		}		
	}
	
	private void sendToEsp(String topic, String message) {
		client.publish(topic, Buffer.buffer(message), MqttQoS.AT_LEAST_ONCE, false, false);
	}

	@Override
	public void onShutdown() {
		sendToEsp("esp/cmd/mode", Integer.toString(MODE_IDLE));

		client.disconnect();
	}

}
