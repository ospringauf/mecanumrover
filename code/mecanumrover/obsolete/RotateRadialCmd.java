package de.konteno.ao.mrover.common.cmd;

import de.konteno.ao.mrover.common.RoverTopic;

@RoverTopic(address = "cmd.rotate_radial")
public class RotateRadialCmd extends RotateCmd implements IRoverCommand {
	private double front;
	private double rear;

	public RotateRadialCmd(double v, double front, double rear) {
		super(v);
		this.front = front;
		this.rear = rear;
	}
	
	public double getFront() {
		return front;
	}

	public void setFront(double front) {
		this.front = front;
	}

	public double getRear() {
		return rear;
	}

	public void setRear(double read) {
		this.rear = read;
	}

	@Override
	public String toString() {
		return String.format("ROTATE_RADIAL(%2.3f, %2.3f, %2.3f)!", getSpeed(), front, rear);
	}

}
