/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.service.comm;

import java.util.concurrent.TimeUnit;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import de.konteno.ao.mrover.common.Rover;
import de.konteno.ao.mrover.common.cmd.CalibrateCompassCmd;
import de.konteno.ao.mrover.common.cmd.CalibrateOdometerCmd;
import de.konteno.ao.mrover.common.cmd.DemoCmd;
import de.konteno.ao.mrover.common.cmd.DriveCmd;
import de.konteno.ao.mrover.common.cmd.RotateCmd;
import de.konteno.ao.mrover.common.cmd.SendSerialMessageCmd;
import de.konteno.ao.mrover.common.cmd.TurnCmd;
import de.konteno.ao.mrover.common.event.ShutdownEvt;
import de.konteno.ao.mrover.common.event.SonarDistanceEvt;
import de.konteno.ao.mrover.common.track.TrackType;
import de.konteno.ao.mrover.common.track.Waypoint;
import de.konteno.ao.mrover.common.unit.Angle;
import de.konteno.ao.mrover.service.MyVerticleBase;
import io.vertx.core.Future;

/**
 * subscribe on MQTT and Vert.x eventbus and pass messages between the two
 *  
 * TODO periodically send speed/heading/track telemetry over MQTT
 */
public class PahoMqttBridgeService extends MyVerticleBase implements MqttCallback {

	private static final Logger logger = LoggerFactory.getLogger( PahoMqttBridgeService.class );
	
	// commands/configuration sent from the "MQTT Dash" app
	private static final String TopicRemoteCmd = "rover/cmd"; // +"/#";
	private static final String TopicRemoteSetting = "rover/set"; // + "/#";
	
	// commands to be relayed from MQTT to ESP32 (via serial), for test only
	private static final String TopicEspCmd = "esp/cmd"; // + "/#";

	public static final String TopicTelemetryOdo = "rover/tm/odo";
	public static final String TopicTelemetryDr = "rover/tm/dr";
	public static final String TopicTelemetrySonar = "rover/tm/sonar";
	
	private MqttClient client;
	
	private String lastMaxSpeed = null;

	private long telemetryTimer; 

	@Override
	public void start(Future<Void> startFuture) throws Exception {
		super.start();

		client = new MqttClient("tcp://localhost:1883", "vertx");
		client.connect();
		logger.info("MQTT client connected");
		client.setCallback(this);

		publishMqtt("rover-info", "hello from vert.x");

		client.subscribe(TopicRemoteCmd + "/#", 0);
		client.subscribe(TopicRemoteSetting + "/#", 0);
		client.subscribe(TopicEspCmd + "/#", 0);
		
		if (Rover.telemetry) {
			// listen to waypoints and publish max 3 per second and type to MQTT
			/*Observable<Waypoint> s = */ 
			
			busListenStream(Waypoint.class)
			.filter(x -> x.type == TrackType.ODO)
			.sample(333, TimeUnit.MILLISECONDS)
			.subscribe(x -> publishMqtt(TopicTelemetryOdo, x.mqttString()));
			
			busListenStream(Waypoint.class)
			.filter(x -> x.type == TrackType.DEAD_RECK)
			.sample(200, TimeUnit.MILLISECONDS)
			.subscribe(x -> publishMqtt(TopicTelemetryDr, x.mqttString()));
			
			busListenStream(SonarDistanceEvt.class)
			.subscribe(x -> publishMqtt(TopicTelemetrySonar, x.mqttString()));
		}
		
		startFuture.complete();
	}

	
	private synchronized void publishMqtt(String topic, String payload ) {
		try {
			//client.publish(topic, new MqttMessage(payload.getBytes()));
			client.publish(topic, payload.getBytes(), 0, false);
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//	private void clientConnected(MqttConnAckMessage m) {
//
//		client.publishHandler(p -> {
//			mqttMessageReceived(p);
//			logger.debug("message handled");
//		});
//
//		publishMqtt("rover-info", "hello from vert.x");
//
//		client.subscribe(TopicRemoteCmd + "/#", MqttQoS.EXACTLY_ONCE.value());
//		client.subscribe(TopicRemoteSetting + "/#", MqttQoS.EXACTLY_ONCE.value());
//		client.subscribe(TopicEspCmd + "/#", MqttQoS.EXACTLY_ONCE.value());
//	}

	@Override
	public void messageArrived(String topic, MqttMessage m) throws Exception {
		String payload = m.toString();

		logger.debug("received: topic: " + topic + ", payload: "+ payload);
		// System.out.println("QoS: " + p.qosLevel());

		if (topic.startsWith(TopicEspCmd)) {
			logger.debug("passing message to ESP32");
			busSend(new SendSerialMessageCmd(topic, payload));
			return;
		}
		
		// translate MQTT messages into vert.x events
		switch (topic) {

		// ---------------------------------------------------------------
		case TopicRemoteCmd + "/stop": {
			sendStop("stopped by user");
			break;
		}

		// ---------------------------------------------------------------
		case "rover/cmd/drive": {
			DriveCmd cmd = new DriveCmd(Angle.fromDegree(Integer.parseInt(payload)));
			busSend(cmd);
			break;
		}

		// ---------------------------------------------------------------
		case "rover/cmd/info": {
			sendInfo();
			if (!Strings.isNullOrEmpty(payload)) {
				int interval = Integer.parseInt(payload);
				startTelemetry(interval);
				
			}
			break;
		}

		// ---------------------------------------------------------------
		case "rover/set/maxspeed": {			
			if (payload.equals(lastMaxSpeed)) {
				// this is the output of "sendInfo", ignore
			} else {
				Rover.speedLimit = Double.parseDouble(payload) / 100.0;
				logger.debug("set max speed to {}", Rover.speedLimit);
				sendInfo();
			}
			break;
		}

		// ---------------------------------------------------------------
		case "rover/cmd/rotate": {
			// payload is "left" or "right"
			boolean clockwise = "RIGHT".equalsIgnoreCase(payload);
			busSend(new RotateCmd(Rover.speedLimit).steer(clockwise ? 100 : -100));
			break;
		}

		// ---------------------------------------------------------------
		case "rover/cmd/turn": {
			// payload is "+60", "-90" (for relative turn) or "60", "270" (for absolute turn)
			Angle angle = Angle.fromDegree(Integer.parseInt(payload));
			boolean relative = payload.startsWith("+") || payload.startsWith("-");
			TurnCmd cmd = new TurnCmd(angle, relative);
			busSend(cmd);
			break;
		}

		// ---------------------------------------------------------------
		case "rover/cmd/*": {
			switch (payload) {
			case "calibratecompass":
				busSend(new CalibrateCompassCmd());				
				break;

			case "calibrateodo":
				busSend(new CalibrateOdometerCmd());				
				break;

			default:
				break;
			}
			break;
		}

		// ---------------------------------------------------------------
		// calibration
		// TODO remove when no longer needed
		
		case "rover/cmd/d": {
			String[] args = payload.split(" ");
			double v = Double.parseDouble(args[0]);
			double angle = Double.parseDouble(args[1]);
			long time = Long.parseLong(args[2]);
			busSend(new DriveCmd(Angle.fromDegree(angle), v).time(time)).compose(x -> sendStop("done"));
			break;
		}
		case "rover/cmd/rr": {
			String[] args = payload.split(" ");
			double v = Double.parseDouble(args[0]);
			double steer = Double.parseDouble(args[1]);
			long time = Long.parseLong(args[2]);
			busSend(new RotateCmd(v).radial().steer(steer).time(time)).compose(x -> sendStop("done"));
			break;
		}
		case "rover/cmd/rt": {
			String[] args = payload.split(" ");
			double v = Double.parseDouble(args[0]);
			double steer = Double.parseDouble(args[1]);
			long time = Long.parseLong(args[2]);
			busSend(new RotateCmd(v).tangential().steer(steer).time(time)).compose(x -> sendStop("done"));
			break;
		}

		// ---------------------------------------------------------------
		case "rover/cmd/demo": {
			busSend(new DemoCmd(payload));
			break;
		}
		
		// ---------------------------------------------------------------
		case "rover/cmd/shutdown": {
			// stopped = true;
			busPublish(new ShutdownEvt());
			break;
		}
		
		default:
			break;
		}		
	}

	private void startTelemetry(int interval) {
		if (telemetryTimer != 0) {
			vertx.cancelTimer(telemetryTimer);
		}
		if (interval != 0)
		{
			telemetryTimer = vertx.setTimer(interval, h -> {});
		}
	}

	private void sendInfo() {
		lastMaxSpeed = String.format("%3.0f", 100*Rover.speedLimit);
		publishMqtt("rover/info/heading", String.format("%3.0f", Rover.getHeading().toDegree()));
		publishMqtt("rover/set/maxspeed", lastMaxSpeed);
	}

	@Override
	public void onShutdown() {
		logger.info("SHUTDOWN");
		publishMqtt("rover-info", "shutting down");

		try {
			if (client != null) {
				System.out.println("MQTT disconnect");
				if (client.isConnected()) {
					client.unsubscribe(TopicRemoteCmd + "/#");
					client.unsubscribe(TopicRemoteSetting + "/#");
					client.unsubscribe(TopicEspCmd + "/#");
					client.disconnect();
				}
				client.close();
			}
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void connectionLost(Throwable t) {
		logger.error("MQTT connection lost", t);
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken arg0) {
		// TODO Auto-generated method stub
		
	}

}
