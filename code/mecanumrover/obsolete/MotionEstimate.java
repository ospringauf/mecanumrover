package de.konteno.ao.mrover.service.dr;

import de.konteno.ao.mrover.common.cmd.DriveCmd;
import de.konteno.ao.mrover.common.cmd.IRoverCommand;
import de.konteno.ao.mrover.common.cmd.RotateCmd;
import de.konteno.ao.mrover.common.unit.Angle;

public class MotionEstimate {

	public static double dx(IRoverCommand cmd, long t) {
		switch (cmd.getClass().getSimpleName()) {

		case "DriveCmd": {
			DriveCmd drive = (DriveCmd) cmd;
			double v0 = drive.getSpeed();
			Angle a = drive.getCourse();

			return (((39 * v0) - 8) * Math.sin(a.toRad()) * t) / 1000;
		}

		case "RotateCmd": {
			RotateCmd rot = (RotateCmd) cmd;
			double v0 = rot.getSpeed();
			double s = rot.getSteering();

			return rot.isRadial() ? (v0 * (70 - (0.7 * s)) * t) / 1000 : 0;
		}

		case "StopCmd":
		default:
			return 0;

		}
	}
	
	public static double dy(IRoverCommand cmd, long t) {
		switch (cmd.getClass().getSimpleName()) {

		case "DriveCmd": {
			DriveCmd drive = (DriveCmd) cmd;
			double v0 = drive.getSpeed();
			Angle a = drive.getCourse();

			return (((39 * v0) - 8) * Math.cos(a.toRad()) * t) / 1000;
		}

		case "RotateCmd": {
			RotateCmd rot = (RotateCmd) cmd;
			double v0 = rot.getSpeed();
			double s = rot.getSteering();
			return rot.isRadial() ? 0 : (v0 * (80 - (0.75 * s)) * t) / 1000;
		}

		case "StopCmd":
		default:
			return 0;

		}
	}

	public static double ddegree(IRoverCommand cmd, long t) {
		switch (cmd.getClass().getSimpleName()) {

		case "DriveCmd": {
			return 0;
		}

		case "RotateCmd": {
			RotateCmd rot = (RotateCmd) cmd;
			double v0 = rot.getSpeed();
			double s = rot.getSteering();

			double da = rot.isRadial() ? 
					(v0 * ((1.2 * s) /*+ 8.2*/) * t) / 1000 : 
					(v0 * ((1.1 * s) /*+ 9.4*/) * t) / 1000;
			return da;
		}

		case "StopCmd":
		default:
			return 0;

		}
	}

}
