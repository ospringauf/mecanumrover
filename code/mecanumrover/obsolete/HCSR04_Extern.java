/*********************************************************************
* Copyright (c) 2018 Angelika Wittek, Oliver Springauf.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

package de.konteno.ao.mrover.driver.sonar;

import com.pi4j.io.gpio.Pin;

import de.konteno.ao.mrover.extern.Sonar;
import de.konteno.ao.mrover.trash.SensorException;

/**
 * Uses native calls (JNI to libmrover.so) to measure echo time.
 * 
 * @author oliver
 *
 */
public class HCSR04_Extern extends HCSR04 {

	private Sonar externSonar;
	private int trigAdr;
	private int echoAdr;

	public HCSR04_Extern(Pin trig, Pin echo) {
		super(trig, echo);
		
		externSonar = new Sonar();
		externSonar.setup();
		
		trigAdr = trig.getAddress();
		echoAdr = echo.getAddress();
	}

	
	/**
	 * This method returns the distance measured by the sensor in cm
	 */
    public float measureDistance() {
        long duration = externSonar.triggerEcho(trigAdr, echoAdr);
        return micros2cm(duration);
    }

	@Override
	public float measureDistance(int samples) throws SensorException {
        long duration = externSonar.triggerEchoAvg(trigAdr, echoAdr, samples);
        return micros2cm(duration);
	}
	
}
