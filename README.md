# A&O Mecanum Rover #

![Screenshot from 2017-03-03 07-20-32.png](https://bitbucket.org/repo/Ex5r5j/images/2632804651-Screenshot%20from%202017-03-03%2007-20-32.png)

This repository contains construction data and software for our "homebrew" robot vehicle. It features mecanum wheels, 4 independent motors, a Raspberry Pi, multiple sensors,
and Arduino/ESP32 microcontroller(s). We use [Eclipse vert.x](https://vertx.io/) as the platform for reactive microservices to control the rover.

You can see the robot in action on [our YouTube channel](https://www.youtube.com/channel/UCZ8ogjJI2dZhg4SzE9XRPkA) or have a look at the 
presentations from 
[Eclipse DemoCamp 2017](https://bitbucket.org/ospringauf/mecanumrover/downloads/Building%20a%20Cool%20Rover%20from%20Scratch.pdf)
and [Eclipse Insight 2019](https://bitbucket.org/ospringauf/mecanumrover/downloads/MECANUM%20ROVER%20-%20Homebrew%20Robotics%20and%20Reactive%20Microservices.pdf).

There's some more technical information in the [Wiki](https://bitbucket.org/ospringauf/mecanumrover/wiki/Home).

If you'd like to know more, contact us at *oliver dot springauf at gmx dot de*


### Development tools ###

* Eclipse
* Arduino IDE
* Visual Studio Code with Platformio plugin
* KiCad schematic editor
* Tinkercad


